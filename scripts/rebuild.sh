#!/usr/bin/bash

if [[ "$1" == "" ]]; then
    echo "Enter JAR file in the argument."
    exit 1
fi

if [[ ! -f "scripts/rebuild.sh" ]]; then
    echo "Run this script from the project root."
    exit 1
fi

jar="$1"
run="java -jar $jar"

rm *.sigarray >/dev/null 2>/dev/null

#$run train -n ascii corpus/ascii/*.txt || exit 1
$run train -n l2 corpus/l2/*.txt || exit 1
$run train -n utf8 corpus/utf8/*txt || exit 1
$run train -n cp1250 corpus/cp1250/*.txt || exit 1
$run train -n invalid_utf8  corpus/invalid_utf8/*.txt || exit 1

mv -v *.sigarray src/main/resources/sigarrays
