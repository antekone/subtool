if(ARGV.size < 1)
  puts "Enter corpus subdirectory as an argument."
  exit 1
end

seq = 1
Dir["#{ARGV[0]}/*"].each() do |f|
  path_in = f
  path_out = "#{ARGV[0]}/#{seq}.txt"

  puts(`mv -v "#{path_in}" "#{path_out}"`)

  seq += 1
end
