name := "subtool"

version := "0.1"

scalaVersion := "2.13.3"

libraryDependencies += "info.picocli" % "picocli" % "4.5.1"
libraryDependencies += "org.fusesource.jansi" % "jansi" % "1.18"
libraryDependencies += "org.scala-lang.modules" %% "scala-parallel-collections" % "0.2.0"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.0" % "test"