import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

import scala.collection.mutable

class TrigraphSpec extends AnyFlatSpec with should.Matchers {
  "Trigraph" should "compare == true with identical other object" in {
    val t1: Trigraph = (1.toByte, 2.toByte, 3.toByte)
    val t2: Trigraph = (1.toByte, 2.toByte, 3.toByte)
    t1 should be (t2)
  }

  it should "compare == false with a different other object" in {
    val t1: Trigraph = (1.toByte, 2.toByte, 3.toByte)
    val t2: Trigraph = (1.toByte, 5.toByte, 3.toByte)
    t1 should not be (t2)
  }

  it should "work properly with a Set container: retain different object" in {
    val set: mutable.Set[Trigraph] = mutable.Set()
    set += Trigraph(Array(1, 2, 3))
    set += Trigraph(Array(2, 3, 4))
    set.size should be (2)

    val lst = set.toList
    lst.head should be (Trigraph(Array(1, 2, 3)))
    lst(1) should be (Trigraph(Array(2, 3, 4)))
  }

  it should "work properly with a Set container: remove same object" in {
    val set: mutable.Set[Trigraph] = mutable.Set()
    set += Trigraph(Array(2, 3, 4))
    set += Trigraph(Array(2, 3, 4))
    set.size should be (1)

    val lst = set.toList
    lst.head should be (Trigraph(Array(2, 3, 4)))
  }
}
