import java.io.{ByteArrayInputStream, InputStream}

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

class FilteringStream(is: InputStream) extends InputStream {
  override def read(): Int = {
    val r = is.read()
    if(r == -1)
      return -1

    if(r == 2 || r == 6 || r == 10) {
      return is.read()
    }

    r
  }
}

class BufferSpec extends AnyFlatSpec with should.Matchers {
  "Buffer" should "iterate on even buffers" in {
    val is = new ByteArrayInputStream(Array(1, 2, 3,  4, 5, 6,  7, 8, 9))

    val detected = ArrayBuffer[Array[Byte]]()
    Buffer.eachFrame(is, 3) { b =>
      detected += b.clone()
    }

    detected.size should be (3)
    detected(0) should be (Array(1, 2, 3))
    detected(1) should be (Array(4, 5, 6))
    detected(2) should be (Array(7, 8, 9))
  }

  it should "iterate on odd buffers" in {
    val is = new ByteArrayInputStream(Array(1, 2, 3,  4, 5, 6,  7, 8, 9, 10))

    val detected = ArrayBuffer[Array[Byte]]()
    Buffer.eachFrame(is, 3) { b =>
      detected += b.clone()
    }

    detected.size should be (4)
    detected(0) should be (Array(1, 2, 3))
    detected(1) should be (Array(4, 5, 6))
    detected(2) should be (Array(7, 8, 9))
    detected(3) should be (Array(10))
  }

  it should "iterate on stream that doesn't return constant byte read count (even)" in {
    val is = new FilteringStream(new ByteArrayInputStream(Array(1, 2, 3,  4, 5, 6,  7, 8, 9, 10, 11, 12)))

    val detected = ArrayBuffer[Array[Byte]]()
    Buffer.eachFrame(is, 3) { b =>
      detected += b.clone()
    }

    detected.size should be (3)
    detected(0) should be (Array(1, 3, 4))
    detected(1) should be (Array(5, 7, 8))
    detected(2) should be (Array(9, 11, 12))
  }

  it should "iterate on stream that doesn't return constant byte read count (odd)" in {
    val is = new FilteringStream(new ByteArrayInputStream(Array(1, 2, 3,  4, 5, 6,  7, 8, 9, 10, 11, 12, 15)))

    val detected = ArrayBuffer[Array[Byte]]()
    Buffer.eachFrame(is, 3) { b =>
      detected += b.clone()
    }

    detected.size should be (4)
    detected(0) should be (Array(1, 3, 4))
    detected(1) should be (Array(5, 7, 8))
    detected(2) should be (Array(9, 11, 12))
    detected(3) should be (Array(15))
  }
}
