import java.io.{BufferedInputStream, DataInputStream, DataOutputStream, FilterInputStream, InputStream, OutputStream}
import java.nio.charset.Charset

import log.Log

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

class Trigraph(
  var a: Byte,
  var b: Byte,
  var c: Byte,
) {
  def set(in: Array[Byte]): Unit = {
    if(in.length != 3)
      throw Errors.TrigraphNot3Error(s"Expected trigraph of size 3, got ${in.length}, can't continue.")

    this.a = in(0)
    this.b = in(1)
    this.c = in(2)
  }

  def lower(a: Byte): Byte = {
    if(a >= 'A' && a <= 'Z') (a - 'A' + 'a').toByte
    else a
  }

  def setTriple(in: (Byte, Byte, Byte)): Unit = {
    val (a, b, c) = in

    this.a = this.lower(a)
    this.b = this.lower(b)
    this.c = this.lower(c)
  }

  override def toString: String = f"<$a%02X,$b%02X,$c%02X>"

  override def equals(other: Any): Boolean = other match {
    case t: Trigraph => this.a == t.a && this.b == t.b && this.c == t.c
    case _ => false
  }

  override def hashCode(): Int = {
    a << 16 | b << 8 | c
  }

  def isSpecial(): Boolean = {
    this.isSpecial(a) || this.isSpecial(b) || this.isSpecial(c)
  }

  def isSpecial(b: Byte): Boolean = b < '0' || b > 'z'
}

object Trigraph {
  def apply(a: Byte, b: Byte, c: Byte): Trigraph = new Trigraph(a, b, c)
  def apply(a: Int, b: Int, c: Int): Trigraph = new Trigraph(a.toByte, b.toByte, c.toByte)
  def apply(in: Array[Byte]): Trigraph = new Trigraph(in(0), in(1), in(2))

  implicit def fromTriple(in: (Byte, Byte, Byte)): Trigraph = new Trigraph(in._1, in._2, in._3)
}

object TrigraphCollection {
  final val BINARY_MAGIC: Array[Int] = Array(0x09, 0xFC, 0x13, 0xCC)

  def deserialize(in: InputStream): TrigraphCollection = {
    val is = new DataInputStream(new BufferedInputStream(in, 16 * 1024))
    val magic = is.readNBytes(4).map(_.toInt & 0xff)
    if(!(magic sameElements TrigraphCollection.BINARY_MAGIC))
      throw Errors.MagicNotFoundError("Wrong magic bytes when trying to deserialize TrigraphCollection")

    val nameCount = is.readInt()
    if(nameCount == 0 || nameCount > 1024)
      throw Errors.NameTooBigError(s"Name size is invalid: stream declares $nameCount, which is invalid")

    val name = new String(is.readNBytes(nameCount), Charset.forName("UTF-8"))
    val itemCount = is.readInt()
    if(itemCount == 0 || itemCount > 1_000_000)
      throw Errors.ItemCountInvalid(s"Item count is invalid: stream declares $itemCount, which is invalid")

    val tc = TrigraphCollection(name)

    for(idx <- 0 until itemCount) {
      val a = is.readByte()
      val b = is.readByte()
      val c = is.readByte()
      tc += (a, b, c)
    }

    tc
  }
}

case class TrigraphCollection(name: String = "unnamed") {
  val set: mutable.Set[Trigraph] = mutable.HashSet()

  def serialize(out: OutputStream): Unit = {
    val os = new DataOutputStream(out)
    os.write(TrigraphCollection.BINARY_MAGIC.map(_.toByte))
    os.writeInt(this.name.length)
    os.writeBytes(this.name)
    os.writeInt(this.set.size)
    this.set.foreach { e =>
      os.writeByte(e.a)
      os.writeByte(e.b)
      os.writeByte(e.c)
    }
  }

  def add(t: Trigraph): Unit = this.set += t
  def add(tc: TrigraphCollection): Unit = tc.set.foreach(this.add)

  def +=(t: Trigraph): this.type = {
    this.add(t)
    this
  }

  def ++=(tc: TrigraphCollection): this.type = {
    this.add(tc)
    this
  }

  def contains(t: Trigraph): Boolean = this.set.contains(t)

  def dump(): Unit = {
    Log.trace(s"--- TrigraphCollection size: ${this.set.size} items ---")
//    this.set.foreach { t =>
//      Log.trace(s"$t")
//    }
  }
}

object Buffer {
  def eachFrame(is: InputStream, size: Int)(f: Array[Byte] => Unit): Unit = {
    val buf: Array[Byte] = Array.ofDim(size)
    var cur: Int = 0
    var finish = false
    while(!finish) {
      val n = is.read(buf, cur, size - cur)
      if(n <= 0) {
        finish = true
        if(cur > 0) {
          f(buf.take(cur))
        }
      } else {
        cur += n
        if(cur >= size) {
          f(buf)
          cur = 0
        }
      }
    }
  }
}

class FilterMap {
  val map: Array[Boolean] = Array.ofDim(256)

  for(idx <- (0 until 256)) {
    map(idx) = false
  }

  Array(
    0, 1, 2, 3, 4, 5, 6, 7, 8,
    9, 10, 11, 12, 13,

    '/', '<', '-', '\'', '>', ':', '=', '@', '^', '$', '\t'
  ).foreach { c => this.setFiltered(c, filtered = true) }

  def isFiltered(b: Int): Boolean = this.map(b)
  def setFiltered(b: Int, filtered: Boolean): Unit = this.map(b) = filtered
}

class NormalizationStream(stream: InputStream) extends InputStream {
  val fmap = new FilterMap()
  var prevChar: Int = -1

  override def read(): Int = {
    while(true) {
      val b = stream.read()
      if(b == -1)
        return b

      if(b == ' ' && prevChar == ' ') {

      } else {
        prevChar = b
        if(!this.fmap.isFiltered(b))
          return b
      }
    }

    -1
  }
}
class TrigraphGenerator(val stream: InputStream) {
  def invoke(): TrigraphCollection = {
    val trigraphs = TrigraphCollection()

    val buf: Array[Byte] = Array.ofDim(3 * 4096)
    Buffer.eachFrame(new NormalizationStream(stream), 3 * 4096) { buf =>
      val items = buf.length / 3
      for(i <- 0 until items) {
        val a = buf(i * 3)
        val b = buf(i * 3 + 1)
        val c = buf(i * 3 + 2)

        val t: Trigraph = (a, b, c)
        if(t.isSpecial) {
          trigraphs += t
        } else {
          // println(s"$t is not special")
        }
      }
    }

    trigraphs
  }
}
