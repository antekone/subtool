import java.io.{File, FileInputStream, FileOutputStream}
import java.util.concurrent.Callable

import log.Log
import picocli.CommandLine.{Command, Parameters}
import timer.Timed

import scala.util.{Failure, Success, Try}
import scala.collection.parallel.CollectionConverters._

@Command(name = "train")
class Train extends Callable[Int] {
  type POption = picocli.CommandLine.Option

  @Parameters
  var fsItems: Array[File] = _

  @POption(names = Array("-n", "--name"))
  var encodingName: String = _

  override def call(): Int = {
    Log.trace("Train")

    if(this.fsItems == null) {
      Log.error("Please specify one or more files in the command line.")
      return 1
    }

    if(this.encodingName == null) {
      Log.error("Please specify the name of this encoding by using the -n option.")
      return 1
    }

    this.fsItems.find { f => !f.exists || !f.canRead } match {
      case Some(f) =>
        Log.error(s"File '${f}' doesn't exist or is not accessible.")
        return 1
      case None => ()
    }

    this.fsItems.find { f => f.isDirectory } match {
      case Some(f) =>
        Log.error(s"Path '${f}' is a directory. Provide list of files instead (and please fix it ;p)")
        return 1
      case None => ()
    }

    Try {
      import Timed.timed

      val wholeTime = timed {
        val trigraphCollections = this.fsItems.par
          .map { f => new FileInputStream(f) }
          .map { s => new TrigraphGenerator(s) }
          .map { g => timed { g.invoke() } }
          .toList

        trigraphCollections.foreach { col =>
          val (time, tc) = col
          Log.trace(s"Job time: $time ms")
          tc.dump()
        }

        val collection = trigraphCollections.map(_._2).fold(TrigraphCollection(this.encodingName))(_ ++= _)
        collection.serialize(new FileOutputStream(s"${this.encodingName}.sigarray"))
      }

      Log.trace(s"Time: $wholeTime ms")
    } match {
      case Success(_) => ()
      case Failure(err) =>
        Log.error(s"Error while generating trigraphs: $err")
        err.printStackTrace()
        return 1
    }

    0
  }
}
