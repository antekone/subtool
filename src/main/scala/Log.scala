package log

import java.text.SimpleDateFormat
import java.util.Calendar

import org.fusesource.jansi.Ansi
import org.fusesource.jansi.Ansi.Color._
import org.fusesource.jansi.Ansi._

object LogLevel extends Enumeration {
  type Level = Value
  val INFO, DEBUG, TRACE, ERROR, WARNING = Value
}

object Log {
  val df = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss")

  def rawPut(level: LogLevel.Level, buf: String): Unit = {
    var col: Ansi.Color = DEFAULT

    level match {
      case LogLevel.INFO => col = GREEN
      case LogLevel.ERROR => col = RED
      case LogLevel.TRACE => col = CYAN
      case LogLevel.DEBUG => col = WHITE
      case LogLevel.WARNING => col = YELLOW
    }

    val time = Calendar.getInstance().getTime
    val timeStr = df.format(time)
    System.out.println(ansi().a(s"[$timeStr ").fg(col).a(level).reset().a(s"] $buf"))
  }

  def put(level: LogLevel.Level, s: String, args: Object*) {
    val buf = String.format(s, args)
    this.rawPut(level, buf)
  }

  def info(s: String): Unit = this.rawPut(LogLevel.INFO, s)
  def ok(s: String): Unit = this.rawPut(LogLevel.INFO, s)
  def warn(s: String): Unit = this.rawPut(LogLevel.WARNING, s)
  def error(s: String): Unit = this.rawPut(LogLevel.ERROR, s)
  def trace(s: String): Unit = this.rawPut(LogLevel.TRACE, s)
}