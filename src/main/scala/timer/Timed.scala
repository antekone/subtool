package timer

object Timed {
  def timed[R](f: => R): (Long, R) = {
    val start = System.currentTimeMillis()
    val ret = f
    val stop = System.currentTimeMillis()
    (stop - start, ret)
  }

  def timed(f: => Unit): Long = {
    val start = System.currentTimeMillis()
    f
    val stop = System.currentTimeMillis()
    stop - start
  }
}
