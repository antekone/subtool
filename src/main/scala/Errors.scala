object Errors {
  class SubtoolException(str: String) extends RuntimeException(str)
  case class TrigraphNot3Error(m: String) extends SubtoolException(m)
  case class MagicNotFoundError(m: String) extends SubtoolException(m)
  case class NameTooBigError(m: String) extends SubtoolException(m)
  case class ItemCountInvalid(m: String) extends SubtoolException(m)
  case class SigarrayFileNotFound(m: String) extends SubtoolException(m)
}
