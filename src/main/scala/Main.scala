import java.util.concurrent.Callable

import log.Log
import org.fusesource.jansi.AnsiConsole
import picocli.CommandLine
import picocli.CommandLine.Command

@Command(name = "subtool",
  subcommands = Array(
    classOf[Train],
    classOf[Match],
))
class Subtool extends Callable[Int] {
  override def call(): Int = {
    Log.error("Nothing to do. Use `-h` to get some help.")
    1
  }
}

class Main {
  def main2(args: Array[String]): Unit = {
    val cli = new CommandLine(new Subtool())

    try {
      val exitCode = cli.execute(args:_*)
      System.exit(exitCode)
    } catch {
      case e: RuntimeException =>
        Log.error(s"CLI error: $e")
        System.exit(1)
    }
  }
}

object Main extends App {
  AnsiConsole.systemInstall()
  new Main().main2(this.args)
}
