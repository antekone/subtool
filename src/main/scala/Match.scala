import java.io.{BufferedInputStream, File, FileInputStream}
import java.util.concurrent.Callable

import log.Log
import picocli.CommandLine.{Command, Parameters}
import timer.Timed.timed

import scala.collection.mutable
import scala.util.{Failure, Success, Try}
import scala.collection.parallel.CollectionConverters._

@Command(name = "match")
class Match extends Callable[Int] {
  type POption = picocli.CommandLine.Option

  @Parameters
  var filenames: Array[File] = _

  @POption(names = Array("-v", "--verbose"))
  var verbose: Boolean = false

  override def call(): Int = {
    if(this.filenames == null || this.filenames.isEmpty) {
      Log.error("Missing filenames, please provide some!")
      return 1
    }

    this.filenames.filter { f => !f.canRead }.toList match {
      case x: List[File] if x.nonEmpty =>
        Log.error("Some of the input files couldn't be accessed (not found or no permissions):")
        x.foreach { f => Log.error(s"- ${f.getAbsolutePath}") }
        return 1
      case _ => ()
    }

    val (loadingTime, data) = timed {
      Try {
        Array("cp1250", "l2", "utf8", "invalid_utf8").map { name =>
          val fullPath = s"sigarrays/$name.sigarray"
          val stream = this.getClass.getClassLoader.getResourceAsStream(fullPath);
          if(stream == null) {
            throw Errors.SigarrayFileNotFound(fullPath)
          }
          TrigraphCollection.deserialize(stream)
        }
      } match {
        case Success(data) => data
        case Failure(msg: Errors.SigarrayFileNotFound) =>
          Log.error(s"Couldn't open sigarray file: ${msg.m}")
          return 1
        case Failure(msg) =>
          Log.error(s"Internal error while loading sigarray resources. Error: $msg")
          return 1
      }
    }

    def processOne(data: Array[TrigraphCollection], f: File): (File, String) = {
      this.process(data, f) match {
        case Some(result) =>
          (f, result)
        case None =>
          (f, "?")
      }
    }

    val (matchTime, results) = timed {
      val results = this.filenames match {
        case filenames if this.filenames.length == 1 =>
          List(processOne(data, filenames.head))
        case filenames =>
          this.filenames.par.map { f =>
            processOne(data, f)
          }.toList
      }

      results.sortBy { case (f, result) => f }.foreach { case (f, result) =>
        println(s"$result $f")
      }

      results
    }

    if(this.verbose) {
      Log.trace(s"loading time:  ${loadingTime} ms")
      Log.trace(s"matching time: ${matchTime} ms")
    }

    0
  }

  def process(data: Array[TrigraphCollection], f: File): Option[String] = Try {
    val hitMap: mutable.Map[String, Int] = mutable.Map()

    data.foreach { tc =>
      hitMap += (tc.name -> 0)
    }

    val gen = new TrigraphGenerator(new BufferedInputStream(new FileInputStream(f)))

    gen.invoke().set.foreach { t =>
      data.foreach { tc =>
        if(tc.contains(t)) {
          hitMap(tc.name) += 1
        }
      }
    }

    if(this.verbose)
      println(hitMap)

    hitMap.toList.sortBy {
      case (a, b) => b
    }.map(_._1).reverse.head
  } match {
    case Success(r) => Some(r)
    case Failure(msg) =>
      Log.error(s"Error while processing file: ${f.getAbsolutePath}")
      Log.error(s"Error details: $msg")
      None
  }
}
