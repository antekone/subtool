1
00:00:00,000 --> 00:00:00,040
"Miasteczko Twin Peaks" odc. 10 (2x03)
kraps v4.02 30.03.08

2
00:01:39,930 --> 00:01:42,850
Panno Pulaski...

3
00:01:43,060 --> 00:01:47,060
Panno Pulaski, proszę się uspokoić...

4
00:02:16,420 --> 00:02:22,180
Ronette odłączyła się od kroplówki.
Już dostała środek uspokajający.

5
00:02:23,720 --> 00:02:27,680
- Wygląda jak farba.
- Harry, pomóż.

6
00:02:27,890 --> 00:02:30,850
Posmaruj chlorkiem jej palec.

7
00:02:31,020 --> 00:02:34,360
Albercie, weź mikroskop.

8
00:02:36,230 --> 00:02:39,700
Ronette, oddychaj głęboko.

9
00:02:45,620 --> 00:02:48,580
- Był tutaj.
- Pilnowano jej na okrągło.

10
00:02:48,750 --> 00:02:54,290
- To był on. Zabarwił też kroplówkę.
- Ciekawe, czy go widziała?

11
00:02:55,000 --> 00:02:59,010
Pewnie usłyszała orkiestrę dętą
i poszła szukać batuty.

12
00:02:59,170 --> 00:03:01,090
Sprawdzę tę kroplówkę.

13
00:03:01,260 --> 00:03:05,220
To on.
Nikt inny nie wiedział o literach.

14
00:03:07,520 --> 00:03:10,980
Panowie, jedną chwileczkę.

15
00:03:13,770 --> 00:03:15,690
Pora, żebym o czymś wspomniał.

16
00:03:15,860 --> 00:03:21,950
Nie dam głowy,
ale chyba odwiedził mnie olbrzym.

17
00:03:22,110 --> 00:03:26,120
Dwukrotnie, w moim pokoju.
Dał mi trzy wskazówki.

18
00:03:26,280 --> 00:03:29,250
Najpierw o Jacques’u Renault w worku.

19
00:03:29,410 --> 00:03:32,370
Potwierdziła się od razu.

20
00:03:32,540 --> 00:03:37,250
Druga brzmiała:
"Sowy nie są tym, czym się wydają."

21
00:03:37,750 --> 00:03:41,760
Trzecia dotyczyła człowieka,
który wskazuje bez chemikaliów.

22
00:03:41,930 --> 00:03:45,220
Odwiedził cię olbrzym?

23
00:03:48,180 --> 00:03:50,390
To znajomy karła?

24
00:03:57,150 --> 00:03:59,480
Masz inny sweter niż wczoraj.

25
00:03:59,530 --> 00:04:02,070
Ten kolor bardziej ci pasuje.

26
00:04:02,110 --> 00:04:05,320
Gdzie moje maniery,
jestem Harold Smith.

27
00:04:05,490 --> 00:04:08,450
Pan chyba wie, kim jestem.

28
00:04:08,620 --> 00:04:11,450
Wejdź, proszę.

29
00:04:19,050 --> 00:04:22,010
Napijesz się lemoniady?

30
00:04:22,170 --> 00:04:25,130
Mam też krakersy i mus jabłkowy.

31
00:04:25,300 --> 00:04:28,260
- Chcesz umyć ręce?
- Nie, dziękuję.

32
00:04:28,430 --> 00:04:30,890
Usiądź.

33
00:04:40,940 --> 00:04:42,740
Bardzo tu ciepło.

34
00:04:42,780 --> 00:04:44,030
Nie denerwuj się.

35
00:04:44,070 --> 00:04:47,910
Jestem tylko ciekawa.

36
00:04:48,240 --> 00:04:52,250
Interesuje cię,
co łączyło mnie z Laurą?

37
00:04:52,410 --> 00:04:54,330
Dlaczego pan do mnie napisał?

38
00:04:54,500 --> 00:04:58,500
Laura chciała, żebym się z tobą
skontaktował, gdyby coś jej się stało.

39
00:04:58,670 --> 00:05:02,840
- Dlaczego?
- Mówiła, że ciągle pytasz.

40
00:05:03,880 --> 00:05:06,970
Długo pan ją znał?

41
00:05:08,050 --> 00:05:11,010
Odkąd zaczęła rozwozić posiłki.

42
00:05:11,180 --> 00:05:17,060
- Byłem jej ulubionym klientem.
- Nie wygląda pan na przykutego do łóżka.

43
00:05:17,440 --> 00:05:20,980
Chcesz zapytać, co mi jest?

44
00:05:23,110 --> 00:05:26,950
Nie lubię wychodzić. Ja...

45
00:05:32,040 --> 00:05:34,580
Nie mogę.

46
00:05:34,950 --> 00:05:39,130
Chciałabyś wyrazić
swój pogląd na tę sprawę?

47
00:05:39,250 --> 00:05:42,420
Laura mówiła, że masz
szlachetny charakter.

48
00:05:42,460 --> 00:05:46,470
Jeśli tak dobrze się znaliście,
dlaczego mi nigdy o panu nie mówiła?

49
00:05:46,630 --> 00:05:50,430
Laura lubiła myśleć, że jestem...

50
00:05:51,850 --> 00:05:53,770
...jej tajemniczym znajomym.

51
00:05:53,930 --> 00:05:57,390
Dlaczego mnie pan wezwał?

52
00:05:57,810 --> 00:06:03,900
Jestem... byłem ogrodnikiem.

53
00:06:04,360 --> 00:06:08,360
Hoduję orchidee.
Dlatego jest tu tak ciepło.

54
00:06:08,530 --> 00:06:11,070
Nie potrzebują dużo światła.

55
00:06:11,120 --> 00:06:16,700
Chciałem cię prosić, żebyś
zaniosła jedną na jej grób.

56
00:06:16,870 --> 00:06:19,540
Oczywiście.

57
00:06:21,040 --> 00:06:24,300
Przepraszam na chwilę.

58
00:06:56,950 --> 00:06:59,750
To hybryda.

59
00:06:59,790 --> 00:07:02,540
Nazywa się "sabotek wytworny".

60
00:07:02,580 --> 00:07:05,460
Jest piękny.

61
00:07:08,380 --> 00:07:13,090
Weź go.
To mój ostatni prezent dla Laury.

62
00:07:14,220 --> 00:07:18,220
To bardzo miło z pańskiej strony,
panie Smith.

63
00:07:18,390 --> 00:07:20,850
Harold.

64
00:07:21,520 --> 00:07:25,190
Ona była dla mnie bardzo miła.

65
00:07:26,730 --> 00:07:30,740
Wybacz, ale wiem o tobie
bardzo dużo i...

66
00:07:30,900 --> 00:07:35,410
- Co?
- Jesteś tak śliczna, jak mówiła Laura.

67
00:07:46,540 --> 00:07:48,000
Wrócę tu.

68
00:07:49,260 --> 00:07:50,340
Będę czekać.

69
00:08:01,520 --> 00:08:04,150
R, B, T...

70
00:08:05,690 --> 00:08:11,780
Te litery i wskazówki olbrzyma
prowadzą do długowłosego.

71
00:08:11,940 --> 00:08:14,910
Pani Palmer zobaczyła go
w swojej wizji.

72
00:08:15,070 --> 00:08:20,120
Mówiła, że ukazał się też Maddy.
Dwa razy.

73
00:08:20,290 --> 00:08:23,250
- Mnie się przyśnił.
- I jeszcze Ronette.

74
00:08:23,410 --> 00:08:26,380
Widziała go na jawie w wagonie.

75
00:08:26,540 --> 00:08:29,500
Czworo z nas widziało go
w różnych postaciach.

76
00:08:29,670 --> 00:08:34,010
Ta więź psychiczna
doprowadzi nas do niego.

77
00:08:35,930 --> 00:08:39,720
A ten olbrzym... jaki miał głos?

78
00:08:40,100 --> 00:08:43,060
Straszliwy, grzmiący głos?

79
00:08:43,230 --> 00:08:46,190
Nie, mówił delikatnie i łagodnie.

80
00:08:46,350 --> 00:08:49,320
A ty dałeś mu fasolki,
za które miałeś kupić krowę.

81
00:08:49,480 --> 00:08:53,280
Nie, Albercie, dałem mu obrączkę.

82
00:08:53,650 --> 00:08:55,570
Dobra

83
00:08:55,740 --> 00:08:58,700
Wracając na planetę Ziemia...

84
00:08:58,870 --> 00:09:04,370
Kokaina znaleziona u Jamesa i Jacques'a
to ta z domu Leo. Jasne?

85
00:09:04,410 --> 00:09:05,620
Wszystko jasne.

86
00:09:05,670 --> 00:09:11,340
Robota Leo Johnsona przebywającego
obecnie w szpitalu jako Pan Roślinka.

87
00:09:11,380 --> 00:09:17,010
Buty rzadkiej marki Circle Brand,
nie noszone, nie przerabiane.

88
00:09:17,430 --> 00:09:22,680
Litera "B" zza paznokcia Ronette
wycięta ze "Świata Ciała".

89
00:09:22,850 --> 00:09:28,020
W tym numerze jest mowa
o klubach dla amatorów pudli.

90
00:09:28,060 --> 00:09:29,980
Nie skomentuję.

91
00:09:30,150 --> 00:09:33,320
Wysłaliśmy portret długowłosego
do wszystkich agencji

92
00:09:33,360 --> 00:09:37,280
od NASA do DEA i nic.
Nie figuruje w niczyich aktach.

93
00:09:37,450 --> 00:09:41,450
Cztery osoby widziały go
w Twin Peaks.

94
00:09:41,620 --> 00:09:43,540
Jasne

95
00:09:43,700 --> 00:09:46,660
Na marginesie:
strzelano do ciebie z Walthera PPK.

96
00:09:46,830 --> 00:09:50,420
To broń Jamesa Bonda, wiesz?

97
00:09:51,000 --> 00:09:55,010
- Wyglądasz dziś lepiej, Coop.
- Dziękuję, Albercie, lepiej się czuję.

98
00:09:55,170 --> 00:10:00,220
Po twoim strzelcu żadnych śladów.
Zebrałem trochę włókien z korytarza

99
00:10:00,390 --> 00:10:03,350
Może coś znajdę.
Idę do laboratorium.

100
00:10:03,510 --> 00:10:06,480
- Jakieś zalecenia dla nas?
- Tak.

101
00:10:06,640 --> 00:10:10,150
Potrenuj chodzenie
bez zamiatania rękami po podłodze.

102
00:10:10,190 --> 00:10:13,020
Albercie, co do moich rąk.

103
00:10:13,070 --> 00:10:16,780
Kiedy uderzyłem cię ostatnio,
czułem się podle,

104
00:10:16,820 --> 00:10:18,990
ale następnym razem
sprawi mi to przyjemność.

105
00:10:19,030 --> 00:10:20,160
Słuchaj!

106
00:10:20,200 --> 00:10:26,200
Przyznaję się do niejakiego cynizmu,
ale przemocy mówię NIE i walczę z nią.

107
00:10:26,450 --> 00:10:30,460
Godnie przyjmuję razy
i chętnie przyjmę jeszcze jeden

108
00:10:30,630 --> 00:10:33,590
ponieważ wybrałem życie
przy boku Ghandiego i Kinga.

109
00:10:33,750 --> 00:10:36,710
Moje troski są natury ogólnej.

110
00:10:36,880 --> 00:10:40,680
Odrzucam zemstę, agresję i odwet.

111
00:10:42,090 --> 00:10:45,890
Podwaliną mojego światopoglądu...

112
00:10:46,270 --> 00:10:49,190
...jest miłość.

113
00:10:51,480 --> 00:10:55,020
Kocham cię, szeryfie Truman.

114
00:11:00,660 --> 00:11:04,410
Albert wybrał niezwykłą i trudną drogę.

115
00:11:14,420 --> 00:11:18,420
Nie zostaniesz oskarżony.
Kokaina została podrzucona.

116
00:11:18,590 --> 00:11:23,640
- Próbowałem wam powiedzieć...
- Nie chcę cię tu znowu widzieć.

117
00:11:23,800 --> 00:11:27,810
- Dobrze, proszę pana.
- Nie próbuj sam wszystkiego wyjaśnić.

118
00:11:27,970 --> 00:11:32,520
Porozmawiaj z kimś - choćby ze mną.
Idź do domu.

119
00:11:36,320 --> 00:11:39,860
- Jak tam, Lucy?
- 75 słów.

120
00:11:41,530 --> 00:11:46,160
- Chwileczkę... 76... 77...
- Dobra robota, Lucy.

121
00:11:47,790 --> 00:11:50,120
78...

122
00:11:53,000 --> 00:11:59,090
Mam wypisać słowa, w których
występuje B, T i R.

123
00:11:59,260 --> 00:12:03,010
Pokaż, kiedyś byłem w tym dobry.

124
00:12:09,680 --> 00:12:12,730
PALENIE WZBRONIONE

125
00:12:14,900 --> 00:12:17,980
Idę z nim na obiad.

126
00:12:18,650 --> 00:12:22,650
To dlatego Andy zadzwonił,
że jest chory?

127
00:12:22,820 --> 00:12:26,120
Cześć, Lucy. Tu jesteś.

128
00:12:26,990 --> 00:12:29,700
Cześć, Dick.

129
00:12:30,120 --> 00:12:35,120
Zawsze uważałem cię
za kobietę pióra.

130
00:12:35,330 --> 00:12:38,000
"Zabronione"

131
00:12:40,550 --> 00:12:45,340
Doprawdy...
Nie znamy się. Richard Tremayne.

132
00:12:45,760 --> 00:12:50,100
Dom Handlowy Horne'a.
Markowa odzież męska.

133
00:12:50,970 --> 00:12:53,390
"Robot"

134
00:12:55,140 --> 00:12:57,060
Czymś go rozzłościłem?

135
00:12:57,230 --> 00:13:01,230
Chociaż rdzenna ludność
i tak ma powody do złości.

136
00:13:01,400 --> 00:13:04,030
- Chodźmy na obiad.
- Cudownie.

137
00:13:04,070 --> 00:13:07,530
Każdy płaci za siebie.

138
00:13:07,660 --> 00:13:11,540
Jak wysoki?
Sięgał głową sufitu?

139
00:13:11,830 --> 00:13:13,750
Prawie.

140
00:13:13,910 --> 00:13:18,960
Pokoje w Great Northern mają
co najmniej trzy metry wysokości.

141
00:13:19,130 --> 00:13:21,050
Co najmniej.

142
00:13:21,210 --> 00:13:24,920
Przepraszam.
Nie przeszkadzam?

143
00:13:26,430 --> 00:13:30,010
Nie, Lelandzie, o co chodzi?

144
00:13:31,640 --> 00:13:34,890
Znam tego człowieka.

145
00:13:36,850 --> 00:13:38,770
Skąd?

146
00:13:38,940 --> 00:13:42,940
Kiedy byłem dzieckiem, mój dziadek
miał domek letniskowy nad Pearl Lakes.

147
00:13:43,730 --> 00:13:46,200
- Jeździliśmy tam co roku.
- Tam go pan poznał?

148
00:13:46,240 --> 00:13:48,160
Tak. Tak...

149
00:13:48,320 --> 00:13:51,280
Teraz wiem, że nie
nazywał się Chalbert.

150
00:13:51,450 --> 00:13:56,500
Oni mieszkali po jednej stronie,
z drugiej była pusta parcela,

151
00:13:56,660 --> 00:14:00,670
a dalej stał biały dom.
Tam mieszkał.

152
00:14:01,880 --> 00:14:06,010
- Pamięta pan, jak się nazywał?
- Nie.

153
00:14:07,090 --> 00:14:10,760
Chyba... Robertson.
Robertson.

154
00:14:11,260 --> 00:14:15,980
Robertson? Robert - RTB.
To właśnie mówią litery.

155
00:14:16,480 --> 00:14:20,480
Hawk, jedź nad Pearl Lakes.
Sprawdź, kto mieszkał w tym domu.

156
00:14:20,650 --> 00:14:22,570
Postaram się.

157
00:14:22,730 --> 00:14:25,480
Leland, dziękujemy.
To cenna informacja.

158
00:14:25,530 --> 00:14:27,780
Dziękujemy, Lelandzie.

159
00:14:27,950 --> 00:14:30,780
Jeszcze jedno.

160
00:14:32,120 --> 00:14:36,080
Rzucał we mnie
zapalonymi zapałkami.

161
00:14:38,370 --> 00:14:40,790
Mawiał:

162
00:14:40,830 --> 00:14:45,550
"Chciałbyś poigrać
z ogniem, chłopczyku?"

163
00:14:55,060 --> 00:14:57,470
To on.

164
00:15:08,440 --> 00:15:11,410
Zazwyczaj na moim stoisku
wszystko działa jak w zegarku.

165
00:15:11,570 --> 00:15:15,580
Ale przed świętami robi się kołowrót.
Nie ma czasu na papierkową robotę.

166
00:15:15,740 --> 00:15:20,790
Wymyśliłem metodę zapamiętywania zamówień
przy pomocy sztuczek mnemotechnicznych.

167
00:15:20,960 --> 00:15:24,750
Na przykład:
kiedy ktoś szuka skarpetek w romby.

168
00:15:24,920 --> 00:15:30,170
Notuję to pod R jak "romby"
podpunkt S jak "skarpetki".

169
00:15:30,340 --> 00:15:34,350
Bywa trudno. Klient zamawia
na przykład płaszcz przeciwdeszczowy.

170
00:15:34,510 --> 00:15:40,560
Zanotować to pod D jak "deszczowiec",
czy pod N jak "nieprzemakalny"?

171
00:15:40,600 --> 00:15:45,110
System skłaniałby mnie raczej ku D.

172
00:15:46,570 --> 00:15:48,980
Lucy, czy mogę cię o coś spytać?

173
00:15:49,690 --> 00:15:52,950
Czy nie dziwi cię, że nie przekładam
widelca z ręki do ręki?

174
00:15:52,990 --> 00:15:55,530
Że wkładam go do ust lewą?

175
00:15:57,120 --> 00:16:00,080
Moja mama mówiła na to
"dziubdzianie".

176
00:16:00,660 --> 00:16:05,130
To wcale nie jest dziwne.
Tak jedzą w Europie.

177
00:16:06,960 --> 00:16:10,920
Minęło sześć tygodni.
Miałeś zadzwonić.

178
00:16:11,010 --> 00:16:12,010
Wiem...

179
00:16:14,050 --> 00:16:15,390
Zgubiłem twój numer.

180
00:16:16,300 --> 00:16:19,390
Pracuję u szeryfa.
Trzeba było wykręcić 997.

181
00:16:20,600 --> 00:16:24,600
Przepraszam, byłem wyjątkowo zajęty.

182
00:16:24,730 --> 00:16:27,060
Ledwo znajdowałem czas,
żeby nakarmić kota.

183
00:16:28,440 --> 00:16:31,820
Przez trzy miesiące
spotykaliśmy się co czwartek.

184
00:16:31,860 --> 00:16:33,740
Wiele mi obiecywałeś.

185
00:16:33,780 --> 00:16:37,780
Miałeś zabrać mnie
na kolację do Seattle.

186
00:16:38,280 --> 00:16:42,960
I choć ostatecznie pojechaliśmy
na familiadę w "Naleśnikowej Farmie",

187
00:16:43,000 --> 00:16:46,960
wydawało mi się, że łączy nas
coś niezwykłego.

188
00:16:48,090 --> 00:16:52,170
Niezwykłe tego wieczoru było tylko to,
że wypiliśmy dwie butelki szampana

189
00:16:52,210 --> 00:16:56,010
i skończyliśmy na łóżku
w dziale meblowym!

190
00:17:02,350 --> 00:17:05,940
Przyznaję. To było odważne,
podniecające.

191
00:17:05,980 --> 00:17:09,940
Ale myślałam, że wyniknie z tego
coś więcej. Choćby telefon.

192
00:17:09,980 --> 00:17:13,570
Przykro mi, że jesteś zła, naprawdę.

193
00:17:13,740 --> 00:17:17,240
- Wynagrodzę ci to.
- Jak?

194
00:17:17,280 --> 00:17:22,080
Na początek: obiecywałem ci sukienkę
z 20% zniżką dla pracowników.

195
00:17:22,620 --> 00:17:25,540
Po obiedzie pojadę do sklepu
i porozmawiam z panną Bolbo

196
00:17:25,580 --> 00:17:28,130
z działu odzieży damskiej.
Wybierzemy coś ładnego.

197
00:17:29,380 --> 00:17:30,460
Ach tak?

198
00:17:31,130 --> 00:17:33,760
Może sukienkę ciążową?

199
00:17:35,300 --> 00:17:36,590
Słucham?

200
00:17:37,300 --> 00:17:42,310
Jestem w ciąży, Richardzie.

201
00:17:44,060 --> 00:17:45,560
W ciąży?

202
00:17:47,850 --> 00:17:50,810
Czy Donna nie wydaje ci się jakaś inna?

203
00:17:50,860 --> 00:17:53,400
Jak to?

204
00:17:53,570 --> 00:17:56,150
Nie przeszkadza ci, że pali i tak dalej?

205
00:17:56,200 --> 00:17:59,030
Nagle zaczęła zgrywać twardziela.

206
00:18:00,120 --> 00:18:02,700
James, znasz ją lepiej niż ja.

207
00:18:03,950 --> 00:18:06,660
Tak. Przyszła do mnie do aresztu i...

208
00:18:08,290 --> 00:18:10,790
Sam nie wiem...

209
00:18:11,250 --> 00:18:11,960
Co?

210
00:18:13,500 --> 00:18:19,470
Zachowywała się, jakby chciała
zrobić to przez kraty.

211
00:18:20,760 --> 00:18:25,140
Nie obchodziło jej,
czy ktokolwiek tam jest.

212
00:18:26,020 --> 00:18:29,810
Była dziwna. Jak nie ona.

213
00:18:30,190 --> 00:18:34,110
Sam już nie wiem. Nic już nie wiem.

214
00:18:34,360 --> 00:18:39,780
Czasami chciałbym
wsiąść na motor i odjechać.

215
00:18:45,120 --> 00:18:47,700
Ucieczka niczego nie załatwi.

216
00:19:01,470 --> 00:19:04,760
Wszystko będzie dobrze.

217
00:19:07,720 --> 00:19:10,690
Donna! Gdzie byłaś?
Spóźniłaś się.

218
00:19:10,850 --> 00:19:14,230
Poznałam kogoś rozwożąc obiady.

219
00:19:14,270 --> 00:19:17,150
Staruszkowie czasem
opowiadają ciekawe historie.

220
00:19:17,190 --> 00:19:21,070
- On jest młody.
- Naprawdę? Co mu jest?

221
00:19:21,910 --> 00:19:24,240
Trudno powiedzieć...

222
00:19:24,700 --> 00:19:28,500
Jest bystry, czarujący, inteligentny.

223
00:19:29,870 --> 00:19:32,420
W przeciwieństwie do moich znajomych.

224
00:19:32,460 --> 00:19:34,000
Co to miało znaczyć?

225
00:19:34,130 --> 00:19:39,050
Siedźcie, trzymajcie się za ręce
i spróbujcie się domyśleć.

226
00:19:52,560 --> 00:19:55,480
Świetnie, Emory.

227
00:19:59,860 --> 00:20:03,240
Jest gotowa do zbliżenia.

228
00:20:15,500 --> 00:20:17,500
Pozbądźmy się jej.

229
00:20:17,540 --> 00:20:21,220
Wie, że przysłałem tu Laurę
i że jej ojciec jest właścicielem.

230
00:20:21,760 --> 00:20:25,970
Ben Horne będzie musiał zapłacić,
żeby odzyskać córeczkę.

231
00:20:26,180 --> 00:20:28,310
Wykupię drania.

232
00:20:29,060 --> 00:20:32,350
Nie chcę stracić pracy.

233
00:20:34,270 --> 00:20:37,360
Skarbie, jeśli się nie uda,
stracisz o wiele więcej.

234
00:20:37,400 --> 00:20:40,320
Sami nie damy rady!
Ben Horne jest groźny.

235
00:20:40,530 --> 00:20:43,360
Słuchaj, ty gaduło bez kręgosłupa.

236
00:20:43,400 --> 00:20:48,830
Siedzisz w tym po czubek tej
świecącej czaszki. Przestań jęczeć.

237
00:20:48,870 --> 00:20:53,660
Zrobisz, co każę.
Otrzymamy wszelką potrzebną pomoc.

238
00:20:58,250 --> 00:21:02,420
Spójrzcie na nią.
Już odleciała.

239
00:21:04,510 --> 00:21:08,680
Jeszcze parę dni
i nigdy nie przestanie.

240
00:21:09,720 --> 00:21:12,680
Jej tatuś zrobił mi to samo.

241
00:21:12,850 --> 00:21:17,270
Te są idealne.

242
00:21:18,060 --> 00:21:21,020
Do pracy w terenie - są uniwersalne.

243
00:21:22,110 --> 00:21:26,360
- Trwałe, z usztywnionymi czubkami.
- To nie to.

244
00:21:26,410 --> 00:21:30,330
A na paradę,
np. z okazji Święta Niepodległości,

245
00:21:30,580 --> 00:21:34,500
albo Dnia Weterana...
te są niezastąpione.

246
00:21:34,750 --> 00:21:36,460
Ładne, co?

247
00:21:36,500 --> 00:21:40,420
Do tego sznurowadła ze srebrnymi
końcówkami... przyciągają wzrok.

248
00:21:40,790 --> 00:21:44,090
Owszem, ale nie mogę sobie
na nie pozwolić, panie Gerard.

249
00:21:44,130 --> 00:21:47,010
Rozumiem.
Woli pan coś bardziej praktycznego.

250
00:21:47,760 --> 00:21:50,260
But na każdą okazję.

251
00:21:52,470 --> 00:21:56,060
Te są dobre.

252
00:21:56,440 --> 00:21:57,600
Model Jim Dandy.

253
00:21:59,770 --> 00:22:02,730
Są bardzo popularne...

254
00:22:02,900 --> 00:22:05,650
Ładne, ale...

255
00:22:07,070 --> 00:22:11,160
Panie Gerard... dobrze się pan czuje?

256
00:22:12,280 --> 00:22:16,960
Przepraszam...
miewam kłopoty z koncentracją.

257
00:22:17,000 --> 00:22:20,380
Coś panu dać? Może wody?

258
00:22:20,630 --> 00:22:24,590
Gdzie jest łazienka?
Mam lekarstwo.

259
00:22:25,840 --> 00:22:29,180
Na końcu korytarza. Tędy.

260
00:22:42,520 --> 00:22:45,570
Dziękuję za przybycie, Shelly.

261
00:22:45,610 --> 00:22:47,820
Szeryfie, powiem jedno.

262
00:22:47,860 --> 00:22:51,280
Wiem, że nie muszę,
więc nie będę zeznawać przeciwko Leo.

263
00:22:51,370 --> 00:22:53,620
Nie prosimy cię o to.

264
00:22:53,660 --> 00:22:56,370
To mój mąż.
Nie muszę zeznawać przeciwko niemu.

265
00:22:57,120 --> 00:23:01,750
Nie musisz go obciążać.
Prosimy tylko o zeznanie.

266
00:23:02,330 --> 00:23:07,300
Doktor Hayward mówi, że Leo jest chory.
Co za różnica?

267
00:23:08,590 --> 00:23:12,590
Różnica w tym, że parę osób
jest rannych, kilka zginęło.

268
00:23:12,760 --> 00:23:17,930
Wiemy, że Leo podpalił suszarnię
i że ty tam byłaś.

269
00:23:21,100 --> 00:23:25,190
Nie powiem nic,
co może mu zaszkodzić.

270
00:23:27,360 --> 00:23:28,780
Kocham go.

271
00:23:28,820 --> 00:23:31,740
W porządku, Shelly.

272
00:23:32,530 --> 00:23:34,200
Dziękujemy za przybycie.

273
00:23:34,240 --> 00:23:38,790
- Co...?
- Widzę, że Shelly dobrze to przemyślała.

274
00:23:38,830 --> 00:23:44,500
Może kiedyś - jeśli dopisze ci szczęście,
a lekarze pomogą - odzyskasz Leo.

275
00:23:44,540 --> 00:23:48,880
Tego samego Leo,
którego wciąż kochasz.

276
00:23:52,380 --> 00:23:57,180
Może wtedy zrozumie,
jak cudowną dziewczynę poślubił.

277
00:23:57,890 --> 00:23:59,220
Co?

278
00:23:59,270 --> 00:24:02,770
Raz jeszcze dziękuję.
Powodzenia.

279
00:24:04,900 --> 00:24:08,280
- O co tu chodzi?
- O odszkodowanie.

280
00:24:08,900 --> 00:24:11,150
Shelly sama tego nie wymyśliła.

281
00:24:11,190 --> 00:24:13,360
Kto więc pociąga za sznurki?

282
00:24:13,410 --> 00:24:16,160
To cholernie dobre pytanie.

283
00:24:52,860 --> 00:24:56,620
Bob... wiem, że jesteś blisko...

284
00:25:03,460 --> 00:25:06,120
Dopadnę cię...

285
00:25:25,100 --> 00:25:29,440
- Panie Horne?
- Muszę kończyć, Jerry.

286
00:25:30,940 --> 00:25:33,360
Czy miał pan wiadomości od córki?

287
00:25:33,530 --> 00:25:36,660
Nic od mojej wczorajszej
rozmowy z szeryfem.

288
00:25:36,740 --> 00:25:39,490
- Dzwoniła do mnie wczoraj w nocy.
- Skąd?

289
00:25:39,530 --> 00:25:42,870
Nie powiedziała.
Wspomniała, że widziała mnie w smokingu.

290
00:25:42,910 --> 00:25:45,870
Miałem go na sobie tej nocy,
kiedy do mnie strzelano.

291
00:25:46,040 --> 00:25:52,300
Mówiłem panu, że Audrey
już dawniej znikała.

292
00:25:52,550 --> 00:25:55,260
Podobnie jak dwie inne
dziewczyny z miasteczka.

293
00:25:55,420 --> 00:25:59,430
Panie Horne, czy w domu
dzieje się źle?

294
00:25:59,590 --> 00:26:03,850
Panie Cooper, czyżbym wyczuwał
w pańskim głosie

295
00:26:03,970 --> 00:26:08,270
coś więcej, niż tylko
zawodowe zainteresowanie.

296
00:26:08,560 --> 00:26:11,940
- Miałem okazję zawrzeć z Audrey znajomość.
- Znajomość?

297
00:26:13,360 --> 00:26:15,490
Agencie Cooper...

298
00:26:16,280 --> 00:26:20,280
Dam panu najlepszą radę,
jaką usłyszy pan w tym tygodniu.

299
00:26:20,450 --> 00:26:25,500
Mężczyźni padają pod urokiem Audrey
jak kaczki na strzelnicy.

300
00:26:25,660 --> 00:26:29,670
Jeśli nie chce pan
oberwać śrutem po ogonie,

301
00:26:29,830 --> 00:26:34,800
niech pan lepiej zaparkuje
swojego gruchota gdzie indziej.

302
00:26:35,130 --> 00:26:39,840
Jeśli przekroczyłem granicę,
to przepraszam.

303
00:26:40,260 --> 00:26:43,220
Mam jak najlepsze intencje.

304
00:26:43,390 --> 00:26:47,100
To boleśnie jasne.
Doceniam to.

305
00:26:47,560 --> 00:26:51,810
Postawię lampę na oknie
i wezwę pana w pierwszej kolejności,

306
00:26:51,860 --> 00:26:54,270
kiedy tylko wróci do domu.

307
00:26:54,320 --> 00:26:58,860
- Dziękuję.
- Nie. To ja panu dziękuję.

308
00:27:30,270 --> 00:27:32,150
Witaj z powrotem.

309
00:27:32,480 --> 00:27:34,060
Gdzie ja jestem?

310
00:27:34,110 --> 00:27:39,690
Coś ci przyniosłem.
Otwórz buzię.

311
00:27:40,700 --> 00:27:44,870
Angielskie karmelki.
Potrzebujesz cukru.

312
00:27:49,080 --> 00:27:52,580
- Proszę...
- Powoli, mała.

313
00:27:53,250 --> 00:27:58,670
Dobre cukiereczki?
Jest ich więcej.

314
00:28:06,220 --> 00:28:09,180
Mam na imię Jean.

315
00:28:21,400 --> 00:28:26,070
Kolorowych snów.
Rozgrzej się.

316
00:28:32,870 --> 00:28:34,790
- To on.
- Boże!

317
00:28:36,000 --> 00:28:38,590
- Znasz go?
- Jest z FBI!

318
00:28:38,630 --> 00:28:41,170
Widziałem go na zebraniu
po śmierci Laury.

319
00:28:41,210 --> 00:28:43,380
- Był tutaj?
- Chcę go dostać.

320
00:28:43,430 --> 00:28:45,470
Jean Renault

321
00:28:45,800 --> 00:28:49,350
Emory, powiedz coś żenującego.

322
00:28:50,270 --> 00:28:53,690
Jacques był twoim bratem?

323
00:28:53,730 --> 00:28:56,560
A to moja siostra - Nancy.

324
00:28:56,600 --> 00:28:59,940
Miałem też drugiego brata - Bernarda.

325
00:28:59,980 --> 00:29:02,940
Ten interes odebrał mi obu.

326
00:29:03,110 --> 00:29:06,660
Mówiłam, że nadejdzie pomoc.

327
00:29:09,370 --> 00:29:15,000
Będę pośrednikiem.
Ojciec mi zapłaci, wezmę 30%.

328
00:29:15,040 --> 00:29:17,960
Nigdy się nie dowie,
kto trzymał jego córkę.

329
00:29:18,000 --> 00:29:22,260
I dostanę faceta, który zabił mi brata.
Jak się nazywa?

330
00:29:22,340 --> 00:29:24,970
Cooper.

331
00:29:25,800 --> 00:29:28,640
Wystarczy, że mi go przyprowadzicie.

332
00:29:29,760 --> 00:29:32,930
Pan Horne może to załatwić.

333
00:29:32,970 --> 00:29:36,690
Ale pan pomoże, prawda?

334
00:29:37,730 --> 00:29:40,610
Jak pan sobie życzy.

335
00:29:40,650 --> 00:29:45,030
- Kaseta z dziewczyną. Daj ją.
- Oczywiście.

336
00:29:48,990 --> 00:29:53,290
Ona wraca na północ,
albo nie będzie umowy.

337
00:29:53,330 --> 00:29:58,920
Jean, powiedz mojej siostrze,
żeby była grzeczna, bo się zdenerwujesz.

338
00:29:59,420 --> 00:30:05,510
Zostanie. Dostaniecie pieniądze,
ja dostanę Coopera. Wszyscy będą szczęśliwi.

339
00:30:07,760 --> 00:30:11,760
Oczywiście nie pozwolimy
dziewczynie przeżyć.

340
00:30:11,930 --> 00:30:13,850
Nie możemy, prawda?

341
00:30:14,770 --> 00:30:19,400
Dziękuję, Pete.
Jestem wdzięczny, że zadzwoniłeś.

342
00:30:21,020 --> 00:30:24,530
Harry? Zrób sobie przerwę.

343
00:30:25,190 --> 00:30:30,240
- Josie dzwoniła do Pete'a. Wraca jutro.
- Świetnie. Przesłuchamy ją.

344
00:30:30,410 --> 00:30:33,370
Coop, mam prośbę.

345
00:30:37,710 --> 00:30:40,670
Chciałbym pomówić z nią pierwszy,
na osobności.

346
00:30:40,830 --> 00:30:44,090
Harry, nie pozwólmy,
aby uczucia kolidowały z pracą.

347
00:30:44,300 --> 00:30:46,460
Łatwo powiedzieć.

348
00:30:47,380 --> 00:30:50,050
Pogadaj z nią i przyprowadź.

349
00:30:50,220 --> 00:30:53,180
Może nie jest w nic zamieszana.

350
00:30:53,350 --> 00:30:56,770
Harry, masz moje błogosławieństwo.

351
00:30:56,890 --> 00:31:00,480
Pearl Lakes.
Koło domu Palmerów jest pusta parcela.

352
00:31:00,650 --> 00:31:05,690
Dalej znajduje się biały dom. Pusty.
Na skrzynce na listy nie ma nazwiska.

353
00:31:05,860 --> 00:31:07,780
- Tytuł własności?
- Szukają.

354
00:31:07,940 --> 00:31:09,860
Zakład energetyczny sprawdza rachunki.

355
00:31:10,030 --> 00:31:12,990
- Kiedy się dowiemy?
- Najwcześniej rano.

356
00:31:13,160 --> 00:31:16,120
- Nie widziałeś pana Gerarda?
- Tego od butów?

357
00:31:16,290 --> 00:31:19,710
- Jednoręki?
- Przyszedł pokazać mi próbki.

358
00:31:19,750 --> 00:31:25,300
- Był tutaj?
- Zasłabł i poszedł do łazienki. Nie wrócił.

359
00:31:26,090 --> 00:31:30,590
Pamiętasz, że w moim śnie
jednoręki znał Boba?

360
00:31:43,400 --> 00:31:46,150
O co chodzi?

361
00:31:53,820 --> 00:31:57,490
"Bez chemikaliów on wskazuje."

362
00:31:57,990 --> 00:32:00,960
Trzecia wskazówka olbrzyma.

363
00:32:01,120 --> 00:32:05,080
Harry, musimy odnaleźć jednorękiego.

364
00:32:17,890 --> 00:32:19,560
Czy to konieczne?

365
00:32:20,640 --> 00:32:22,060
Dla jej dobra.

366
00:32:22,810 --> 00:32:25,770
W nocy zdarła rzemienne pasy,
jakby były z papieru.

367
00:32:26,150 --> 00:32:31,780
Zbadałem jej krew.
Nieprawdopodobne ilości adrenaliny.

368
00:32:31,820 --> 00:32:34,240
W życiu czegoś takiego nie widziałem.

369
00:32:34,280 --> 00:32:36,240
Może twoja obecność ją uspokoi?

370
00:32:37,120 --> 00:32:40,700
Mów do niej, śpiewaj.
Ma jakąś ulubioną piosenkę?

371
00:32:41,160 --> 00:32:42,790
Ulubioną piosenkę?

372
00:32:43,670 --> 00:32:47,670
To, albo pielgrzymka do Lourdes.

373
00:32:47,840 --> 00:32:52,590
Jeśli mam śpiewać,
wolałbym, żebyśmy zostali sami.

374
00:32:53,050 --> 00:32:56,220
Zamknę za sobą drzwi.

375
00:33:03,480 --> 00:33:08,190
Nadine... lekarz mówi,
że powinienem ci zaśpiewać.

376
00:33:09,730 --> 00:33:14,950
Przepraszam, kochanie,
ale nie wiem, co byś chciała usłyszeć.

377
00:33:21,830 --> 00:33:30,500
/Na szczycie Old Smokey
Zalega wciąż śnieg./

378
00:33:30,710 --> 00:33:39,930
/Straciłem mą miłość
Nie chciała już mnie./

379
00:33:40,010 --> 00:33:48,270
/Bo słodko jest kochać,
Rozłąka to ból./

380
00:33:48,350 --> 00:33:54,110
/Niestała kochanka
Serce złamie na pół./

381
00:33:58,740 --> 00:34:01,280
O Boże...

382
00:34:07,120 --> 00:34:11,210
/Ja się wspinam, ty się wspinasz,
pniemy razem się./

383
00:34:11,250 --> 00:34:15,260
/Gdy dotrzemy na sam szczyt,
zwycięstwo ujrzy dzień!/

384
00:34:15,420 --> 00:34:18,590
Z-W-Y-C-I-Ę-S-T-W-O!

385
00:34:22,720 --> 00:34:23,600
Cześć, Eddie!

386
00:34:23,770 --> 00:34:27,020
Przyjechałeś po mnie?

387
00:34:28,980 --> 00:34:32,980
Doktor Hayward mówi,
że mimo zapalenia migdałków

388
00:34:33,150 --> 00:34:36,110
mogę pójść na próbę zespołu kibicek.

389
00:34:36,280 --> 00:34:41,320
- Próbę zespołu kibicek...?
- Jestem w ostatniej klasie, Eddie.

390
00:34:41,490 --> 00:34:44,450
Jak to się mówi...

391
00:34:44,620 --> 00:34:48,540
raz w życiu ma się osiemnaście lat.

392
00:35:25,700 --> 00:35:30,790
- Doktorze Jacoby.
- Wejdźcie, panowie.

393
00:35:30,830 --> 00:35:36,800
To moja żona, Eolani.
Mieszka w naszym domu w Hanalei.

394
00:35:37,460 --> 00:35:42,890
Przeprowadzamy leczenie kahuna.

395
00:35:43,760 --> 00:35:46,140
Witam.

396
00:35:47,560 --> 00:35:50,520
Jest pan gotów do seansu?

397
00:35:50,690 --> 00:35:54,940
Tak.
Wielokrotnie poddawałem się hipnozie.

398
00:35:55,270 --> 00:35:59,900
Stosuję metodę autosugestii
czytając to.

399
00:36:00,070 --> 00:36:03,160
Eolani, puść taśmę.

400
00:36:04,240 --> 00:36:10,290
Szeryfie Truman, może pan
potrzymać ten kamień nad łóżkiem?

401
00:36:14,670 --> 00:36:18,800
Świetnie, tylko...
trochę wyżej... tak.

402
00:36:19,880 --> 00:36:24,090
W porządku, agencie Cooper.
Jestem gotów.

403
00:36:26,140 --> 00:36:30,140
Stoisz na miękkim,
zielonym kobiercu trawy.

404
00:36:30,310 --> 00:36:34,440
Piłka jest cztery i pół metra od dołka.

405
00:36:36,560 --> 00:36:40,400
Za zielenią są dwa doły z piachem.

406
00:36:40,740 --> 00:36:44,740
Staw pokryty liliami wodnymi
otwiera się na szmaragdowy kanał.

407
00:36:44,780 --> 00:36:49,740
Dołek zdaje się powoli
płynąć po zieleni ku stawowi,

408
00:36:49,870 --> 00:36:51,790
Unosi go wiosenny wiatr.

409
00:36:52,710 --> 00:36:55,290
Zieleń się rozrasta.

410
00:36:55,330 --> 00:37:01,420
Porywa cię i owija
zieloną chustą spokoju.

411
00:37:02,380 --> 00:37:04,880
Harry...

412
00:37:05,760 --> 00:37:09,760
Uderzasz w piłkę,
a ona sunie w kierunku dołka

413
00:37:09,930 --> 00:37:13,430
i lekko wpada w sam środek.

414
00:37:14,100 --> 00:37:17,060
Słyszy mnie pan,
doktorze Jacoby?

415
00:37:17,230 --> 00:37:19,480
Tak...

416
00:37:19,520 --> 00:37:23,320
Jest pan w szpitalu,
w jednym pokoju z Jakiem Renault.

417
00:37:23,490 --> 00:37:27,490
Czuje pan coś?
Zapach oleju napędowego?

418
00:37:27,660 --> 00:37:31,120
Czułem ten zapach w parku.

419
00:37:31,830 --> 00:37:34,790
Powietrze było nim przesycone.

420
00:37:34,960 --> 00:37:39,630
Wracamy do pokoju szpitalnego.
Czy ktoś wchodził?

421
00:37:40,170 --> 00:37:45,630
Pielęgniarki, doktor Hayward,
pan, szeryf Truman.

422
00:37:45,680 --> 00:37:48,340
Co dzieje się
po naszym wyjściu?

423
00:37:48,390 --> 00:37:50,970
Śni mi się, że jestem
nad zatoką Hanauma.

424
00:37:51,010 --> 00:37:53,680
Żongluję orzechami kokosowymi.

425
00:37:53,720 --> 00:37:57,020
Wszyscy goście to Nigeryjczycy.

426
00:37:59,980 --> 00:38:03,480
Czy później ktoś wchodził do pokoju?

427
00:38:03,530 --> 00:38:06,530
Budzi mnie odgłos rozrywanego plastra.

428
00:38:06,570 --> 00:38:10,370
Widzę, jak poduszka nakrywa mu twarz.

429
00:38:10,410 --> 00:38:13,620
Facet wydaje odgłos...

430
00:38:14,580 --> 00:38:18,540
...takie "wrrr", jakby warczał pies.

431
00:38:18,750 --> 00:38:22,340
Jacques przestaje się szarpać.

432
00:38:22,920 --> 00:38:25,880
Próbuję zobaczyć, kto trzyma poduszkę.

433
00:38:26,050 --> 00:38:29,010
Kto to jest? Widzi go pan?

434
00:38:29,180 --> 00:38:31,720
Znam go!

435
00:38:51,320 --> 00:38:53,780
Cześć!

436
00:38:55,950 --> 00:38:59,960
To od Harolda Smitha.

437
00:39:00,080 --> 00:39:06,130
Przepraszam, że nie przychodziłam od pogrzebu,
ale sprawy się skomplikowały.

438
00:39:06,340 --> 00:39:10,130
Sypiałaś z tym Haroldem?

439
00:39:10,510 --> 00:39:14,260
Jest miły. Trochę ekscentryczny.

440
00:39:18,850 --> 00:39:24,940
Ale chyba każdy okaże się taki,
jeśli mu się uważnie przyjrzeć.

441
00:39:27,190 --> 00:39:30,150
Musimy porozmawiać.

442
00:39:30,320 --> 00:39:34,330
Pewnie już wiesz o mnie i Jamesie.

443
00:39:34,490 --> 00:39:38,410
Od twojej śmierci jesteśmy razem.

444
00:39:38,660 --> 00:39:43,290
Nie wydaje mi się,
żebym musiała ci to tłumaczyć.

445
00:39:43,880 --> 00:39:49,010
Znałaś nasze uczucia,
zanim my sami je poznaliśmy.

446
00:39:49,090 --> 00:39:52,800
Jak to możliwe, że jesteś
taka bystra w tych sprawach...

447
00:39:52,890 --> 00:39:56,640
...i tak głupia w innych.

448
00:39:59,810 --> 00:40:02,020
Jestem na ciebie zła.

449
00:40:02,060 --> 00:40:05,650
Kiedy było nas troje, wszystko grało.

450
00:40:05,770 --> 00:40:10,950
Teraz cię nie ma.
Kocham Jamesa. I jest źle.

451
00:40:13,070 --> 00:40:16,030
Przyjechała twoja kuzynka Maddy.

452
00:40:16,200 --> 00:40:19,160
Myślę, że coś między nimi jest.

453
00:40:21,000 --> 00:40:24,290
Boję się, że w końcu
stracę was oboje.

454
00:40:29,760 --> 00:40:34,340
Tak bardzo chciałam
być do ciebie podobna.

455
00:40:34,890 --> 00:40:39,430
Chciałam mieć twoją siłę i odwagę.

456
00:40:42,350 --> 00:40:47,020
I spójrz, co ci one dały

457
00:40:47,060 --> 00:40:50,940
Do czego cię doprowadziły.

458
00:40:51,650 --> 00:40:53,570
Kocham cię, Lauro,

459
00:40:53,740 --> 00:40:58,160
ale przez cały czas
rozwiązywałyśmy twoje problemy.

460
00:40:58,410 --> 00:41:01,000
I wiesz co? Tak pozostało.

461
00:41:01,040 --> 00:41:03,870
Nie moje, Jamesa, czy Maddy.
Twoje.

462
00:41:03,910 --> 00:41:08,420
Nie żyjesz, ale twoje problemy pozostały.

463
00:41:08,630 --> 00:41:13,630
Jakby nie zakopali cię
dostatecznie głęboko.

464
00:41:17,720 --> 00:41:20,680
- James!
- Szukam Donny. Nie mogę jej znaleźć.

465
00:41:20,850 --> 00:41:24,850
- Muszę z kimś pogadać.
- Co się stało?

466
00:41:25,020 --> 00:41:28,060
- Wróciła.
- Kto?

467
00:41:29,190 --> 00:41:32,150
Moja mama.
Jest pijana do nieprzytomności.

468
00:41:32,320 --> 00:41:34,240
Nienawidzę jej!

469
00:41:34,280 --> 00:41:37,030
Nie mów tak.

470
00:41:37,070 --> 00:41:40,780
Jaki jesteś rozpalony.

471
00:41:49,420 --> 00:41:51,750
Przytul mnie.

472
00:42:06,190 --> 00:42:09,310
Boże... Donna...

473
00:42:23,290 --> 00:42:26,040
Dlaczego?

474
00:42:32,880 --> 00:42:37,090
Ja tylko przyjechałam na pogrzeb.

475
00:42:37,170 --> 00:42:41,720
I jakbym zapadła się w jakiś sen.

476
00:42:45,430 --> 00:42:50,650
Tak jakby ludzie myśleli,
że jestem Laurą...

477
00:42:51,860 --> 00:42:56,860
...a ja nią nie jestem.
W niczym jej nie przypominam.

478
00:42:56,900 --> 00:43:01,030
Ciężko ci, wiem.
Wiem, skarbie.

479
00:43:01,070 --> 00:43:05,160
Wiem tylko, że Laura
była moją kuzynką.

480
00:43:05,200 --> 00:43:09,040
Kochałam ją, a ona umarła.

481
00:43:09,370 --> 00:43:12,330
To wszystko, co wiem.

482
00:43:13,090 --> 00:43:15,920
Chcesz po prostu
żyć jak dawniej.

483
00:43:16,090 --> 00:43:18,010
- Tak...
- Ja też.

484
00:43:18,840 --> 00:43:21,930
Wszyscy tego pragniemy.

485
00:43:22,300 --> 00:43:27,060
I staramy się,
tylko nam nie wychodzi.

486
00:43:32,100 --> 00:43:37,110
Gdyby było tak, jak przed laty...
w Pearl Lakes.

487
00:43:39,700 --> 00:43:42,620
Lelandzie...

488
00:43:43,990 --> 00:43:47,080
Drzwi były otwarte.

489
00:43:47,790 --> 00:43:50,620
Lelandzie...

490
00:43:50,660 --> 00:43:51,830
O co chodzi?

491
00:43:56,210 --> 00:44:01,970
Jesteś aresztowany pod zarzutem
zabójstwa Jacques'a Renault.

492
00:44:16,440 --> 00:44:19,400
Przepraszam, nie miałam dokąd pójść.

493
00:44:19,440 --> 00:44:21,490
Donna? Co się stało?

494
00:44:21,650 --> 00:44:26,490
Nie wiem, czy w ogóle chcę o tym mówić.
Czuję się jak idiotka.

495
00:44:26,530 --> 00:44:29,580
Spokojnie.
Usiądź.

496
00:44:30,000 --> 00:44:36,210
Wmawiam sobie, że przechodzi trudny okres,
ale jak długo mam to robić?

497
00:44:36,250 --> 00:44:39,210
- James Hurley?
- Tak.

498
00:44:39,380 --> 00:44:43,720
Myślałam, że mam to poukładane.
Dlaczego tak się tym przejmuję?

499
00:44:43,760 --> 00:44:48,970
Przejmujesz się, bo ci na nim zależy.
Prawda?

500
00:44:49,010 --> 00:44:52,940
A nie powinno.
Niech to się skończy.

501
00:44:53,310 --> 00:44:58,070
To zależy od ciebie.
Jak to ona mówiła?

502
00:44:59,190 --> 00:45:03,200
"Donna - Madonna,
jutro też będzie dzień"

503
00:45:03,360 --> 00:45:06,070
Skąd wiesz?

504
00:45:13,710 --> 00:45:17,880
Zrobię ci coś do picia.

505
00:46:05,800 --> 00:46:09,510
[... to jest pamiętnik Laury Palmer... ]

506
00:46:16,600 --> 00:46:19,400
Tekst polski i opracowanie:
Jakub "Qbin" Jedynak

507
00:46:19,440 --> 00:46:22,320
Redakcja: Kraps
<kraps@poczta.onet.pl>

508
00:46:22,360 --> 00:46:25,240
Polska strona o Davidzie Lynchu
www.lynchland.pl

509
00:46:27,240 --> 00:46:30,240
.:: Napisy24 - Nowy Wymiar Napisów ::.
Napisy24.pl

