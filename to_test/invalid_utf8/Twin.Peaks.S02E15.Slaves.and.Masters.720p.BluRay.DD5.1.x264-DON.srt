1
00:00:28,900 --> 00:00:33,670
MIASTECZKO TWIN PEAKS

2
00:02:42,200 --> 00:02:44,110
Jak on się nazywa?

3
00:02:44,190 --> 00:02:47,150
Ledwie go znałem. Jim.

4
00:02:49,310 --> 00:02:53,010
James. James Hurley.

5
00:02:54,480 --> 00:02:56,390
Mówił, że jest z Twin Peaks.

6
00:02:56,820 --> 00:03:03,490
Pewnie był już daleko,
kiedy pan Marsh siadł za kółkiem.

7
00:03:04,580 --> 00:03:06,410
Długo tu pracował?

8
00:03:06,500 --> 00:03:09,320
Pani Marsh najęła go
do naprawy jaguara.

9
00:03:09,400 --> 00:03:10,710
Jaguara?

10
00:03:15,750 --> 00:03:18,500
Samochodu. Gdzie go pani spotkała?

11
00:03:19,010 --> 00:03:21,900
Jaguar nawalił przy barze Wallies.

12
00:03:22,980 --> 00:03:25,020
Weszłam zadzwonić.

13
00:03:26,700 --> 00:03:28,470
Zaproponował mi pomoc.

14
00:03:29,290 --> 00:03:32,030
Sprawdzimy to. Dziękuję.

15
00:03:38,410 --> 00:03:41,670
Żegnaj, Jamesie Hurleyu.

16
00:03:41,760 --> 00:03:43,530
Daj spokój.

17
00:03:45,180 --> 00:03:50,460
Uważaj. Nie znoszę
nerwowych wspólników.

18
00:03:50,540 --> 00:03:53,890
Chcę to mieć za sobą.

19
00:03:55,240 --> 00:03:57,510
Jak pani każe, pani Marsh.

20
00:04:22,350 --> 00:04:27,350
Sami się z tego nie wywiniemy,
potrzebujemy pomocy.

21
00:04:27,430 --> 00:04:33,120
Nic nie zrobiłem.
To Malcolm go zabił i wrabia mnie.

22
00:04:33,200 --> 00:04:36,640
- Podać coś?
- Nie, dziękuję.

23
00:04:43,480 --> 00:04:48,060
To bez znaczenia.
Nie czekajmy, aż nas złapią.

24
00:04:48,150 --> 00:04:51,240
- Zadzwonię do Eda.
- Nie.

25
00:04:51,320 --> 00:04:54,530
Pogadam z Evelyn.

26
00:04:54,610 --> 00:04:58,790
Wyznała mi, że to spisek,
może powtórzy to policji.

27
00:04:58,870 --> 00:05:00,110
Na pewno?

28
00:05:00,700 --> 00:05:01,720
Tak.

29
00:05:04,140 --> 00:05:08,870
Nie masz szans, będą na ciebie czekać.
Musimy wyjechać.

30
00:05:08,950 --> 00:05:13,010
- Wiem, że mnie wysłucha.
- Wiesz?

31
00:05:15,200 --> 00:05:16,320
Dlaczego?

32
00:05:20,350 --> 00:05:22,110
Dzwonię do Eda.

33
00:05:22,780 --> 00:05:23,960
Zaczekaj tu.

34
00:05:29,800 --> 00:05:32,420
Ed? Tu Donna.

35
00:05:32,640 --> 00:05:36,800
Nie mam wiele czasu,
a James ma chyba kłopoty.

36
00:05:37,300 --> 00:05:42,090
Reperował samochód jednej kobiety,

37
00:05:42,180 --> 00:05:44,520
a potem był wypadek...

38
00:05:46,350 --> 00:05:50,360
Cześć, Yvette.
Podobno masz nowego chłopaka.

39
00:05:51,970 --> 00:05:55,200
To super. Bardzo się cieszę.

40
00:05:55,700 --> 00:05:57,260
Opowiedz mi o nim.

41
00:06:05,660 --> 00:06:07,710
- Przepraszam.
- Cześć, Frank.

42
00:06:10,020 --> 00:06:12,890
Co robiłeś wczoraj
wieczorem u Shelly?

43
00:06:12,970 --> 00:06:16,950
Omal nie zginęła, a wy pytacie,
co tam robiłem? A Leo?

44
00:06:17,040 --> 00:06:19,890
Co robiłeś u Shelly?

45
00:06:21,610 --> 00:06:23,780
No dobra.

46
00:06:24,250 --> 00:06:27,330
Jesteśmy razem.

47
00:06:28,350 --> 00:06:34,350
I byliśmy na długo przed śmiercią Laury
i zanim Leo został rośliną.

48
00:06:34,440 --> 00:06:38,680
Bobby, gdzie byłeś,
kiedy spłonął tartak?

49
00:06:39,430 --> 00:06:41,510
Co to ma do rzeczy?

50
00:06:41,590 --> 00:06:45,260
Leo chciał cię zabić. Postrzeliłeś go?

51
00:06:45,340 --> 00:06:50,420
Nie. Poszedłem do Shelly
w noc pożaru, bo się o nią bałem.

52
00:06:50,500 --> 00:06:54,220
Chciał mnie zdzielić siekierą,
ale załatwił go Hank Jennings.

53
00:06:54,300 --> 00:06:57,110
- To na pewno był Hank?
- Jasne.

54
00:06:57,190 --> 00:07:01,370
- Co go łączy z Leo?
- Wy mi powiedzcie.

55
00:07:01,460 --> 00:07:05,270
Shelly, wiesz, gdzie jest Leo?

56
00:07:06,870 --> 00:07:07,940
Nie.

57
00:07:08,240 --> 00:07:12,490
Przydzielę ci całodobową ochronę.

58
00:07:12,580 --> 00:07:16,270
Sam ją obronię. Nie marnujcie ludzi.

59
00:07:16,350 --> 00:07:19,910
Bobby, nie wygłupiaj się.

60
00:07:20,240 --> 00:07:21,360
Możecie iść.

61
00:07:23,620 --> 00:07:28,360
- Shelly? Nie spuścimy cię z oka.
- Dziękuję.

62
00:07:29,040 --> 00:07:32,050
Od razu czuję się bezpieczniej, a ty?

63
00:07:41,400 --> 00:07:42,840
Znajdź pracę, łobuzie!

64
00:07:43,550 --> 00:07:47,560
Truman! Znów tu wylądowałem.

65
00:07:47,640 --> 00:07:48,650
Jak się masz?

66
00:07:53,690 --> 00:07:56,150
- Panowie?
- Co masz?

67
00:07:57,310 --> 00:08:01,070
Powrotny bilet do Twin Peaks,
Windoma Earle'a

68
00:08:01,150 --> 00:08:04,410
i rozkaz Gordona Cola.
Jak to on powiedział:

69
00:08:04,840 --> 00:08:06,720
"Martwię się o Coopa!".

70
00:08:13,870 --> 00:08:18,520
Springfield, Kansas City,
Lawton, Dallas, Jackson.

71
00:08:18,600 --> 00:08:23,860
Earle powysyłał tam paczki
do komend policji.

72
00:08:23,940 --> 00:08:26,670
Wyglądały jak bomby,
ale były niegroźne.

73
00:08:26,760 --> 00:08:29,910
Zapłacono za nie lipną kartą
na nazwisko Windom Earle.

74
00:08:29,990 --> 00:08:33,950
- Co w nich było?
- Po jednej sztuce odzieży.

75
00:08:35,680 --> 00:08:41,750
Biały welon, podwiązka,
białe pantofle,

76
00:08:41,830 --> 00:08:45,810
naszyjnik z pereł
i wreszcie ślubna suknia.

77
00:08:47,300 --> 00:08:48,910
Caroline...

78
00:08:48,990 --> 00:08:55,310
Szuka go całe FBI i stanowa
z Mississippi, Oklahomy i Illinois.

79
00:08:55,390 --> 00:09:00,680
Wszyscy są na tym balu,
ale on chce zatańczyć tylko z tobą.

80
00:09:04,840 --> 00:09:09,370
Włóczęga z posterunku
zginął od ciosu nożem w aortę.

81
00:09:09,970 --> 00:09:12,790
Wskazywał dokładnie na to pole.

82
00:09:12,870 --> 00:09:15,110
Jak Earle pokonał stężenie pośmiertne?

83
00:09:15,510 --> 00:09:18,080
Proces postępuje od głowy do stóp,
ale po dwóch dniach

84
00:09:18,170 --> 00:09:20,200
odpuszcza w odwrotnej kolejności.

85
00:09:20,290 --> 00:09:25,230
Zaczekał z rękami i palcami,
żeby móc je odpowiednio ustawić.

86
00:09:26,280 --> 00:09:29,970
A potem zabawił się
w Zeusa w rozdzielni.

87
00:09:30,050 --> 00:09:32,170
- Co znaleźliście?
- Tę mapę,

88
00:09:32,250 --> 00:09:34,090
przyklejoną pod blatem stołu.

89
00:09:35,090 --> 00:09:36,470
Dobra robota.

90
00:09:45,920 --> 00:09:49,090
Zrobi następny ruch, to pewne.

91
00:09:54,130 --> 00:09:56,540
Słówko o stroju.

92
00:09:57,520 --> 00:09:58,870
Słucham.

93
00:09:58,950 --> 00:10:04,420
Zastąpienie ciemnego garnituru
wiejskim strojem w barwach ziemi

94
00:10:04,500 --> 00:10:10,210
to stylistyczne samobójstwo,
ale tobie dziwnie służy.

95
00:10:11,880 --> 00:10:13,210
Dziękuję.

96
00:10:50,000 --> 00:10:52,010
Obudziłeś się.

97
00:10:53,310 --> 00:10:55,160
Chyba dobrze spałeś?

98
00:10:55,730 --> 00:10:59,820
A ja w tym czasie wszystkiego
się o tobie dowiedziałem...

99
00:11:01,140 --> 00:11:03,680
Leo Johnsonie.

100
00:11:03,770 --> 00:11:07,390
Policja wszędzie cię szuka.

101
00:11:07,470 --> 00:11:09,600
Zobaczmy...

102
00:11:11,080 --> 00:11:14,980
Narkotyki, podpalenie...

103
00:11:17,030 --> 00:11:22,090
usiłowanie zabójstwa, przemoc domowa.

104
00:11:22,180 --> 00:11:24,200
To lubię.

105
00:11:27,500 --> 00:11:31,240
Leo... opatrzyłem twoje rany,
wyliżesz się.

106
00:11:31,790 --> 00:11:34,830
Usunąłem ci przysłowiowy cierń z łapy.

107
00:11:35,980 --> 00:11:38,500
Leo! Mój ty lwie.

108
00:11:42,430 --> 00:11:47,220
A teraz mi pomożesz
i będziesz posłuszny.

109
00:11:47,430 --> 00:11:48,840
Zrobisz to dla mnie?

110
00:11:51,010 --> 00:11:58,590
Wiesz, że w XIII wieku
japońskim samurajom odebrano broń,

111
00:11:58,680 --> 00:12:08,440
ale odkryli, że bambusowy flet
shakuhachi to doskonała maczuga?

112
00:12:11,050 --> 00:12:17,200
Taki flet dostarcza
wielu przyjemności, nieprawdaż?

113
00:12:22,250 --> 00:12:25,590
To był naszyjnik na specjalną okazję,

114
00:12:25,670 --> 00:12:27,860
ale mi to zepsułeś.

115
00:12:35,710 --> 00:12:37,600
Chodź tu.

116
00:12:39,500 --> 00:12:41,640
Ugotowałem ci owsianki.

117
00:12:50,750 --> 00:12:53,470
Chodź, Leo, zjesz kaszkę.

118
00:13:06,240 --> 00:13:08,690
To już 20 lat.

119
00:13:10,690 --> 00:13:14,730
Dopiero co była studniówka i proszę...

120
00:13:16,400 --> 00:13:20,500
A po drodze nic, tylko męka.

121
00:13:27,030 --> 00:13:29,180
Próbowałam o tym nie myśleć.

122
00:13:30,200 --> 00:13:32,830
Cały czas poświęciłam barowi,

123
00:13:33,330 --> 00:13:35,220
żeby tylko przetrwał.

124
00:13:36,250 --> 00:13:38,190
Nie miałam nic innego.

125
00:13:40,250 --> 00:13:42,200
Pracujemy codziennie...

126
00:13:43,540 --> 00:13:45,910
więc nie mam na nic czasu.

127
00:13:47,870 --> 00:13:49,540
Żadnych urodzin...

128
00:13:50,390 --> 00:13:52,060
czwartych lipca...

129
00:13:53,810 --> 00:13:55,390
świąt...

130
00:13:59,890 --> 00:14:04,120
Zamieniłem święta
w tygodnie pełne nudnych dni.

131
00:14:05,110 --> 00:14:07,790
A gdy nie pracowałem,

132
00:14:07,870 --> 00:14:12,020
myślałem o prezentach,
których ci nie kupiłem...

133
00:14:12,940 --> 00:14:14,720
i nie dałem.

134
00:14:18,130 --> 00:14:20,700
Rok temu coś ci kupiłam.

135
00:14:21,510 --> 00:14:25,920
Spinkę z turkusem i onyksem.

136
00:14:27,840 --> 00:14:30,510
I poszłam ci ją dać.

137
00:14:33,750 --> 00:14:37,490
Ale nie mogłam wejść.
Stałam i czekałam.

138
00:14:38,030 --> 00:14:40,030
Widziałam cię przez okno.

139
00:14:42,380 --> 00:14:44,280
Dlaczego nie weszłam?

140
00:14:49,380 --> 00:14:51,360
Przepraszam.

141
00:14:53,570 --> 00:14:55,320
Zawsze...

142
00:14:57,600 --> 00:14:59,850
powinnaś być w moim domu.

143
00:15:02,760 --> 00:15:04,390
I w łóżku.

144
00:15:05,900 --> 00:15:10,660
Pomówmy o przyszłości
i o tym, co zrobimy.

145
00:15:13,650 --> 00:15:19,110
Przyjdź dziś, kiedy zamknę lokal,
i zabierz mnie do domu.

146
00:15:19,710 --> 00:15:23,210
Masz rację, wszystko przed nami.

147
00:15:24,780 --> 00:15:26,350
Mamy mnóstwo czasu.

148
00:15:32,140 --> 00:15:34,250
Równie dobrze
można jej powiedzieć teraz.

149
00:15:48,370 --> 00:15:49,870
Cześć.

150
00:15:51,060 --> 00:15:54,670
Jechałam aż na finał do Knife River,

151
00:15:54,750 --> 00:15:59,380
żeby się dowiedzieć,
że samolot nie jest dopuszczony.

152
00:16:01,410 --> 00:16:03,450
Zdyskwalifikowali mnie.

153
00:16:04,330 --> 00:16:06,930
Drugie miejsce. Dno!

154
00:16:07,600 --> 00:16:10,310
Fatalna sprawa.

155
00:16:11,570 --> 00:16:12,870
Cześć, Nadine.

156
00:16:15,660 --> 00:16:18,440
Powinnam cię przeprosić.

157
00:16:19,300 --> 00:16:23,090
Strasznie sprałam Hanka.

158
00:16:24,530 --> 00:16:27,320
Bałam się, że zrobi Eddiemu krzywdę...

159
00:16:28,740 --> 00:16:30,580
i poniosło mnie.

160
00:16:32,220 --> 00:16:33,400
Dziękuję.

161
00:16:42,750 --> 00:16:46,810
I jeszcze coś. Od dawna o was wiem.

162
00:16:47,940 --> 00:16:53,750
Ale nic nie szkodzi, bo nie będę
się czuła winna, chodząc z Mikiem.

163
00:16:53,830 --> 00:16:54,940
Z Mikiem?

164
00:16:55,540 --> 00:17:01,280
Nie będę się w to zagłębiać,
ale to naprawdę coś poważnego.

165
00:17:01,520 --> 00:17:05,740
Róbcie, co chcecie.

166
00:17:06,430 --> 00:17:09,670
Mnie to nie przeszkadza.

167
00:17:11,990 --> 00:17:12,950
Naprawdę.

168
00:17:32,950 --> 00:17:34,200
Josie...

169
00:17:35,520 --> 00:17:37,050
dość tego.

170
00:17:38,480 --> 00:17:42,030
Muszę coś powiedzieć
kolegom z Seattle.

171
00:17:42,610 --> 00:17:45,300
Wiedzą, że coś cię z nim łączyło.

172
00:17:47,090 --> 00:17:50,870
- Jak zginął?
- Od trzech strzałów w tył głowy.

173
00:17:56,030 --> 00:17:58,250
Jeśli w ogóle chcesz
powiedzieć prawdę...

174
00:18:01,110 --> 00:18:04,660
to teraz.

175
00:18:05,290 --> 00:18:07,230
Doleję sobie kawki.

176
00:18:23,330 --> 00:18:27,010
- To ty, Pete?
- Tak. Cooper?

177
00:18:29,230 --> 00:18:36,110
Byłem w pralni, gdzie za ladą
stoi Jeanie Pombelek.

178
00:18:36,190 --> 00:18:39,860
Nie mówi ani słowa po angielsku.

179
00:18:42,890 --> 00:18:49,410
Jest z Budapesztu. Gapiliśmy się
na siebie jak sroka w gnat.

180
00:18:49,490 --> 00:18:51,420
Czyje to rzeczy?

181
00:18:53,480 --> 00:18:56,850
- Catherine chce ją wykończyć.
- Pomóc ci?

182
00:18:56,930 --> 00:19:02,550
Pojechałem do pralni po ubrania Josie.

183
00:19:03,060 --> 00:19:07,990
Znam po węgiersku tylko
dwa słowa: papryka i gulasz.

184
00:19:08,070 --> 00:19:14,030
Dogadywaliśmy się przez 20 minut.
Odbiorę. Weźmiesz to?

185
00:19:18,620 --> 00:19:22,780
Chwileczkę. Do Josie. Jest tutaj?

186
00:19:22,860 --> 00:19:24,440
W pokoju obok.

187
00:19:48,160 --> 00:19:54,180
Josie? Tu Thomas.
Jednak cię znalazłem.

188
00:19:55,010 --> 00:19:57,590
Może do mnie wrócisz?

189
00:20:00,400 --> 00:20:02,840
Słyszałaś o Jonathanie?

190
00:20:03,950 --> 00:20:07,090
Co za tragedia.

191
00:20:07,730 --> 00:20:10,940
Witamy w Twin Peaks, panie Eckhardt.

192
00:20:19,090 --> 00:20:21,670
Wróciła do Catherine Martell.

193
00:20:21,750 --> 00:20:23,440
Jestem zawiedziony.

194
00:20:23,920 --> 00:20:26,480
�atwo j� rozgry��.

195
00:20:27,850 --> 00:20:30,560
Mówiłam, żeby jej nie ufać.

196
00:20:32,040 --> 00:20:33,760
Spytałem Stonewall Jacksona,

197
00:20:33,840 --> 00:20:36,780
co zrobi, gdy już wygnamy
Jankesów do Kanady.

198
00:20:36,870 --> 00:20:40,890
Chciał pójść za nimi.
Będę księciem Montrealu.

199
00:20:43,720 --> 00:20:45,050
Stonewall...

200
00:20:46,150 --> 00:20:50,090
Raz go spytałem,
co zrobić z Lincolnem.

201
00:20:50,780 --> 00:20:54,420
A on na to, że stary Abe przyda się

202
00:20:54,500 --> 00:20:58,430
jako spluwaczka w domu publicznym!

203
00:21:05,920 --> 00:21:07,620
I co, doktorze?

204
00:21:08,460 --> 00:21:11,850
Podtrzymywanie iluzji
się nie sprawdziło.

205
00:21:11,940 --> 00:21:14,090
Ale przyznacie, że jest zabawny.

206
00:21:14,170 --> 00:21:19,150
Jak możesz? To mój ojciec
i twój brat. Nie zostawisz go tak.

207
00:21:20,350 --> 00:21:26,730
Audrey, zauważ i dobre strony
jego... obłędu.

208
00:21:32,340 --> 00:21:39,810
Mnie też to martwi, ale są sprawy,
którymi chciałbym się bliżej zająć.

209
00:21:39,900 --> 00:21:43,350
Jego choroba to dla mnie okazja.

210
00:21:43,980 --> 00:21:50,320
Coś ci wyjaśnię, stryjku.
W tej sytuacji to ja zarządzam firmą.

211
00:21:51,080 --> 00:21:53,470
To nieco bardziej złożone...

212
00:21:53,550 --> 00:21:56,700
Nie, czytałam jego testament.

213
00:21:56,790 --> 00:22:01,480
W razie niedyspozycji ojca
wszystko przejdzie na mnie.

214
00:22:01,560 --> 00:22:06,000
Jestem pełnoletnia, a on jest chory.

215
00:22:06,720 --> 00:22:08,580
I tak wygram...

216
00:22:09,990 --> 00:22:16,300
a ty będziesz sprzedawał kaloryfery
w miejscowej hurtowni.

217
00:22:18,960 --> 00:22:21,980
Naprawdę aż tak z nim źle?

218
00:22:22,060 --> 00:22:23,780
Bardzo źle.

219
00:22:30,200 --> 00:22:33,760
Doktorze, on musi wrócić.

220
00:22:33,840 --> 00:22:35,030
Do gabinetu?

221
00:22:36,620 --> 00:22:38,220
Do rzeczywistości.

222
00:22:40,110 --> 00:22:44,030
Chyba trzeba zastosować leki.

223
00:22:45,480 --> 00:22:48,090
Spóźniłem się, bo motocykliści
chcieli mi zabrać mundur.

224
00:22:48,180 --> 00:22:50,420
Stary Stonewall mnie opuścił,

225
00:22:50,950 --> 00:22:54,010
skoszony jankeskim ostrzałem.

226
00:22:54,090 --> 00:22:57,830
Padł samotnie, z dala ode mnie.

227
00:22:58,840 --> 00:22:59,640
Chłopcze!

228
00:23:01,160 --> 00:23:02,160
Do mnie!

229
00:23:03,470 --> 00:23:06,800
Wiecie, co chcę usłyszeć.

230
00:23:08,930 --> 00:23:11,610
Naprzód marsz!

231
00:23:45,710 --> 00:23:47,250
Pani Marsh?

232
00:23:48,400 --> 00:23:50,880
Jestem Donna, znajoma Jamesa.

233
00:23:53,820 --> 00:23:55,490
Pamiętam cię.

234
00:23:56,700 --> 00:24:00,680
Nalej mi drinka, z parasolką.

235
00:24:01,900 --> 00:24:04,000
Możesz ze mną usiąść.

236
00:24:04,080 --> 00:24:07,130
- Dlaczego pani mu to robi?
- A dlaczego nie?

237
00:24:07,210 --> 00:24:08,710
Bo to dobry człowiek.

238
00:24:10,170 --> 00:24:14,250
W dwóch sprawach.
W warsztacie i w łóżku.

239
00:24:15,120 --> 00:24:18,400
Nie rozumie pani?
Chodzi o nasze życie.

240
00:24:18,490 --> 00:24:24,670
Życie. Będzie cudowne,
jeśli wykręcisz się z głupiej wpadki.

241
00:24:24,750 --> 00:24:27,850
Może tak, a może nie.

242
00:24:28,770 --> 00:24:32,390
Pogadamy za parę lat,
gdy wpadniesz jak ja.

243
00:24:32,470 --> 00:24:35,230
Wszystko pani lekceważy?

244
00:24:35,750 --> 00:24:42,200
Gdyby było po co wygrzebywać się
z błota, zrobiłabym to.

245
00:24:42,280 --> 00:24:43,980
Czyżby?

246
00:24:44,070 --> 00:24:45,280
Tu jesteś.

247
00:24:45,820 --> 00:24:47,840
Samochód czeka, pani Marsh.

248
00:24:48,730 --> 00:24:49,870
Idziemy.

249
00:24:57,400 --> 00:25:00,670
Żal mi twojego przyjaciela,
ale nic nie poradzisz.

250
00:25:01,240 --> 00:25:06,390
Zmykaj do swojego miasteczka
i pamiętaj,

251
00:25:06,470 --> 00:25:09,200
że jak cię tu znów zobaczę...

252
00:25:10,340 --> 00:25:11,840
to zabiję.

253
00:25:14,960 --> 00:25:17,650
Nie róbcie mu tego! Proszę!

254
00:25:35,250 --> 00:25:36,420
Coop, popatrz.

255
00:25:38,170 --> 00:25:43,830
To wełna wigonia z płaszcza Josie
i próbka sprzed twojego pokoju.

256
00:25:43,910 --> 00:25:47,200
- To samo ubranie?
- Ujmę to tak:

257
00:25:47,280 --> 00:25:51,730
Albo to Josie strzelała,
albo pożycza płaszcz zbirom.

258
00:25:52,310 --> 00:25:55,700
- A rękawiczki?
- Rano będą wyniki badania na proch.

259
00:25:55,780 --> 00:25:56,980
Masz wieści z Seattle?

260
00:25:57,530 --> 00:26:00,440
Szukają pięknej Azjatki
około dwudziestki

261
00:26:00,530 --> 00:26:02,860
w związku z zabójstwem
Jonathana Kumagaia.

262
00:26:02,940 --> 00:26:08,000
Pan Kumagai ma sporą kartotekę
i trzy dziury w potylicy.

263
00:26:08,080 --> 00:26:11,500
Zakład, że kulki pasują
do tych z twojej kamizelki?

264
00:26:12,200 --> 00:26:15,580
Oby to nie Josie,
przez wzgląd na Harry'ego.

265
00:26:18,300 --> 00:26:20,640
Jak wiesz, dawniej się nie lubiliśmy,

266
00:26:20,720 --> 00:26:24,180
ale ten gość jest naprawdę w porządku

267
00:26:24,270 --> 00:26:27,380
i czuję do niego wiele sympatii.

268
00:26:27,460 --> 00:26:29,290
- O co chodzi?
- Szczerze?

269
00:26:29,370 --> 00:26:30,440
Wal.

270
00:26:31,640 --> 00:26:34,380
Szeryf ma poważny kłopot
z dziewczyną.

271
00:26:40,740 --> 00:26:41,840
Albercie.

272
00:26:42,540 --> 00:26:45,200
Ani słowa bez dowodów.

273
00:26:53,240 --> 00:26:55,490
Nasz nieznany trup ma nazwisko.

274
00:26:55,570 --> 00:26:59,440
To Erik Powell,
były marynarz floty handlowej.

275
00:26:59,520 --> 00:27:00,650
Powell?

276
00:27:01,950 --> 00:27:03,310
Coś ci to mówi?

277
00:27:04,930 --> 00:27:07,350
To panieńskie nazwisko Caroline.

278
00:27:09,610 --> 00:27:13,530
- To jakiś krewny?
- Nie.

279
00:27:13,610 --> 00:27:18,740
Earle gra według własnych zasad.
Każdy ruch to wiadomość.

280
00:27:19,260 --> 00:27:23,320
Wybór nazwiska Powell świadczy,
że nadal pamięta, co się stało.

281
00:27:23,950 --> 00:27:27,450
Więc z każdym zbitym pionkiem...

282
00:27:27,960 --> 00:27:29,540
Ktoś zginie.

283
00:27:31,880 --> 00:27:33,900
Nie wygram z nim.

284
00:27:34,290 --> 00:27:38,790
Jeśli potrzebujesz arcymistrza,
to mamy tu takiego.

285
00:27:40,830 --> 00:27:42,590
Szach i mat.

286
00:27:44,100 --> 00:27:45,480
Szach i mat.

287
00:27:48,520 --> 00:27:49,880
I mat.

288
00:27:54,950 --> 00:27:56,100
Jestem pełen podziwu.

289
00:27:57,040 --> 00:28:01,410
To zasługa wielkiego
José Raúla Capablanki.

290
00:28:01,490 --> 00:28:06,370
Jeśli w niebie grają w szachy,
to siedzi obok Boga.

291
00:28:06,450 --> 00:28:09,800
- Doktorze?
- Wygrałeś.

292
00:28:09,880 --> 00:28:12,120
Ale ci nie odpuszczę.

293
00:28:12,200 --> 00:28:13,690
Pete, pomóż mi.

294
00:28:14,790 --> 00:28:19,890
Rozgrywam bardzo ważną partię,
tyle mogę powiedzieć.

295
00:28:19,980 --> 00:28:23,630
Chciałbym ją przeciągnąć,
tracąc jak najmniej figur,

296
00:28:23,710 --> 00:28:25,910
a jeśli się da, to żadnej.

297
00:28:27,310 --> 00:28:31,010
Agencie Cooper, będę zaszczycony.

298
00:28:31,400 --> 00:28:32,690
Dziękuję.

299
00:28:33,450 --> 00:28:36,890
Dobry szachista to skarb.

300
00:28:52,210 --> 00:28:55,900
Tak mi przykro. Dobrze się czujesz?

301
00:28:57,490 --> 00:28:58,840
Nie bardzo.

302
00:29:00,140 --> 00:29:04,300
Chciałam wpaść, ale bez Hanka
mam urwanie głowy.

303
00:29:05,810 --> 00:29:09,140
Nie przydałaby ci się pomoc?

304
00:29:10,430 --> 00:29:11,800
Bo ja...

305
00:29:14,940 --> 00:29:16,180
Czuję się tu bezpieczna.

306
00:29:17,140 --> 00:29:18,710
Żartujesz?

307
00:29:19,610 --> 00:29:21,480
Bez ciebie to nie to.

308
00:29:21,800 --> 00:29:23,390
Kiedy chcesz zacząć?

309
00:29:26,980 --> 00:29:30,140
- Zaraz?
- Cudownie! Witaj!

310
00:29:30,450 --> 00:29:31,630
Dziękuję.

311
00:29:32,710 --> 00:29:34,850
Normo? Cześć, Shelly.

312
00:29:35,360 --> 00:29:38,680
Mogę cię na chwilę prosić?

313
00:29:47,090 --> 00:29:51,360
Powinnaś wiedzieć, że Hank
niedługo wyjdzie ze szpitala.

314
00:29:52,140 --> 00:29:57,340
A co z warunkowym zwolnieniem?
Nie chcę, żeby wrócił.

315
00:29:57,420 --> 00:30:01,320
Nie martw się, nie wróci.
Pójdzie do pudła.

316
00:30:01,400 --> 00:30:05,840
Do złamania warunków zwolnienia
doszła próba zabójstwa Leo.

317
00:30:06,370 --> 00:30:08,750
Długo posiedzi.

318
00:30:11,260 --> 00:30:12,740
To dobrze.

319
00:30:34,360 --> 00:30:37,430
Witam, panie Eckhardt.

320
00:30:37,510 --> 00:30:40,350
Josie, weź płaszcz naszego gościa.

321
00:30:47,530 --> 00:30:51,950
Przyznam, że mnie pani zaskoczyła.

322
00:30:52,040 --> 00:30:54,910
Wieści szybko się tu rozchodzą.

323
00:30:54,990 --> 00:31:00,130
Wypadało zaprosić rywala
mojego brata. Josie, podaj wino.

324
00:31:00,210 --> 00:31:02,820
Dawniej byliśmy przyjaciółmi.

325
00:31:02,910 --> 00:31:05,770
- To minęło.
- Niestety.

326
00:31:05,850 --> 00:31:11,360
Czasami tworzymy swój wizerunek
kosztem życia osobistego.

327
00:31:11,450 --> 00:31:14,760
Uważa się pan za artystę?
Josie, wino.

328
00:31:14,850 --> 00:31:17,990
W pewnym sensie. Jak pani brat.

329
00:31:18,930 --> 00:31:24,480
Zabił go pan dla sztuki
czy dla pieniędzy?

330
00:31:24,570 --> 00:31:26,220
Nie stój tak, nalewaj.

331
00:31:26,310 --> 00:31:28,400
Proszę mi mówić Thomas.

332
00:31:29,640 --> 00:31:31,420
Podaj przystawki.

333
00:31:31,500 --> 00:31:37,410
Sztuka i pieniądze nie są tego warte,
łatwo je zdobyć i stracić.

334
00:31:37,490 --> 00:31:43,270
Ale znalazłbym powody,
by zabić dla miłości.

335
00:31:44,040 --> 00:31:47,160
- Aż tak ją pan kochał?
- Tak.

336
00:31:48,070 --> 00:31:53,110
Josie ma przepiękne dłonie.
Każdy palec to cud.

337
00:31:53,200 --> 00:31:54,560
Pamiętam.

338
00:31:59,920 --> 00:32:05,920
Co z nią zrobimy? Szkoda byłoby,
żeby pan tu jechał na próżno.

339
00:32:07,460 --> 00:32:10,660
Oczywiście będzie mi jej brakowało.

340
00:32:11,420 --> 00:32:16,390
Naszej przyjaźni
i pogaduszek przy herbacie...

341
00:32:16,480 --> 00:32:19,950
Radziłbym jakiegoś zwierzaka.
Kota albo psa.

342
00:32:20,340 --> 00:32:23,410
Jeśli ją panu oddam,
co dostanę w zamian?

343
00:32:23,740 --> 00:32:26,910
Jak wycenić taki skarb?

344
00:32:30,570 --> 00:32:33,600
Ale można spróbować?

345
00:32:35,150 --> 00:32:36,360
Tak.

346
00:32:38,640 --> 00:32:43,290
Josie, podaj danie główne.

347
00:33:15,550 --> 00:33:16,860
Dlaczego to zrobiłaś?

348
00:33:22,470 --> 00:33:25,940
Zastanawiam się,
ale wciąż nie rozumiem.

349
00:33:26,020 --> 00:33:27,940
Nie jesteś tu bezpieczny.

350
00:33:28,020 --> 00:33:31,060
Chodziło o kasę?

351
00:33:31,140 --> 00:33:36,880
Nie, już wiem.
To Malcolm cię zmusił, tak?

352
00:33:37,670 --> 00:33:41,470
Powiedz mi coś, bo nic już nie wiem.

353
00:33:41,550 --> 00:33:44,580
- Dlaczego mi to zrobiłaś?
- James...

354
00:33:46,620 --> 00:33:50,220
Spodziewasz się przeprosin?

355
00:33:50,300 --> 00:33:55,280
Przebłysku szczerości?
Tak, zrobiłam to wszystko.

356
00:33:55,370 --> 00:33:59,530
Zwabiłam cię tu i zatrzymałam,

357
00:33:59,620 --> 00:34:03,170
a Malcolm obmyślił resztę.

358
00:34:04,730 --> 00:34:06,820
- Dlaczego?
- Dla pieniędzy...

359
00:34:07,880 --> 00:34:12,030
ze strachu, bo tego chciałam.

360
00:34:12,830 --> 00:34:15,680
Nie to chciałeś usłyszeć?

361
00:34:16,690 --> 00:34:18,740
Prawda boli, prawda?

362
00:34:21,150 --> 00:34:25,380
Jesteś dobry i uczciwy.

363
00:34:26,250 --> 00:34:27,680
Ja nie...

364
00:34:28,520 --> 00:34:32,310
ale to nie znaczy,
że cię tutaj nie chciałam.

365
00:34:33,950 --> 00:34:37,870
Nie dla Malcolma ani forsy,

366
00:34:37,960 --> 00:34:39,390
tylko dla mnie.

367
00:34:40,330 --> 00:34:43,690
Zasmakowałam w twojej szczerości.

368
00:34:46,970 --> 00:34:49,380
A ja w tobie...

369
00:34:54,030 --> 00:34:57,840
Co ty robisz? Zostaw go!

370
00:35:03,730 --> 00:35:05,520
Włamał się.

371
00:35:06,430 --> 00:35:09,540
Był wściekły, wariował.

372
00:35:09,620 --> 00:35:13,900
Zabił Jeffreya i wrócił po ciebie...

373
00:35:15,050 --> 00:35:17,440
biedną, bezbronną wdowę.

374
00:35:18,480 --> 00:35:20,650
Ale byłaś przygotowana.

375
00:35:20,730 --> 00:35:22,330
Miałaś broń...

376
00:35:22,910 --> 00:35:24,860
i zabiłaś Jamesa.

377
00:35:25,740 --> 00:35:27,840
Strzelałaś do skutku.

378
00:36:02,570 --> 00:36:03,590
Generale Lee!

379
00:36:10,340 --> 00:36:13,340
Witamy w naszych skromnych progach.

380
00:36:13,420 --> 00:36:15,270
Dziękuję, panno...

381
00:36:15,350 --> 00:36:18,810
Scarlet McLean. A to jest mój ojciec.

382
00:36:19,220 --> 00:36:21,400
Mój ojciec!

383
00:36:23,310 --> 00:36:25,840
Wilmer McLean, do usług.

384
00:36:26,450 --> 00:36:29,800
Jestem zaszczycony,
mogąc pana powitać.

385
00:36:29,890 --> 00:36:32,990
Dlaczego zabrał mi pan szpadę?

386
00:36:33,840 --> 00:36:35,740
Bardzo mi się podoba.

387
00:36:36,270 --> 00:36:39,660
- Dziękuję, papo.
- Gdzie generał Grant?

388
00:36:39,880 --> 00:36:42,550
Grant przybędzie niezwłocznie.

389
00:36:55,590 --> 00:36:58,330
- Generał Grant!
- Generale Lee?

390
00:36:58,750 --> 00:37:02,290
Spotkaliśmy się podczas
wojny meksykańskiej?

391
00:37:02,370 --> 00:37:04,610
Kapituluję w imieniu Północy.

392
00:37:04,690 --> 00:37:10,100
- Pamięta pan wojnę z Meksykiem?
- Tak, tak. To była niezła wojna.

393
00:37:10,190 --> 00:37:17,150
Nonsens. Każda wojna to szaleństwo
przedstawiane jako akt patriotyzmu

394
00:37:17,230 --> 00:37:20,710
przez polityków za starych
i zbyt tchórzliwych,

395
00:37:20,800 --> 00:37:24,540
by ruszyć w pole.
Ja za tym nie przepadam.

396
00:37:24,630 --> 00:37:27,580
- Ja też.
- A za polityką jeszcze mniej.

397
00:37:28,960 --> 00:37:36,220
Za to szanuję tych, którzy walczą.

398
00:37:37,950 --> 00:37:41,720
No dobrze, dość tych pogaduszek.

399
00:37:41,810 --> 00:37:43,550
Oto warunki...

400
00:37:44,290 --> 00:37:50,150
Oficerowie mogą zatrzymać
broń i rzeczy osobiste.

401
00:37:50,350 --> 00:37:54,490
Posiadający konie także je zatrzymają.

402
00:37:54,580 --> 00:37:59,580
Wszyscy mogą wrócić do domów

403
00:37:59,660 --> 00:38:03,060
i nie będą nękani
przez władze Konfederacji.

404
00:38:03,850 --> 00:38:06,580
- Poddaję się.
- To oczywiste.

405
00:38:07,340 --> 00:38:08,550
Akt kapitulacji!

406
00:38:11,450 --> 00:38:16,410
Tak jest, oto on.

407
00:38:18,160 --> 00:38:19,430
Proszę o podpis.

408
00:38:19,510 --> 00:38:25,700
Tak. Dodam tylko,
że był pan godnym przeciwnikiem.

409
00:38:25,790 --> 00:38:28,090
Myślałem, że to Północ wygrała.

410
00:38:48,630 --> 00:38:50,130
Ben?

411
00:38:50,540 --> 00:38:51,460
Tato!

412
00:38:57,690 --> 00:38:59,260
Odsuńcie się.

413
00:38:59,470 --> 00:39:02,210
Gdzie ja jestem?

414
00:39:02,290 --> 00:39:04,030
W Twin Peaks.

415
00:39:07,730 --> 00:39:10,800
Miałem dziwaczny sen.

416
00:39:13,390 --> 00:39:15,700
Byłeś w nim.

417
00:39:15,940 --> 00:39:19,280
I wy też.

418
00:39:19,990 --> 00:39:21,500
To było niesamowite.

419
00:39:22,190 --> 00:39:23,650
Trwała wojna...

420
00:39:24,630 --> 00:39:29,770
a ja byłem generałem Robertem Lee.

421
00:39:31,600 --> 00:39:35,670
I jakimś cudem, mimo wszystko...

422
00:39:37,330 --> 00:39:38,270
wygrałem.

423
00:39:39,010 --> 00:39:40,390
Ale wróciłeś.

424
00:39:40,840 --> 00:39:41,990
Tak.

425
00:39:45,140 --> 00:39:46,860
- Wróciłem.
- Brawo, Ben!

426
00:39:46,940 --> 00:39:50,560
Jak samopoczucie?
Czuje pan zawroty głowy?

427
00:39:52,220 --> 00:39:54,110
Czuję się świetnie.

428
00:39:56,660 --> 00:39:58,360
Jakbym...

429
00:39:59,130 --> 00:40:02,530
- Co, tato?
- Po co się poprzebieraliście?

430
00:40:08,580 --> 00:40:09,870
Nareszcie!

431
00:40:10,550 --> 00:40:12,250
Co to za żarty?

432
00:40:23,360 --> 00:40:25,020
Pisz powoli...

433
00:40:25,950 --> 00:40:27,370
i starannie.

434
00:40:28,480 --> 00:40:34,000
To musi być skreślone pewną ręką.

435
00:40:38,340 --> 00:40:41,530
Nie, to okropne.

436
00:40:42,070 --> 00:40:43,680
Wymaż to.

437
00:40:44,860 --> 00:40:45,790
Wymaż.

438
00:40:52,810 --> 00:40:54,540
I spróbuj od nowa.

439
00:40:58,320 --> 00:40:59,710
Skup się!

440
00:41:02,610 --> 00:41:03,760
Leo, na miłość boską!

441
00:41:09,000 --> 00:41:11,050
Pokażę ci.

442
00:41:11,140 --> 00:41:12,810
W ten sposób.

443
00:41:13,670 --> 00:41:18,190
No właśnie, o wiele lepiej.

444
00:41:19,850 --> 00:41:22,950
Jestem z ciebie dumny.

445
00:41:34,730 --> 00:41:39,160
- Windom Earle...
- Tak...

446
00:41:40,390 --> 00:41:41,660
Windom.

447
00:41:44,790 --> 00:41:51,390
Piękne słowa dla pięknych dziewcząt.
Która będzie moją królową?

448
00:41:52,400 --> 00:41:53,580
Królowa?

449
00:41:58,210 --> 00:42:00,900
Nie...

450
00:42:04,920 --> 00:42:05,830
Poliż.

451
00:42:19,100 --> 00:42:21,500
Będzie prawdziwiej
z twoimi odciskami palców.

452
00:42:21,580 --> 00:42:22,750
Nie mogę.

453
00:42:22,830 --> 00:42:25,440
Wystarczą odciski,
a resztę zostaw mnie.

454
00:42:25,520 --> 00:42:27,970
Postaram się odtworzyć
histeryczną reakcję.

455
00:42:28,060 --> 00:42:32,150
Pięć strzałów w jeden punkt.

456
00:42:33,170 --> 00:42:34,980
- Raz po raz.
- Nie!

457
00:42:40,120 --> 00:42:43,630
- Nie róbcie mu krzywdy!
- Daj spluwę.

458
00:42:43,710 --> 00:42:48,090
- Policja już tu jedzie, to na nic!
- Kłamie. Dawaj!

459
00:42:48,170 --> 00:42:51,520
Evelyn, proszę, nie daj mu umrzeć!

460
00:42:56,030 --> 00:42:57,760
Daj broń.

461
00:43:06,880 --> 00:43:08,970
Nie potrafię.

462
00:43:09,590 --> 00:43:11,000
Sam to zrobię.

463
00:43:36,310 --> 00:43:40,220
Włamał się, był wściekły i wariował.

464
00:43:40,300 --> 00:43:44,840
Zabił Jeffreya i wrócił po mnie.

465
00:43:45,100 --> 00:43:46,840
Ale byłam przygotowana.

466
00:43:48,230 --> 00:43:51,750
Miałam broń i zabiłam go.

467
00:43:52,350 --> 00:43:55,450
Strzelałam do skutku.

468
00:44:24,510 --> 00:44:25,780
Caroline...

469
00:44:35,640 --> 00:44:38,190
Dla panny Audrey Horne.

470
00:44:39,680 --> 00:44:40,880
Sowy?

471
00:45:24,710 --> 00:45:26,980
Była cudowna, prawda?

472
00:45:27,070 --> 00:45:30,200
Przepiękna Caroline.

473
00:45:30,750 --> 00:45:33,820
Ciekawa sprawa. Po tylu latach

474
00:45:34,580 --> 00:45:37,290
i po tym, co się stało w Pittsburghu,

475
00:45:38,200 --> 00:45:40,550
wciąż ją kocham.

476
00:45:41,240 --> 00:45:43,210
Wiem, że ty też.

477
00:45:44,450 --> 00:45:47,790
Słuchaj uważnie, Dale.

478
00:45:48,690 --> 00:45:50,890
Czas na twój ruch.

