1
00:00:28,840 --> 00:00:33,590
MIASTECZKO TWIN PEAKS

2
00:01:38,300 --> 00:01:41,280
Załatwione. Czekają na nas w hotelu.

3
00:01:41,370 --> 00:01:47,700
Wielki dom z drewna, otoczony lasem.

4
00:01:47,790 --> 00:01:52,420
Jest tam wiele
podobnych do siebie pokoi,

5
00:01:52,510 --> 00:01:55,650
ale zamieszkanych przez różne dusze,

6
00:01:55,740 --> 00:01:58,640
noc po nocy.

7
00:02:00,130 --> 00:02:02,360
Wszyscy będą w holu?

8
00:02:02,450 --> 00:02:04,550
Bez wyjątku.

9
00:02:04,630 --> 00:02:08,400
Hawk, masz nakaz
przeszukania domu Smitha?

10
00:02:08,490 --> 00:02:11,460
Dopiję kawę i jadę.

11
00:02:11,550 --> 00:02:14,170
Gordon mówił...

12
00:02:14,250 --> 00:02:19,850
Mówiłem, że kartki znalezione
z zakrwawionym ręcznikiem przy torach

13
00:02:19,930 --> 00:02:22,380
pochodziły z pamiętnika.

14
00:02:22,470 --> 00:02:28,770
Donna mogła mówić prawdę,
twierdząc, że Smith ma pamiętnik Laury.

15
00:02:28,860 --> 00:02:30,790
Daj znać, co znalazłeś.

16
00:02:32,190 --> 00:02:35,920
Gordon, byłeś w drodze do Oregonu.

17
00:02:36,000 --> 00:02:40,730
Jestem w drodze do Oregonu.
Służbowo i w tajemnicy.

18
00:02:40,810 --> 00:02:42,940
Powodzenia.

19
00:02:51,240 --> 00:02:53,390
Pilnujcie Mike'a.

20
00:02:54,570 --> 00:02:58,860
Było mi bardzo miło. Szeryfie.

21
00:02:58,950 --> 00:03:01,720
- Szerokiej drogi.
- Dzięki.

22
00:03:17,310 --> 00:03:18,650
Nie.

23
00:03:20,630 --> 00:03:22,240
Nie.

24
00:03:27,400 --> 00:03:28,900
Nie.

25
00:03:38,850 --> 00:03:40,410
Nie.

26
00:03:47,600 --> 00:03:49,040
Nie.

27
00:03:53,140 --> 00:03:54,810
Nie.

28
00:04:17,310 --> 00:04:21,480
Co tu się dzieje?
Co to za cyrk, u diabła?

29
00:04:26,110 --> 00:04:29,740
Czy ktoś mi powie, co to ma znaczyć?

30
00:04:59,250 --> 00:05:00,870
Panie Smith?

31
00:05:02,260 --> 00:05:04,160
Haroldzie Smith?

32
00:05:34,860 --> 00:05:40,000
Jaki piękny mógłby być świat,

33
00:05:40,090 --> 00:05:44,050
gdybyśmy tylko dali mu szansę.

34
00:05:44,760 --> 00:05:46,830
Miłość, tylko miłość.

35
00:05:46,910 --> 00:05:49,700
- Dzień dobry, ciociu.
- Dzień dobry.

36
00:05:52,980 --> 00:05:59,950
Gdybyśmy się bardziej kochali,
byłoby mniej problemów.

37
00:06:00,040 --> 00:06:02,510
A wtedy...

38
00:06:02,590 --> 00:06:07,470
świat byłby cudowny.

39
00:06:07,550 --> 00:06:10,880
Tak mówi dobry Bóg.

40
00:06:21,000 --> 00:06:22,830
Wujku Lelandzie...

41
00:06:23,730 --> 00:06:25,360
ciociu Saro...

42
00:06:26,850 --> 00:06:30,520
Siadaj, napijmy się kawy.

43
00:06:31,990 --> 00:06:36,950
- Dobrze spałaś?
- Tak, dziękuję.

44
00:06:40,470 --> 00:06:42,350
Tak sobie myślałam...

45
00:06:43,110 --> 00:06:46,430
Jest mi z wami tu bardzo dobrze...

46
00:06:48,420 --> 00:06:53,070
ale powinnam wracać do domu.

47
00:06:57,650 --> 00:07:03,500
Do Missoula.
Mam tam pracę i mieszkanie.

48
00:07:03,590 --> 00:07:05,530
Po prostu...

49
00:07:07,410 --> 00:07:10,310
tęsknię do własnego życia.

50
00:07:14,340 --> 00:07:18,040
Więc chyba jutro wyjadę.

51
00:07:18,120 --> 00:07:19,630
Jutro?

52
00:07:21,050 --> 00:07:24,140
Będzie nam ciebie brakowało...

53
00:07:25,220 --> 00:07:27,740
ale rozumiemy cię.

54
00:07:27,820 --> 00:07:31,020
- Tak?
- Oczywiście.

55
00:07:34,850 --> 00:07:38,270
Bardzo nam pomogłaś.

56
00:07:38,360 --> 00:07:42,050
Tak, ale teraz musi pomyśleć o sobie.

57
00:07:42,130 --> 00:07:44,040
Ma swoje sprawy.

58
00:07:44,810 --> 00:07:47,100
I przecież będziesz nas odwiedzać.

59
00:07:47,190 --> 00:07:49,270
Pewnie!

60
00:07:49,350 --> 00:07:52,870
Gdy tylko będę mogła.

61
00:07:52,950 --> 00:07:55,860
Przecież Missoula
to nie drugi koniec świata.

62
00:07:55,940 --> 00:07:57,420
Prawie.

63
00:07:57,500 --> 00:07:59,240
Dobrze.

64
00:07:59,890 --> 00:08:01,560
Trudno.

65
00:08:04,810 --> 00:08:08,010
Ale wiedz, że bardzo cię kochamy.

66
00:08:09,560 --> 00:08:11,740
Ja was też.

67
00:08:32,630 --> 00:08:35,340
- A to co? List?
- Tak.

68
00:08:45,890 --> 00:08:48,150
"Jestem samotną duszą".

69
00:08:48,920 --> 00:08:50,510
Biedak.

70
00:08:52,140 --> 00:08:55,340
- Dobrze, że Andy go nie widział.
- Fakt.

71
00:08:59,630 --> 00:09:00,340
Popatrzcie.

72
00:09:10,240 --> 00:09:14,850
- "To jest pamiętnik Laury Palmer".
- No proszę.

73
00:09:38,720 --> 00:09:42,880
Rachunki są w sumie na 1014 dolarów.

74
00:09:42,970 --> 00:09:47,720
Z forsą z polisy daje ci to...

75
00:09:50,230 --> 00:09:52,140
42 dolary.

76
00:09:53,590 --> 00:09:54,980
42 dolary!

77
00:09:55,830 --> 00:09:57,110
Na początek.

78
00:09:57,910 --> 00:10:00,370
42 dolce na miesiąc!

79
00:10:01,600 --> 00:10:05,910
Jak ja sobie poradzę? Jak wyżyjemy
za 42 dolary miesięcznie?

80
00:10:06,790 --> 00:10:08,050
My?

81
00:10:08,130 --> 00:10:11,960
Nie mogę wciąż kłamać,
że nocuję u Mike'a.

82
00:10:12,050 --> 00:10:14,270
Która godzina? Spóźnię się na ekonomię.

83
00:10:14,360 --> 00:10:17,370
Mówię o sobie i Leo.

84
00:10:17,940 --> 00:10:20,630
Obiecałeś, że się nami zajmiesz.

85
00:10:21,360 --> 00:10:24,130
I zrobię to.

86
00:10:25,930 --> 00:10:28,080
Zabierz naszyjnik.

87
00:10:30,930 --> 00:10:33,420
Nie musisz go sprzedawać.

88
00:10:35,680 --> 00:10:38,910
Weź go, potrzebujemy pieniędzy.

89
00:10:39,000 --> 00:10:41,860
Zatrzymasz ten naszyjnik.

90
00:10:42,550 --> 00:10:45,420
Niby kiedy mam się stroić?

91
00:10:45,510 --> 00:10:48,990
Kąpiąc Leo? Karmiąc go owsianką?

92
00:10:52,670 --> 00:10:57,080
A jego ciężarówka? Raczej
nieprędko wróci za kierownicę.

93
00:10:57,170 --> 00:10:59,210
Zarekwirowali ją.

94
00:10:59,290 --> 00:11:01,210
To ją odbierzemy.

95
00:11:01,300 --> 00:11:05,750
Mówią, że jest narzędziem przestępstwa
i przejmują ją władze.

96
00:11:06,460 --> 00:11:09,970
- O nic go nie oskarżyli.
- Nie ja to wymyśliłam!

97
00:11:12,540 --> 00:11:18,270
- Jakbyś mnie o coś winił.
- Nic podobnego, uspokój się.

98
00:11:20,080 --> 00:11:23,970
Leo robił interesy z różnymi ludźmi.

99
00:11:25,750 --> 00:11:27,490
I zarabiał na tym.

100
00:11:27,710 --> 00:11:31,660
Rozejrzyj się.
Naprawdę myślisz, że ma forsę?

101
00:11:32,380 --> 00:11:35,150
Tak. A sądząc po wyglądzie
książeczki czekowej,

102
00:11:35,230 --> 00:11:38,590
nie chadzał do banku.

103
00:11:38,680 --> 00:11:40,940
Musiał to gdzieś schować.

104
00:11:46,360 --> 00:11:47,670
On żyje!

105
00:11:49,760 --> 00:11:51,350
Nowe buty.

106
00:11:53,540 --> 00:11:55,740
Bobby, on żyje!

107
00:12:13,910 --> 00:12:15,550
Nie, Shelly.

108
00:12:16,230 --> 00:12:17,840
Ale mówi.

109
00:12:18,490 --> 00:12:21,210
Beknął. Nałykał się powietrza.

110
00:12:22,650 --> 00:12:24,210
To jest bekanie?

111
00:12:26,080 --> 00:12:27,700
Nowe buty.

112
00:12:33,520 --> 00:12:37,050
Kupił ostatnio nowe buty?

113
00:12:38,170 --> 00:12:39,620
Nie.

114
00:12:41,140 --> 00:12:43,020
Kazał mi oddać stare do reperacji.

115
00:12:44,210 --> 00:12:46,650
- Kiedy?
- W zeszłym tygodniu.

116
00:12:47,430 --> 00:12:50,330
- Masz kwit?
- Tak, a co?

117
00:12:50,420 --> 00:12:53,410
Nie wiem, ale go przynieś.

118
00:12:58,590 --> 00:13:00,350
Nowe buty.

119
00:13:02,670 --> 00:13:04,790
Chcesz nowe buty?

120
00:13:05,510 --> 00:13:09,470
Daj nam, czego chcemy,
to kupię ci cały sklep.

121
00:13:15,910 --> 00:13:17,600
Nowe buty.

122
00:13:22,500 --> 00:13:26,060
Tato, wiem o Jednookim Jacku.

123
00:13:29,130 --> 00:13:32,610
Nie mam pojęcia, o czym mówisz.

124
00:13:33,080 --> 00:13:37,910
Wiem o Blackie i o Battisie.
Wiem o Ronette

125
00:13:37,990 --> 00:13:39,740
i o Laurze.

126
00:13:46,040 --> 00:13:48,840
Byłam tam i widziałam cię.

127
00:13:53,250 --> 00:13:55,430
Pamiętasz tę wstydliwą?

128
00:13:56,340 --> 00:13:59,240
Miałam białą maskę.

129
00:14:09,010 --> 00:14:12,060
Chcę cię o coś spytać.

130
00:14:13,610 --> 00:14:15,510
Pytaj.

131
00:14:16,420 --> 00:14:19,450
Jak długo jesteś właścicielem?

132
00:14:23,540 --> 00:14:25,890
Pięć lat.

133
00:14:27,900 --> 00:14:30,880
Wiedziałeś, że Laura tam pracowała?

134
00:14:30,970 --> 00:14:33,100
Była tam bardzo krótko.

135
00:14:33,190 --> 00:14:35,380
- Wiedziałeś?
- Tak.

136
00:14:36,150 --> 00:14:40,190
- Zachęcałeś ją, żeby to zrobiła?
- Nie.

137
00:14:40,280 --> 00:14:42,780
Nie wiedziałem.

138
00:14:42,870 --> 00:14:45,340
Prosiła o posadę w sklepie.

139
00:14:45,420 --> 00:14:47,900
Battis ją wysłał bez mojej wiedzy.

140
00:14:51,980 --> 00:14:54,030
Spałeś z nią?

141
00:15:05,720 --> 00:15:07,460
Spałeś?

142
00:15:15,510 --> 00:15:17,130
Tak.

143
00:15:29,330 --> 00:15:31,230
Zabiłeś ją?

144
00:15:43,620 --> 00:15:44,740
Ja ją kochałem.

145
00:16:10,520 --> 00:16:13,690
Mogę cię na chwilę prosić?

146
00:16:14,690 --> 00:16:17,350
O co chodzi?

147
00:16:20,610 --> 00:16:23,340
Teraz, kiedy Leo jest w domu,

148
00:16:24,230 --> 00:16:26,770
powinnam się nim opiekować...

149
00:16:28,160 --> 00:16:31,350
i to raczej na pełny etat.

150
00:16:33,860 --> 00:16:36,010
Więc...

151
00:16:36,100 --> 00:16:38,090
pomyślałam,

152
00:16:38,470 --> 00:16:41,030
że muszę odejść z pracy...

153
00:16:42,780 --> 00:16:45,430
W każdym razie, na trochę.

154
00:16:46,670 --> 00:16:48,530
Przepraszam.

155
00:16:50,710 --> 00:16:53,890
A obiecałam sobie nie beczeć.

156
00:16:57,740 --> 00:16:59,830
W porządku.

157
00:17:02,490 --> 00:17:04,760
Tak mi przykro.

158
00:17:06,470 --> 00:17:08,300
Uwielbiam cię,

159
00:17:08,380 --> 00:17:11,040
kocham tę pracę...

160
00:17:11,700 --> 00:17:14,330
i nie chcę cię zawieść.

161
00:17:15,640 --> 00:17:18,460
Nie zawiedziesz mnie.

162
00:17:19,830 --> 00:17:21,090
Nie?

163
00:17:22,530 --> 00:17:24,140
Posłuchaj.

164
00:17:24,230 --> 00:17:27,510
Masz teraz co innego na głowie.

165
00:17:27,590 --> 00:17:29,220
Ja sobie poradzę.

166
00:17:29,710 --> 00:17:31,020
Na pewno?

167
00:17:31,600 --> 00:17:33,270
Tak.

168
00:17:34,250 --> 00:17:36,630
Trzymaj się.

169
00:17:37,480 --> 00:17:43,070
A gdy będziesz mogła,
od razu do nas wracaj.

170
00:17:45,150 --> 00:17:47,610
Przyjmiesz mnie?

171
00:17:47,690 --> 00:17:51,660
Tylko spróbuj poszukać
innej roboty, to pożałujesz.

172
00:17:57,310 --> 00:17:59,600
Nie wiem, co powiedzieć.

173
00:18:02,120 --> 00:18:04,060
Nie mów nic.

174
00:18:18,310 --> 00:18:20,840
Powiedziałam, że byłam
z rodzicami w podróży,

175
00:18:20,930 --> 00:18:24,180
ale chcę wrócić na wiosenny semestr.

176
00:18:24,850 --> 00:18:26,980
Siądźmy przy barze.

177
00:18:27,710 --> 00:18:31,240
Mam chęć na czekoladowego szejka, a ty?

178
00:18:31,930 --> 00:18:34,290
Cześć, Normo.

179
00:18:35,820 --> 00:18:38,500
Długo tu pracujesz?

180
00:18:40,360 --> 00:18:42,170
W kwietniu minie 20 lat.

181
00:18:45,390 --> 00:18:47,470
Żartujesz.

182
00:18:48,240 --> 00:18:49,970
Ale dowcip!

183
00:18:50,920 --> 00:18:55,350
Jakieś półtora miesiąca, prawda?

184
00:18:56,630 --> 00:18:58,730
Półtora miesiąca.

185
00:18:59,660 --> 00:19:02,270
Tak, właśnie.

186
00:19:02,360 --> 00:19:04,160
Widzisz?

187
00:19:05,010 --> 00:19:08,580
Wystarczy, że starzy
wyciągną cię na miesiąc do Europy,

188
00:19:08,670 --> 00:19:12,490
a gdy wracasz, to jakby minęły wieki.

189
00:19:15,480 --> 00:19:17,870
Dwa czekoladowe szejki.

190
00:19:18,910 --> 00:19:20,320
Dla mnie kawa.

191
00:19:20,400 --> 00:19:23,420
Z podwójną bitą śmietaną.

192
00:19:23,510 --> 00:19:24,510
Cześć.

193
00:19:25,690 --> 00:19:28,100
Jak masz na imię?

194
00:19:28,190 --> 00:19:29,770
Shelly.

195
00:19:31,310 --> 00:19:34,020
Jesteś w naszej klasie?

196
00:19:35,810 --> 00:19:38,390
Chyba nie...

197
00:19:43,240 --> 00:19:45,290
Przyniosę szejka i kawę.

198
00:19:50,720 --> 00:19:51,780
Do Europy?

199
00:19:52,510 --> 00:19:54,650
Tak.

200
00:19:54,740 --> 00:19:59,390
Starym tak się spodobało,
że jeszcze tam zostali.

201
00:19:59,470 --> 00:20:04,010
Pozwolili mi pomieszkać z Edem.

202
00:20:04,750 --> 00:20:08,400
To wasz dom czy nasz?

203
00:20:08,490 --> 00:20:09,630
Zgadza się.

204
00:20:12,250 --> 00:20:16,060
Norma, nie masz nam tego za złe?

205
00:20:16,470 --> 00:20:19,230
Bo my właściwie ze sobą chodzimy.

206
00:20:21,670 --> 00:20:26,060
Ty i Ed? Nie, dlaczego?

207
00:20:26,150 --> 00:20:30,190
Nie wiem. Ed mówił, że zerwaliście.

208
00:20:31,990 --> 00:20:37,650
Strasznie mnie wzięło.

209
00:20:41,710 --> 00:20:44,760
Jest cudowny.

210
00:20:47,370 --> 00:20:51,300
Nie mogę się doczekać
sezonu futbolowego.

211
00:20:51,800 --> 00:20:53,420
A ty?

212
00:20:53,500 --> 00:20:56,160
Ja też.

213
00:20:56,240 --> 00:20:58,590
Ani ja.

214
00:20:58,680 --> 00:21:02,020
Jestem taka szczęśliwa!

215
00:21:03,500 --> 00:21:05,660
- Przepraszam.
- Jak to zrobiłaś?

216
00:21:05,740 --> 00:21:08,530
Znów to samo.

217
00:21:10,280 --> 00:21:12,950
Szkło musiało być pęknięte.

218
00:21:13,030 --> 00:21:15,140
Na pewno nie.

219
00:21:15,220 --> 00:21:18,080
Wszystko leci mi z rąk.

220
00:21:18,160 --> 00:21:20,770
Aż trudno uwierzyć.

221
00:21:21,790 --> 00:21:24,380
Shelly, przynieś plaster. Zetrę to.

222
00:21:29,550 --> 00:21:31,340
Eddie...

223
00:21:32,560 --> 00:21:36,220
tak mi dobrze.

224
00:21:36,300 --> 00:21:42,320
Zacałowałabym cię na śmierć!

225
00:21:58,040 --> 00:22:00,520
Rany, Nadine...

226
00:22:14,820 --> 00:22:18,080
Może jej nie ma. Shelly?

227
00:22:25,440 --> 00:22:29,820
Oto świętej pamięci Leo Johnson.

228
00:22:42,810 --> 00:22:44,730
Hank go załatwił?

229
00:22:44,820 --> 00:22:48,250
Rąbnął go przez to okno.

230
00:22:50,320 --> 00:22:51,380
Leo...

231
00:22:52,300 --> 00:22:53,460
byłem u szewca,

232
00:22:54,840 --> 00:22:58,050
pokazałem kwit i patrz, co mi dali.

233
00:23:00,020 --> 00:23:01,600
Nowe buty.

234
00:23:02,440 --> 00:23:05,610
Nie, matole. Stare.

235
00:23:05,700 --> 00:23:07,970
Mózg mu się zwarzył.

236
00:23:08,050 --> 00:23:10,440
Oby na dobre.

237
00:23:11,770 --> 00:23:14,610
Za dobrze cię znam, Leo.

238
00:23:14,700 --> 00:23:18,260
Te buty są ważne, tak?

239
00:23:19,330 --> 00:23:21,690
Mają jakieś znaczenie.

240
00:23:26,340 --> 00:23:30,200
Słyszałem, że ludzie
trzymają w butach różne rzeczy.

241
00:23:30,290 --> 00:23:34,200
- Myślisz, że nie sprawdziłem?
- Spróbuj młotkiem.

242
00:23:35,930 --> 00:23:37,850
Leo, nie odchodź.

243
00:23:55,730 --> 00:23:57,320
Boże!

244
00:24:10,350 --> 00:24:11,080
Co to?

245
00:24:14,800 --> 00:24:16,620
Nie pluj.

246
00:24:20,350 --> 00:24:22,170
To nie forsa.

247
00:24:24,620 --> 00:24:26,500
Ale kto wie...

248
00:24:42,300 --> 00:24:47,340
Jest 14:47, siedzę
w sali konferencyjnej

249
00:24:47,430 --> 00:24:50,480
nad szczątkami pamiętnika Laury Palmer.

250
00:24:50,560 --> 00:24:52,790
Większość została zniszczona.

251
00:24:52,880 --> 00:24:57,200
To, co zdołałem odczytać,
potwierdza słowa jednorękiego.

252
00:24:57,290 --> 00:25:00,610
Jest tu wiele odniesień do "Boba".

253
00:25:00,700 --> 00:25:04,840
Prześladował ją od lat.

254
00:25:04,930 --> 00:25:09,580
Sporo wskazuje na częste
przypadki przemocy i molestowania.

255
00:25:09,660 --> 00:25:13,610
Kilka razy określa go

256
00:25:13,690 --> 00:25:16,320
jako "przyjaciela ojca".

257
00:25:16,410 --> 00:25:21,300
Znalazłem ustęp datowany
na dwa tygodnie przed jej śmiercią.

258
00:25:21,390 --> 00:25:25,540
"Pewnego dnia powiem
wszystkim o Benie Hornie.

259
00:25:25,620 --> 00:25:29,250
Powiem im, kim jest naprawdę".

260
00:25:32,250 --> 00:25:33,000
Proszę.

261
00:25:37,040 --> 00:25:38,420
Audrey?

262
00:25:38,510 --> 00:25:40,620
Przeszkadzam, ale musiałam przyjść.

263
00:25:40,700 --> 00:25:42,450
Jak się czujesz?

264
00:25:42,530 --> 00:25:44,620
Rozmawiałam z ojcem.

265
00:25:46,390 --> 00:25:49,210
Sypiał z nią.

266
00:25:49,290 --> 00:25:51,360
Z Laurą.

267
00:25:51,440 --> 00:25:54,480
Nie wiem jak długo, ale chyba od dawna.

268
00:25:55,250 --> 00:25:57,240
Rany...

269
00:25:57,330 --> 00:26:00,400
Pracowała w Jednookim Jacku,

270
00:26:00,490 --> 00:26:03,400
a on jest właścicielem. Tyle wiem.

271
00:26:03,490 --> 00:26:06,640
- Potwierdził to?
- Tak.

272
00:26:08,280 --> 00:26:10,100
Co zrobicie?

273
00:26:13,800 --> 00:26:15,960
Aresztujecie go?

274
00:26:17,820 --> 00:26:21,060
Nikomu o tym nie mów.

275
00:26:24,220 --> 00:26:26,440
Idź do domu.

276
00:26:35,370 --> 00:26:37,080
Co jest?

277
00:26:37,160 --> 00:26:39,900
"Wskazuje bez chemikaliów".

278
00:26:40,460 --> 00:26:41,830
Dziś rano w hotelu

279
00:26:41,920 --> 00:26:47,590
Mike sięgnął do kikuta ręki
i zemdlał, gdy weszła pewna osoba.

280
00:26:49,280 --> 00:26:55,300
Potrzebny nam nakaz
zatrzymania Benjamina Horne'a.

281
00:27:06,680 --> 00:27:11,700
Mam dobre wieści. Mój brat Jerry
rozmawiał w Osace z pana ludźmi.

282
00:27:11,790 --> 00:27:13,970
Sprawdził też pańską sytuację
w tokijskim banku

283
00:27:14,360 --> 00:27:20,020
i nasza odpowiedź na waszą ofertę,
przyjacielu, jest pozytywna.

284
00:27:20,800 --> 00:27:24,550
Cenimy takie zaangażowanie.

285
00:27:24,630 --> 00:27:30,180
Jeśli chodzi o naszą społeczność,
to nie umiem wyrazić ich radości

286
00:27:30,270 --> 00:27:33,540
z waszego udziału
w projekcie Ghostwood.

287
00:27:34,030 --> 00:27:36,340
Dziękuję.

288
00:27:36,420 --> 00:27:39,170
Proszę przejrzeć kontrakt.

289
00:27:39,250 --> 00:27:41,460
I szybko podpisać...

290
00:27:43,390 --> 00:27:44,900
Szeryfie?

291
00:27:48,260 --> 00:27:50,530
- O co chodzi?
- Przyszliśmy służbowo.

292
00:27:50,610 --> 00:27:52,050
Pójdzie pan z nami.

293
00:27:54,640 --> 00:27:59,230
Przypadkiem mam spotkanie.
Może pan to wyjaśnić?

294
00:28:00,440 --> 00:28:05,700
Zostanie pan przesłuchany
w sprawie zabójstwa Laury Palmer.

295
00:28:05,780 --> 00:28:07,860
To wystarczająco jasne?

296
00:28:13,850 --> 00:28:17,160
Zwariował pan.

297
00:28:17,250 --> 00:28:20,140
Panie Tojamura,
to jakaś okropna pomyłka.

298
00:28:20,230 --> 00:28:24,630
Pójdzie pan po dobroci
czy mamy pana wywlec w kajdankach?

299
00:28:27,070 --> 00:28:28,880
Cooper, czy to jakieś żarty?

300
00:28:28,960 --> 00:28:32,250
Bo jeśli tak,
to mam szerokie znajomości!

301
00:28:32,330 --> 00:28:34,430
Radzę posłuchać.

302
00:28:40,220 --> 00:28:41,110
Wyjdźcie.

303
00:28:42,690 --> 00:28:44,950
Wynoście się.

304
00:28:46,280 --> 00:28:48,510
Zamierzam zjeść kanapkę.

305
00:28:49,780 --> 00:28:51,220
Nie!

306
00:29:05,790 --> 00:29:07,750
Nie wolno wam!

307
00:29:08,470 --> 00:29:10,090
A jednak.

308
00:30:42,050 --> 00:30:44,510
Zabierzcie go do celi.

309
00:31:16,820 --> 00:31:20,010
Nie wiemy, co się stanie ani kiedy.

310
00:31:20,800 --> 00:31:25,040
Ale w Roadhouse są sowy.

311
00:31:26,440 --> 00:31:28,190
W Roadhouse.

312
00:31:30,770 --> 00:31:35,360
Na coś się zanosi, prawda?

313
00:31:37,850 --> 00:31:38,780
Tak.

314
00:32:30,060 --> 00:32:31,920
Wolnego, łobuzie!

315
00:32:32,500 --> 00:32:36,090
Od pierwszego spotkania
czuję do pana dziwny pociąg.

316
00:32:36,630 --> 00:32:39,260
Odczep się pan ode mnie!

317
00:32:40,100 --> 00:32:45,760
To przez te oczy, pełne ciepła i głębi...

318
00:32:47,030 --> 00:32:50,440
Błękitne jak niebo.

319
00:32:53,810 --> 00:32:55,610
To ja, głuptasie!

320
00:32:55,690 --> 00:32:57,520
To ja!

321
00:32:59,280 --> 00:33:00,360
Catherine...

322
00:33:04,610 --> 00:33:07,040
Jak ty wyglądasz?

323
00:33:11,080 --> 00:33:13,460
Okropnie...

324
00:33:16,280 --> 00:33:18,890
Strasznie...

325
00:35:20,560 --> 00:35:23,980
- Słyszałeś o Haroldzie?
- Tak.

326
00:35:26,750 --> 00:35:29,180
Nikt tu nie zawinił.

327
00:35:29,580 --> 00:35:31,110
Był chory.

328
00:35:31,200 --> 00:35:35,300
Bardzo cierpiał, ale tego nie widziałam.

329
00:35:37,820 --> 00:35:40,150
Każdy z nas cierpi.

330
00:35:43,090 --> 00:35:48,410
Ten dom był jego życiem,
a ja to zepsułam.

331
00:35:48,870 --> 00:35:51,910
Chciałaś się czegoś
dowiedzieć o Laurze.

332
00:35:56,870 --> 00:35:59,210
Ale on nie żyje.

333
00:36:00,070 --> 00:36:02,560
Nie zasłużył na to.

334
00:36:15,930 --> 00:36:17,350
To szeryf Truman.

335
00:36:39,930 --> 00:36:41,790
Maddie wyjeżdża.

336
00:36:43,480 --> 00:36:45,300
Tak?

337
00:36:46,730 --> 00:36:49,400
Wraca do domu.

338
00:36:51,510 --> 00:36:55,470
To dziwne, bo nic mi nie mówiła.

339
00:38:50,710 --> 00:38:53,670
To się znów dzieje.

340
00:38:57,900 --> 00:39:01,660
To się znowu dzieje.

341
00:39:58,610 --> 00:40:00,560
Ciociu Saro?

342
00:40:02,340 --> 00:40:05,050
Wujku Lelandzie!

343
00:40:05,140 --> 00:40:08,580
Ciociu, wujku, co tak śmierdzi?

344
00:40:08,660 --> 00:40:11,380
Cuchnie spalenizną!

345
00:40:12,260 --> 00:40:13,630
Ciociu?

346
00:40:52,530 --> 00:40:56,970
Nie podchodź do mnie!

347
00:41:59,990 --> 00:42:01,940
Moje dziecko!

348
00:43:09,810 --> 00:43:11,640
Leland mówi...

349
00:43:12,510 --> 00:43:15,840
że wracasz do Missoula!

350
00:43:15,930 --> 00:43:17,560
Nie pozwolę ci!

351
00:44:48,280 --> 00:44:51,370
Ogromnie mi przykro.

