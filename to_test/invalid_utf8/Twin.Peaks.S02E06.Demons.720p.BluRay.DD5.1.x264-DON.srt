1
00:00:28,850 --> 00:00:33,620
MIASTECZKO TWIN PEAKS

2
00:01:42,530 --> 00:01:44,740
Zaufałem ci.

3
00:01:46,370 --> 00:01:49,990
Uwierzyłem z całego serca i duszy.

4
00:01:50,070 --> 00:01:52,450
- Chcę go przeczytać.
- To nic złego.

5
00:01:52,840 --> 00:01:55,080
Myślałem, że jesteś inna.

6
00:01:56,110 --> 00:02:06,590
Czułem, że mógłbym wrócić do świata
i czegoś dobrego, czystego.

7
00:02:06,810 --> 00:02:09,570
Ale jesteś taka jak wszyscy.

8
00:02:09,650 --> 00:02:11,530
Kłamiesz,

9
00:02:11,610 --> 00:02:13,230
zdradzasz,

10
00:02:13,560 --> 00:02:15,060
a potem się z tego śmiejesz!

11
00:02:15,720 --> 00:02:18,160
Jesteś nieczysta!

12
00:02:19,530 --> 00:02:22,030
Skalałaś mnie!

13
00:02:24,420 --> 00:02:25,120
Oddaj to!

14
00:02:39,830 --> 00:02:43,090
Nie wyjdzie, boi się.

15
00:02:43,970 --> 00:02:48,500
- Skrzywdził cię?
- Nic mi nie jest. Dzięki.

16
00:02:51,550 --> 00:02:54,410
Przepraszam, to wszystko przeze mnie.

17
00:02:56,220 --> 00:02:58,930
Nie płacz. Ja też zawiniłem.

18
00:02:59,010 --> 00:03:00,810
Jedźmy do szeryfa.

19
00:03:01,260 --> 00:03:04,680
- Masz już dość kłopotów.
- Pojadę.

20
00:03:04,760 --> 00:03:06,680
Przytul mnie.

21
00:04:09,480 --> 00:04:11,500
Wszystko gotowe.

22
00:04:24,090 --> 00:04:26,200
Słabe tętno.

23
00:04:27,750 --> 00:04:32,080
Zwężone źrenice, płytki oddech...

24
00:04:36,130 --> 00:04:39,290
Ślady igły. To heroina.

25
00:04:39,380 --> 00:04:41,330
Biedactwo.

26
00:04:41,420 --> 00:04:42,830
Tato?

27
00:04:43,670 --> 00:04:45,460
Widzisz mnie?

28
00:04:45,950 --> 00:04:47,590
Widzisz mnie, tatusiu?

29
00:04:47,900 --> 00:04:50,000
Nie zasypiaj.

30
00:04:50,080 --> 00:04:52,310
Możesz mnie złapać?

31
00:04:55,250 --> 00:04:58,770
Audrey, mów do mnie. Nie zasypiaj.

32
00:05:00,290 --> 00:05:02,700
Ma ciężkie ręce...

33
00:05:02,780 --> 00:05:05,170
trzyma mnie za gardło...

34
00:05:06,360 --> 00:05:07,650
To boli.

35
00:05:08,320 --> 00:05:10,570
Boże, jak boli.

36
00:05:11,250 --> 00:05:13,100
Czarny chłód...

37
00:05:13,800 --> 00:05:15,900
Duszę się!

38
00:05:15,980 --> 00:05:17,620
Ratunku!

39
00:05:18,750 --> 00:05:20,900
Tonę! Topię się!

40
00:05:20,990 --> 00:05:22,960
Już dobrze, jestem z tobą.

41
00:05:23,140 --> 00:05:24,820
Audrey, jestem tu.

42
00:05:32,780 --> 00:05:34,310
Modliłam się...

43
00:05:36,140 --> 00:05:38,590
Modliłam się, żeby pan przyszedł.

44
00:05:39,640 --> 00:05:41,100
I jest pan.

45
00:05:54,210 --> 00:05:58,500
Jest taka dzielna.
Gdyby coś się stało...

46
00:05:58,580 --> 00:06:02,450
Nie róbcie nic więcej same.

47
00:06:02,530 --> 00:06:06,110
On ma pamiętnik Laury. Mógł ją zabić.

48
00:06:06,200 --> 00:06:08,540
Ciebie też.

49
00:06:09,950 --> 00:06:12,000
Głupio by było.

50
00:06:12,720 --> 00:06:15,420
Po śmierci Laury

51
00:06:15,500 --> 00:06:20,440
tyle się stało,
że już nie wiadomo, w co wierzyć.

52
00:06:22,410 --> 00:06:26,290
Ale gdy cię dziś zobaczyłem,
wiedziałem.

53
00:06:26,370 --> 00:06:28,040
Naprawdę?

54
00:06:28,790 --> 00:06:34,520
Gdybyśmy na zawsze połączyli
nasze serca, bylibyśmy bezpieczni.

55
00:06:35,270 --> 00:06:37,040
Ty i ja.

56
00:06:37,830 --> 00:06:39,730
Od dziś.

57
00:07:00,050 --> 00:07:03,680
- Co z nią?
- Najgorsze już za nią.

58
00:07:05,650 --> 00:07:07,540
To heroina.

59
00:07:07,630 --> 00:07:09,860
Niemal śmiertelna dawka.

60
00:07:11,640 --> 00:07:13,610
Trudno pojąć takie okrucieństwo.

61
00:07:14,920 --> 00:07:17,900
Niezupełnie. Spójrz.

62
00:07:18,500 --> 00:07:20,190
To zabójca Blackie.

63
00:07:20,280 --> 00:07:22,030
Znam go.

64
00:07:22,430 --> 00:07:24,790
- To Jean Renault.
- Renault?

65
00:07:25,680 --> 00:07:28,860
Starszy brat Jacques'a i Bernarda,
dużo bardziej niebezpieczny.

66
00:07:28,950 --> 00:07:33,920
Rządzi na północy: hazard,
narkotyki, rozbój. Co chcesz.

67
00:07:36,100 --> 00:07:43,590
U Blackie widziałem cię na nagraniu
z wieczora, gdy byłeś tam z Edem.

68
00:07:44,330 --> 00:07:46,060
Renault dybał na mnie.

69
00:07:47,030 --> 00:07:48,310
Złapałeś jego brata.

70
00:07:48,740 --> 00:07:52,280
Chciał mnie zabić,
a Audrey była przynętą.

71
00:07:55,400 --> 00:07:58,790
Dwa razy przekroczyłem swoje obowiązki.

72
00:07:58,870 --> 00:08:02,530
Złamałem zasady, a Audrey za to płaci.

73
00:08:03,730 --> 00:08:06,580
Odbiłeś ją i wyzdrowieje.

74
00:08:06,660 --> 00:08:09,810
Nie po raz pierwszy
cierpi przeze mnie ktoś bliski.

75
00:08:09,900 --> 00:08:13,440
W imię wyższych celów.

76
00:08:13,530 --> 00:08:15,130
Co ja sobie myślałem?

77
00:08:15,620 --> 00:08:19,470
Ale teraz ona jest tutaj,
a nie u Jacka z igłą w żyle.

78
00:08:28,110 --> 00:08:33,010
Nie spotkałem lepszego gliny.

79
00:08:34,490 --> 00:08:36,480
Tylko za dużo kombinujesz.

80
00:08:39,420 --> 00:08:40,700
Dzięki, Harry.

81
00:09:01,620 --> 00:09:03,390
Agencie Cooper...

82
00:09:04,090 --> 00:09:06,000
odebrałem wiadomość.

83
00:09:07,750 --> 00:09:09,270
Co się stało?

84
00:09:09,350 --> 00:09:12,890
Uwolniłem pańską córkę bez okupu.

85
00:09:14,650 --> 00:09:16,230
Dzięki Bogu!

86
00:09:16,310 --> 00:09:21,550
Była więziona w domu uciech
"Jednooki Jack", tuż za granicą.

87
00:09:21,640 --> 00:09:22,570
Nie...

88
00:09:23,250 --> 00:09:29,240
Współodpowiedzialna za porwanie
i szantaż jest burdelmama.

89
00:09:30,200 --> 00:09:34,940
- Aresztowaliście ją?
- Niestety została zamordowana.

90
00:09:37,160 --> 00:09:40,090
Przez niejakiego Jeana Renault.

91
00:09:41,310 --> 00:09:44,550
A tego Renaulta złapaliście?

92
00:09:45,880 --> 00:09:46,970
Zbiegł.

93
00:09:49,750 --> 00:09:50,810
Fatalnie.

94
00:09:55,840 --> 00:09:58,440
Chyba wszystko się zgadza.

95
00:09:58,840 --> 00:10:01,110
Pańska córka przedawkowała narkotyki.

96
00:10:04,520 --> 00:10:07,180
Narkotyki?

97
00:10:08,510 --> 00:10:10,510
Mój Boże...

98
00:10:10,590 --> 00:10:12,680
Wyjdzie z tego.

99
00:10:14,100 --> 00:10:17,670
- Mogę ją zobaczyć?
- Odpoczywa, nie przeszkadzajmy jej.

100
00:10:18,280 --> 00:10:20,200
Zadzwonię rano.

101
00:10:25,580 --> 00:10:27,970
Bardzo o to proszę.

102
00:10:28,600 --> 00:10:30,260
Dziękuję.

103
00:10:33,760 --> 00:10:37,690
Dziękuję, że odzyskał pan Audrey.

104
00:10:42,200 --> 00:10:43,360
Dobranoc, panie Horne.

105
00:11:25,970 --> 00:11:27,450
Biedak naprawdę odleciał.

106
00:11:28,070 --> 00:11:29,670
Bez jaj.

107
00:11:29,750 --> 00:11:34,450
Nie sposób wyrazić, jak was podziwiam,
że tak mu pomagacie.

108
00:11:34,530 --> 00:11:38,620
Nie idziecie na łatwiznę.
Inni by się go pozbyli.

109
00:11:39,170 --> 00:11:43,000
Leo jest dla nas kimś wyjątkowym.

110
00:11:43,840 --> 00:11:49,300
Wierzcie, że rekonwalescencja
w domu działa cuda.

111
00:11:49,500 --> 00:11:51,940
Pan Johnson bardzo skorzysta na tym,

112
00:11:52,030 --> 00:11:54,260
że przebywa wśród kochających krewnych.

113
00:11:54,340 --> 00:11:56,890
Mamy nadzieję.

114
00:11:56,970 --> 00:11:59,770
Zatrudnicie pielęgniarkę na stałe?

115
00:12:01,550 --> 00:12:04,970
Zajmę się nim z kuzynem.

116
00:12:07,570 --> 00:12:09,400
Cóż za poświęcenie!

117
00:12:12,310 --> 00:12:15,570
Wszystko w porządku.

118
00:12:15,650 --> 00:12:19,940
Przydadzą się osłony na kontakty.

119
00:12:20,030 --> 00:12:21,750
Proszę podpisać...

120
00:12:29,120 --> 00:12:31,380
- Oto czek.
- Dziękuję.

121
00:12:37,160 --> 00:12:38,220
Zaszła pomyłka.

122
00:12:43,990 --> 00:12:47,260
- 1700 dolców?
- Przykre, prawda?

123
00:12:47,350 --> 00:12:50,460
Koszty leczenia są niebotyczne.

124
00:12:50,550 --> 00:12:52,420
To rozbój.

125
00:12:52,500 --> 00:12:55,380
Miało być 5 tysięcy miesięcznie!

126
00:12:55,460 --> 00:13:00,710
Gdy odpiszemy podatek stanowy
i lokalny, opłaty za sprzęt,

127
00:13:00,800 --> 00:13:04,670
koszty opieki medycznej
oraz administracji, to tyle zostaje.

128
00:13:05,240 --> 00:13:11,420
Na szczęście pan Johnson może
liczyć na waszą troskliwą opiekę.

129
00:13:11,900 --> 00:13:13,850
Powodzenia.

130
00:13:13,940 --> 00:13:16,180
Trafię do drzwi.

131
00:13:19,030 --> 00:13:21,610
Muszę odejść z pracy.

132
00:13:21,690 --> 00:13:24,310
Jak z tego wyżyję?

133
00:13:24,390 --> 00:13:27,220
- Nie wiem.
- Nie wiesz?

134
00:13:27,300 --> 00:13:29,740
To ty chciałeś go zatrzymać!

135
00:13:29,830 --> 00:13:32,490
Cicho, myślę!

136
00:13:33,540 --> 00:13:36,700
I lepiej dla nas, żebyś coś wymyślił.

137
00:13:44,320 --> 00:13:47,230
Już mamy pamiętnik Laury.

138
00:13:47,320 --> 00:13:50,400
Tamten jest tajny.

139
00:13:50,490 --> 00:13:54,390
- Dała go temu Howardowi?
- Haroldowi.

140
00:13:54,470 --> 00:13:58,340
- Dlaczego?
- Nie wiem. Żeby coś ukryć.

141
00:13:59,610 --> 00:14:02,270
Pokazał ci ten pamiętnik?

142
00:14:04,420 --> 00:14:07,240
Nie... ale mi go czytał.

143
00:14:07,330 --> 00:14:09,940
Ma tytuł na pierwszej stronie,
to jej pismo.

144
00:14:10,510 --> 00:14:14,430
Co wy znów kombinujecie?
James miał się trzymać z daleka.

145
00:14:14,520 --> 00:14:16,570
Nie ma z tym nic wspólnego.

146
00:14:17,220 --> 00:14:19,990
To przypomina bajkę
o chłopcu i wilkach.

147
00:14:20,550 --> 00:14:25,250
Już raz się zabawiliście
i doktor Jacoby trafił do szpitala.

148
00:14:25,980 --> 00:14:27,580
To nie zabawa.

149
00:14:32,370 --> 00:14:34,460
Wyślę kogoś, żeby tam zajrzał.

150
00:14:34,550 --> 00:14:37,040
Ale jestem zajęty i nie obiecuję wiele.

151
00:14:37,130 --> 00:14:41,100
- Szukam Harry'ego Trumana.
- I znalazł pan.

152
00:14:41,430 --> 00:14:44,800
Przepraszam, szukam
szeryfa Harry'ego Trumana.

153
00:14:44,880 --> 00:14:45,720
To ja.

154
00:14:45,810 --> 00:14:49,220
Szef regionalnego oddziału FBI,
Gordon Cole.

155
00:14:49,310 --> 00:14:52,120
Długi tytuł, ale i tak nic nie słyszę.

156
00:14:52,200 --> 00:14:55,480
- Jestem przełożonym Coopera.
- Miło mi poznać.

157
00:14:55,570 --> 00:14:57,990
- Można na słowo?
- Jasne.

158
00:14:58,170 --> 00:14:59,490
Donna...

159
00:15:00,760 --> 00:15:01,330
zaczekaj.

160
00:15:04,820 --> 00:15:07,830
- Z kim mam przyjemność?
- Szeryf Truman.

161
00:15:08,180 --> 00:15:13,240
Niech to szlag! Proszę głośniej,
bo nie słyszę! Długo by gadać.

162
00:15:13,330 --> 00:15:15,090
Muszę podkręcać na maksa.

163
00:15:16,200 --> 00:15:18,630
Szuka pan agenta Coopera?

164
00:15:18,710 --> 00:15:22,510
Gdy ktoś z moich ludzi obrywa,
natychmiast pędzę z wizytą.

165
00:15:22,590 --> 00:15:23,950
Teraz go nie ma.

166
00:15:24,030 --> 00:15:29,300
Albert Rosenfield nie przyjedzie,
ale mam wyniki badań.

167
00:15:29,390 --> 00:15:31,520
To wełna wigonia.

168
00:15:31,690 --> 00:15:36,280
Przed pokojem Coopera
Albert znalazł włókna z wełny wigonia.

169
00:15:37,270 --> 00:15:39,070
Wełna z wigonia.

170
00:15:39,990 --> 00:15:42,330
Brzmi świetnie, ale już jadłem.

171
00:15:43,300 --> 00:15:46,830
Mam też wiadomość o zawartości
strzykawki jednorękiego.

172
00:15:46,920 --> 00:15:51,170
Albert nie zna tego leku.
To jakaś dziwna mieszanka.

173
00:15:51,350 --> 00:15:53,270
- Mówił coś jeszcze?
- Papier.

174
00:15:53,350 --> 00:15:56,880
Kartki znalezione z zakrwawionym
ręcznikiem przy torach

175
00:15:57,270 --> 00:15:59,010
pochodzą z pamiętnika.

176
00:16:00,780 --> 00:16:04,430
- Jest jednoręki.
- Od kiedy sprzedaż butów to zbrodnia?

177
00:16:04,910 --> 00:16:07,730
Mamy do pana kilka pytań.

178
00:16:09,110 --> 00:16:10,440
Przejdźmy do mojego biura.

179
00:16:14,380 --> 00:16:17,320
- Do mojego biura!
- Jasne.

180
00:16:25,300 --> 00:16:28,030
Wygląda jak anioł.

181
00:16:30,820 --> 00:16:33,280
Audrey?

182
00:16:34,520 --> 00:16:36,520
Jest tu twój ojciec.

183
00:16:38,850 --> 00:16:41,520
Dzięki Bogu, Audrey.

184
00:16:44,490 --> 00:16:46,720
Chwała Bogu.

185
00:16:48,570 --> 00:16:50,940
Tak się o ciebie bałem.

186
00:16:51,570 --> 00:16:53,350
- Poważnie?
- Tak.

187
00:16:53,750 --> 00:16:55,480
Zamartwiałem się.

188
00:16:56,940 --> 00:16:59,780
W takich chwilach

189
00:17:00,070 --> 00:17:04,680
doceniamy wartość życia.

190
00:17:05,600 --> 00:17:09,850
Każdej kropli deszczu, zachodu słońca.

191
00:17:10,880 --> 00:17:13,670
Ja też się wiele nauczyłam.

192
00:17:18,310 --> 00:17:22,590
Zaraz wrócisz do domu,
do własnego łóżka.

193
00:17:25,150 --> 00:17:28,390
Co za przeżycia, jacy straszni ludzie!

194
00:17:29,170 --> 00:17:30,470
Co ty tam musiałaś oglądać!

195
00:17:30,980 --> 00:17:33,190
Dużo widziałam.

196
00:17:37,920 --> 00:17:41,650
Poradzimy sobie z tym. Razem.

197
00:17:43,460 --> 00:17:45,460
Tak, tatku.

198
00:17:45,550 --> 00:17:47,700
Ty i ja.

199
00:17:52,200 --> 00:17:53,290
Zuch dziewczyna.

200
00:18:03,150 --> 00:18:04,130
Przyjechałem samochodem.

201
00:18:05,280 --> 00:18:06,890
Zabiorę ją.

202
00:18:09,200 --> 00:18:10,930
Tato?

203
00:18:11,010 --> 00:18:12,600
Słucham.

204
00:18:14,220 --> 00:18:17,310
Wolę, żeby mnie odwiózł
agent Cooper, jeśli pozwolisz.

205
00:18:20,210 --> 00:18:21,720
Dobrze.

206
00:18:23,550 --> 00:18:25,060
A może...

207
00:18:26,190 --> 00:18:28,150
Może jedźmy wszyscy razem?

208
00:18:35,420 --> 00:18:37,980
Ed, wróciłam!

209
00:18:38,060 --> 00:18:41,150
Super kiecka, prawda?

210
00:18:41,230 --> 00:18:45,010
I nic nie podejrzewali,
kiedy zapłaciłam kartą.

211
00:18:45,100 --> 00:18:49,500
A kiedy moi starzy wracają z Europy?

212
00:18:50,520 --> 00:18:51,860
Niedługo.

213
00:18:51,950 --> 00:18:56,560
Fajnie mieć cały dom dla siebie.

214
00:18:56,650 --> 00:19:00,030
Udawajmy, że jest nasz,
jakbyśmy byli po ślubie.

215
00:19:00,120 --> 00:19:02,210
To się da zrobić.

216
00:19:02,300 --> 00:19:05,200
Możemy też zrobić coś innego.

217
00:19:05,290 --> 00:19:09,300
- Na przykład co?
- Przecież wiesz.

218
00:19:11,260 --> 00:19:13,750
Wiesz, co wymyśliłam?

219
00:19:13,840 --> 00:19:18,820
Jedźmy się popieścić
do parku nad jeziorem.

220
00:19:19,520 --> 00:19:20,990
Zaraz!

221
00:19:21,660 --> 00:19:23,940
Nie musimy nigdzie jechać!

222
00:19:24,620 --> 00:19:28,110
Możemy zostać tutaj.

223
00:19:43,090 --> 00:19:46,320
Nieźle się tu urządziłaś.

224
00:19:47,460 --> 00:19:52,700
Kolczyki z szafirami, paryskie
perfumy, swetry z kaszmiru...

225
00:19:53,150 --> 00:19:54,200
Idź już.

226
00:19:57,590 --> 00:20:01,210
W jedną stronę. Seattle – Hongkong.

227
00:20:05,460 --> 00:20:08,550
- Mam jeszcze dzień.
- Weź, co twoje.

228
00:20:09,230 --> 00:20:11,740
Co zmieścisz, do walizek.

229
00:20:11,830 --> 00:20:14,010
Wieczorem lecimy.

230
00:20:15,850 --> 00:20:18,580
Nie dostałam pieniędzy z polisy.

231
00:20:18,670 --> 00:20:21,490
Horne też jeszcze nie zapłacił.

232
00:20:22,160 --> 00:20:24,440
Czekałam na to pięć lat.

233
00:20:24,900 --> 00:20:28,360
Pan Eckhardt ci to wynagrodzi.

234
00:20:30,420 --> 00:20:33,320
Nie zmusisz mnie.

235
00:20:40,070 --> 00:20:42,390
Dostosuj się.

236
00:20:43,060 --> 00:20:46,660
Plan był inny.

237
00:20:48,220 --> 00:20:52,300
Chodzi o szeryfa Trumana.

238
00:20:52,390 --> 00:20:54,680
Okłamałaś go,

239
00:20:54,770 --> 00:20:57,190
zrobiłaś z niego durnia,

240
00:20:57,890 --> 00:21:01,570
a mimo to coś dla ciebie znaczy.

241
00:21:03,520 --> 00:21:06,780
Leć dziś ze mną albo go zabiję.

242
00:21:06,860 --> 00:21:09,060
O niczym nie wie.

243
00:21:10,500 --> 00:21:12,860
Samolot startuje o północy.

244
00:21:12,950 --> 00:21:14,490
Przyjdź,

245
00:21:14,570 --> 00:21:18,190
bo załatwię was oboje.

246
00:21:18,930 --> 00:21:21,590
Taki jest plan.

247
00:21:51,270 --> 00:21:51,860
Cześć.

248
00:21:56,180 --> 00:21:58,690
Powinienem cię przeprosić.

249
00:21:59,580 --> 00:22:01,580
Nie musisz.

250
00:22:03,170 --> 00:22:09,020
Kiedy byliśmy razem
i rozmawialiśmy, coś czułem.

251
00:22:09,110 --> 00:22:11,010
Ale nie wiedziałem co.

252
00:22:11,430 --> 00:22:13,420
Widziałeś we mnie Laurę.

253
00:22:14,770 --> 00:22:15,760
Chyba tak.

254
00:22:17,280 --> 00:22:21,130
- Wiesz, co jest dziwne?
- Co?

255
00:22:21,920 --> 00:22:23,180
Spodobało mi się.

256
00:22:24,880 --> 00:22:25,850
Tak?

257
00:22:26,940 --> 00:22:31,900
Kiedyś byłyśmy sobie tak bliskie,
że aż się bałam.

258
00:22:32,580 --> 00:22:36,450
Znałam jej myśli,
jakby nasze umysły były połączone.

259
00:22:36,680 --> 00:22:40,870
Gdy zginęła, mogłam się nią stać.

260
00:22:40,950 --> 00:22:43,000
Przynajmniej w oczach innych.

261
00:22:43,720 --> 00:22:46,710
Na przykład w twoich.

262
00:22:48,110 --> 00:22:50,440
Polubiłam to.

263
00:22:53,590 --> 00:22:55,510
Ale tak nie wolno.

264
00:22:56,650 --> 00:22:59,420
Nie miałam wyboru.

265
00:23:01,010 --> 00:23:04,440
Przez chwilę musiałam być kimś innym.

266
00:23:06,650 --> 00:23:09,230
Ale już nie jestem.

267
00:23:13,180 --> 00:23:16,640
Ty i Donna jesteście
dla siebie stworzeni.

268
00:23:16,720 --> 00:23:19,200
To wspaniała rzecz.

269
00:23:24,430 --> 00:23:27,450
Ale czasami bywa trudno.

270
00:23:27,540 --> 00:23:28,370
Dlaczego?

271
00:23:29,490 --> 00:23:33,980
Jeśli kogoś naprawdę kochasz, to jakby
wciąż spływało na ciebie światło.

272
00:23:34,070 --> 00:23:37,180
Stoisz w nim i jest ci dobrze.

273
00:23:37,260 --> 00:23:41,390
Ale chyba nie może tak być cały czas.

274
00:23:42,320 --> 00:23:44,720
Może.

275
00:23:45,400 --> 00:23:47,530
Chciałbym.

276
00:23:50,130 --> 00:23:54,940
Chciałbym, żeby to
trwało wiecznie. To dziwne.

277
00:24:03,190 --> 00:24:05,970
Jutro wyjeżdżam.

278
00:24:07,860 --> 00:24:10,920
Przyjechałam na pogrzeb Laury,

279
00:24:11,840 --> 00:24:13,670
ale już pora wracać.

280
00:24:17,960 --> 00:24:19,470
Więc to pożegnanie?

281
00:24:24,020 --> 00:24:26,050
Chyba tak.

282
00:24:37,560 --> 00:24:39,400
Żegnaj, James.

283
00:24:56,740 --> 00:24:58,470
Za pożar!

284
00:25:05,430 --> 00:25:11,490
A teraz mów,
czemu zawdzięczam tę wizytę?

285
00:25:12,470 --> 00:25:17,850
Mam umowę z podpisem Pete'a
i chcę pieniędzy.

286
00:25:17,940 --> 00:25:19,660
To zrozumiałe.

287
00:25:19,750 --> 00:25:23,190
Wszystko i zaraz. Zanim to dostaniesz.

288
00:25:23,280 --> 00:25:25,440
Josie...

289
00:25:26,160 --> 00:25:29,530
jestem pewien,
że twój bystry umysł zdoła pojąć

290
00:25:29,610 --> 00:25:34,230
złożoność naszej sytuacji.
Grunt to płynność.

291
00:25:34,310 --> 00:25:38,140
Nie zbiorę gotówki, której nie mam.

292
00:25:38,220 --> 00:25:45,090
Ale... są dobre perspektywy.

293
00:25:45,170 --> 00:25:54,130
Gdy tylko zaczną spływać wpłaty
z Islandii, dostaniesz pieniądze.

294
00:25:54,210 --> 00:25:58,210
Nie wyjdę stąd bez nich.

295
00:26:00,660 --> 00:26:05,430
Chyba nie jesteś
w nastroju do negocjacji.

296
00:26:09,190 --> 00:26:11,700
Przycisnęło cię, co?

297
00:26:12,470 --> 00:26:14,670
Nie pora na gierki.

298
00:26:16,450 --> 00:26:18,240
Gierki?

299
00:26:21,680 --> 00:26:23,210
Josie...

300
00:26:23,840 --> 00:26:29,010
to klucz do mojego osobistego sejfu.

301
00:26:29,840 --> 00:26:35,750
Mam w nim ciekawe materiały o tobie

302
00:26:35,830 --> 00:26:40,070
i łódce twojego zmarłego męża,
która nagle wybuchła.

303
00:26:40,160 --> 00:26:43,470
Bądź więc grzeczna,

304
00:26:43,560 --> 00:26:46,470
bo wierz mi...

305
00:26:46,550 --> 00:26:49,920
inaczej cię załatwię.

306
00:26:50,990 --> 00:26:53,240
Tak mi przykro.

307
00:26:53,330 --> 00:26:57,600
Bo jeśli spotka mnie coś złego,

308
00:26:57,690 --> 00:27:04,560
policja trafi do bankowej skrytki
w innym mieście.

309
00:27:04,640 --> 00:27:10,900
A jest tam dość, żeby sąd
skazał cię na trzy dożywocia.

310
00:27:10,990 --> 00:27:14,730
Oboje będziemy załatwieni.

311
00:27:20,770 --> 00:27:23,030
Mamy pata.

312
00:27:24,130 --> 00:27:26,070
Twój ruch.

313
00:27:27,730 --> 00:27:30,120
Forsa.

314
00:27:35,250 --> 00:27:39,050
Na szczęście ją mam.

315
00:27:43,480 --> 00:27:45,620
To gest dobrej woli.

316
00:27:50,100 --> 00:27:52,130
Bank w Tokio.

317
00:27:54,610 --> 00:27:59,870
Pięć milionów dolarów.

318
00:28:07,660 --> 00:28:09,940
Załatwione.

319
00:28:12,980 --> 00:28:14,690
Josie...

320
00:28:17,920 --> 00:28:19,950
brawo.

321
00:28:40,050 --> 00:28:41,510
Zdrowie Leo!

322
00:28:43,630 --> 00:28:47,540
Skoro wywinąłeś się od pudła,
to jest okazja.

323
00:28:47,630 --> 00:28:54,210
Urządzimy ci przyjęcie.
Leo, witaj w domu!

324
00:28:54,290 --> 00:28:56,460
Cześć, kochanie.

325
00:28:56,550 --> 00:28:59,660
Świetny z ciebie gość.
Powinieneś wiedzieć,

326
00:29:00,120 --> 00:29:04,850
że pamiętamy o wszystkim,
co zrobiłeś dla najbliższych.

327
00:29:04,940 --> 00:29:06,890
Na przykład dla Shelly.

328
00:29:08,980 --> 00:29:12,460
Czy można sobie wymarzyć lepszego męża?

329
00:29:12,540 --> 00:29:18,470
Prać twoje brudne łachy,
zmywać, szorować podłogi.

330
00:29:18,550 --> 00:29:22,240
- Jesteś ideałem, stary.
- Bez wad.

331
00:29:22,330 --> 00:29:25,190
Leo Johnson, kat swojej żony.

332
00:29:25,270 --> 00:29:28,130
Chętnie się z nim zabawię.

333
00:29:28,220 --> 00:29:29,740
Mydło w skarpetce.

334
00:29:29,990 --> 00:29:32,740
Obrażenia wewnętrzne bez siniaków.

335
00:29:34,210 --> 00:29:37,490
- No i w końcu...
- Na deser!

336
00:29:37,880 --> 00:29:40,420
...morderca.

337
00:29:40,510 --> 00:29:44,770
Powiesił żonę na belce
i zostawił, żeby umarła.

338
00:29:44,860 --> 00:29:48,960
Nikt ci nie powie, że nie umiesz
postępować z kobietą.

339
00:29:53,750 --> 00:29:55,470
Teraz będziesz tak żył.

340
00:29:57,130 --> 00:29:58,910
Zasłużyłeś na to.

341
00:30:00,280 --> 00:30:03,580
Na wszystko.

342
00:30:27,470 --> 00:30:28,710
Ruszył się!

343
00:30:29,190 --> 00:30:30,580
To okropne.

344
00:30:30,660 --> 00:30:33,760
Już dobrze, może masz rację.

345
00:30:34,460 --> 00:30:36,680
Nie chcę go dobijać.

346
00:30:36,760 --> 00:30:41,060
Przepraszam, Leo, poniosło mnie.

347
00:30:41,370 --> 00:30:46,960
Nie utrudniajmy ci
i tak ciężkiej rehabilitacji.

348
00:30:47,040 --> 00:30:50,020
Doktor mówił, że potrzebujesz
znajomych bodźców

349
00:30:50,100 --> 00:30:55,660
i chcieliśmy spróbować.

350
00:30:55,750 --> 00:31:02,040
Zrobimy, co w naszej mocy,
żeby ci tu było dobrze.

351
00:31:03,390 --> 00:31:04,620
Chcesz torcik?

352
00:31:11,090 --> 00:31:12,590
Patrz.

353
00:31:13,370 --> 00:31:14,720
I lody.

354
00:31:18,940 --> 00:31:19,720
Rany...

355
00:31:23,500 --> 00:31:25,520
Musisz bardziej uważać.

356
00:31:28,310 --> 00:31:29,650
Dobrze, że nie było świeczek.

357
00:31:42,300 --> 00:31:45,100
Agent Dale Cooper.

358
00:31:45,180 --> 00:31:47,300
Gordon! Obyś długo nie czekał.

359
00:31:47,380 --> 00:31:50,940
Przyjechałem już dawno.
Czekam tu całe popołudnie.

360
00:31:51,030 --> 00:31:54,940
- Czym mogę ci służyć?
- Nie ma sprawy, nie przepraszaj.

361
00:31:55,030 --> 00:31:59,980
Poznałem miejscowych
stróżów prawa. Świetny zespół.

362
00:32:02,710 --> 00:32:07,520
Przypominasz mi małego
meksykańskiego pieska chihuahua.

363
00:32:09,400 --> 00:32:13,380
- Możemy zamienić słowo na osobności?
- Oczywiście.

364
00:32:13,460 --> 00:32:18,000
- Harry, możemy u ciebie?
- Może szeryf udostępni nam biuro.

365
00:32:18,650 --> 00:32:20,400
Proszę.

366
00:32:21,270 --> 00:32:23,110
Chodźmy.

367
00:32:25,130 --> 00:32:29,210
Zachowajmy dyskrecję. Do rzeczy.

368
00:32:29,400 --> 00:32:31,170
Nie chciałem mówić przy nich,

369
00:32:31,940 --> 00:32:36,190
ale zdaniem Alberta
znów ponoszą cię emocje.

370
00:32:36,280 --> 00:32:39,970
Martwi mnie to,
bo za ciebie odpowiadam.

371
00:32:40,230 --> 00:32:43,480
- Albert jest w błędzie.
- W Pittsburghu to się źle skończyło

372
00:32:43,570 --> 00:32:48,960
i chcę mieć absolutną pewność,
że to się nie powtórzy, jasne?

373
00:32:49,040 --> 00:32:50,690
Pittsburgh to zupełnie inna historia.

374
00:32:51,920 --> 00:32:57,030
Tam też byłem ranny,
ale to jedyna zbieżność.

375
00:32:57,960 --> 00:33:01,120
Przyznam, że dobrze wyglądasz,
poza podkrążonymi oczami.

376
00:33:01,210 --> 00:33:06,280
Wyśpij się. Choć brak snu
i koszmary to dla nas norma.

377
00:33:06,970 --> 00:33:13,580
Jestem zmęczony, mam dużo pracy.
Ale na pewno temu podołam.

378
00:33:13,670 --> 00:33:16,210
Kondycja mi dopisuje.

379
00:33:18,310 --> 00:33:19,950
Jestem z ciebie dumny.

380
00:33:21,480 --> 00:33:24,590
O co chodzi
z tym meksykańskim chihuahua?

381
00:33:24,670 --> 00:33:27,030
To zupełnie inna sprawa.

382
00:33:27,120 --> 00:33:30,100
Dwa plus dwa nie zawsze jest cztery.

383
00:33:31,440 --> 00:33:34,820
Chcę tylko powiedzieć,
że tam słychać każde słowo.

384
00:33:34,910 --> 00:33:37,020
- Wejdź.
- Zaprośmy szeryfa.

385
00:33:39,030 --> 00:33:41,950
Doręczono do nas anonim
na twoje nazwisko.

386
00:33:42,030 --> 00:33:43,660
Nie wygląda to znajomo?

387
00:33:50,340 --> 00:33:51,830
To szachowy ruch.

388
00:33:53,250 --> 00:33:55,670
Jakby ruch szachowy.

389
00:33:56,510 --> 00:33:59,660
Otwarcie Windoma Earle'a.

390
00:33:59,750 --> 00:34:01,690
Windom Earle.

391
00:34:02,450 --> 00:34:06,500
Nie ma co, Coop. Musisz się pilnować.

392
00:34:12,660 --> 00:34:15,870
Skoro Jerry wyjechał...

393
00:34:17,730 --> 00:34:20,050
będziesz mi potrzebny.

394
00:34:20,130 --> 00:34:24,420
Przyznaję, że teraz lepiej rozumiem,

395
00:34:24,500 --> 00:34:26,800
co czułeś przez ostatnie tygodnie.

396
00:34:26,880 --> 00:34:31,850
Jestem wdzięczny.
Doceniam twoje zaufanie.

397
00:34:31,940 --> 00:34:35,920
Gdyby nie sprawa w sądzie,
wróciłbyś do dawnych obowiązków.

398
00:34:36,180 --> 00:34:41,050
Przyjaźnimy się, wiele przeszliśmy.

399
00:34:41,610 --> 00:34:45,930
Ale powiedz, czy wszystko gra.

400
00:34:46,200 --> 00:34:48,510
Na sto procent. Sto dziesięć.

401
00:34:52,220 --> 00:34:53,180
Udowodnię ci to.

402
00:34:54,960 --> 00:34:59,270
Wzięliśmy od Islandczyków
zaliczkę na Ghostwood,

403
00:34:59,360 --> 00:35:04,320
ale teraz pchają się do nas
na chama inwestorzy z Azji.

404
00:35:04,400 --> 00:35:06,400
Jerry poleciał do Tokio
ich prześwietlić.

405
00:35:07,030 --> 00:35:10,110
I wszystkie gejsze w okolicy.

406
00:35:10,190 --> 00:35:13,670
Zaproszę na kolację ich delegata.

407
00:35:18,960 --> 00:35:22,380
Musimy zyskać na czasie.

408
00:35:23,390 --> 00:35:28,890
Po pierwsze, wystąpimy o ponowną
wycenę gruntu po pożarze.

409
00:35:29,520 --> 00:35:32,660
Ściągniemy tłum rzeczoznawców
federalnych i stanowych,

410
00:35:32,740 --> 00:35:34,430
póki nie wróci Jerry.

411
00:35:35,210 --> 00:35:36,910
Jeśli wpłyną inne wpłaty,

412
00:35:36,990 --> 00:35:39,810
prześlemy je na Kajmany
i zarobimy na zmianach kursów,

413
00:35:39,890 --> 00:35:44,440
a w razie potrzeby utopimy kasę
w rachunek powierniczy.

414
00:35:50,980 --> 00:35:53,200
To ja rozumiem!

415
00:36:12,480 --> 00:36:13,950
Josie?

416
00:36:15,390 --> 00:36:18,550
Chyba nie znasz
mojego asystenta, pana Lee.

417
00:36:18,640 --> 00:36:22,420
Miło mi. Zaniosę do samochodu.

418
00:36:26,990 --> 00:36:29,860
- Co robisz?
- Wyjeżdżam.

419
00:36:30,320 --> 00:36:32,570
Dokąd? Dlaczego?

420
00:36:41,670 --> 00:36:44,990
Stąd do ciebie dzwoniłam.

421
00:36:46,080 --> 00:36:50,690
Tu mogłam z tobą rozmawiać...

422
00:36:51,340 --> 00:36:54,600
i być taka, jakiej mnie chciałeś.

423
00:36:54,680 --> 00:36:56,930
Więc zostań.

424
00:36:58,850 --> 00:37:01,770
Sprzedałam tartak.

425
00:37:01,850 --> 00:37:05,280
To koniec, wracam do domu.

426
00:37:05,370 --> 00:37:06,960
Nie.

427
00:37:17,110 --> 00:37:21,070
- Przykro mi. Nie myśl o mnie źle.
- Panie Lee...

428
00:37:21,770 --> 00:37:22,960
proszę nas zostawić.

429
00:37:29,410 --> 00:37:31,980
Nie możesz wyjechać.

430
00:37:32,070 --> 00:37:34,680
Mówisz jako policjant?

431
00:37:36,840 --> 00:37:38,250
Nie.

432
00:37:40,430 --> 00:37:43,520
- Lepiej o mnie zapomnij.
- Kocham cię.

433
00:37:54,780 --> 00:37:55,870
Josie, kocham cię.

434
00:38:14,130 --> 00:38:18,030
- Może kieliszek na trawienie?
- Nie, dziękuję.

435
00:38:19,000 --> 00:38:22,820
Pan ma mój czek na pięć milionów,

436
00:38:23,530 --> 00:38:26,540
a ja nic.

437
00:38:27,230 --> 00:38:28,150
Dlaczego?

438
00:38:33,690 --> 00:38:37,760
Właściwie nic o panu nie wiem.

439
00:38:37,840 --> 00:38:41,720
To małe miasteczko i jestem ostrożny.

440
00:38:41,800 --> 00:38:44,910
Dbam o interesy całej społeczności.

441
00:38:45,710 --> 00:38:51,130
Ta transakcja wywrze na nią
wielki wpływ.

442
00:38:52,150 --> 00:38:55,180
Tracę czas.

443
00:38:55,270 --> 00:38:56,720
Zrezygnujemy.

444
00:38:58,160 --> 00:39:00,970
Nie, bardzo pana proszę.

445
00:39:01,260 --> 00:39:04,080
Proszę mnie zrozumieć.

446
00:39:04,170 --> 00:39:07,710
Pożar tartaku ma mnóstwo
nieprzewidzianych skutków.

447
00:39:07,800 --> 00:39:10,110
Zarówno emocjonalnych,
jak i finansowych.

448
00:39:10,920 --> 00:39:17,420
Jako odpowiedzialny obywatel
muszę zadbać o ich złagodzenie.

449
00:39:17,510 --> 00:39:19,680
Choćbym musiał...

450
00:39:20,560 --> 00:39:24,080
chwilowo wstrzymać się z inwestycjami.

451
00:39:24,330 --> 00:39:28,710
Pożary mnie nie wzruszają,
pochodzę z Nagasaki.

452
00:39:32,020 --> 00:39:33,580
Przykro mi.

453
00:39:48,350 --> 00:39:50,510
Przepraszam na chwilę.

454
00:39:59,750 --> 00:40:02,110
- Skąd pan pochodzi?
- Jestem miejscowy.

455
00:40:02,200 --> 00:40:06,010
Tutejszy. Wspaniale. A panowie?

456
00:40:42,560 --> 00:40:44,570
Dziękuję.

457
00:40:44,650 --> 00:40:47,500
Nie ma lepszej publiczności.

458
00:40:47,580 --> 00:40:51,250
- Dobrej nocy, jedźcie ostrożnie.
- To Król i ja.

459
00:40:51,590 --> 00:40:53,280
Daj już spokój.

460
00:40:54,310 --> 00:40:56,750
Chwilowa niepoczytalność?

461
00:40:56,840 --> 00:41:01,010
Jeśli ktoś z nich trafi między
przysięgłych, to już wygrałeś.

462
00:41:04,440 --> 00:41:05,810
Lubi pan musicale?

463
00:41:09,070 --> 00:41:10,690
Nie.

464
00:41:14,730 --> 00:41:17,440
Nawet Skrzypka na dachu?

465
00:41:18,180 --> 00:41:21,740
Mnie rozczula do łez.

466
00:41:22,070 --> 00:41:27,240
To fantazje, w których brak sensu.

467
00:41:37,880 --> 00:41:41,050
Nie jest pan tutejszy, prawda?

468
00:41:43,750 --> 00:41:45,640
Przyjezdny.

469
00:41:48,340 --> 00:41:50,020
Może postawię panu sake?

470
00:41:51,230 --> 00:41:52,270
Nie, dziękuję.

471
00:41:57,040 --> 00:42:00,030
To w takim razie zimne mleko?

472
00:42:06,800 --> 00:42:09,160
Dajcie mi moje lekarstwo.

473
00:42:09,870 --> 00:42:11,670
Czuję przemianę.

474
00:42:11,760 --> 00:42:13,500
Jaką?

475
00:42:13,590 --> 00:42:19,380
Znaleziony specyfik zawiera
składniki typowe dla haloperidolu.

476
00:42:19,470 --> 00:42:22,230
Ma pan schizofrenię?

477
00:42:22,310 --> 00:42:23,940
Rozszczepienie osobowości?

478
00:42:24,030 --> 00:42:25,690
Za późno.

479
00:42:26,990 --> 00:42:28,760
Zna go pan, prawda?

480
00:42:29,010 --> 00:42:30,850
Dlaczego pan kłamał?

481
00:42:31,390 --> 00:42:35,040
Nie ja. Pan nie rozumie, to nie ja.

482
00:42:35,130 --> 00:42:36,860
- Dajmy mu to.
- Jeszcze nie.

483
00:42:37,080 --> 00:42:40,940
Dając mu to, nie poznamy
jego drugiej twarzy.

484
00:42:41,970 --> 00:42:43,900
- Już.
- Nie.

485
00:43:05,380 --> 00:43:07,890
Nie ma potrzeby.

486
00:43:08,210 --> 00:43:10,600
Nie czuję bólu.

487
00:43:13,520 --> 00:43:14,560
Kim jesteś?

488
00:43:16,890 --> 00:43:18,950
Nazywam się Mike.

489
00:43:19,030 --> 00:43:23,990
- Czym jesteś?
- Duchem, który go posiadł.

490
00:43:26,250 --> 00:43:27,860
A Phillip Gerard?

491
00:43:28,850 --> 00:43:31,330
To mój gospodarz.

492
00:43:31,920 --> 00:43:34,000
W moim śnie mówiłeś...

493
00:43:35,330 --> 00:43:37,870
o Bobie.

494
00:43:38,710 --> 00:43:43,380
Był mi znajomy.

495
00:43:44,190 --> 00:43:45,670
Skąd przybył?

496
00:43:46,450 --> 00:43:50,470
Tego nie wyjawię.

497
00:43:54,400 --> 00:43:55,850
Czego chce?

498
00:43:57,600 --> 00:43:59,580
To jest Bob,

499
00:43:59,660 --> 00:44:01,800
na zabawę czeka.

500
00:44:01,880 --> 00:44:04,920
Gdy się uśmiechnie,

501
00:44:05,010 --> 00:44:07,820
to każdy ucieka.

502
00:44:10,770 --> 00:44:13,450
Wiecie, czym jest pasożyt?

503
00:44:13,900 --> 00:44:19,170
Żeruje na innym organizmie.

504
00:44:21,190 --> 00:44:25,440
Bob wybiera ludzi.

505
00:44:25,530 --> 00:44:32,330
Żywi się ich strachem i przyjemnościami.

506
00:44:32,410 --> 00:44:35,680
Są jego dziećmi.

507
00:44:39,220 --> 00:44:42,140
Jestem podobny.

508
00:44:43,290 --> 00:44:45,150
Działaliśmy razem.

509
00:44:47,590 --> 00:44:51,580
Wśród przyszłych czasów minionych

510
00:44:51,670 --> 00:44:54,370
czarodziej zbiec pragnie

511
00:44:54,850 --> 00:45:00,960
przez otchłań ciemną,
między dwoma światami.

512
00:45:01,040 --> 00:45:02,460
Ogniu...

513
00:45:03,270 --> 00:45:06,750
krocz ze mną.

514
00:45:09,380 --> 00:45:15,600
Ale ujrzałem oblicze Boga

515
00:45:15,800 --> 00:45:19,310
i zostałem oczyszczony.

516
00:45:20,820 --> 00:45:26,770
Odciąłem rękę i pozostaję
blisko mojego gospodarza,

517
00:45:26,850 --> 00:45:31,650
zjawiając się czasem w jednym celu.

518
00:45:32,580 --> 00:45:34,910
- Żeby znaleźć Boba.
- Powstrzymać go!

519
00:45:35,900 --> 00:45:38,840
To jego prawdziwa twarz...

520
00:45:41,020 --> 00:45:43,490
ale niewielu ją widzi.

521
00:45:44,920 --> 00:45:46,960
Tylko wybrani...

522
00:45:48,850 --> 00:45:52,380
i przeklęci.

523
00:45:53,640 --> 00:45:55,550
Jest teraz w pobliżu?

524
00:45:57,470 --> 00:45:59,470
Od blisko 40 lat.

525
00:46:00,350 --> 00:46:01,180
Gdzie?

526
00:46:04,980 --> 00:46:10,360
W wielkim domu z drewna,
otoczonym lasem.

527
00:46:11,280 --> 00:46:16,710
Jest tam wiele
podobnych do siebie pokoi,

528
00:46:16,800 --> 00:46:20,970
ale zamieszkanych przez różne dusze,

529
00:46:21,050 --> 00:46:22,500
noc po nocy.

530
00:46:25,370 --> 00:46:27,630
To hotel Great Northern.

