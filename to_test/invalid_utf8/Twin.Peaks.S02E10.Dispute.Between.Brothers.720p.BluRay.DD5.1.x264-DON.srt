1
00:00:28,850 --> 00:00:33,590
MIASTECZKO TWIN PEAKS

2
00:01:58,050 --> 00:02:01,880
TRZY DNI PÓŹNIEJ

3
00:02:02,610 --> 00:02:04,500
Saro...

4
00:02:05,370 --> 00:02:07,710
zrobię ci zastrzyk.

5
00:02:11,910 --> 00:02:13,880
Doktorze...

6
00:02:13,970 --> 00:02:16,200
nie chcę.

7
00:02:16,290 --> 00:02:18,470
Nie chcę.

8
00:02:18,560 --> 00:02:20,730
Wolę tam odejść.

9
00:02:20,820 --> 00:02:25,990
Całym sercem pragnę być z nimi.

10
00:02:27,990 --> 00:02:29,990
Dziś pochowałam męża...

11
00:02:31,900 --> 00:02:35,290
obok jedynej córki.

12
00:02:36,340 --> 00:02:42,910
Jej grób wciąż jest świeży.
Jeszcze nie porósł trawą.

13
00:02:46,430 --> 00:02:51,600
Pani Palmer, są na świecie
siły mroczne i ohydne,

14
00:02:51,690 --> 00:02:54,350
tak straszne, że nie mówimy
o nich dzieciom.

15
00:02:55,590 --> 00:03:01,270
Pani mąż padł ich ofiarą dawno temu,
gdy był niewinny i ufny.

16
00:03:01,990 --> 00:03:05,850
Leland tego nie zrobił.
Nie ten, którego pani znała.

17
00:03:05,930 --> 00:03:07,570
Nie.

18
00:03:07,650 --> 00:03:10,340
To tamten...

19
00:03:11,450 --> 00:03:15,990
z długimi, brudnymi, wstrętnymi włosami.

20
00:03:16,070 --> 00:03:19,030
Odszedł na zawsze.

21
00:03:22,790 --> 00:03:25,690
Jak wszystko, co kochałam.

22
00:03:29,740 --> 00:03:34,720
Pomoże to pani, jeśli powiem,
co się stało przed jego śmiercią?

23
00:03:36,550 --> 00:03:43,600
Trudno pojąć rozumem i sercem
czyny pani męża.

24
00:03:44,600 --> 00:03:50,280
Świadomie panią odurzał, żeby to ukryć.

25
00:03:50,370 --> 00:03:56,570
Ale zanim umarł, uświadomił sobie,
czego się dopuścił wobec Laury

26
00:03:56,660 --> 00:04:00,710
i jak straszne zadał pani cierpienia.

27
00:04:02,060 --> 00:04:05,100
Odszedł w pokoju.

28
00:04:06,020 --> 00:04:10,230
W ostatnich chwilach ujrzał Laurę.

29
00:04:10,310 --> 00:04:18,820
Powtarzał, jak bardzo ją kochał,
i wierzę, że mu wybaczyła.

30
00:04:24,100 --> 00:04:25,150
Czas na nas.

31
00:04:41,630 --> 00:04:44,670
Chętnie panią odwiozę.

32
00:04:53,460 --> 00:04:57,240
Leland zawsze umiał znaleźć drugi klips.

33
00:06:29,090 --> 00:06:31,850
Chciałbym wziąć trochę sałatki.

34
00:06:51,180 --> 00:06:53,130
To dla Sary.

35
00:06:58,560 --> 00:07:00,080
Proszę.

36
00:07:00,740 --> 00:07:02,610
Zdążyła pani.

37
00:07:02,690 --> 00:07:04,910
Dziękuję bardzo.

38
00:07:09,700 --> 00:07:13,120
Zaprosili ich czy sami przyszli?

39
00:07:14,510 --> 00:07:17,460
W takich chwilach ludzie chcą być blisko.

40
00:07:17,540 --> 00:07:20,080
Wyciągają do siebie rękę.

41
00:07:20,820 --> 00:07:26,160
Po śmierci mojej mamy
przyszło ze sto osób.

42
00:07:32,000 --> 00:07:35,010
W tym mieście wszystko się wali.

43
00:07:36,220 --> 00:07:39,550
A James myśli, że to jego wina.

44
00:07:39,640 --> 00:07:42,240
Najpierw Laura, potem Maddie.

45
00:07:42,320 --> 00:07:45,610
Może i mnie obwinia.

46
00:07:45,700 --> 00:07:48,770
Nie mogliśmy tego wywołać, prawda?

47
00:07:49,230 --> 00:07:51,130
Dziecko...

48
00:07:52,130 --> 00:07:54,200
on wróci.

49
00:07:54,930 --> 00:07:57,150
Wszystko się ułoży.

50
00:08:03,930 --> 00:08:05,990
Witamy w domu, doktorze.

51
00:08:06,500 --> 00:08:09,780
Wydaje się, że w pełni
doszedł pan do siebie.

52
00:08:09,860 --> 00:08:14,440
Tylko dzięki leczniczej mocy
zatoki Hanalei.

53
00:08:15,420 --> 00:08:16,420
Co słychać?

54
00:08:18,370 --> 00:08:22,190
Co ci jest? Kobita cię nie karmi?

55
00:08:24,970 --> 00:08:27,000
Zazdrościsz!

56
00:08:27,080 --> 00:08:28,840
Ja?

57
00:08:28,920 --> 00:08:34,060
Nie mam ochoty marnować życia
na zmienianie pieluch.

58
00:08:34,590 --> 00:08:36,190
Co pan teraz zamierza?

59
00:08:36,710 --> 00:08:44,770
Jeszcze nie wiem.
Mam parę tygodni zaległego urlopu.

60
00:08:44,860 --> 00:08:50,500
Mogę panu zaproponować
przyjemny wieczór z wędką.

61
00:08:51,510 --> 00:08:52,500
Zgoda.

62
00:08:52,660 --> 00:08:54,450
Eddie...

63
00:08:54,540 --> 00:08:59,550
czy majtki odbijają mi się w butach?

64
00:09:01,840 --> 00:09:06,020
Zdawało mi się, że je widzę.

65
00:09:08,780 --> 00:09:10,980
Myślisz, że chłopcy
zaglądają mi pod sukienkę?

66
00:09:14,100 --> 00:09:16,770
W twoich butach nic się nie odbija,

67
00:09:17,100 --> 00:09:18,950
słowo daję.

68
00:09:28,990 --> 00:09:32,040
Donna przyszła kiedyś do Laury

69
00:09:32,130 --> 00:09:34,770
i wiecie, co zrobiły?

70
00:09:34,860 --> 00:09:43,580
Było późno, uprażyliśmy popcorn,
a one poprzysięgły sobie

71
00:09:43,670 --> 00:09:46,380
wieczną przyjaźń.

72
00:09:47,460 --> 00:09:52,260
Coś takiego wiąże po grób.

73
00:09:57,940 --> 00:10:00,460
Chcę to zapamiętać.

74
00:10:02,010 --> 00:10:05,540
Ta baba to wiedźma.

75
00:10:11,950 --> 00:10:14,070
Panowie...

76
00:10:14,160 --> 00:10:17,750
Wsadzę ci łeb do ścieku,
gdzie jego miejsce!

77
00:10:20,190 --> 00:10:23,790
Uspokójcie się! Przestańcie!

78
00:10:23,880 --> 00:10:26,840
Pamiętajcie, gdzie jesteście i po co.

79
00:10:26,920 --> 00:10:28,380
Proszę!

80
00:10:33,590 --> 00:10:35,460
Dobrze się bawią.

81
00:10:38,810 --> 00:10:41,480
To burmistrz i jego brat.

82
00:10:41,560 --> 00:10:43,500
Dougie wydaje gazetę.

83
00:10:43,590 --> 00:10:47,270
Spierają się od 50 lat.

84
00:10:47,350 --> 00:10:51,270
Nie wiadomo, od czego się zaczęło.
Chyba poszło o dziewczynę.

85
00:10:51,350 --> 00:10:53,330
Sami już nie pamiętają.

86
00:10:53,410 --> 00:10:56,060
Teraz znów się czubią,

87
00:10:56,140 --> 00:11:00,310
bo Dougie żeni się z młodą panną.

88
00:11:00,390 --> 00:11:02,600
Piąty raz.

89
00:11:02,680 --> 00:11:06,380
Ona nie ma 20 lat, a on skończył 110.

90
00:11:06,460 --> 00:11:10,740
Na przełomie grudnia i stycznia.

91
00:11:10,820 --> 00:11:14,180
Pamiętacie, kiedy Dwayne
pierwszy raz został burmistrzem?

92
00:11:16,570 --> 00:11:19,730
W '62? Tak, w 1962.

93
00:11:20,320 --> 00:11:25,360
Dougie obsmarował go wtedy w gazecie,
a Dwayne i tak nie miał konkurencji.

94
00:11:28,690 --> 00:11:31,620
Będzie mi brakowało tego miasteczka.

95
00:11:40,620 --> 00:11:46,090
Mam przyjąć do klasy maturalnej
35-letnią kobietę?

96
00:11:46,990 --> 00:11:49,140
Otóż to.

97
00:11:49,220 --> 00:11:52,350
Długo jeszcze? Zaraz będzie dzwonek.

98
00:11:52,440 --> 00:11:55,240
Zaczekaj. Jeszcze chwilę.

99
00:11:55,320 --> 00:11:56,720
Zgadnij co.

100
00:11:56,800 --> 00:12:00,650
Dziś są eliminacje cheerleaderek.
Czytałam na tablicy ogłoszeń.

101
00:12:00,730 --> 00:12:02,770
To wspaniale.

102
00:12:04,780 --> 00:12:07,220
Może idź poćwiczyć?

103
00:12:24,520 --> 00:12:26,080
Obsługa?

104
00:12:28,180 --> 00:12:30,240
Dział skarg i wniosków.

105
00:12:30,330 --> 00:12:33,510
Czy jest pan zadowolony z pobytu u nas?

106
00:12:34,130 --> 00:12:36,140
Nie mam żadnych zastrzeżeń.

107
00:12:38,020 --> 00:12:39,840
Wyjeżdża pan?

108
00:12:39,920 --> 00:12:41,650
Na ryby.

109
00:12:42,660 --> 00:12:44,060
Ale pan wyjedzie.

110
00:12:45,860 --> 00:12:48,110
Tak.

111
00:12:54,890 --> 00:12:58,200
To koniec? Ocalił mnie pan
i złamał mi serce.

112
00:12:59,520 --> 00:13:02,540
Tłumaczyłem ci, że się nie angażuję.

113
00:13:02,630 --> 00:13:05,560
Wiem, jestem nastolatką.

114
00:13:05,640 --> 00:13:07,670
I byłaś wmieszana w moje dochodzenie.

115
00:13:16,250 --> 00:13:18,810
Ktoś musiał pana bardzo skrzywdzić.

116
00:13:21,950 --> 00:13:25,430
Nie, to ja kogoś skrzywdziłem...

117
00:13:25,510 --> 00:13:27,690
i to się nie powtórzy.

118
00:13:30,630 --> 00:13:32,950
Co się stało? Umarła?

119
00:13:33,560 --> 00:13:36,990
Tak właśnie było. Jesteś ciekawa?

120
00:13:42,770 --> 00:13:46,140
Była świadkiem w sprawie federalnej.

121
00:13:46,960 --> 00:13:51,070
Mieliśmy jej nie spuszczać z oka.

122
00:13:51,870 --> 00:13:53,640
Mój partner i ja.

123
00:13:54,430 --> 00:13:56,670
To był Windom Earle.

124
00:13:57,560 --> 00:13:59,970
Nauczył mnie wszystkiego,
co musi wiedzieć agent.

125
00:14:01,540 --> 00:14:04,810
A gdy dokonano zamachu na jej życie,

126
00:14:05,630 --> 00:14:07,940
nawaliłem...

127
00:14:08,020 --> 00:14:10,030
bo ją kochałem.

128
00:14:13,030 --> 00:14:15,300
Umarła mi na rękach.

129
00:14:19,210 --> 00:14:21,580
Zostałem ciężko ranny,

130
00:14:21,660 --> 00:14:23,750
a mój partner oszalał.

131
00:14:26,390 --> 00:14:27,920
To ci wystarczy?

132
00:14:34,580 --> 00:14:37,250
Lubię cię i zależy mi na tobie.

133
00:14:37,330 --> 00:14:39,510
Zawsze będziemy przyjaciółmi.

134
00:14:41,310 --> 00:14:44,430
Przyjaźń to podstawa trwałych związków.

135
00:14:45,450 --> 00:14:47,310
Miło, że o tym pamiętasz.

136
00:14:49,860 --> 00:14:52,210
Coś panu powiem.

137
00:14:52,290 --> 00:14:58,780
Zanim pan się obejrzy, dorosnę
i wtedy niech się pan strzeże.

138
00:15:00,020 --> 00:15:02,160
Dobra, umowa stoi.

139
00:15:06,170 --> 00:15:08,270
Ma pan tylko jedną wadę:

140
00:15:08,350 --> 00:15:10,590
jest pan ideałem.

141
00:15:27,970 --> 00:15:29,720
Za duży...

142
00:15:30,610 --> 00:15:32,910
ale świetny.

143
00:15:38,260 --> 00:15:40,560
Leo niedawno go kupił.

144
00:15:41,490 --> 00:15:43,990
Na nim leży lepiej.

145
00:15:45,780 --> 00:15:49,150
Szkoda, żeby się zmarnował.

146
00:15:49,230 --> 00:15:50,880
Który lepszy?

147
00:15:50,960 --> 00:15:54,090
Zielony czy żółty?

148
00:15:55,240 --> 00:15:59,620
Chcę dobrze wypaść.
Ben Horne zwraca na to uwagę.

149
00:15:59,700 --> 00:16:02,270
Wyglądasz wspaniale.

150
00:16:03,590 --> 00:16:06,770
Wyjdziemy wieczorem? Też się wystroję.

151
00:16:07,830 --> 00:16:10,520
A Leo? Zostawisz go tak?

152
00:16:14,820 --> 00:16:17,990
Znajdę opiekunkę.
Muszę się stąd wyrwać.

153
00:16:18,490 --> 00:16:22,040
Kochana, posłuchaj.

154
00:16:22,120 --> 00:16:26,200
Jeśli dogadam się z Horne'em,

155
00:16:26,280 --> 00:16:28,350
wyjdziemy na prostą.

156
00:16:28,430 --> 00:16:31,210
Zrobię dla ciebie rzeczy,
o jakich nie marzyłaś.

157
00:16:32,020 --> 00:16:37,950
Wytrzymaj i zdaj się na mnie.

158
00:16:38,030 --> 00:16:40,860
Dla dobra nas obojga.

159
00:16:44,180 --> 00:16:46,430
Spadam.

160
00:16:47,050 --> 00:16:51,770
Trzymaj kciuki. Do dzieła! Na razie.

161
00:17:13,840 --> 00:17:15,210
Cześć, Harry.

162
00:17:17,360 --> 00:17:19,940
Wybacz, że spytam, Catherine...

163
00:17:20,660 --> 00:17:22,180
ale nie zginęłaś?

164
00:17:24,740 --> 00:17:26,510
Pete wie?

165
00:17:26,590 --> 00:17:29,910
Widziałam się już z mężem.

166
00:17:30,000 --> 00:17:33,920
Skoro tak, to witaj w domu.

167
00:17:34,860 --> 00:17:36,570
Masz pytania?

168
00:17:36,660 --> 00:17:38,850
Kilka.

169
00:17:38,930 --> 00:17:41,720
Jestem podejrzana?

170
00:17:41,800 --> 00:17:46,930
To zależy od twoich odpowiedzi.
Możesz wezwać adwokata.

171
00:17:48,390 --> 00:17:51,800
To nie będzie konieczne.
Nie mam nic do ukrycia.

172
00:17:51,880 --> 00:17:54,550
Dobrze.

173
00:17:54,640 --> 00:17:59,980
Ale gdzie byłaś przez dwa tygodnie?

174
00:18:03,080 --> 00:18:05,830
Wierzysz w opiekuńcze anioły?

175
00:18:05,920 --> 00:18:08,360
W anioły?

176
00:18:08,450 --> 00:18:14,140
Prawdę mówiąc,
ostatnio nie wiem, w co wierzyć.

177
00:18:15,830 --> 00:18:17,990
Wierzę...

178
00:18:18,070 --> 00:18:20,780
że to anioł mnie uratował.

179
00:18:22,340 --> 00:18:24,990
W noc pożaru byłam w domu.

180
00:18:25,080 --> 00:18:28,690
Zadzwonił ktoś o nieznajomym głosie.

181
00:18:28,780 --> 00:18:33,110
Kazał mi przyjść do suszarni.
Wyczułam zagrożenie

182
00:18:33,200 --> 00:18:37,960
i wzięłam ze sobą broń.
Ta dziewczyna... Jak jej tam?

183
00:18:38,750 --> 00:18:40,390
Shelly Johnson.

184
00:18:40,480 --> 00:18:43,380
Wisiała tam związana.

185
00:18:43,910 --> 00:18:46,430
Chyba coś wybuchło.

186
00:18:46,510 --> 00:18:48,480
Potem niewiele pamiętam,

187
00:18:48,560 --> 00:18:52,590
tylko ścianę ognia i krzyk.

188
00:18:52,670 --> 00:18:57,860
Nie wiem, jak uciekłam,
ale ocknęłam się w lesie.

189
00:19:00,800 --> 00:19:02,860
Bałam się.

190
00:19:05,490 --> 00:19:08,490
Pierwszy raz w życiu się bałam.

191
00:19:09,170 --> 00:19:12,270
Szłam przez całą noc...

192
00:19:13,920 --> 00:19:16,510
a gdy się rozjaśniło,

193
00:19:16,590 --> 00:19:19,250
zaczęłam poznawać okolicę:

194
00:19:19,660 --> 00:19:24,910
drzewa, ścieżkę, skały...

195
00:19:26,010 --> 00:19:28,000
Wtedy...

196
00:19:29,350 --> 00:19:31,330
dopadły mnie...

197
00:19:32,500 --> 00:19:34,720
wspomnienia z dzieciństwa.

198
00:19:35,460 --> 00:19:40,040
Pomyślałam, że tak wygląda niebo,

199
00:19:40,120 --> 00:19:44,470
i wtedy trafiłam do naszego
starego letniego domku.

200
00:19:46,690 --> 00:19:52,860
Aż do Pearl Lakes.
Dotarłam tam na ostatnich nogach.

201
00:19:52,950 --> 00:19:56,500
Tylko anioł mógł
mnie tam przyprowadzić.

202
00:20:01,540 --> 00:20:03,160
Dziękuję.

203
00:20:03,940 --> 00:20:07,800
To szczęście, że spiżarnia
zawsze była pełna.

204
00:20:07,880 --> 00:20:12,480
Weszłam i otworzyłam puszkę z tuńczykiem.

205
00:20:12,560 --> 00:20:16,640
I czekałam na mordercę,
który chciał mnie zabić.

206
00:20:17,660 --> 00:20:20,570
Nie wypuszczałam broni z ręki...

207
00:20:20,650 --> 00:20:24,990
w obawie, że każda chwila
może być tą ostatnią.

208
00:20:25,070 --> 00:20:27,990
Dlaczego wróciłaś?

209
00:20:29,310 --> 00:20:31,680
Tuńczyk mi się skończył.

210
00:20:32,710 --> 00:20:35,410
Wspaniale, rzuciłem palenie.

211
00:20:35,490 --> 00:20:38,540
- Co tu robisz?
- Chciałem cię zobaczyć.

212
00:20:38,620 --> 00:20:40,550
Chciałem...

213
00:20:40,640 --> 00:20:45,290
Tak naprawdę pomówić o naszym dzidziusiu.

214
00:20:45,380 --> 00:20:47,270
O dzidziusiu?

215
00:20:47,350 --> 00:20:51,190
Przeżyłem wstrząs i wiesz, co odkryłem?

216
00:20:51,270 --> 00:20:55,010
Ja, Richard Tremayne,
jestem koszmarnym nudziarzem

217
00:20:55,090 --> 00:20:59,970
i rozpaczliwie szukam czegoś...

218
00:21:00,050 --> 00:21:06,250
kogoś, kim mógłbym się opiekować.

219
00:21:06,330 --> 00:21:13,990
I chyba dlatego
tak mnie pociąga rodzicielstwo.

220
00:21:14,070 --> 00:21:16,980
- Rodzicielstwo?
- Dokładniej ojcostwo.

221
00:21:17,070 --> 00:21:19,450
Nie mam w tym doświadczenia,

222
00:21:20,020 --> 00:21:23,210
to dla mnie co najmniej nowość.

223
00:21:23,290 --> 00:21:28,470
Zgłosiłem się więc do opieki społecznej

224
00:21:28,550 --> 00:21:33,390
i przydzielili mi pewną uroczą sierotkę.

225
00:21:35,730 --> 00:21:41,310
Czy to ten sam Dick Tremayne
ze sklepu Horne'a?

226
00:21:41,390 --> 00:21:42,760
Ten sam.

227
00:21:42,840 --> 00:21:46,310
Powiem więcej. Jestem pewien,

228
00:21:47,030 --> 00:21:49,310
że to dziecko jest moje.

229
00:21:50,920 --> 00:21:52,540
Lucy!

230
00:21:54,230 --> 00:21:58,640
Lucy, Dick, mam coś do powiedzenia.

231
00:22:00,230 --> 00:22:02,310
Co takiego?

232
00:22:03,070 --> 00:22:04,620
Chcę powiedzieć,

233
00:22:05,160 --> 00:22:08,130
że znaleźliśmy się w trudnym położeniu.

234
00:22:08,210 --> 00:22:11,380
Pochodzę z licznej rodziny
i mama zawsze mówiła,

235
00:22:11,460 --> 00:22:15,630
że nie znosi awantur,
gdy spodziewa się dziecka.

236
00:22:16,570 --> 00:22:18,460
Póki nie wiemy na pewno,

237
00:22:18,540 --> 00:22:22,730
kto będzie rzucał piłką
albo zbuduje dom dla lalek,

238
00:22:23,450 --> 00:22:25,440
żyjmy w zgodzie.

239
00:22:44,310 --> 00:22:48,370
Pogadajcie sobie, a ja przyjdę później.

240
00:22:53,400 --> 00:22:55,300
Zwariowałeś?

241
00:22:55,380 --> 00:23:00,900
Znam Lucy i wiem, że ceni
moralną i męską postawę.

242
00:23:02,220 --> 00:23:04,100
Czyżbym przesadził?

243
00:23:11,500 --> 00:23:14,430
Czas się pożegnać.

244
00:23:14,520 --> 00:23:20,000
Major Briggs obiecał mi
dorodnego pstrąga z patelni.

245
00:23:20,080 --> 00:23:23,460
Dlatego uznałem, że to ci się przyda.

246
00:23:33,730 --> 00:23:34,880
Harry...

247
00:23:38,420 --> 00:23:40,930
jest fantastyczna.

248
00:23:41,860 --> 00:23:44,600
Gdy pstrągi płyną w górę rzeki,

249
00:23:44,690 --> 00:23:48,460
w głowie mają wyłącznie seks.

250
00:23:48,540 --> 00:23:51,880
Działa na nie tylko zielona mucha.

251
00:23:51,970 --> 00:23:54,820
Zielona mucha...

252
00:23:58,270 --> 00:24:01,620
Sam ją zrobiłem. Tata mnie nauczył,

253
00:24:02,150 --> 00:24:04,440
a jego – dziadek.

254
00:24:04,520 --> 00:24:06,250
Nie wiem, co powiedzieć.

255
00:24:07,280 --> 00:24:09,150
I jeszcze jedno.

256
00:24:13,710 --> 00:24:15,960
Wezmę to.

257
00:24:20,870 --> 00:24:23,050
Naszywka Księgarzy.

258
00:24:25,590 --> 00:24:29,020
Uznaliśmy, że jesteś jednym z nas.

259
00:24:33,010 --> 00:24:36,490
Nie umiem wyrazić,
jaki to dla mnie zaszczyt.

260
00:24:36,580 --> 00:24:38,360
Noś ją na zdrowie.

261
00:24:38,960 --> 00:24:42,530
Zawsze możesz na nas liczyć.

262
00:24:55,830 --> 00:24:57,340
Hawk...

263
00:24:58,140 --> 00:25:01,300
Gdybym się zgubił, chciałbym,
żebyś to ty mnie szukał.

264
00:25:01,960 --> 00:25:05,180
Oby wiatr zawsze wiał ci w plecy.

265
00:25:06,730 --> 00:25:08,270
Andy...

266
00:25:08,730 --> 00:25:11,900
twoja odwaga i szlachetność
nie mają sobie równych.

267
00:25:11,980 --> 00:25:13,560
To rzadko spotykane.

268
00:25:17,050 --> 00:25:19,690
Lucy, życzę szczęścia.

269
00:25:20,130 --> 00:25:24,370
I zaproś mnie na ślub,
z kimkolwiek go weźmiesz.

270
00:25:28,700 --> 00:25:31,580
- Szeryfie Truman?
- Słucham.

271
00:25:31,970 --> 00:25:34,460
To agent Roger Hardy z FBI.

272
00:25:34,840 --> 00:25:36,380
Roger, co ty tu robisz?

273
00:25:36,730 --> 00:25:41,750
Dale, mamy problem.
Z przykrością informuję cię,

274
00:25:41,830 --> 00:25:46,010
że zostałeś zawieszony
ze skutkiem natychmiastowym.

275
00:25:55,020 --> 00:25:58,160
- Wiesz, dlaczego tu jestem.
- Ja nie wiem.

276
00:25:58,250 --> 00:26:01,480
- Sprawy wewnętrzne.
- Kontrolujemy działania agentów.

277
00:26:01,560 --> 00:26:04,120
A sprawa dotyczy przejścia do Kanady.

278
00:26:04,200 --> 00:26:07,350
To miało związek z prowadzonym śledztwem.

279
00:26:07,430 --> 00:26:09,750
Nie zawiadomiłeś nas.

280
00:26:09,830 --> 00:26:13,300
- Podwójna niesubordynacja?
- Co?

281
00:26:13,380 --> 00:26:17,500
Samowolnie podjąłem
z zasady bezprawne działania.

282
00:26:17,590 --> 00:26:19,920
- Sprawa Audrey Horne.
- Nie tylko.

283
00:26:20,000 --> 00:26:24,280
Są też zastrzeżenia
co do twoich pobudek i metod.

284
00:26:24,360 --> 00:26:25,620
Jakie?

285
00:26:25,700 --> 00:26:28,270
Czekam na akta sprawy.

286
00:26:28,350 --> 00:26:30,420
Pomówimy za godzinę.

287
00:26:30,500 --> 00:26:32,210
Po kolei.

288
00:26:46,200 --> 00:26:47,870
Nie był pan umówiony.

289
00:26:47,960 --> 00:26:52,220
Pan Horne może pana przyjąć
w przyszłym miesiącu.

290
00:26:53,030 --> 00:26:57,280
Powiedz, że chodzi o taśmę,
którą dostał wczoraj, i że to pilne.

291
00:26:57,370 --> 00:27:00,120
Bardzo proszę!

292
00:27:05,720 --> 00:27:08,230
Halloween już było.

293
00:27:09,010 --> 00:27:10,850
Co?

294
00:27:12,720 --> 00:27:14,980
Przebrałeś się za lwa salonowego?

295
00:27:16,910 --> 00:27:19,610
Ty nie w szkole?

296
00:27:21,010 --> 00:27:23,820
Nudzi mnie. A ty?

297
00:27:25,310 --> 00:27:27,280
Chcę pogadać z twoim ojcem.

298
00:27:28,960 --> 00:27:31,160
A o czym?

299
00:27:31,740 --> 00:27:35,120
O pracy. To moja sprawa.

300
00:27:37,260 --> 00:27:40,040
Nie wiedziałam, że szuka pracowników.

301
00:27:40,500 --> 00:27:44,210
Najpierw chcę to z nim omówić.

302
00:27:46,800 --> 00:27:51,220
Ty coś kombinujesz
i nie zamierzam w to wnikać.

303
00:27:51,310 --> 00:27:52,220
Policz do 10.

304
00:27:57,440 --> 00:27:59,090
Raz...

305
00:27:59,170 --> 00:28:00,720
dwa...

306
00:28:00,800 --> 00:28:02,870
trzy...

307
00:28:02,950 --> 00:28:04,790
cztery...

308
00:28:04,880 --> 00:28:06,690
pięć...

309
00:28:10,970 --> 00:28:14,620
Grunt to wiedzieć, do kogo się zwrócić.

310
00:28:14,970 --> 00:28:16,390
Audrey!

311
00:28:20,100 --> 00:28:21,710
Dzięki.

312
00:28:28,480 --> 00:28:31,510
Rozumiem, że jest pan zajęty,
więc przejdę do rzeczy.

313
00:28:32,180 --> 00:28:36,920
Samantho, daj packę!
Lata tu jakaś mucha!

314
00:28:40,640 --> 00:28:43,960
Popełnia pan błąd! Puszczajcie!

315
00:28:44,040 --> 00:28:46,780
To mój kolega!

316
00:28:52,280 --> 00:28:55,800
Powiedzcie szefowi, że to tylko kopia!

317
00:28:58,400 --> 00:29:00,280
W porządku?

318
00:29:03,270 --> 00:29:06,630
Wolę nie wiedzieć, o co poszło.

319
00:29:06,710 --> 00:29:09,630
Dwa razy uratowałaś mi tyłek.

320
00:29:09,710 --> 00:29:12,150
Jesteś święta.

321
00:29:12,230 --> 00:29:14,670
Jak się odwdzięczę?

322
00:29:15,720 --> 00:29:17,910
Postawisz mi lody?

323
00:29:17,990 --> 00:29:20,010
Pucharek czy rożek?

324
00:29:23,000 --> 00:29:26,360
Rożek. Lubię lizać.

325
00:29:27,980 --> 00:29:30,870
Jaki był cel wizyty w Jednookim Jacku?

326
00:29:30,950 --> 00:29:34,160
Przesłuchanie Jacques'a Renault
w sprawie śmierci Laury Palmer.

327
00:29:34,650 --> 00:29:39,220
Przeszliście przez granicę,
on został ranny przy zatrzymaniu,

328
00:29:39,310 --> 00:29:41,410
a później zamordowany w szpitalu.

329
00:29:42,300 --> 00:29:45,740
Renault był świadkiem w sprawie Laury.

330
00:29:45,820 --> 00:29:49,690
Do zabicia go przyznał się Leland Palmer.

331
00:29:49,780 --> 00:29:51,590
To też moja wina?

332
00:29:53,270 --> 00:29:54,840
A za drugim razem?

333
00:29:55,370 --> 00:29:57,760
Ratowałem Audrey Horne z rąk porywaczy.

334
00:29:58,490 --> 00:30:01,140
- Oboje nie żyją.
- Znasz przepisy.

335
00:30:01,220 --> 00:30:03,590
Przekroczenie granicy
bez wiedzy kanadyjskich władz

336
00:30:03,680 --> 00:30:06,520
to poważne wykroczenie.

337
00:30:07,110 --> 00:30:09,330
Nie przeczę.

338
00:30:09,410 --> 00:30:12,510
Czy te zabójstwa też mnie obciążają?

339
00:30:12,590 --> 00:30:13,900
To chcemy ustalić.

340
00:30:14,780 --> 00:30:18,860
Sprawa ma związek z akcją
Królewskiej Policji Konnej

341
00:30:18,940 --> 00:30:21,300
przeciwko Jeanowi Renault.

342
00:30:23,180 --> 00:30:25,610
Przez pół roku montowaliśmy zasadzkę.

343
00:30:25,700 --> 00:30:27,710
Gdy pan się wtrącił,

344
00:30:27,790 --> 00:30:29,610
Renault zbiegł,

345
00:30:29,690 --> 00:30:31,420
zginęły dwie osoby,

346
00:30:32,080 --> 00:30:35,790
a kokainę użytą w prowokacji skradziono.

347
00:30:35,870 --> 00:30:39,330
Nic nie wiem o kokainie i źle liczycie.

348
00:30:39,410 --> 00:30:41,150
Zginęły trzy osoby.

349
00:30:41,230 --> 00:30:45,430
Jean Renault zabił Blackie.
O śmierci Battisa nie wiedziałem,

350
00:30:45,510 --> 00:30:48,660
a ochroniarza zastrzeliłem
w obronie własnej.

351
00:30:48,740 --> 00:30:54,490
Mamy więc przekroczenie granicy,
trzy trupy i przemyt narkotyków.

352
00:30:55,010 --> 00:31:00,840
Przyznaję się do pierwszego
i wyjaśniłem związek z zabójstwami.

353
00:31:00,920 --> 00:31:05,460
Za dobrze mnie znasz,
by mnie podejrzewać o przemyt.

354
00:31:05,860 --> 00:31:08,110
Będziesz musiał to udowodnić.

355
00:31:08,200 --> 00:31:11,980
Dochodzenie przejął
wydział antynarkotykowy.

356
00:31:12,060 --> 00:31:15,310
Masz dobę na ustalenie obrony.

357
00:31:16,110 --> 00:31:20,960
Tymczasem oddaj broń...

358
00:31:21,050 --> 00:31:23,150
i odznakę.

359
00:31:33,780 --> 00:31:35,440
Szeryfie Truman?

360
00:31:47,950 --> 00:31:51,500
Proszę. Zadam panu kilka pytań.

361
00:31:52,600 --> 00:31:54,170
Postoję.

362
00:31:56,130 --> 00:32:02,230
Od razu powiem, że nie jest pan
współodpowiedzialny.

363
00:32:02,310 --> 00:32:06,520
Będziemy wdzięczni za pańską pomoc.

364
00:32:09,140 --> 00:32:12,800
O ile się orientuję,

365
00:32:13,500 --> 00:32:17,700
aby uzyskać moje zeznania,
musicie wystąpić o ekstradycję

366
00:32:17,790 --> 00:32:21,070
i nakaz sądu.

367
00:32:21,150 --> 00:32:29,540
A póki tego nie macie,
nie liczcie na moją współpracę.

368
00:32:31,670 --> 00:32:37,130
Ten sprzeciw może zaszkodzić panu
albo Cooperowi.

369
00:32:37,580 --> 00:32:40,620
To nie sprzeciw, a oświadczenie.

370
00:32:40,700 --> 00:32:45,060
Agent Cooper to najlepszy policjant,
jakiego znam.

371
00:32:45,150 --> 00:32:49,830
Podziwiam go, odkąd tu przyjechał.

372
00:32:51,250 --> 00:32:56,860
Nie wiem, co wiecie ani skąd,

373
00:32:56,940 --> 00:32:58,940
ale to wszystko nieprawda.

374
00:33:01,100 --> 00:33:02,660
Dziękujemy panu.

375
00:33:05,710 --> 00:33:09,890
Przy wyjściu zapraszam na kawę.

376
00:33:25,110 --> 00:33:27,200
Za niska.

377
00:33:28,400 --> 00:33:30,160
Kolej na Nadine!

378
00:33:32,070 --> 00:33:33,560
Nadine Butler.

379
00:33:36,580 --> 00:33:40,600
Prosimy, najpierw akrobacje.

380
00:33:49,620 --> 00:33:52,290
Teraz korkociąg.

381
00:33:52,840 --> 00:33:55,600
- Uważaj, zaraz polecisz.
- Akurat.

382
00:33:55,680 --> 00:33:56,920
Korkociąg!

383
00:34:23,060 --> 00:34:25,040
Nie odbieram.

384
00:34:28,860 --> 00:34:31,510
Od rana się nie odezwał.

385
00:34:39,830 --> 00:34:41,810
Dom opieki Johnsonów.

386
00:34:43,230 --> 00:34:48,220
To świetnie, że tak dobrze poszło.
To chyba najdłuższe spotkanie.

387
00:34:49,900 --> 00:34:51,780
Tak, kocham cię.

388
00:34:53,740 --> 00:34:58,790
Ale myślę, że trzeba coś zrobić
z Leo. Gdzieś go oddać.

389
00:34:59,530 --> 00:35:03,020
To nie jest warte tych pieniędzy.

390
00:35:04,830 --> 00:35:06,940
Chcę żyć.

391
00:35:13,090 --> 00:35:14,970
Ruszył się.

392
00:35:18,550 --> 00:35:19,960
Leo się poruszył.

393
00:35:26,780 --> 00:35:28,930
Czego brakuje obrusom?

394
00:35:29,020 --> 00:35:32,500
Czytałam recenzję. Wyszłam na idiotkę.

395
00:35:32,590 --> 00:35:37,140
Nie załamuj się, nie było źle.

396
00:35:37,220 --> 00:35:39,430
Nie pocieszaj mnie.

397
00:35:39,510 --> 00:35:43,020
Mam tylko ten bar
i chcę utrzymać klientów.

398
00:35:43,100 --> 00:35:46,200
Nie dbam o to, czy przyjdą nowi,

399
00:35:46,290 --> 00:35:49,450
bo raczej nie. Nie po tekście:

400
00:35:49,530 --> 00:35:56,290
"Wstąp, jeśli szukasz lokalnego
kolorytu, nie, żeby dobrze zjeść".

401
00:35:57,160 --> 00:35:59,930
To był lokalny czar, nie koloryt.

402
00:36:01,470 --> 00:36:04,220
Nauczyłaś się tego na pamięć?

403
00:36:04,300 --> 00:36:06,630
Nie, napisałam to.

404
00:36:08,410 --> 00:36:13,400
Możemy sobie darować tę maskaradę.

405
00:36:13,480 --> 00:36:16,730
M.T. Wentz to ja.

406
00:36:18,330 --> 00:36:20,450
Nie wierzę.

407
00:36:21,100 --> 00:36:24,910
Przyjechałaś, żeby mnie wdeptać w ziemię?

408
00:36:25,000 --> 00:36:29,140
Nie. Chciałam ci wystawić dobrą recenzję,

409
00:36:29,220 --> 00:36:32,080
ale nie mogłam.

410
00:36:33,210 --> 00:36:35,700
To kwestia etyki zawodowej.

411
00:36:36,120 --> 00:36:37,710
Etyki?

412
00:36:38,660 --> 00:36:40,040
Jestem twoją córką!

413
00:36:41,550 --> 00:36:43,500
Muszę się trzymać zasad.

414
00:36:46,090 --> 00:36:49,420
A gdzie zwykła przyzwoitość?

415
00:36:50,540 --> 00:36:53,380
Nie mieści się w zawodowych standardach?

416
00:36:53,460 --> 00:36:55,640
Oczywiście, że tak.

417
00:36:56,350 --> 00:36:58,990
Ja nie potraktowałabym tak nawet psa.

418
00:36:59,080 --> 00:37:02,650
- Jak zwykle przesadzasz.
- Czyżby?

419
00:37:04,180 --> 00:37:06,290
Możliwe...

420
00:37:06,380 --> 00:37:08,540
ale mam prawo.

421
00:37:08,620 --> 00:37:11,270
Tak reaguję, gdy mnie obrażają,

422
00:37:11,350 --> 00:37:14,240
a tobie nic do tego.

423
00:37:15,800 --> 00:37:18,790
- Bądź rozsądna.
- Jestem.

424
00:37:20,010 --> 00:37:23,370
Chcę, żebyś stąd wyszła
i zniknęła z mojego życia.

425
00:37:23,460 --> 00:37:25,420
Nie rań mnie więcej.

426
00:37:29,780 --> 00:37:31,420
Żegnam.

427
00:37:32,590 --> 00:37:33,980
Wyjdź.

428
00:37:59,740 --> 00:38:02,050
Zmykajcie, dziewczyny!

429
00:38:04,230 --> 00:38:07,360
Ubawiliśmy się po pachy,
i to bez jednego strzału.

430
00:38:07,440 --> 00:38:10,010
Witaj w najlepszym...

431
00:38:10,840 --> 00:38:12,470
- Przypominam.
- O czym?

432
00:38:12,550 --> 00:38:15,070
Od niedawna jestem szczęśliwie żonaty.

433
00:38:15,150 --> 00:38:17,590
Z bardzo bogatą babką.

434
00:38:17,670 --> 00:38:22,010
Chętnie robilibyśmy z tobą interesy.

435
00:38:22,090 --> 00:38:24,680
Nie oskubię żony.

436
00:38:24,760 --> 00:38:28,110
I tak już skłamałem,
że jestem na polowaniu.

437
00:38:28,200 --> 00:38:30,370
Ale to kupiła, prawda?

438
00:38:30,450 --> 00:38:34,870
Nie wiem, jest bystra. Nie lubię kraść.

439
00:38:34,960 --> 00:38:37,890
- A kto tu mówi o kradzieży?
- Mam dość.

440
00:38:37,970 --> 00:38:39,910
Taki komputerowy czarodziej?

441
00:38:40,000 --> 00:38:42,400
Stuknij w klawisze, to się nie połapie.

442
00:38:42,490 --> 00:38:43,930
Powiem tak...

443
00:38:44,010 --> 00:38:46,820
Pomóż nam, bo niedługo
będziesz jej mężem.

444
00:38:46,900 --> 00:38:48,500
Puścić cię?

445
00:38:48,580 --> 00:38:52,760
Będę musiał cię zabić, lepiej się zgódź.

446
00:39:00,060 --> 00:39:01,770
To on?

447
00:39:03,240 --> 00:39:05,130
Ernie...

448
00:39:05,220 --> 00:39:07,150
poznaj Jeana Renault.

449
00:39:08,950 --> 00:39:12,650
Poznaliśmy się w lesie,
gdy wetknął mi lufę w ucho.

450
00:39:12,730 --> 00:39:16,910
Na szczęście miałem
prokuratorską legitymację.

451
00:39:16,990 --> 00:39:19,300
Nie ruszam się bez niej.

452
00:39:20,090 --> 00:39:22,470
Jean, to jest Profesor.

453
00:39:22,550 --> 00:39:27,120
Kosi taką kasę, że wysiada przy nim
nawet najdroższa cizia.

454
00:39:29,490 --> 00:39:32,700
Ostatni interes nie wypalił

455
00:39:32,780 --> 00:39:35,300
i sporo straciliśmy.

456
00:39:35,390 --> 00:39:39,470
Natychmiast muszę
skombinować 125 tysięcy.

457
00:39:42,430 --> 00:39:44,430
Rozumiem.

458
00:39:45,470 --> 00:39:46,630
Tak?

459
00:39:46,710 --> 00:39:48,900
To nic trudnego.

460
00:39:49,160 --> 00:39:52,640
Niewiele pan o mnie wie,

461
00:39:52,720 --> 00:39:55,260
ale pracowałem

462
00:39:55,340 --> 00:39:58,610
dla najważniejszych
instytucji finansowych w kraju...

463
00:39:59,470 --> 00:40:02,030
i wyprałem mnóstwo pieniędzy.

464
00:40:02,110 --> 00:40:06,560
Załatwiałem sprawy
dla Kolumbijczyków i Boliwijczyków.

465
00:40:06,640 --> 00:40:10,770
Innymi słowy, mam kontakty.

466
00:40:10,850 --> 00:40:13,960
Jestem do tego najlepszy.

467
00:40:14,810 --> 00:40:17,970
To chciałem usłyszeć.

468
00:40:20,580 --> 00:40:23,060
Brawo. Wymyśliłeś to?

469
00:40:23,140 --> 00:40:24,850
Nie.

470
00:40:28,020 --> 00:40:30,390
To Profesor,

471
00:40:30,480 --> 00:40:32,450
nowy księgowy.

472
00:40:48,490 --> 00:40:51,560
To dla ciebie błahostka, prawda?

473
00:40:51,640 --> 00:40:56,190
- Poradzisz sobie?
- Oczywiście.

474
00:40:56,270 --> 00:41:00,030
Hank, zabierz Profesora do kasyna.

475
00:41:00,110 --> 00:41:07,190
Domyślam się, że ma jakiś
uczony system na kości i ruletkę.

476
00:41:07,270 --> 00:41:12,250
Kiedyś grywałem, ale już nie.
To jedno oszustwo.

477
00:41:13,230 --> 00:41:15,610
Wszyscy tu gramy.

478
00:41:15,690 --> 00:41:18,210
Cieszmy się życiem.

479
00:41:20,490 --> 00:41:22,480
Racja, spróbuję.

480
00:41:30,390 --> 00:41:33,520
Nie podoba mi się. Jest spięty.

481
00:41:33,600 --> 00:41:35,730
Przekonajmy się.

482
00:41:36,640 --> 00:41:42,800
Cztery dla nas i szczypta
jako gwóźdź do trumny Coopera.

483
00:41:45,940 --> 00:41:47,870
Dać znać, jak skończę?

484
00:41:47,960 --> 00:41:49,880
Gdzie to podrzucisz?

485
00:41:51,850 --> 00:41:53,660
W samochodzie.

486
00:41:53,750 --> 00:41:55,680
Niewiele,

487
00:41:55,760 --> 00:41:57,780
wystarczy ślad.

488
00:41:59,560 --> 00:42:03,550
Mają go ukrzyżować.

489
00:43:00,330 --> 00:43:01,610
Josie?

490
00:43:29,470 --> 00:43:33,880
Robiłem, co uznałem za słuszne.
Teraz za to płacę.

491
00:43:33,970 --> 00:43:35,620
Nic pan na to nie poradzi.

492
00:43:38,570 --> 00:43:41,840
Bardzo dużo myślę o Bobie.

493
00:43:42,810 --> 00:43:45,120
O ile istnieje.

494
00:43:45,980 --> 00:43:50,230
Ja też sporo o tym myślę,

495
00:43:50,310 --> 00:43:53,050
odkąd zaczął się ten koszmar.

496
00:43:55,200 --> 00:43:58,100
Wyobrażam go sobie, jak krąży...

497
00:43:59,990 --> 00:44:03,150
i szuka kogoś, kogo by opętał.

498
00:44:04,680 --> 00:44:09,340
Siły zła są potężne.

499
00:44:10,690 --> 00:44:16,180
Niektórzy muszą stawić czoło ciemności.

500
00:44:17,550 --> 00:44:19,620
Wszyscy musimy dokonać wyboru.

501
00:44:21,040 --> 00:44:23,400
Jeśli ulegamy strachowi,

502
00:44:23,490 --> 00:44:26,150
stajemy się bezbronni wobec mroku.

503
00:44:26,230 --> 00:44:28,590
Ale można mu się oprzeć.

504
00:44:28,670 --> 00:44:32,550
Pan ma szczególny dar.

505
00:44:32,640 --> 00:44:34,060
I nie jest pan sam.

506
00:44:37,060 --> 00:44:41,150
Czy słyszał pan o Białej Chacie?

507
00:44:43,010 --> 00:44:45,030
O Białej Chacie?

508
00:44:46,100 --> 00:44:48,050
Raczej nie.

509
00:44:58,360 --> 00:45:01,550
Majorze, pozwoli pan...

510
00:45:02,760 --> 00:45:04,730
że odpowiem na zew natury.

511
00:45:06,140 --> 00:45:09,250
Nie ma to jak siusianie w plenerze.

512
00:45:09,950 --> 00:45:13,970
A potem chętnie posłucham
o tej Białej Chacie.

513
00:45:17,440 --> 00:45:20,510
Gdy wrócę z wyprawy.

514
00:45:59,240 --> 00:46:00,310
Cooper?

515
00:46:07,330 --> 00:46:08,190
Majorze Briggs?

516
00:46:10,860 --> 00:46:11,740
Majorze!

