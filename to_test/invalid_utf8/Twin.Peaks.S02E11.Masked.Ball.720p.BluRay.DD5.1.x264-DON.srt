1
00:00:28,840 --> 00:00:33,670
MIASTECZKO TWIN PEAKS

2
00:02:52,230 --> 00:02:55,770
Nie chciałam was niepokoić,
bo Garland już tak przepadał.

3
00:02:56,530 --> 00:02:58,400
Zawsze w związku z pracą.

4
00:02:59,530 --> 00:03:02,520
Czy to tak wyglądało?

5
00:03:04,330 --> 00:03:05,740
Trudno powiedzieć.

6
00:03:07,520 --> 00:03:13,710
Takie zachowania są u niego normalne?

7
00:03:13,790 --> 00:03:18,920
Mogę spytać, czy zniknął nagle?

8
00:03:19,520 --> 00:03:22,460
Tak. Prowadziliśmy filozoficzną dysputę

9
00:03:22,540 --> 00:03:27,130
i musiałem odejść za potrzebą.
Gdy wróciłem, już go nie było.

10
00:03:29,430 --> 00:03:31,800
To ważne, że byliście w lesie.

11
00:03:32,770 --> 00:03:33,720
Dlaczego?

12
00:03:37,270 --> 00:03:39,250
Tak między nami...

13
00:03:39,940 --> 00:03:43,070
bardzo często o nim mówi.

14
00:03:46,560 --> 00:03:51,200
Czy major, człowiek wielkiego ducha,

15
00:03:51,290 --> 00:03:55,620
próbował kontaktów z żyjącą w lesie siłą

16
00:03:55,700 --> 00:03:58,390
w ramach obowiązków służbowych?

17
00:03:59,870 --> 00:04:01,420
To poufne.

18
00:04:03,700 --> 00:04:08,220
Cechuje go ciekawość
człowieka renesansu, prawda?

19
00:04:10,040 --> 00:04:12,180
On tak siebie postrzega.

20
00:04:13,750 --> 00:04:16,570
Do tego nie ma instrukcji.

21
00:04:16,650 --> 00:04:19,310
Ani poradnika, jak z kimś takim żyć.

22
00:04:22,400 --> 00:04:23,560
Dziękujemy, że pani przyszła.

23
00:04:24,430 --> 00:04:28,570
Nie zrobimy wiele
poza zachowaniem czujności.

24
00:04:31,510 --> 00:04:33,930
Garland zostawił przy łóżku notatki.

25
00:04:34,020 --> 00:04:37,240
- Przyniosę je.
- Mogą nam pomóc.

26
00:04:45,610 --> 00:04:49,280
Major nie odszedł
w żadnej służbowej sprawie.

27
00:04:49,370 --> 00:04:54,380
Blask, który widziałem,
to potężna siła żyjąca w tych lasach.

28
00:04:57,860 --> 00:05:01,170
Mamy ślubny prezent
dla państwa Milfordów.

29
00:05:01,250 --> 00:05:05,390
Szalik z krawatem do kompletu.

30
00:05:06,160 --> 00:05:09,610
Świetnie. Poprzednio nic mu nie dałem.

31
00:05:10,040 --> 00:05:12,740
Nie do wiary, że znów się żeni.

32
00:05:12,820 --> 00:05:16,700
Wesela Dougiego są regularne
jak tarło łososi.

33
00:05:17,670 --> 00:05:20,160
Ślub szybki, żal długi.

34
00:05:21,340 --> 00:05:26,690
Szeryfie? Telefon do pana
Crewpera, zamiejscowa.

35
00:05:26,780 --> 00:05:29,150
Chyba nie rozłączyłam.

36
00:05:29,230 --> 00:05:32,670
Zastępstwo. Lucy pomaga przy weselu.

37
00:05:33,820 --> 00:05:36,930
Coop? Jesteś tam?

38
00:05:37,020 --> 00:05:40,360
Tu Gordon Cole z Bend w Oregonie.

39
00:05:40,440 --> 00:05:42,180
Co u ciebie?

40
00:05:42,470 --> 00:05:46,730
Chcę tylko powiedzieć,
że masz moje pełne poparcie.

41
00:05:48,410 --> 00:05:52,580
- Przetrwamy te ciężkie chwile.
- Dziękuję.

42
00:05:52,670 --> 00:05:54,570
Mnie to też dziwi.

43
00:05:55,560 --> 00:05:58,390
Czy to prawda?

44
00:05:58,480 --> 00:06:04,030
Podwójne zabójstwo,
narkotyki i dom publiczny?

45
00:06:04,110 --> 00:06:05,680
To bzdury.

46
00:06:05,770 --> 00:06:09,590
Wydział narkotykowy już węszy.

47
00:06:10,360 --> 00:06:14,330
Wysyłają do was
swojego najlepszego agenta.

48
00:06:14,410 --> 00:06:16,260
Kto to?

49
00:06:19,160 --> 00:06:20,390
Mówi Gordon.

50
00:06:21,100 --> 00:06:22,520
Gordon Cole.

51
00:06:23,220 --> 00:06:25,420
Kogo przyślą?

52
00:06:25,500 --> 00:06:27,020
Dennisa Brysona.

53
00:06:27,110 --> 00:06:29,610
To specjalista. Poważny gość.

54
00:06:30,560 --> 00:06:36,280
Nie daj się sprowokować. Oni żyją
z przeglądania cudzych szuflad,

55
00:06:36,360 --> 00:06:41,440
a każdemu zdarzy się
jakaś brudna skarpetka.

56
00:06:41,530 --> 00:06:44,260
Coś ci poradzę:

57
00:06:44,350 --> 00:06:46,710
niech uśmiech będzie twoim parasolem.

58
00:06:47,350 --> 00:06:50,430
- Dzięki.
- Do zobaczenia.

59
00:06:50,510 --> 00:06:51,380
No cóż.

60
00:06:55,390 --> 00:06:57,840
Ruszamy do tańca.

61
00:07:05,150 --> 00:07:06,230
Dzień dobry, Roger.

62
00:07:06,590 --> 00:07:08,690
Panowie.

63
00:07:10,270 --> 00:07:13,510
Agencie Cooper, zaczynajmy.

64
00:07:13,590 --> 00:07:18,380
Znasz zarzuty. Co masz na swoją obronę?

65
00:07:18,470 --> 00:07:19,910
Nie będę się tłumaczył.

66
00:07:21,830 --> 00:07:25,470
Jestem przekonany
o słuszności moich poczynań.

67
00:07:25,560 --> 00:07:29,750
Przekroczyłem swoje uprawnienia
i za to odpowiem,

68
00:07:29,830 --> 00:07:32,270
ale nie popełniłem żadnego przestępstwa.

69
00:07:32,350 --> 00:07:35,590
Jeśli mnie oskarżycie,
będę się bronił przed sądem.

70
00:07:43,050 --> 00:07:44,570
Dale...

71
00:07:45,300 --> 00:07:48,020
możesz to załatwić właściwie albo nie.

72
00:07:49,220 --> 00:07:54,930
Oczekujemy, że nasz człowiek
będzie walczył.

73
00:07:55,010 --> 00:07:58,900
Kto tego nie umie, a nawet nie próbuje...

74
00:07:59,750 --> 00:08:04,240
ma być może pierze w miejscu kręgosłupa.

75
00:08:06,520 --> 00:08:08,310
Roger...

76
00:08:08,400 --> 00:08:12,460
wiem, jakie ruchy wykonać,
i znam szachownicę.

77
00:08:12,950 --> 00:08:14,290
I co?

78
00:08:14,810 --> 00:08:17,210
Ostatnio dużo myślałem...

79
00:08:17,860 --> 00:08:22,290
i skupiłem się na tym,
co dzieje się poza nią.

80
00:08:23,030 --> 00:08:24,680
To ważniejsza partia.

81
00:08:25,910 --> 00:08:27,020
Jaka znów partia?

82
00:08:29,460 --> 00:08:32,650
Szum wiatru w gałęziach sosen.

83
00:08:33,690 --> 00:08:35,070
Czujność zwierząt.

84
00:08:36,500 --> 00:08:41,360
To, co nas przeraża w ciemności,
i to, co jest poza nią.

85
00:08:41,640 --> 00:08:44,450
O czym ty bredzisz, u diabła?

86
00:08:49,550 --> 00:08:53,360
O życiu ponad strachem

87
00:08:53,440 --> 00:08:55,840
i patrzeniu na świat z miłością.

88
00:08:56,470 --> 00:09:00,090
Oskarżą cię o zabójstwo
i handel narkotykami.

89
00:09:02,060 --> 00:09:04,800
Nic nie poradzę.

90
00:09:10,100 --> 00:09:12,020
Utrzymuję twoje zawieszenie.

91
00:09:12,590 --> 00:09:18,270
Wszystko zależy od Kanadyjczyków
i wyników rozpoczętego śledztwa.

92
00:09:20,340 --> 00:09:24,110
Rozgryzłeś ważną sprawę,
pracując w wielkim napięciu.

93
00:09:25,090 --> 00:09:29,370
Radziłbym testy psychologiczne.

94
00:09:30,440 --> 00:09:32,800
Dziękuję za troskę.

95
00:09:33,720 --> 00:09:35,720
Do zobaczenia.

96
00:10:07,810 --> 00:10:10,190
Cześć, Donna.

97
00:10:10,270 --> 00:10:13,370
Cześć. Nie wiesz nic o Jamesie?

98
00:10:14,080 --> 00:10:16,080
O tym koledze Eddiego z motorem?

99
00:10:17,940 --> 00:10:19,920
Nie widziałam go od paru dni.

100
00:10:21,340 --> 00:10:25,010
Chcę o coś spytać
i mam nadzieję, że się nie obrazisz.

101
00:10:25,940 --> 00:10:26,800
O co?

102
00:10:27,490 --> 00:10:31,640
Czy ty i Mike wciąż jesteście razem?

103
00:10:32,360 --> 00:10:33,480
Nie.

104
00:10:34,290 --> 00:10:35,810
Super!

105
00:10:36,510 --> 00:10:38,010
Dlaczego?

106
00:10:38,100 --> 00:10:41,480
- Ale obiecaj nikomu nie mówić.
- Jasne.

107
00:10:41,970 --> 00:10:46,960
Między nami chyba coś iskrzy.

108
00:10:47,040 --> 00:10:49,490
Rany, to on!

109
00:10:50,550 --> 00:10:51,660
Cześć, Donna.

110
00:10:52,800 --> 00:10:54,450
Cześć, Mike.

111
00:11:00,170 --> 00:11:03,170
Ma cudny tyłeczek.

112
00:11:04,810 --> 00:11:06,550
Nadine?

113
00:11:06,630 --> 00:11:11,050
A co z Edem? Nie jesteś z nim?

114
00:11:11,680 --> 00:11:13,260
Tak jakby.

115
00:11:14,650 --> 00:11:21,020
Skoro jesteś z Edem, to nie możesz
zacząć chodzić z Mikiem.

116
00:11:22,760 --> 00:11:25,810
Eddie jest w domu, a Mike w szkole.

117
00:11:25,890 --> 00:11:28,690
Eddie jest domatorem, a Mike się bawi.

118
00:11:28,770 --> 00:11:33,890
Prawdę mówiąc, Eddie zachowuje się,
jakby był moim ojcem.

119
00:11:33,970 --> 00:11:35,120
Cześć!

120
00:12:15,960 --> 00:12:17,780
Proszę o piwo.

121
00:12:22,080 --> 00:12:25,250
- Dokąd zmierzasz?
- Słucham?

122
00:12:25,340 --> 00:12:29,050
Jedziesz dokądś czy skądś uciekasz?

123
00:12:32,140 --> 00:12:34,050
U mnie wszystko gra.

124
00:12:34,410 --> 00:12:39,240
Wszyscy tak mówią,
póki nie pociągną za spust.

125
00:12:39,320 --> 00:12:42,640
A potem sąsiedzi stają przed kamerami,

126
00:12:42,730 --> 00:12:47,110
twierdząc, że "to był taki
miły i spokojny człowiek".

127
00:12:48,310 --> 00:12:50,740
Ja tylko tak wyglądam.

128
00:12:51,360 --> 00:12:53,530
Niemal słyszę, co się dzieje w środku.

129
00:12:57,740 --> 00:12:59,120
Jesteś tutejszy?

130
00:13:00,090 --> 00:13:02,380
Z Twin Peaks.

131
00:13:06,230 --> 00:13:08,610
�adny �akiet.

132
00:13:08,690 --> 00:13:11,170
Miły jesteś.

133
00:13:11,250 --> 00:13:13,800
- Znasz się na samochodach?
- Tak.

134
00:13:14,360 --> 00:13:16,350
Jakiś kłopot z tą corvettą?

135
00:13:16,530 --> 00:13:21,200
Nie, z jaguarem męża.
Ktoś mnie zepchnął z szosy

136
00:13:21,280 --> 00:13:23,900
i wjechałam do rowu.

137
00:13:24,650 --> 00:13:28,910
Chcę go naprawić, zanim mąż wróci.

138
00:13:29,000 --> 00:13:31,450
Mogę go obejrzeć.

139
00:13:31,540 --> 00:13:32,950
Jestem James.

140
00:13:33,240 --> 00:13:37,580
Evelyn Marsh. Mieszkam niedaleko.

141
00:13:39,340 --> 00:13:41,080
Spieszy nam się?

142
00:13:44,830 --> 00:13:46,670
Tylko coś puszczę.

143
00:13:48,490 --> 00:13:50,710
Mnie też czas nie goni.

144
00:14:41,700 --> 00:14:43,730
Witaj, Dick.

145
00:14:43,810 --> 00:14:45,720
Cześć, Andy.

146
00:14:45,800 --> 00:14:51,400
A to mały Nicky,
mój podopieczny z fundacji.

147
00:14:51,590 --> 00:14:54,490
Nicky, to jest posterunkowy Andy.

148
00:14:55,320 --> 00:14:56,820
Jak się masz, kolego?

149
00:14:57,710 --> 00:14:59,280
Cześć.

150
00:15:00,180 --> 00:15:06,530
Wiesz, że właśnie byliśmy w sklepie,
gdzie Nicky dostał nowe ubranie?

151
00:15:06,610 --> 00:15:10,750
A teraz idziemy na koktajl
i wpadliśmy po Lucy.

152
00:15:11,170 --> 00:15:15,420
Lucy jest w Great Northern
i pomaga przy weselu.

153
00:15:16,460 --> 00:15:20,090
Rzeczywiście, Dougie znów się żeni.

154
00:15:21,610 --> 00:15:25,730
Nie ma Lucy?
To znaczy, że nie pójdziemy?

155
00:15:25,810 --> 00:15:28,940
Niestety trzeba to przełożyć.

156
00:15:29,350 --> 00:15:32,540
Obiecałeś! Pójdziemy?

157
00:15:34,700 --> 00:15:37,700
Zaraz mam przerwę na lunch.

158
00:15:38,910 --> 00:15:43,410
Jeśli Nicky chce, zapraszam was obu.

159
00:15:43,670 --> 00:15:47,080
- Co ty na to?
- Fajnie!

160
00:15:57,380 --> 00:16:00,970
Co zrobisz, jeśli nie damy rady
cię wybronić?

161
00:16:01,350 --> 00:16:05,640
Olbrzym mówił, że ścieżkę
układa się kamień po kamieniu.

162
00:16:08,560 --> 00:16:11,270
Słyszeliście o miejscu
zwanym "Białą Chatą"?

163
00:16:14,990 --> 00:16:16,030
Skąd o tym wiesz?

164
00:16:17,530 --> 00:16:20,450
Major powiedział mi
tuż przed zniknięciem.

165
00:16:21,660 --> 00:16:26,490
Na tym świecie nic ci nie straszne,
ale są inne światy.

166
00:16:29,020 --> 00:16:30,780
Mów dalej.

167
00:16:31,580 --> 00:16:34,220
Moi bracia wierzą, że w Białej Chacie

168
00:16:34,300 --> 00:16:38,550
mieszkają duchy
rządzące ludźmi i naturą.

169
00:16:39,460 --> 00:16:42,090
To bardzo stara legenda.

170
00:16:43,430 --> 00:16:46,610
Opowiadają też o miejscu
zwanym Czarną Chatą,

171
00:16:46,690 --> 00:16:51,590
która jest cieniem Białej.
Ponoć każda dusza

172
00:16:51,680 --> 00:16:55,760
musi przez nią przejść
w drodze do doskonałości.

173
00:16:55,850 --> 00:17:00,260
Spotyka tam własną ciemną stronę.

174
00:17:00,350 --> 00:17:03,360
Zwiemy ją Mieszkańcem Sieni.

175
00:17:04,570 --> 00:17:06,240
Mieszkaniec Sieni.

176
00:17:06,460 --> 00:17:12,270
Jeśli ktoś wejdzie do Czarnej Chaty,
a nie ma dość odwagi,

177
00:17:12,350 --> 00:17:15,210
na zawsze straci duszę.

178
00:17:16,220 --> 00:17:20,260
Agencie Cooper? Agent Bryce do pana.

179
00:17:20,350 --> 00:17:22,020
Wpuść go.

180
00:17:22,100 --> 00:17:26,540
Pracowałem z Brysonem w Oakland.
Należy do najlepszych.

181
00:17:26,630 --> 00:17:29,350
Jesteśmy w dobrych rękach.

182
00:17:32,470 --> 00:17:34,190
Coop.

183
00:17:36,030 --> 00:17:37,760
Dennis?

184
00:17:40,190 --> 00:17:44,320
To długa historia, ale wolę Denise.

185
00:17:45,780 --> 00:17:47,300
Jasne.

186
00:17:48,040 --> 00:17:50,500
- To jest szeryf Truman.
- Miło mi.

187
00:17:50,590 --> 00:17:52,030
A to Hawk.

188
00:17:52,120 --> 00:17:55,250
Denise Bryson, agencja antynarkotykowa.

189
00:17:56,940 --> 00:17:59,100
Wspaniale mi się jechało.

190
00:17:59,180 --> 00:18:01,460
Trudno uwierzyć,
że macie tu coś do roboty

191
00:18:01,540 --> 00:18:05,380
poza szukaniem zabłąkanych psów
i zamykaniem pijaczków.

192
00:18:05,920 --> 00:18:08,720
Niestety bywa znacznie gorzej.

193
00:18:10,300 --> 00:18:11,560
Przejdźmy do rzeczy.

194
00:18:11,820 --> 00:18:16,410
Funkcjonariusz Królewskiej
Kanadyjskiej Policji Konnej

195
00:18:16,500 --> 00:18:19,270
oskarżył cię o kradzież narkotyków
użytych w prowokacji.

196
00:18:20,190 --> 00:18:22,030
Ktoś mnie wrabia.

197
00:18:22,490 --> 00:18:27,370
Ostatnimi czasy unikam
pochopnego wydawania sądów.

198
00:18:31,410 --> 00:18:34,120
Rozejrzę się. Na razie.

199
00:18:35,470 --> 00:18:39,000
Zatrzymam się w Great Northern.
Jak tam karmią?

200
00:18:39,680 --> 00:18:43,880
- Czeka cię niespodzianka.
- Ich też.

201
00:18:45,870 --> 00:18:49,170
Do zobaczenia.
Opowiem ci o moim nowym życiu.

202
00:18:49,260 --> 00:18:51,230
Nie mogę się doczekać.

203
00:18:51,950 --> 00:18:54,680
Było mi bardzo miło, szeryfie.

204
00:18:56,270 --> 00:18:57,850
Hawk.

205
00:19:07,760 --> 00:19:09,410
Dobrze mu w tym kolorze.

206
00:19:52,290 --> 00:19:54,240
Chcesz czegoś?

207
00:19:54,690 --> 00:19:58,660
Mike! Może trochę zwolnijmy.

208
00:19:59,420 --> 00:20:03,510
Co z wami? Chcę poczuć pot!

209
00:20:04,520 --> 00:20:07,550
Dobrze, Nelson. Przyłóż się!

210
00:20:13,700 --> 00:20:16,590
- Jak się nazywasz, panienko?
- Nadine Butler.

211
00:20:16,670 --> 00:20:20,650
Nie myślałaś o uprawianiu zapasów?

212
00:21:31,960 --> 00:21:33,600
Josie...

213
00:21:34,610 --> 00:21:36,480
już czas.

214
00:21:40,590 --> 00:21:43,940
Muszę o wszystkim wiedzieć.

215
00:21:46,160 --> 00:21:48,050
Mów prawdę.

216
00:21:55,100 --> 00:21:56,900
Prawdę...

217
00:22:00,630 --> 00:22:05,300
W Hongkongu pracowałam...

218
00:22:06,370 --> 00:22:09,330
dla Thomasa Eckhardta.

219
00:22:11,290 --> 00:22:13,470
Pomógł mi.

220
00:22:16,840 --> 00:22:20,030
Zabrał mnie z ulicy, gdy miałam 16 lat.

221
00:22:22,460 --> 00:22:28,540
U nas biedacy czasami sprzedają córki.

222
00:22:28,620 --> 00:22:30,250
Ja miałam szczęście.

223
00:22:33,670 --> 00:22:38,260
Wiele mnie nauczył o życiu i interesach.

224
00:22:39,220 --> 00:22:41,550
Był mi ojcem...

225
00:22:41,630 --> 00:22:43,470
mistrzem...

226
00:22:44,950 --> 00:22:46,170
i kochankiem.

227
00:22:48,700 --> 00:22:52,110
Gdy poznałam Andrew Packarda,

228
00:22:52,200 --> 00:22:55,690
zaczęłam się bać o życie.

229
00:22:55,770 --> 00:22:58,680
Prowadziliśmy interesy z Andrew.

230
00:23:00,490 --> 00:23:04,600
Gdy poprosił mnie o rękę, zgodziłam się.

231
00:23:07,560 --> 00:23:10,050
A kim jest pan Lee?

232
00:23:10,130 --> 00:23:12,740
Ten kuzyn.

233
00:23:12,820 --> 00:23:14,930
Wybacz, Harry.

234
00:23:15,020 --> 00:23:19,280
Nie chciałam cię w to wciągać.
Im mniej wiesz, tym lepiej.

235
00:23:19,370 --> 00:23:21,220
Kto to jest?

236
00:23:23,240 --> 00:23:25,180
Pracuje dla Eckhardta.

237
00:23:25,260 --> 00:23:27,910
Groził, że jeśli z nim nie wrócę,

238
00:23:27,990 --> 00:23:29,870
to cię zabije.

239
00:23:32,390 --> 00:23:34,060
Dlaczego?

240
00:23:34,750 --> 00:23:37,850
Bo Eckhardt mnie pragnie.

241
00:23:37,940 --> 00:23:41,970
Nigdy nie przestał.
Jestem jego własnością.

242
00:23:42,050 --> 00:23:45,210
Gdy Andrew żył, mógł mnie obronić.

243
00:23:47,760 --> 00:23:55,090
Teraz myślę,
że to Eckhardt kazał go zabić.

244
00:23:57,650 --> 00:24:02,890
Gdy byliśmy na lotnisku w Seattle,

245
00:24:02,970 --> 00:24:05,380
uciekłam.

246
00:24:07,610 --> 00:24:09,630
Wolę śmierć.

247
00:24:11,050 --> 00:24:13,750
Umrę, a nie wrócę do tego bydlaka.

248
00:24:13,830 --> 00:24:15,510
Josie...

249
00:24:17,350 --> 00:24:20,340
nie musisz tam wracać.

250
00:24:20,430 --> 00:24:22,670
Zostaniesz ze mną.

251
00:24:22,750 --> 00:24:26,310
I teraz zabije nas oboje.

252
00:24:26,400 --> 00:24:27,880
Nie...

253
00:24:37,940 --> 00:24:40,040
Niech spróbuje.

254
00:24:55,100 --> 00:24:58,210
Przepraszam, że pan czekał,
ale jestem dziś sama.

255
00:24:58,300 --> 00:25:02,930
Nie szkodzi. Chętnie spróbuję
tego placka. Dużo o nim słyszałem.

256
00:25:03,580 --> 00:25:05,010
Nie zawiedzie się pan.

257
00:25:08,870 --> 00:25:11,060
Przyniosę ci, co zechcesz.

258
00:25:11,140 --> 00:25:13,990
Kochanie, mamy kawę?

259
00:25:14,070 --> 00:25:17,600
- Jak polowanie?
- Trochę męczące.

260
00:25:23,700 --> 00:25:26,080
- Złapaliście coś?
- Oby nie...

261
00:25:27,490 --> 00:25:29,790
Czy coś upolowaliśmy?

262
00:25:29,870 --> 00:25:31,350
Tak.

263
00:25:32,560 --> 00:25:36,880
Wytropiłem pięknego rogacza,
nad jeziorem.

264
00:25:37,300 --> 00:25:43,260
Podszedłem go bardzo ostrożnie,
wycelowałem i łup!

265
00:25:44,310 --> 00:25:47,300
Trzeba było zrobić zdjęcie.
Zabiera go pan?

266
00:25:48,060 --> 00:25:52,400
W rzeczy samej. Było fantastycznie.

267
00:25:53,760 --> 00:25:56,180
Lepiej pójdę do twojej mamy.

268
00:25:56,260 --> 00:26:00,940
- Nie ma jej. Wróciła do Seattle.
- Do Seattle?

269
00:26:02,490 --> 00:26:05,000
Ty też powinieneś.

270
00:26:19,470 --> 00:26:22,980
Co jest? Skąd ta mina?

271
00:26:23,060 --> 00:26:26,850
- Vivian wróciła do Seattle.
- To nawet lepiej.

272
00:26:26,940 --> 00:26:31,170
Masz cztery kilo towaru.
Lepiej, żeby ci nie przeszkadzała.

273
00:26:31,250 --> 00:26:32,880
Chyba masz rację.

274
00:26:33,490 --> 00:26:35,700
Lepiej łap za telefon.

275
00:26:35,780 --> 00:26:37,550
Czas nas goni.

276
00:26:44,240 --> 00:26:47,330
Proszę. Dwie porcje placka z jagodami

277
00:26:47,420 --> 00:26:50,530
i czekoladowy koktajl z bitą śmietaną.

278
00:26:50,610 --> 00:26:52,160
Dziękuję!

279
00:26:53,280 --> 00:26:56,910
Wygląda jak ośnieżona góra, prawda?

280
00:26:56,990 --> 00:27:02,000
- Ile kosztował?
- Pieniądze się nie liczą.

281
00:27:03,050 --> 00:27:04,670
O cholera!

282
00:27:06,840 --> 00:27:08,310
Przepraszam, wujku.

283
00:27:08,400 --> 00:27:12,200
Nie szkodzi, wujek się nie gniewa.
Prawda, wujku?

284
00:27:12,280 --> 00:27:17,060
Jestem tylko mokry.
Wujku Andy, podaj mi serwetkę.

285
00:27:27,980 --> 00:27:31,450
- Niech to drzwi ścisną...
- Przepraszam.

286
00:27:32,240 --> 00:27:33,830
Nic się nie stało.

287
00:27:34,620 --> 00:27:38,040
Ale fajnie się wykopyrtnął.

288
00:27:44,240 --> 00:27:46,650
Chyba sobie poradzę.

289
00:27:48,140 --> 00:27:50,450
Jest piękny.

290
00:27:50,540 --> 00:27:53,410
Jeffrey kocha ten samochód.

291
00:27:53,490 --> 00:27:55,360
Mój mąż.

292
00:27:55,450 --> 00:27:57,290
Uwielbia go.

293
00:27:58,340 --> 00:28:00,970
Gdzie on jest?

294
00:28:01,060 --> 00:28:06,740
Nie wiem. Dużo podróżuje w interesach.

295
00:28:07,590 --> 00:28:10,880
Wóz jest z 1948 roku.

296
00:28:10,970 --> 00:28:15,280
Jeffrey przepada za pięknymi zabawkami.

297
00:28:15,360 --> 00:28:17,780
Muszą być doskonałe.

298
00:28:17,870 --> 00:28:21,200
To dotyczy wszystkiego.

299
00:28:21,280 --> 00:28:25,200
Powinnam wyciągnąć z tego naukę.

300
00:28:25,290 --> 00:28:29,620
Też taki jesteś?
Tak traktujesz motocykl?

301
00:28:29,700 --> 00:28:33,870
Mniej mnie obchodzi, jak wygląda,
niż dokąd mnie zawiezie.

302
00:28:33,950 --> 00:28:36,400
A dokąd byś chciał?

303
00:28:36,480 --> 00:28:39,550
To nie miejsce, a raczej uczucie.

304
00:28:41,810 --> 00:28:44,360
Czasami, gdy jadę w nocy...

305
00:28:45,860 --> 00:28:47,660
wyłączam światła,

306
00:28:47,740 --> 00:28:50,840
dodaję gazu...

307
00:28:50,920 --> 00:28:53,860
i pędzę na ślepo w ciemność.

308
00:29:01,730 --> 00:29:04,200
Naprawisz to?

309
00:29:04,280 --> 00:29:05,670
Tak.

310
00:29:07,480 --> 00:29:12,000
Nad garażem jest pokój.
Przez ten czas możesz tu zostać.

311
00:29:12,080 --> 00:29:16,140
Masz mieszkanie, wikt i co zechcesz.

312
00:29:16,830 --> 00:29:20,350
Ale musisz zdążyć
przed powrotem Jeffreya.

313
00:29:21,320 --> 00:29:25,560
Chciałabym, żebyś został,
dla towarzystwa.

314
00:29:30,080 --> 00:29:33,080
Nie będę ci przeszkadzać.

315
00:30:44,040 --> 00:30:48,660
"Dziś zimy naszej zmienia się niełaska

316
00:30:48,740 --> 00:30:54,870
na złote lato przy Yorku słońcu".

317
00:31:19,020 --> 00:31:25,280
"Wianek zwycięstwa skronie nasze stroi..."

318
00:31:30,820 --> 00:31:34,610
Wieczór kawalerski?
I nie zostałem zaproszony?

319
00:31:35,610 --> 00:31:39,030
Gdzie byłeś, u diabła?

320
00:31:40,590 --> 00:31:44,830
Miałem cholernie dużo pracy.

321
00:32:00,910 --> 00:32:06,230
Wiesz, jakie piekło przeżyłem
w ostatnich dniach?

322
00:32:07,070 --> 00:32:12,620
Zapewniałeś mnie,
że Catherine zginęła w pożarze,

323
00:32:12,710 --> 00:32:14,390
a ona żyje.

324
00:32:14,480 --> 00:32:18,910
I, jak się domyślasz,
nie jest zadowolona.

325
00:32:18,990 --> 00:32:23,090
Podstępem i szantażem

326
00:32:23,180 --> 00:32:28,370
wyłudziła ode mnie prawa własności

327
00:32:28,460 --> 00:32:31,150
do Ghostwood i do tartaku.

328
00:32:32,200 --> 00:32:34,920
W dodatku

329
00:32:35,000 --> 00:32:39,760
aresztowali mnie pod zarzutem
zamordowania Laury Palmer.

330
00:32:41,580 --> 00:32:43,600
To pomaga w interesach.

331
00:32:46,220 --> 00:32:50,820
Na dobitkę mój zaufany prawnik,
świętej pamięci Leland Palmer,

332
00:32:50,900 --> 00:32:54,310
okazał się maniakalnym zabójcą.

333
00:32:54,770 --> 00:32:56,020
Ciężki tydzień.

334
00:32:58,810 --> 00:33:02,250
Myślisz, że meble
są tu dobrze ustawione?

335
00:33:03,690 --> 00:33:11,850
Zetknąłem się z opinią, że idealne
rozmieszczenie przedmiotów

336
00:33:11,940 --> 00:33:16,150
w konkretnej przestrzeni
wywołuje rezonans,

337
00:33:16,240 --> 00:33:22,070
który może bardzo korzystnie
wpływać na osobę.

338
00:33:23,330 --> 00:33:26,750
Choć może to być przesąd.

339
00:33:28,360 --> 00:33:30,530
Pomóż mi przestawić biurko.

340
00:33:30,620 --> 00:33:33,040
To bardzo ciekawe,

341
00:33:33,120 --> 00:33:37,770
ale teraz musimy pogadać
o Jednookim Jacku.

342
00:33:37,850 --> 00:33:39,940
Tak, Jednooki Jack...

343
00:33:40,030 --> 00:33:41,530
Chodzi o to...

344
00:33:45,280 --> 00:33:47,180
Wypadasz z interesu.

345
00:33:47,260 --> 00:33:48,750
Słucham?

346
00:33:50,470 --> 00:33:52,620
Jednooki Jack jest mój.

347
00:33:55,180 --> 00:33:56,790
Nastąpiło przyjacielskie przejęcie.

348
00:33:58,760 --> 00:34:00,900
Przychodzisz tu...

349
00:34:00,980 --> 00:34:03,310
jako mój pracownik

350
00:34:03,390 --> 00:34:05,410
i śmiesz mi mówić...

351
00:34:05,490 --> 00:34:07,070
A właśnie.

352
00:34:07,780 --> 00:34:09,730
Już u ciebie nie pracuję.

353
00:34:15,550 --> 00:34:17,540
Renault.

354
00:34:17,620 --> 00:34:19,110
Jean Renault.

355
00:34:23,400 --> 00:34:25,430
To Renault, prawda?

356
00:34:26,420 --> 00:34:27,860
Coś ci powiem.

357
00:34:28,530 --> 00:34:34,810
To wariat. Psychopata.

358
00:34:34,900 --> 00:34:39,550
A ty, kolego, igrasz z diabłem.

359
00:34:39,630 --> 00:34:42,070
Odbiło ci.

360
00:34:42,150 --> 00:34:46,450
Myślisz, że będę tu siedział
i dam się oskubać?

361
00:34:49,070 --> 00:34:51,140
Wiele się zmieniło.

362
00:34:51,230 --> 00:34:55,760
Słuchaj, kiedy ci mówię, co masz robić.

363
00:34:57,010 --> 00:35:00,170
Nie traktuj mnie z góry.

364
00:35:02,360 --> 00:35:05,070
Jesteś w fatalnym stanie.

365
00:35:05,150 --> 00:35:07,680
Nie nadajesz się na szefa.

366
00:35:08,740 --> 00:35:10,640
Wylatujesz, Ben.

367
00:35:26,620 --> 00:35:28,270
"Wylatujesz, Ben".

368
00:35:32,060 --> 00:35:34,180
Wylatujesz, Ben.

369
00:35:39,050 --> 00:35:42,590
Wylatujesz. Ben, wylatujesz.

370
00:35:49,690 --> 00:35:51,350
Ben, wylatujesz.

371
00:36:02,670 --> 00:36:04,830
Windom Earle...

372
00:36:30,380 --> 00:36:35,860
To jasne, że nie mogłeś się oprzeć
mojemu nad wyraz tradycyjnemu otwarciu.

373
00:36:35,940 --> 00:36:40,070
Twoja reakcja na mój ruch
była oczywista i wynika

374
00:36:40,150 --> 00:36:44,310
z upodobania do porządku
i dbałości o szczegóły.

375
00:36:44,390 --> 00:36:50,680
Dostrzegasz, iż moja odpowiedź
doprowadzi do konfrontacji?

376
00:36:50,760 --> 00:36:56,470
Ale nie czujesz się pewnie.
Jakie mam zamiary?

377
00:36:56,550 --> 00:36:59,760
Jak odpowiedzieć tym razem?

378
00:36:59,840 --> 00:37:02,130
To dławi jak zmora.

379
00:37:02,210 --> 00:37:07,360
Stałość, przewidywalność,
uleganie schematom.

380
00:37:07,440 --> 00:37:13,490
Obaj wiemy aż za dobrze,
jak nas to wystawia na atak.

381
00:37:13,580 --> 00:37:16,900
I ty odniosłeś rany, i ja.

382
00:37:16,980 --> 00:37:21,410
Wyobraź to sobie...
Moje konie ruszą do boju.

383
00:37:21,490 --> 00:37:26,210
Przed moimi gońcami i wieżami
otworzą się drogi władzy i wpływów.

384
00:37:26,290 --> 00:37:28,630
Pionki oczywiście stracę.

385
00:37:28,710 --> 00:37:31,360
Jestem nawet gotów poświęcić hetmana,

386
00:37:31,440 --> 00:37:37,650
bo, wierz mi, mój drogi,
będę dążył do celu za wszelką cenę.

387
00:37:37,730 --> 00:37:41,480
Król musi umrzeć.

388
00:37:47,240 --> 00:37:51,480
Czy ty, Douglasie Milford,
bierzesz tę kobietę za żonę

389
00:37:51,560 --> 00:37:55,280
i będziesz ją kochał
i szanował aż do śmierci?

390
00:38:01,990 --> 00:38:03,790
Może być pastor pewny.

391
00:38:04,100 --> 00:38:07,960
Jeśli ktoś zna przeszkody
do zawarcia tego małżeństwa,

392
00:38:08,490 --> 00:38:11,130
niech przemówi albo zamilknie na wieki.

393
00:38:11,210 --> 00:38:15,590
Święte słowa! Ja się sprzeciwiam!
Tej panience chodzi tylko

394
00:38:15,680 --> 00:38:17,600
o jego forsę...

395
00:38:18,240 --> 00:38:20,180
i gazetę.

396
00:38:20,450 --> 00:38:23,640
I tylko Bóg to widzi.
Spójrzcie na niego.

397
00:38:23,720 --> 00:38:26,470
Jest już jedną nogą w grobie!

398
00:38:27,610 --> 00:38:30,320
W porządku, Dwayne, uspokój się.

399
00:38:31,810 --> 00:38:34,900
Wiesz, że od lat mi się to nie podoba.

400
00:38:34,980 --> 00:38:36,700
Wyjdźmy na powietrze.

401
00:38:39,090 --> 00:38:41,610
Nie rezygnuj, mała.

402
00:38:41,690 --> 00:38:47,330
Takie jest moje zdanie
i powinniście to zrozumieć.

403
00:38:47,410 --> 00:38:50,670
Co dwa miesiące ślub. Dobry Boże!

404
00:38:51,420 --> 00:38:54,420
Nie ma innych protestów?

405
00:39:03,460 --> 00:39:05,310
Cooper.

406
00:39:05,390 --> 00:39:06,810
Denise?

407
00:39:07,740 --> 00:39:09,850
Chętnie, już schodzę.

408
00:39:18,040 --> 00:39:23,030
Diane, w wolnej chwili
opowiem ci o agencie Brysonie.

409
00:39:46,400 --> 00:39:49,730
Uwielbiam śluby Milforda.

410
00:39:50,870 --> 00:39:54,610
Wygrałam. Ile z tych dziewczyn
grało na pozycji przechwytującego?

411
00:39:55,680 --> 00:39:56,570
Niewiele.

412
00:39:58,680 --> 00:40:00,190
Najpierw złe wieści.

413
00:40:00,280 --> 00:40:05,720
W twoim samochodzie są ślady koki,
pewnie identycznej z tą skradzioną.

414
00:40:05,800 --> 00:40:08,620
- Dennis, nie daj się zwieść.
- Denise.

415
00:40:09,850 --> 00:40:12,040
- Przepraszam.
- Nie szkodzi.

416
00:40:12,120 --> 00:40:16,670
To istotnie wygląda na podpuchę,
ale twoje słowo to za mało.

417
00:40:18,050 --> 00:40:22,900
- Możesz mi pomóc?
- To delikatna sprawa.

418
00:40:22,980 --> 00:40:27,480
Ale ostatnio staram się
częściej kierować uczuciami.

419
00:40:31,760 --> 00:40:33,720
Zapraszamy na tort.

420
00:40:34,720 --> 00:40:38,030
Dougie zawsze był frajerem.

421
00:40:38,110 --> 00:40:43,690
Wystarczy, że któraś
na niego spojrzy i zakręci tyłkiem,

422
00:40:43,780 --> 00:40:46,260
a ten już łapie haczyk.

423
00:40:55,490 --> 00:40:56,420
Denise...

424
00:40:57,840 --> 00:41:01,680
jeśli mogę spytać,
to co się z tobą stało?

425
00:41:01,760 --> 00:41:06,660
Oczywiście, chętnie ci opowiem.
To proste.

426
00:41:06,740 --> 00:41:09,930
W zeszłym roku
pracowałam na przedmieściu.

427
00:41:10,430 --> 00:41:12,350
Obiekt miał dziwne nawyki.

428
00:41:12,430 --> 00:41:15,690
Na przykład sprzedawał
tylko transwestytom.

429
00:41:15,770 --> 00:41:20,920
Padło na mnie, że mam zagrać klienta.

430
00:41:21,000 --> 00:41:25,670
Okazało się, że noszenie
damskich ciuszków...

431
00:41:26,590 --> 00:41:28,410
mnie odpręża.

432
00:41:29,210 --> 00:41:31,080
Chodziłam w nich do wieczora.

433
00:41:31,790 --> 00:41:35,190
Partner uznał to za profesjonalizm.

434
00:41:35,980 --> 00:41:38,000
To były trudne dwa tygodnie.

435
00:41:38,460 --> 00:41:42,720
To zaskakujące wyznanie, Denise.

436
00:41:43,770 --> 00:41:48,680
Wyobraź sobie moje zdziwienie.
Takich rzeczy się nie planuje.

437
00:41:48,990 --> 00:41:50,060
Fakt.

438
00:41:51,580 --> 00:41:53,280
Szeryfie.

439
00:41:54,410 --> 00:41:56,160
Napiję się piwa.

440
00:41:56,250 --> 00:42:01,190
Proszę ją aresztować.
Jest za piękna, żeby żyć w tym stanie.

441
00:42:03,190 --> 00:42:08,370
- Pan jest z FBI?
- Chwilowo nie. Dale Cooper.

442
00:42:10,410 --> 00:42:12,670
To pan wyjaśnił sprawę Laury Palmer?

443
00:42:12,760 --> 00:42:15,690
Chodź, grają naszą melodię.

444
00:42:21,360 --> 00:42:24,480
Oby ci dwoje szybko poszli na górę,

445
00:42:24,560 --> 00:42:27,440
bo Dwayne już dwa razy się awanturował.

446
00:42:27,520 --> 00:42:30,720
- Jak na każdym weselu.
- Chłopcy nie dorastają.

447
00:42:30,980 --> 00:42:35,210
Ja przez pół wieku żyłem z jedną żoną.

448
00:42:36,460 --> 00:42:39,050
Niech spoczywa w pokoju.

449
00:42:39,140 --> 00:42:43,560
To dlatego, że myślę głową,
a nie wężem ogrodowym.

450
00:42:43,650 --> 00:42:47,370
Tort jest przepyszny.

451
00:42:51,780 --> 00:42:55,790
Całkiem nieźle grają.

452
00:42:58,760 --> 00:43:01,010
To powinien być marsz żałobny.

453
00:43:07,340 --> 00:43:09,450
Dobrze pan tańczy.

454
00:43:09,540 --> 00:43:13,150
W tańcu najlepsze jest to,
że nie wiesz, dokąd cię zawiedzie,

455
00:43:13,240 --> 00:43:16,190
i trzeba się poddać muzyce.

456
00:43:27,880 --> 00:43:31,410
Twój brat, a mój mąż, Andrew Packard,

457
00:43:32,060 --> 00:43:35,540
został zamordowany
przez Thomasa Eckhardta.

458
00:43:36,230 --> 00:43:38,960
Powiedz coś, o czym nie wiem.

459
00:43:40,400 --> 00:43:43,090
Grozi ci wielkie niebezpieczeństwo.

460
00:43:45,060 --> 00:43:48,600
Przyczyniłaś się
do śmierci mojego brata,

461
00:43:48,680 --> 00:43:56,640
próbowałaś mnie okraść
z rodzinnego majątku i pozbawić życia,

462
00:43:56,720 --> 00:43:59,020
a teraz chcesz mnie ratować.

463
00:43:59,910 --> 00:44:03,710
Musiałam to zrobić, żeby żyć.

464
00:44:05,360 --> 00:44:07,860
Nie mam do kogo pójść.

465
00:44:10,160 --> 00:44:14,590
Nic nie mam i jestem na twojej łasce.

466
00:44:20,440 --> 00:44:21,540
Wystarczy.

467
00:44:22,810 --> 00:44:25,170
Bądźmy rozsądne.

468
00:44:26,080 --> 00:44:28,010
Co proponujesz?

469
00:44:29,800 --> 00:44:32,850
- Nie wiem.
- To ja ci powiem.

470
00:44:33,550 --> 00:44:35,530
Od tej pory...

471
00:44:37,440 --> 00:44:39,520
pracujesz u mnie...

472
00:44:40,320 --> 00:44:45,080
tutaj, w domu, jako pokojówka.

473
00:44:45,160 --> 00:44:50,700
Zanieś swoje rzeczy do służbówki.
Jeśli będziesz nieposłuszna...

474
00:44:52,250 --> 00:44:54,100
okłamiesz mnie...

475
00:44:55,400 --> 00:44:59,550
choć raz mi się sprzeciwisz...

476
00:45:01,000 --> 00:45:03,830
znajdę tego Eckhardta...

477
00:45:04,480 --> 00:45:07,670
i mu ciebie wydam.

478
00:45:08,120 --> 00:45:10,250
Jasne?

479
00:45:11,450 --> 00:45:13,390
Odpowiadaj!

480
00:45:14,530 --> 00:45:16,450
Rozumiem.

481
00:45:19,090 --> 00:45:21,890
O Eckhardcie pomówimy innym razem.

482
00:45:22,800 --> 00:45:24,520
Możesz odejść.

483
00:45:26,430 --> 00:45:26,860
Dziękuję.

484
00:45:29,080 --> 00:45:30,180
Josie?

485
00:45:32,090 --> 00:45:34,790
Zjem śniadanie o siódmej, u siebie.

486
00:45:36,110 --> 00:45:39,520
Kawa, sok, grzanka i owsianka.

487
00:45:41,160 --> 00:45:43,790
- Dobrze.
- Słodkich snów.

488
00:45:53,600 --> 00:45:54,720
I jak, Andrew?

489
00:45:57,900 --> 00:46:01,600
Wszystko idzie zgodnie z planem.

490
00:46:02,230 --> 00:46:03,230
Co teraz?

491
00:46:03,900 --> 00:46:06,570
Teraz, siostrzyczko, Thomas Eckhardt

492
00:46:06,660 --> 00:46:11,770
przyjedzie szukać swojej ukochanej.

493
00:46:13,150 --> 00:46:15,280
A gdy się zjawi...

494
00:46:16,920 --> 00:46:19,540
Będziemy na niego czekać.

