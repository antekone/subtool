1
00:00:28,890 --> 00:00:33,730
MIASTECZKO TWIN PEAKS

2
00:01:56,870 --> 00:01:58,330
Dobry Boże...

3
00:02:06,180 --> 00:02:09,840
Andy, zdejmij odciski palców.

4
00:02:11,720 --> 00:02:16,060
Nie ma świadków. Awaria i pożar
wszystkich wypłoszyły.

5
00:02:16,780 --> 00:02:20,530
- Od dawna nie żyje?
- Nie wiem. Dowiem się na sekcji.

6
00:02:20,620 --> 00:02:22,920
Jeśli podwinie mu pan koszulę,

7
00:02:23,000 --> 00:02:26,560
2 centymetry poniżej mostka
znajdzie pan ranę.

8
00:02:26,640 --> 00:02:29,280
Przecięto mu aortę.

9
00:02:39,290 --> 00:02:40,420
Już to widziałeś.

10
00:02:42,650 --> 00:02:43,820
Tak.

11
00:02:43,900 --> 00:02:45,510
Są ślady?

12
00:02:46,320 --> 00:02:48,180
Brak krwi.

13
00:02:51,540 --> 00:02:52,930
A to?

14
00:02:54,570 --> 00:02:58,130
- Daglezja?
- Sosna wydmowa.

15
00:02:59,540 --> 00:03:02,740
- Hawk, masz coś?
- Ślady stóp.

16
00:03:02,820 --> 00:03:05,430
Te same buty, w obie strony.

17
00:03:05,510 --> 00:03:08,530
- Pierwsze nieco głębsze.
- Niósł zwłoki.

18
00:03:09,900 --> 00:03:13,580
Jesteś pewien,
że to sprawka Windoma Earle'a?

19
00:03:21,060 --> 00:03:24,750
To było niedawno. Ofiara to włóczęga.

20
00:03:24,840 --> 00:03:30,010
Sprawca zaoferował mu podwózkę
i zwabił gdzieś w góry.

21
00:03:30,090 --> 00:03:34,920
Samochód pewnie wciąż tam jest.
Otrzymał cios nożem,

22
00:03:35,010 --> 00:03:38,230
ale zanim upadł,
zdołał kawałek przebiec.

23
00:03:38,470 --> 00:03:43,440
Windom Earle spowodował
wybuch i awarię oraz pożar,

24
00:03:43,520 --> 00:03:47,460
który wszystkich stąd wywabił,

25
00:03:48,210 --> 00:03:50,740
a potem wniósł ciało przez okno.

26
00:03:52,860 --> 00:03:55,780
Windom Earle tu był.

27
00:03:56,900 --> 00:03:58,750
Czuję jego obecność.

28
00:04:00,310 --> 00:04:06,380
Nie znajdziemy żadnych śladów.
On nie popełnia błędów.

29
00:04:06,470 --> 00:04:11,460
Działa celowo,
precyzyjnie i pomysłowo.

30
00:04:12,990 --> 00:04:15,550
To geniusz.

31
00:04:15,630 --> 00:04:19,850
Zbił pierwszego pionka
w tej pokręconej grze.

32
00:04:36,050 --> 00:04:39,450
Słuchaj uważnie, powiem, co zrobimy.

33
00:04:40,050 --> 00:04:43,360
Jedyne, czego nauczyłam się
od ojca, zanim odbiła mu palma,

34
00:04:43,440 --> 00:04:47,290
to, że interesy są jak tajny pakt.

35
00:04:47,370 --> 00:04:53,400
Dorównuje temu tylko intymny związek,
gdzie nie ma żadnych tajemnic.

36
00:04:54,070 --> 00:04:55,370
Jasne.

37
00:04:57,720 --> 00:04:59,590
Chcesz być bogaty?

38
00:05:01,780 --> 00:05:03,200
I to zaraz.

39
00:05:03,290 --> 00:05:05,090
Mogę ci pomóc.

40
00:05:07,390 --> 00:05:10,950
Wyobraź sobie, że to mój ojciec.

41
00:05:11,030 --> 00:05:13,880
- Kostka lodu?
- Tak.

42
00:05:13,960 --> 00:05:18,590
A my musimy mu pomóc
wrócić z krainy cudów,

43
00:05:18,680 --> 00:05:23,430
zanim się rozpuści
i zostawi nas z niczym.

44
00:05:26,540 --> 00:05:30,680
- Tego byśmy nie chcieli.
- Nie.

45
00:05:33,130 --> 00:05:37,930
Od tej pory trzymaj się mnie.

46
00:05:39,860 --> 00:05:42,560
- A Shelly?
- Co z nią?

47
00:05:52,560 --> 00:05:53,450
Leo?

48
00:06:42,270 --> 00:06:43,200
Pomocy!

49
00:06:44,640 --> 00:06:46,110
Na pomoc!

50
00:06:46,200 --> 00:06:49,670
Pomocy! Ratunku!

51
00:07:00,580 --> 00:07:02,890
Leo, proszę...

52
00:07:32,630 --> 00:07:37,260
- Niegrzeczna...
- Nie, proszę!

53
00:08:01,560 --> 00:08:02,570
Przysięgam...

54
00:08:02,650 --> 00:08:04,380
Zabiję cię...

55
00:08:11,090 --> 00:08:13,140
Ratunku!

56
00:08:32,250 --> 00:08:33,970
Otwórz.

57
00:08:39,890 --> 00:08:41,360
Nie!

58
00:08:48,670 --> 00:08:49,770
Shelly?

59
00:08:51,160 --> 00:08:52,590
Nie, Leo!

60
00:09:04,070 --> 00:09:05,550
Żegnaj, żoneczko.

61
00:09:44,130 --> 00:09:45,260
Do widzenia!

62
00:10:04,790 --> 00:10:08,100
Już dobrze. Poszedł sobie.

63
00:10:52,210 --> 00:10:55,010
- Proszę.
- Dzięki, Harry.

64
00:10:55,910 --> 00:10:58,890
Nie ma żadnych śladów, miałeś rację.

65
00:10:58,970 --> 00:11:02,420
Jeśli to Earle, niczego nie przeoczył.

66
00:11:04,680 --> 00:11:06,820
Denise się odzywała?

67
00:11:07,510 --> 00:11:11,110
Zostałem oczyszczony
z kryminalnych zarzutów,

68
00:11:11,190 --> 00:11:15,010
ale zawieszenie trwa.
Czekam na telefon od Gordona.

69
00:11:15,960 --> 00:11:18,750
Wciąż jesteś moim zastępcą.

70
00:11:20,890 --> 00:11:24,330
Jeśli chcesz, sprawa jest twoja.

71
00:11:26,640 --> 00:11:29,080
Bardzo chcę.

72
00:11:32,560 --> 00:11:33,740
Harry?

73
00:11:34,810 --> 00:11:38,370
Znalazłem samochód w lesie,
jak mówiłeś.

74
00:11:38,450 --> 00:11:43,310
Żadnych śladów.
A Hanka Jenningsa nie było na farmie,

75
00:11:43,390 --> 00:11:47,520
bo leży w szpitalu.
Ponoć wpadł pod autobus.

76
00:11:48,130 --> 00:11:52,080
Zatrzymałem go za złamanie
warunków zwolnienia.

77
00:11:53,940 --> 00:11:55,800
I dzwoniła Shelly Johnson.

78
00:11:55,890 --> 00:12:00,350
Leo oprzytomniał,
zaatakował ją i uciekł do lasu.

79
00:12:01,940 --> 00:12:03,730
Jasny gwint!

80
00:12:25,390 --> 00:12:30,470
- Lucy, musimy pogadać o Nickym.
- O Nickym?

81
00:12:31,260 --> 00:12:37,380
Trochę poszperaliśmy z Dickiem
i sądzimy, że on... jest...

82
00:12:43,640 --> 00:12:44,690
No co?

83
00:12:45,300 --> 00:12:48,030
Że zamordował rodziców.

84
00:12:49,360 --> 00:12:51,370
Ma dziewięć lat.

85
00:12:51,450 --> 00:12:55,700
Wiem. W chwili zabójstwa
musiał mieć sześć.

86
00:12:57,310 --> 00:13:00,790
Poznałam Nicky'ego. Rozmawiałam z nim.

87
00:13:01,730 --> 00:13:04,000
Jeśli według was uwierzę,

88
00:13:04,080 --> 00:13:10,080
że ta biedna sierota
jest zdolna zabić,

89
00:13:10,160 --> 00:13:14,260
to nie nadajecie się nawet
na ojców szympansa!

90
00:13:18,150 --> 00:13:20,310
Sama to wyjaśnię!

91
00:13:47,770 --> 00:13:50,140
Ty jesteś James?

92
00:13:51,950 --> 00:13:56,960
Jeffrey Marsh. Słyszałem
o tobie od Evelyn wiele dobrego.

93
00:13:57,040 --> 00:14:00,500
- To się okaże.
- Nie mam obaw.

94
00:14:00,580 --> 00:14:04,230
- Dobrze, że cię znaleźliśmy.
- Na moje szczęście.

95
00:14:04,870 --> 00:14:09,540
- Podobno jesteś w drodze.
- Niedługo ruszam dalej.

96
00:14:09,620 --> 00:14:14,240
Nie ma pośpiechu. Zazdroszczę.
Chętnie bym z tobą zwiał.

97
00:14:15,180 --> 00:14:18,090
Przejadę się, a potem pogadamy.

98
00:14:19,250 --> 00:14:21,540
Chyba łączy nas uczucie...

99
00:14:24,290 --> 00:14:25,080
do samochodów.

100
00:14:26,470 --> 00:14:29,250
Trochę się zasiedziałem...

101
00:14:29,780 --> 00:14:34,230
Jasne, że zostanie.
Ma tu jeszcze sporo do zrobienia.

102
00:14:36,540 --> 00:14:37,720
Prawda?

103
00:14:38,660 --> 00:14:42,210
Tak, może. Przepraszam.

104
00:14:44,630 --> 00:14:45,480
Miły chłopak.

105
00:14:45,790 --> 00:14:47,950
Wyremontował samochód.

106
00:14:48,760 --> 00:14:50,390
Przekonamy się.

107
00:14:50,470 --> 00:14:51,770
Do zobaczenia.

108
00:15:16,440 --> 00:15:19,010
Chciałbym porozmawiać o Nadine.

109
00:15:19,440 --> 00:15:23,060
Chce się umawiać z chłopakami
i nie wiem, co robić.

110
00:15:24,570 --> 00:15:27,870
- Jest aktywna seksualnie?
- Czy jest?

111
00:15:29,020 --> 00:15:32,860
Co rano czuję się
jak potrącony przez ciężarówkę.

112
00:15:35,320 --> 00:15:37,450
To nadmiar adrenaliny.

113
00:15:37,790 --> 00:15:43,240
Jest niebezpieczna.
Może kogoś zabić. Sam nie wiem.

114
00:15:43,970 --> 00:15:45,370
Cierpliwości.

115
00:15:46,700 --> 00:15:50,630
Każ jej wracać do domu
przed dziewiątą.

116
00:15:57,180 --> 00:16:00,550
- Dodatkowe ziemniaki.
- Dzięki.

117
00:16:09,250 --> 00:16:13,150
Donna wzięła rano samochód
i pojechała szukać Jamesa.

118
00:16:13,980 --> 00:16:16,730
Czy mam powody do niepokoju?

119
00:16:17,860 --> 00:16:20,020
James jest parę godzin drogi stąd.

120
00:16:20,110 --> 00:16:23,620
Chce ochłonąć,
a Donna zawiozła mu pieniądze.

121
00:16:25,550 --> 00:16:29,580
- Niełatwo być rodzicem.
- To fakt.

122
00:16:34,980 --> 00:16:37,490
- Do widzenia.
- Dziękuję.

123
00:16:43,900 --> 00:16:48,270
Hank leży w szpitalu.
Podobno przygniotło go drzewo.

124
00:16:48,950 --> 00:16:54,230
Kłoda o imieniu Nadine.
Gdy wczoraj wyszłaś,

125
00:16:54,310 --> 00:16:58,050
rzucił się na mnie w moim
własnym domu. Gdyby nie Nadine...

126
00:16:58,130 --> 00:17:02,410
- Znokautowała Hanka?
- Stłukła go na miazgę.

127
00:17:04,670 --> 00:17:08,670
Harry zatrzyma Hanka
za złamanie warunków zwolnienia.

128
00:17:08,750 --> 00:17:11,040
Gdy dojdzie do siebie,
może wrócić za kratki.

129
00:17:11,970 --> 00:17:15,590
Czas, żebym naprawiła
kilka błędów, które popełniłam.

130
00:17:17,670 --> 00:17:22,430
Na początek wynajmijmy domek
i jedźmy ze stekami

131
00:17:22,700 --> 00:17:25,450
i butelką wina.

132
00:17:26,080 --> 00:17:29,180
- Wszyscy się dowiedzą.
- No to co?

133
00:17:51,740 --> 00:17:54,830
- Nie pukasz?
- Przepraszam.

134
00:17:54,910 --> 00:17:56,950
- Wyjeżdżasz?
- Tak.

135
00:17:59,320 --> 00:18:01,300
Dobrze ci poszło z Jeffreyem.

136
00:18:02,320 --> 00:18:04,140
Było mi głupio.

137
00:18:07,410 --> 00:18:09,760
Mogę to zmienić.

138
00:18:14,890 --> 00:18:16,210
Tak nie można.

139
00:18:18,440 --> 00:18:20,230
Miłość to nic złego.

140
00:18:25,150 --> 00:18:29,620
Nie opuszczaj mnie.
Nie zostawiaj mnie z nim.

141
00:18:43,460 --> 00:18:45,220
Sprawdzę motocykl.

142
00:19:08,930 --> 00:19:11,630
Jakie znaczenie mają tu szachy?

143
00:19:13,550 --> 00:19:17,700
Windom Earle i ja graliśmy w nie
codziennie przez trzy lata.

144
00:19:19,260 --> 00:19:23,900
Uważał, że na szachownicy odbijają się
wszystkie życiowe konflikty.

145
00:19:25,430 --> 00:19:27,430
Nigdy go nie pokonałem.

146
00:19:30,100 --> 00:19:32,010
Teraz mój ruch.

147
00:19:33,770 --> 00:19:37,140
Opublikuję go w miejscowej gazecie.

148
00:19:37,800 --> 00:19:39,820
Tylko to mogę zrobić.

149
00:19:41,350 --> 00:19:44,140
Musisz mi powiedzieć coś więcej.

150
00:19:46,670 --> 00:19:50,700
Są sprawy, o których ci nie mówiłem.

151
00:19:57,830 --> 00:19:59,460
Windom Earle był moim
pierwszym partnerem.

152
00:20:00,530 --> 00:20:03,210
Wszystkiego, co wiem,
nauczyłem się od niego.

153
00:20:04,900 --> 00:20:09,840
Cztery lata temu przydzielono nas
do ochrony ważnego świadka.

154
00:20:10,920 --> 00:20:16,630
To była piękna i delikatna kobieta.

155
00:20:18,140 --> 00:20:19,830
Miała na imię Caroline.

156
00:20:20,800 --> 00:20:22,360
Pokochaliśmy się.

157
00:20:27,600 --> 00:20:32,310
Pewnej nocy straciłem czujność.

158
00:20:33,390 --> 00:20:37,000
Doszło do zamachu,
a ja nie byłem gotów.

159
00:20:37,080 --> 00:20:40,190
Zostałem ranny
i straciłem przytomność.

160
00:20:40,280 --> 00:20:44,190
Gdy się ocknąłem,
leżała w moich ramionach.

161
00:20:44,840 --> 00:20:47,600
Nie żyła. Dostała nożem.

162
00:20:50,550 --> 00:20:52,510
Tak jak włóczęga?

163
00:20:53,580 --> 00:20:54,790
Identycznie.

164
00:20:56,840 --> 00:20:59,200
Zabójcy nie ujęto.

165
00:20:59,280 --> 00:21:02,650
Ja wyzdrowiałem, Windom oszalał.

166
00:21:02,730 --> 00:21:05,230
Niedawno uciekł z zakładu.

167
00:21:05,850 --> 00:21:07,900
Dlaczego cię prześladuje?

168
00:21:10,460 --> 00:21:12,610
Caroline była jego żoną.

169
00:21:24,180 --> 00:21:25,880
Wini cię za jej śmierć.

170
00:21:31,050 --> 00:21:32,990
O wiele gorzej.

171
00:21:34,630 --> 00:21:35,930
Myślę, że to on ją zabił.

172
00:21:37,280 --> 00:21:40,730
I popełnił zbrodnię,
której była świadkiem.

173
00:21:43,070 --> 00:21:46,130
Umysł Windoma jest jak diament:

174
00:21:46,730 --> 00:21:50,660
zimny, twardy i doskonały.

175
00:21:52,750 --> 00:21:55,360
Wydaje mi się, że symulował obłęd.

176
00:21:55,450 --> 00:22:00,650
W końcu utracił zdolność
odróżniania dobra od zła.

177
00:22:04,100 --> 00:22:07,090
Nie wiesz, do czego jest zdolny.

178
00:22:08,270 --> 00:22:10,010
Nie masz pojęcia.

179
00:22:30,230 --> 00:22:31,900
Poproszę kawę.

180
00:22:42,720 --> 00:22:45,230
Szukam motocyklisty, Jamesa.

181
00:22:49,360 --> 00:22:51,620
Potrzebujesz pomocy?

182
00:23:01,060 --> 00:23:02,840
Szukam kogoś.

183
00:23:03,570 --> 00:23:05,080
Jak się nazywa?

184
00:23:06,320 --> 00:23:07,620
James.

185
00:23:09,610 --> 00:23:10,690
James Hurley?

186
00:23:16,200 --> 00:23:17,270
Tak.

187
00:23:18,620 --> 00:23:22,490
- Zna go pani?
- Ostatnio u mnie pracował.

188
00:23:23,270 --> 00:23:27,040
- Co robił?
- Reperował samochód męża.

189
00:23:29,370 --> 00:23:30,650
Gdzie jest?

190
00:23:31,620 --> 00:23:34,590
Wczoraj wyjechał.

191
00:23:35,530 --> 00:23:36,840
Dokąd?

192
00:23:37,420 --> 00:23:40,720
Mówił coś o oceanie. Chyba do Meksyku.

193
00:23:42,360 --> 00:23:43,850
To wszystko?

194
00:23:44,530 --> 00:23:46,100
To od ciebie ucieka?

195
00:23:49,710 --> 00:23:51,560
Jedź do domu, dziecko.

196
00:23:52,250 --> 00:23:53,690
Wróci.

197
00:24:01,200 --> 00:24:02,320
Zapłacę za kawę.

198
00:25:04,150 --> 00:25:08,310
Idzie na Waszyngton i jest już blisko.

199
00:25:09,890 --> 00:25:12,480
- Ben?
- Jest generałem.

200
00:25:19,670 --> 00:25:20,490
Generale?

201
00:25:24,990 --> 00:25:26,240
Jeb!

202
00:25:26,840 --> 00:25:29,280
Jesteś generałem Jebem Stuartem.

203
00:25:31,280 --> 00:25:32,920
Przybył pan z kawalerią.

204
00:25:35,310 --> 00:25:39,250
Jeb, przed nami wielki dzień.

205
00:25:40,340 --> 00:25:45,830
O świcie dotrzemy
do przedmieść Waszyngtonu,

206
00:25:46,520 --> 00:25:48,970
a do południa miasto padnie.

207
00:25:49,880 --> 00:25:52,820
I znów będziemy spacerować

208
00:25:52,910 --> 00:25:55,770
po naszych ukochanych
polach Arlingtonu.

209
00:26:04,350 --> 00:26:06,880
Tylko Bóg może nas zatrzymać.

210
00:26:06,970 --> 00:26:10,040
A ja głęboko wierzę...

211
00:26:11,150 --> 00:26:14,710
że Wszechmogący jest południowcem.

212
00:26:21,990 --> 00:26:25,360
Myśli pan, że powinien nosić szpadę?

213
00:26:26,710 --> 00:26:31,350
Uważa, że idzie na Waszyngton.
Przekracza granicę Maryland.

214
00:26:31,590 --> 00:26:34,220
Zachowuje się całkiem zdrowo.

215
00:26:34,300 --> 00:26:37,470
Odwracając klęskę Południa
w wojnie secesyjnej,

216
00:26:37,550 --> 00:26:41,330
wyjdzie z psychicznego dołka.

217
00:26:41,420 --> 00:26:46,210
Trzeba mu tylko waszego zrozumienia
i zwycięstwa Konfederatów.

218
00:26:46,300 --> 00:26:50,990
Południowcy! Będą o nas
pamiętać aż do końca świata.

219
00:26:51,680 --> 00:26:54,200
O nas, którzy tu dziś walczymy!

220
00:26:55,680 --> 00:27:00,540
O naszej odwadze
i odniesionych w boju ranach!

221
00:27:01,750 --> 00:27:03,690
Jest nas mało...

222
00:27:06,260 --> 00:27:08,690
ale szczęśliwych!

223
00:27:14,260 --> 00:27:20,120
Za mną, bracia! Na Waszyngton!

224
00:27:20,200 --> 00:27:24,420
Chcę ujrzeć znowu mój bawełny kraj,

225
00:27:24,500 --> 00:27:28,690
bo to prawdziwy raj!

226
00:28:02,400 --> 00:28:04,230
Majorze Briggs?

227
00:28:05,380 --> 00:28:07,120
Chcę się widzieć z szeryfem.

228
00:28:08,810 --> 00:28:10,250
Majorze!

229
00:28:16,600 --> 00:28:17,710
Dolać?

230
00:28:18,970 --> 00:28:20,300
Mam dość.

231
00:28:20,380 --> 00:28:22,090
- Wystarczy.
- Mnie też.

232
00:28:23,660 --> 00:28:27,450
Majorze, co się stało?

233
00:28:27,530 --> 00:28:30,190
Wy także poświęciliście życie służbie,

234
00:28:30,270 --> 00:28:35,360
więc rozumiecie, jak wielką wagę
przykładam do złożonej przysięgi.

235
00:28:35,440 --> 00:28:38,420
I jaka może być cena za jej złamanie.

236
00:28:38,510 --> 00:28:40,020
Bardzo wysoka.

237
00:28:40,110 --> 00:28:42,390
Sądziłem, że lotnictwo

238
00:28:42,480 --> 00:28:47,450
nie różni się od innych stowarzyszeń
poświęcających się walce o dobro.

239
00:28:48,660 --> 00:28:54,410
Mam jednak pewne obawy.
Wypytując mnie o moje zniknięcie,

240
00:28:54,490 --> 00:28:59,800
przełożeni okazali podejrzliwość
i nietolerancję bliskie paranoi.

241
00:29:00,830 --> 00:29:05,220
Okazuje się, iż ich motywy
poszukiwania Białej Chaty

242
00:29:05,310 --> 00:29:07,830
nie są czyste.

243
00:29:09,570 --> 00:29:15,230
Sądzę, że podczas zniknięcia
zostałem tam zabrany.

244
00:29:16,840 --> 00:29:19,580
Zupełnie nic nie pamiętam,

245
00:29:20,270 --> 00:29:26,840
ale mam bardzo wyraźne przeczucie,
że czeka nas jakieś nieszczęście.

246
00:29:28,310 --> 00:29:33,210
- Jakie, majorze?
- Nie wiem, jaką przybierze formę.

247
00:29:34,590 --> 00:29:36,250
Wrócę tu.

248
00:29:37,300 --> 00:29:42,460
A gdybym był potrzebny,
będę wśród cieni.

249
00:29:47,940 --> 00:29:49,200
Do widzenia.

250
00:29:55,630 --> 00:29:59,120
Wśród cieni? Rozumiesz to?

251
00:29:59,210 --> 00:30:00,330
Nie.

252
00:30:01,030 --> 00:30:04,680
Szeryfie, agencie Cooper,
musicie coś zobaczyć.

253
00:30:18,810 --> 00:30:20,440
Wszyscy powinniście to usłyszeć.

254
00:30:20,520 --> 00:30:24,060
Spędziłem ostatnią dobę
w towarzystwie tej uroczej damy.

255
00:30:24,150 --> 00:30:29,430
I nie widać żadnych sińców
ani połamanych kości.

256
00:30:29,510 --> 00:30:32,630
Twierdzenie brata jej zmarłego męża,

257
00:30:32,710 --> 00:30:37,090
że w jakiś sposób odpowiada
za ten zgon, jest bzdurą.

258
00:30:37,480 --> 00:30:42,350
Naprawdę jest tylko obdarzona
wzmożonym popędem seksualnym

259
00:30:42,440 --> 00:30:45,730
i posiada głęboką wiedzę
o technikach, anatomii

260
00:30:45,810 --> 00:30:52,930
i sposobach, jakich mało który
mężczyzna miał szczęście zakosztować.

261
00:30:56,140 --> 00:30:58,660
- Zrobiło się gorąco?
- Tak.

262
00:31:02,210 --> 00:31:05,420
Lano, pierwszy ci pogratuluję.

263
00:31:05,500 --> 00:31:06,820
Dziękuję.

264
00:31:08,980 --> 00:31:11,550
Bez doktora nie dałabym rady.

265
00:31:14,410 --> 00:31:16,050
No to chodźmy na kręgle.

266
00:31:19,070 --> 00:31:20,860
Na razie, chłopcy.

267
00:31:33,870 --> 00:31:35,440
Ani kroku!

268
00:31:36,290 --> 00:31:40,080
Burmistrzu, proszę opuścić broń.

269
00:31:40,170 --> 00:31:43,810
Rozwalę każdego, kto się ruszy!

270
00:31:43,890 --> 00:31:45,750
Hipisa też.

271
00:31:47,490 --> 00:31:48,710
Dwayne...

272
00:31:50,440 --> 00:31:52,870
to niczego nie załatwi.

273
00:31:52,950 --> 00:31:56,750
- Zabiła mi brata.
- Nikogo nie zabiła.

274
00:31:56,840 --> 00:31:57,510
Gadanie!

275
00:31:57,890 --> 00:32:00,720
Panowie, mam pomysł.

276
00:32:00,810 --> 00:32:04,350
Niech Lana i burmistrz
omówią to na osobności.

277
00:32:04,430 --> 00:32:06,810
Nie będę gadał, tylko strzelał!

278
00:32:07,030 --> 00:32:09,940
Strzelać może pan później.
Teraz porozmawiajcie.

279
00:32:10,880 --> 00:32:12,320
Proszę.

280
00:32:13,220 --> 00:32:15,650
Lano, nie bój się. Burmistrzu.

281
00:32:18,440 --> 00:32:20,910
Zapraszam.

282
00:32:37,490 --> 00:32:40,020
- I co teraz?
- Czekamy.

283
00:32:40,640 --> 00:32:42,160
Czekamy...

284
00:32:55,170 --> 00:32:58,600
Trudno tak... czekać.

285
00:33:02,750 --> 00:33:03,800
Dobra.

286
00:33:04,840 --> 00:33:06,000
Gotowi?

287
00:33:08,110 --> 00:33:09,120
Wchodzimy.

288
00:33:18,340 --> 00:33:21,260
Chcemy adoptować dziecko.

289
00:33:24,680 --> 00:33:30,480
Jest tak podobny do Dougiego.
Jakby mój mąż powrócił do życia.

290
00:33:30,560 --> 00:33:34,240
Byłem samotny i samolubny.

291
00:33:35,630 --> 00:33:39,100
Mam nadzieję,
że mi wybaczycie mój wyskok.

292
00:33:40,520 --> 00:33:42,290
Chodź, moja droga.

293
00:33:49,230 --> 00:33:53,330
Nie wiem jak wy,
ale ja bym się czegoś napił.

294
00:33:53,420 --> 00:33:54,080
My też.

295
00:34:06,370 --> 00:34:08,230
Hot dogi.

296
00:34:09,320 --> 00:34:12,220
Zapomnieliśmy o hot dogach...

297
00:34:12,300 --> 00:34:16,610
Nie będzie wołowych kiełbasek...
Cała wołowina...

298
00:34:16,690 --> 00:34:20,580
- Josie pojechała na zakupy?
- Tak, już wyszła.

299
00:34:20,670 --> 00:34:23,700
Czy mógłbyś na chwileczkę
oderwać się od problemów

300
00:34:23,780 --> 00:34:27,060
na najwyższym poziomie zarządzania?

301
00:34:27,720 --> 00:34:29,890
Chcę ci coś pokazać.

302
00:34:33,680 --> 00:34:40,500
Zastanawiałeś się kiedyś,
jak przeżyłam po pożarze?

303
00:34:41,510 --> 00:34:48,390
Jak zdobyłam środki,
dzięki którym odebrałam Benowi tartak?

304
00:34:48,470 --> 00:34:52,060
Skoro o tym mówisz, to tak.

305
00:34:52,740 --> 00:34:53,980
Chodź.

306
00:35:03,080 --> 00:35:04,410
Pete Martell.

307
00:35:09,410 --> 00:35:11,110
Andrew?

308
00:35:13,440 --> 00:35:15,290
Widziałem tę łódź.

309
00:35:16,560 --> 00:35:17,540
Ty...

310
00:35:17,630 --> 00:35:21,250
Rozumiem cię. Kochany Pete.

311
00:35:21,340 --> 00:35:25,260
Jak się już domyślasz,
pomógł mi mój brat.

312
00:35:26,880 --> 00:35:29,230
Powiedzmy to wprost.

313
00:35:30,990 --> 00:35:34,930
Nie zginąłeś w tamtym wypadku.

314
00:35:35,270 --> 00:35:39,810
Nie powiedzieliśmy ci, przepraszam.
Ale nie musiałeś wiedzieć.

315
00:35:42,880 --> 00:35:47,570
Dowiedzieliśmy się o zamachu,
więc urządziliśmy ten pokaz.

316
00:35:48,170 --> 00:35:51,190
Kto chciał cię sprzątnąć?

317
00:35:51,270 --> 00:35:55,360
Mówi ci coś nazwisko
Thomas Eckhardt?

318
00:35:57,590 --> 00:35:58,580
Nie.

319
00:35:58,860 --> 00:36:01,210
Prowadziliśmy wspólne interesy.

320
00:36:01,290 --> 00:36:06,850
Miałem drewno, on znał Hongkong.
Dużo zarobiliśmy i zabawiliśmy się.

321
00:36:06,930 --> 00:36:12,350
A później zrobiłem lepszy interes
i próbował wbić mi nóż w plecy.

322
00:36:13,740 --> 00:36:17,560
- Naprawdę?
- To groźny i mocny przeciwnik.

323
00:36:17,640 --> 00:36:20,270
Przez sześć lat
planował zabójstwo Andrew.

324
00:36:20,860 --> 00:36:23,820
Josie wie, że żyjesz?

325
00:36:25,560 --> 00:36:27,860
Josie pracowała dla Eckhardta.

326
00:36:30,020 --> 00:36:32,030
Mój Boże!

327
00:36:33,380 --> 00:36:34,410
Teraz też?

328
00:36:37,550 --> 00:36:40,460
Zobaczymy, gdy przyjedzie Eckhardt.

329
00:36:40,820 --> 00:36:42,370
A przyjedzie?

330
00:36:42,450 --> 00:36:46,840
Nikt nie oprze się Josie.

331
00:36:46,920 --> 00:36:51,150
Przybiegnie jak szczur do sera.

332
00:36:58,690 --> 00:37:01,020
Czym mogę służyć?

333
00:37:01,290 --> 00:37:03,860
- Zarezerwowaliśmy dwa pokoje.
- Nazwisko?

334
00:37:03,950 --> 00:37:08,320
Eckhardt. Thomas Eckhardt.

335
00:37:09,840 --> 00:37:11,400
Witamy w Twin Peaks.

336
00:37:52,010 --> 00:37:54,730
Chodź, Andy, pogadajmy.

337
00:38:04,310 --> 00:38:07,410
Cooper? Mam prośbę.

338
00:38:07,490 --> 00:38:11,320
Ustalmy coś. Pracuję u ciebie.

339
00:38:11,400 --> 00:38:13,800
Jeśli każesz mi pić kawę, to piję.

340
00:38:16,700 --> 00:38:18,480
Dostałem to z Seattle.

341
00:38:18,570 --> 00:38:19,890
Zamordowany Azjata

342
00:38:20,460 --> 00:38:22,560
Josie twierdzi, że mu uciekła.

343
00:38:24,590 --> 00:38:28,030
- Ma z tym coś wspólnego?
- Nie wiem.

344
00:38:28,650 --> 00:38:30,660
Chcę, żebyś to zbadał.

345
00:38:37,680 --> 00:38:42,250
Gdy Lucy powiedziała mi o waszej
teorii, zadzwoniłem do sierocińca.

346
00:38:43,580 --> 00:38:47,020
Nicky jest takim samym mordercą
jak Lucy i ja.

347
00:38:47,110 --> 00:38:50,830
- Widziałem słowo patologiczny...
- Nie upuściłem cię na głowę,

348
00:38:50,910 --> 00:38:55,180
przyjmując na ten świat,
i nie chciałbym tego żałować.

349
00:38:55,260 --> 00:38:58,680
- Chodzi o to, że...
- Zamknijcie się i słuchajcie!

350
00:39:00,130 --> 00:39:03,580
Nicky przyszedł na świat
tylnymi drzwiami

351
00:39:03,660 --> 00:39:06,380
i bez nadziei na powodzenie.

352
00:39:07,130 --> 00:39:11,590
Jego matką była imigrantka,
pokojówka z hotelu.

353
00:39:12,200 --> 00:39:17,350
Nicky był owocem gwałtu,
a sprawca zwiał za granicę.

354
00:39:17,430 --> 00:39:22,940
Nie zgłosiła tego na policję,
zwierzyła się tylko mnie.

355
00:39:24,500 --> 00:39:29,760
Marzyła o lepszym życiu
dla siebie i swojego dziecka.

356
00:39:29,840 --> 00:39:33,080
Po to przyjechała do tego kraju.

357
00:39:33,750 --> 00:39:37,060
Postanowiła urodzić.

358
00:39:38,790 --> 00:39:42,160
Ale biedaczka nawet go nie zobaczyła,

359
00:39:42,250 --> 00:39:44,970
bo umarła, gdy tylko wyjrzał na świat.

360
00:39:45,540 --> 00:39:50,020
Pochowaliśmy ją,
a mały trafił do domu dziecka.

361
00:39:51,060 --> 00:39:55,890
W następnych latach przesyłali go
z jednej placówki do drugiej,

362
00:39:55,970 --> 00:39:59,880
aż zaświeciła nadzieja.

363
00:40:00,380 --> 00:40:03,700
Wzięło go bezdzietne małżeństwo.

364
00:40:04,480 --> 00:40:12,990
Byli wspaniałymi rodzicami. Ale to
się skończyło na oblodzonej drodze.

365
00:40:13,080 --> 00:40:16,390
Doszło do wypadku.
Sześcioletni Nicky zdołał

366
00:40:16,470 --> 00:40:20,310
wyciągnąć rodziców z płonącego wraku,
ale było za późno.

367
00:40:20,400 --> 00:40:22,100
Nicky znów był sam.

368
00:40:24,010 --> 00:40:25,850
I tak jest do dziś.

369
00:40:28,100 --> 00:40:30,730
Jesteście zadowoleni?

370
00:40:54,260 --> 00:40:58,110
- Co robisz?
- A jak ci się wydaje?

371
00:40:58,810 --> 00:41:02,370
Wyjeżdżasz?

372
00:41:02,460 --> 00:41:04,410
Nie mogę zostać!

373
00:41:06,410 --> 00:41:08,830
- Nie rozumiem.
- Tak nie wolno!

374
00:41:10,010 --> 00:41:11,900
Bo mam męża?

375
00:41:11,980 --> 00:41:13,480
Tak!

376
00:41:18,960 --> 00:41:20,610
Kocham cię.

377
00:41:23,590 --> 00:41:28,240
Jeszcze nikomu tego
nie powiedziałam. Kocham cię.

378
00:41:46,850 --> 00:41:50,680
Był wypadek. Jeffrey zginął.

379
00:41:53,040 --> 00:41:56,020
- W samochodzie?
- Tak.

380
00:41:58,850 --> 00:42:00,650
Zabiłaś go.

381
00:42:02,740 --> 00:42:03,740
Nie...

382
00:42:06,220 --> 00:42:07,450
Wrobiłaś mnie!

383
00:42:07,530 --> 00:42:10,540
To był pomysł Malcolma!
Nie jest moim bratem.

384
00:42:12,340 --> 00:42:13,500
Uciekaj.

385
00:42:15,170 --> 00:42:16,830
Wracaj do dziewczyny,
która cię kocha.

386
00:42:25,640 --> 00:42:26,470
Jedź!

387
00:42:42,000 --> 00:42:43,380
Pospieszmy się.

388
00:42:53,360 --> 00:42:54,940
Chodź!

389
00:43:58,880 --> 00:44:02,150
Była niegrzeczna.

390
00:44:12,610 --> 00:44:15,640
Nie bój się, wejdź.

391
00:44:15,720 --> 00:44:17,910
Jestem przyjacielem.

392
00:44:23,710 --> 00:44:24,980
Wejdź.

393
00:44:29,670 --> 00:44:32,200
Miałeś ciężką noc.

394
00:44:33,760 --> 00:44:35,560
Siadaj.

395
00:44:36,930 --> 00:44:38,320
Pomogę ci.

396
00:44:40,500 --> 00:44:44,310
- Jak masz na imię?
- Leo.

397
00:44:44,390 --> 00:44:45,710
Leo?

398
00:44:52,790 --> 00:44:55,070
No cóż, Leo...

399
00:44:56,770 --> 00:44:58,950
Ja jestem Windom.

400
00:45:01,140 --> 00:45:02,660
Windom Earle.

