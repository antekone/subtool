1
00:00:00,000 --> 00:00:00,040
"Miasteczko Twin Peaks" odc. 9 (2x02)
kraps v4.01 23.02.07

2
00:01:55,320 --> 00:02:00,370
Buddyzm dotarł do krainy śniegów
w V wieku naszej ery.

3
00:02:00,530 --> 00:02:04,540
Pierwszym królem Tybetu - buddystą
był Ha-tho-tho-ri Gnyan-btsan.

4
00:02:04,700 --> 00:02:09,370
On i jego następcy znani są jako
"szczęśliwe pokolenia".

5
00:02:09,420 --> 00:02:13,800
Cześć historyków umieszcza ich
w Roku Węża - 213 n.e.

6
00:02:14,090 --> 00:02:17,590
Inni w Roku Wołu - 173 n.e.

7
00:02:18,220 --> 00:02:20,890
Niesamowite - "szczęśliwe pokolenia".

8
00:02:20,930 --> 00:02:24,680
Cieszę się, że buddyzm
zstąpił na króla Ho-Ho-Ho

9
00:02:24,890 --> 00:02:29,640
ale spróbujmy skupić się na problemach
naszego wieku, tu w Twin Peaks.

10
00:02:29,690 --> 00:02:32,860
Albercie, byłbyś zaskoczony
istniejącymi związkami.

11
00:02:33,440 --> 00:02:34,860
Zaskocz mnie.

12
00:02:34,900 --> 00:02:37,440
Ronette Pulaski obudziła się
ze śpiączki.

13
00:02:38,360 --> 00:02:39,570
No i...?

14
00:02:40,110 --> 00:02:44,660
Myślę, że opowie nam ciekawą historię,
kiedy wróci jej mowa.

15
00:02:44,700 --> 00:02:45,990
Nie mówi?

16
00:02:46,040 --> 00:02:49,660
Przytomna, ale milczy.
Prawdopodobnie szok.

17
00:02:51,580 --> 00:02:56,630
Pokażę jej portrety Leo Johnsona
i Boba, który ukazał się Sarah Palmer.

18
00:02:56,800 --> 00:02:58,800
Tego, który mi się przyśnił.

19
00:02:58,840 --> 00:03:01,840
Czy ktoś widział tego Boba na ziemi
w ostatnich tygodniach?

20
00:03:01,890 --> 00:03:03,180
Jeszcze nie.

21
00:03:04,050 --> 00:03:09,100
Dobra. Zrobiłem sekcję zwłok
Jacques’a Renault. W żołądku znalazłem...

22
00:03:09,270 --> 00:03:14,310
puszki po piwie, tablicę rejestracyjną
z Maryland, pół dętki rowerowej, kozła

23
00:03:14,480 --> 00:03:17,860
i małą drewnianą laleczkę
imieniem Pinokio.

24
00:03:18,110 --> 00:03:19,440
Żartujesz.

25
00:03:19,490 --> 00:03:22,780
Miło jest należeć do
szczęśliwego pokolenia.

26
00:03:22,910 --> 00:03:26,950
Zabójca udusił grubasa poduszką.

27
00:03:26,990 --> 00:03:32,170
W rękawiczkach. Związał go taśmą
z magazynu szpitalnego.

28
00:03:32,210 --> 00:03:36,210
Resztę tajemnic Jacques
zabierze pod ziemię.

29
00:03:36,380 --> 00:03:37,380
A tartak?

30
00:03:38,590 --> 00:03:41,090
Podpalenie.
Zapewne Leo Johnson.

31
00:03:42,590 --> 00:03:44,760
Przesłuchamy Shelly Johnson.

32
00:03:44,890 --> 00:03:46,550
Ta przynajmniej może mówić.

33
00:03:48,850 --> 00:03:49,810
Jak się czujesz?

34
00:03:51,640 --> 00:03:52,600
Ja?

35
00:03:52,640 --> 00:03:57,020
Wypada zapytać o zdrowie osoby,
do której trzykrotnie strzelono.

36
00:03:57,190 --> 00:04:00,150
- Dziękuję za troskę.
- Tylko się nie rozklejaj.

37
00:04:00,320 --> 00:04:02,240
Kto do mnie strzelał?

38
00:04:02,400 --> 00:04:04,160
Moi ludzie przepytali
gości hotelowych

39
00:04:04,200 --> 00:04:07,580
tę bandę wioskowych głupków
i pijanych wędkarzy.

40
00:04:07,620 --> 00:04:13,670
Na razie nic.
Zramolały kelner nic nie zauważył.

41
00:04:13,870 --> 00:04:19,050
Nic dziwnego.
Stary piernik nieco szwankuje na umyśle.

42
00:04:21,130 --> 00:04:24,930
Albercie,
zniknęła moja obrączka.

43
00:04:27,390 --> 00:04:31,390
Jednego dnia była,
następnego dnia już nie.

44
00:04:31,560 --> 00:04:33,560
I co z tego?

45
00:04:33,600 --> 00:04:36,730
Cieszę się, że jesteś z nami.
Potrzeba nam najlepszych.

46
00:04:36,770 --> 00:04:40,780
Sprowadziło mnie tutaj
nie tylko poczucie obowiązku.

47
00:04:40,940 --> 00:04:42,490
Co jeszcze?

48
00:04:42,530 --> 00:04:45,410
Windom Earle.

49
00:04:46,160 --> 00:04:51,200
Agent Earle?
Przeszedł w stan spoczynku.

50
00:04:52,410 --> 00:04:57,080
Spoczywał przywiązany
do fotela w wariatkowie.

51
00:04:57,130 --> 00:05:00,250
- Co się stało?
- Nie wiadomo.

52
00:05:01,750 --> 00:05:06,930
Twój dawny partner zabrał
swój kuper, Cooper. Uciekł.

53
00:05:06,970 --> 00:05:11,180
- Rozpłynął się w powietrzu.
- Niedobrze.

54
00:06:09,490 --> 00:06:13,370
Pani Tremond? Przywiozłam obiad.

55
00:06:15,410 --> 00:06:17,870
Proszę.

56
00:06:25,090 --> 00:06:27,880
Pani Tremond?

57
00:06:30,300 --> 00:06:34,890
- Mogę postawić tu jedzenie?
- Tak, oczywiście.

58
00:06:50,110 --> 00:06:51,780
Panienko.

59
00:06:51,820 --> 00:06:53,160
Cześć.

60
00:06:53,200 --> 00:06:58,120
Czasem coś zdarza się ot tak.

61
00:07:07,800 --> 00:07:10,800
Duszona kukurydza.

62
00:07:13,720 --> 00:07:18,060
Widzisz duszoną kukurydzę
na moim talerzu?

63
00:07:20,310 --> 00:07:22,230
Tak.

64
00:07:22,400 --> 00:07:26,190
Prosiłam, żeby mi jej nie dawali.

65
00:07:28,650 --> 00:07:32,950
Widzisz duszoną kukurydzę
na moim talerzu?

66
00:07:35,910 --> 00:07:38,200
Nie

67
00:07:45,290 --> 00:07:49,630
Mój wnuk zajmuje się magią.

68
00:07:51,550 --> 00:07:54,180
To urocze.

69
00:08:00,930 --> 00:08:03,640
Kim jesteś?

70
00:08:04,060 --> 00:08:08,610
Zastępuję Laurę Palmer
przy rozwożeniu posiłków.

71
00:08:11,320 --> 00:08:13,990
Ona umarła.

72
00:08:15,490 --> 00:08:18,740
Dobrze ją pani znała?

73
00:08:23,830 --> 00:08:26,130
Nie.

74
00:08:33,220 --> 00:08:38,800
Pójdę już, pani Tremond.
Przyjadę jutro i przywiozę obiad.

75
00:08:39,470 --> 00:08:43,390
Przywozili mi jedzenie
ze szpitala.

76
00:08:43,640 --> 00:08:46,650
Wyobrażasz sobie?

77
00:08:49,860 --> 00:08:55,660
Młoda damo,
zapytaj pana Smitha z sąsiedztwa.

78
00:08:56,110 --> 00:08:59,410
Był przyjacielem Laury.

79
00:09:00,280 --> 00:09:04,870
J'ai une ame solitaire.

80
00:09:05,500 --> 00:09:09,540
Pan Smith nie wychodzi z domu.

81
00:09:54,460 --> 00:09:57,380
Miła dziewczyna.

82
00:10:07,980 --> 00:10:11,190
[ Drogi Panie Smith. ]

83
00:10:51,100 --> 00:10:54,190
Dziękuję, Harry.

84
00:11:05,280 --> 00:11:07,790
Ronette.

85
00:11:19,880 --> 00:11:23,510
Nie wiem, jak to działa.

86
00:11:26,810 --> 00:11:30,350
Wybacz, Ronette.

87
00:11:31,310 --> 00:11:33,900
"Uwaga!"

88
00:11:35,480 --> 00:11:42,110
"Przed użyciem obrotowego stołka
przeczytać instrukcję obsługi pod spodem."

89
00:11:45,910 --> 00:11:49,910
- Nie dam rady
- "Stanąć obok stołka bądź krzesła."

90
00:11:50,080 --> 00:11:55,670
"Nie siadać.
Umieścić stopę na podstawie."

91
00:11:55,710 --> 00:12:01,880
"Następnie pociągnąć siedzenie
aż do usłyszenia trzaśnięcia."

92
00:12:07,140 --> 00:12:09,970
Przepraszamy, Ronette.

93
00:12:21,740 --> 00:12:24,660
Słyszysz mnie?

94
00:12:26,950 --> 00:12:30,330
Cieszę się, że już ci lepiej.

95
00:12:31,750 --> 00:12:35,750
Nie chcę cię denerwować,
ale potrzebujemy twojej pomocy.

96
00:12:35,920 --> 00:12:39,000
Pokażę ci kilka portretów.

97
00:12:39,300 --> 00:12:42,880
Powiedz, czy poznajesz tych mężczyzn.

98
00:12:43,220 --> 00:12:46,930
Czy to oni coś ci zrobili.

99
00:12:52,560 --> 00:12:55,520
Rozpoznajesz go?

100
00:13:00,900 --> 00:13:04,320
Czy to on cię skrzywdził?

101
00:13:15,500 --> 00:13:17,880
Nie...

102
00:13:48,110 --> 00:13:50,870
Pociąg... w pociągu...

103
00:13:50,910 --> 00:13:54,910
- Pociąg... pociąg...
- Siostro! Szybko!

104
00:13:55,080 --> 00:14:00,130
- Pociąg? Jesteś w wagonie kolejowym?
- Pociąg... tak...

105
00:14:00,290 --> 00:14:03,210
Wagon kolejowy?

106
00:14:06,510 --> 00:14:11,220
W porządku, uspokój się.

107
00:14:12,760 --> 00:14:16,770
Bracie Ben, mamy dwie księgi

108
00:14:16,930 --> 00:14:19,940
i jedną wędzoną serową świnkę.

109
00:14:20,060 --> 00:14:21,980
Co spalimy?

110
00:14:22,900 --> 00:14:25,230
Na pewno nie moją świnkę.

111
00:14:29,450 --> 00:14:35,290
W prawdziwej księdze widać,
jak tartak zmierza do bankructwa.

112
00:14:35,330 --> 00:14:38,580
Catherine to wymyśliła.

113
00:14:38,830 --> 00:14:45,750
W oficjalnej - fałszywej,
tartak świetnie prosperuje.

114
00:14:46,090 --> 00:14:49,800
Więc którą spalimy?

115
00:14:50,260 --> 00:14:55,220
Catherine nie żyje.
Stawiamy na Josie.

116
00:14:55,470 --> 00:14:59,270
Na Catherine spada wina za pożar.

117
00:14:59,640 --> 00:15:06,110
Josie zgarnia polisę.
Sprzedaje nam tartak i ziemię i...

118
00:15:06,730 --> 00:15:13,070
Nie... zaraz.
Najpierw musi uzyskać podpis Pete'a.

119
00:15:13,120 --> 00:15:16,290
Dlaczego Pete musi podpisać?

120
00:15:16,330 --> 00:15:19,250
Zgodnie z testamentem Andrew,

121
00:15:19,290 --> 00:15:23,710
Catherine lub jej spadkobiercy
muszą zaakceptować sprzedaż.

122
00:15:23,750 --> 00:15:26,550
Więc którą spalimy?

123
00:15:27,210 --> 00:15:33,640
Kiedy Josie sprzeda nam tartak,
zyskamy na nim, jeśli okaże się rentowny.

124
00:15:33,680 --> 00:15:35,510
Więc zniszczymy prawdziwą?

125
00:15:35,680 --> 00:15:41,940
Z drugiej strony prawdziwa
stawia nas poza podejrzeniami.

126
00:15:45,060 --> 00:15:51,780
W zależności od sytuacji
obie mogą się przydać.

127
00:15:52,360 --> 00:15:53,570
Wiem.

128
00:15:55,280 --> 00:15:57,530
Może powinniśmy spalić obydwie?

129
00:15:57,580 --> 00:16:02,580
Jesteśmy w 100% pewni,
że nie mamy pewności.

130
00:16:05,420 --> 00:16:08,960
Coś trzeba spalić.

131
00:16:09,000 --> 00:16:15,220
I jeśli nie twoją wędzoną świnkę...

132
00:16:17,350 --> 00:16:21,270
...to może...

133
00:16:21,310 --> 00:16:23,560
...spróbujemy to?

134
00:16:23,940 --> 00:16:26,900
Pianki żelowe?

135
00:16:27,270 --> 00:16:29,940
Mamy jeszcze te leszczynowe kijki?

136
00:16:36,530 --> 00:16:40,080
[ CZY WIDZIAŁEŚ TEGO MĘŻCZYZNĘ? ]

137
00:17:26,080 --> 00:17:30,250
Margaret.
Jak zawsze miło cię widzieć...

138
00:17:31,300 --> 00:17:35,920
ale jeśli zamierzasz wypluwać żywicę,
proponuję użyć popielniczki,

139
00:17:35,970 --> 00:17:41,100
a nie kontuaru lub kabiny
telefonicznej, jak ostatnim razem.

140
00:17:44,850 --> 00:17:48,560
Poproszę "niedźwiedzi pazur".

141
00:17:57,320 --> 00:18:01,330
Masz na piersi błyszczące przedmioty.

142
00:18:01,490 --> 00:18:03,410
Tak.

143
00:18:03,580 --> 00:18:07,580
- Jesteś dumny?
- Osiągnięcie to największa nagroda.

144
00:18:07,750 --> 00:18:11,340
Duma je zaćmiewa.
Śmietanki?

145
00:18:17,550 --> 00:18:21,970
Mój pieniek ma ci coś do powiedzenia.
Wiesz?

146
00:18:23,810 --> 00:18:26,770
Nie zostaliśmy sobie przedstawieni.

147
00:18:26,930 --> 00:18:30,270
Nie przedstawiam Pieńka.

148
00:18:31,110 --> 00:18:33,690
Słyszysz?

149
00:18:35,280 --> 00:18:38,200
Nie, proszę pani.

150
00:18:38,360 --> 00:18:41,240
Będę tłumaczyć.

151
00:18:47,750 --> 00:18:50,920
Przekaż... wiadomość.

152
00:18:54,000 --> 00:18:56,670
Rozumiesz?

153
00:18:59,220 --> 00:19:02,010
Tak, rozumiem.

154
00:20:08,990 --> 00:20:13,420
Nie było wiadomości,
funkcjonariuszu Brennan.

155
00:20:17,340 --> 00:20:20,300
Posłuchaj mnie, Lucy Moran.

156
00:20:20,460 --> 00:20:24,470
Bank spermy w Tacoma poszukiwał
dawców, więc się zgłosiłem.

157
00:20:24,640 --> 00:20:28,640
To mój obywatelski obowiązek
i lubię wieloryby.

158
00:20:28,810 --> 00:20:33,770
Badanie lekarskie wykazało,
że jestem sterylny.

159
00:20:33,980 --> 00:20:37,980
Myślałem, że to znaczy, że nie muszę
się kąpać, ale lekarze mi to wyjaśnili.

160
00:20:38,150 --> 00:20:42,150
Powiedzieli, że nie mogę mieć dzieci.

161
00:20:42,320 --> 00:20:48,660
Chciałbym więc wiedzieć, dlaczego
ty będziesz miała dziecko i skąd?

162
00:21:24,820 --> 00:21:28,030
[ TU SKOŃCZYŁ KOZIOŁ ]

163
00:21:34,410 --> 00:21:37,120
Cześć, Hank.

164
00:21:39,630 --> 00:21:42,590
Lucy prosiła,
żebyś poczekał na zewnątrz.

165
00:21:42,750 --> 00:21:45,260
Możliwe.

166
00:21:47,970 --> 00:21:51,140
Niezła sztuka.

167
00:21:52,510 --> 00:21:56,310
- Nie traćmy czasu.
- Dobra.

168
00:22:00,650 --> 00:22:06,780
Podpisz to i w przyszłym tygodniu
nadal bądź grzeczny.

169
00:22:28,590 --> 00:22:32,390
Miło było pana znowu widzieć.

170
00:22:45,230 --> 00:22:47,990
Interesujące.

171
00:22:48,360 --> 00:22:50,280
Od jak dawna się przyjaźnicie?

172
00:22:50,450 --> 00:22:53,410
Razem dorastaliśmy.

173
00:22:53,580 --> 00:22:56,540
Hank też był Chłopcem z Czytelni.

174
00:22:56,870 --> 00:23:01,710
W swoim czasie...
był jednym z najlepszych.

175
00:23:01,920 --> 00:23:07,630
Szeryfie, dzwoni Ben Horne.
Przełączyć go?

176
00:23:07,670 --> 00:23:11,550
To znaczy, nie jego,
tylko telefon?

177
00:23:12,300 --> 00:23:15,220
Połącz go, Lucy.

178
00:23:18,560 --> 00:23:23,610
Panie Horne, mówi Lucy.
Połączę pana z gabinetem szeryfa.

179
00:23:23,770 --> 00:23:28,610
Jest z agentem specjalnym
Dalem Cooperem. Chwileczkę.

180
00:23:36,280 --> 00:23:39,250
Szeryfie Truman,
przełączyłam Bena Horne'a.

181
00:23:39,410 --> 00:23:43,380
Na tę linię z migającym światełkiem.

182
00:23:44,630 --> 00:23:46,170
Tak, Ben?

183
00:23:46,250 --> 00:23:49,590
Harry... Audrey zaginęła.

184
00:23:49,800 --> 00:23:53,930
- Co?
- Mniej więcej... dwa dni temu.

185
00:23:53,970 --> 00:23:56,510
Zaczekaj.

186
00:23:57,100 --> 00:24:00,310
Zaginęła Audrey Horne.

187
00:24:10,650 --> 00:24:15,320
Polisa ubezpieczeniowa niepodpisana.

188
00:24:15,870 --> 00:24:18,740
Nie?

189
00:24:18,990 --> 00:24:24,170
Agent mówi, że Catherine
znalazła jakieś nieprawidłowości.

190
00:24:24,210 --> 00:24:29,170
Choćby to, że Josie miała być
głównym beneficjentem.

191
00:24:29,210 --> 00:24:31,550
Miał jej tego nie pokazywać.

192
00:24:31,590 --> 00:24:37,680
Biorąc pod uwagę los Catherine -
tym lepiej dla nas.

193
00:24:41,890 --> 00:24:44,850
Tu wygrać, tam stracić.

194
00:24:45,440 --> 00:24:49,230
Dzwoń do Islandczyków.

195
00:24:53,360 --> 00:24:56,320
Panowie, długo się zastanawiałem.

196
00:24:56,490 --> 00:24:59,450
Pożar tartaku niewątpliwie opóźni
podpisanie kontraktu.

197
00:24:59,620 --> 00:25:02,580
Postarajmy się, żeby ten czas
nie był stracony.

198
00:25:02,750 --> 00:25:06,750
Proponuję krótki, acz serdeczny
telefon do Islandczyków,

199
00:25:06,920 --> 00:25:10,460
niech Einar przekona się,
że panujemy nad sytuacją.

200
00:25:10,630 --> 00:25:12,960
Poradzimy sobie.

201
00:25:13,130 --> 00:25:16,720
Ben Horne do Einara Thorsona.

202
00:25:17,300 --> 00:25:20,810
Cóż za zbieg okoliczności...

203
00:25:21,470 --> 00:25:24,230
Witaj, Einar.

204
00:25:26,690 --> 00:25:29,400
Co takiego?

205
00:25:29,810 --> 00:25:32,530
Skąd wiesz?

206
00:25:32,940 --> 00:25:37,820
Pan Palmer dzwonił do ciebie
w sprawie pożaru.

207
00:25:41,280 --> 00:25:46,080
Nie nazywałbym tego katastrofą.
To się zdarza.

208
00:25:46,120 --> 00:25:49,420
Nieszczęśliwy wypadek.
Zapewniam cię, Einarze...

209
00:25:49,580 --> 00:25:52,550
że w najmniejszym stopniu
nie wpłynie to na nasze plany.

210
00:25:52,710 --> 00:25:57,340
Tu Jerry Horne... pozdrowienia...
cała naprzód...

211
00:25:58,970 --> 00:26:04,890
Tak, oczywiście, Einar.
Przefaksujemy wszystkie szczegóły.

212
00:26:05,230 --> 00:26:10,610
Tak, obiecuję. Nie zaprzątaj sobie
tym swojej jasnowłosej głowy.

213
00:26:10,650 --> 00:26:14,360
Mam zebranie.
Muszę kończyć.

214
00:26:18,780 --> 00:26:24,160
Zrobimy tak. Nie będziesz się
zajmować szacowaniem strat.

215
00:26:24,200 --> 00:26:27,410
Skoncentrujesz się na czymś,
co cię nie przerasta:

216
00:26:27,460 --> 00:26:29,670
na moich podatkach.

217
00:26:35,170 --> 00:26:38,930
- Znam go.
- Słucham?

218
00:26:41,680 --> 00:26:45,680
Domek mojego dziadka
nad Pearl Lakes.

219
00:26:47,930 --> 00:26:51,230
Mieszkał w sąsiedztwie.

220
00:26:52,110 --> 00:26:55,150
Byłem dzieckiem...

221
00:26:56,280 --> 00:26:59,030
Ale znam go!

222
00:27:01,490 --> 00:27:05,580
Muszę natychmiast
powiedzieć szeryfowi.

223
00:27:06,660 --> 00:27:11,040
Jerry, proszę, zabij Lelanda.

224
00:27:13,960 --> 00:27:17,710
Czy to dzieje się naprawdę?

225
00:27:17,760 --> 00:27:22,260
Czy to jakiś dziwaczny,
pokręcony sen?

226
00:27:24,390 --> 00:27:30,440
Kula utkwiła w jego kręgosłupie.
Możesz podejść, Shelly.

227
00:27:30,640 --> 00:27:35,070
Udało nam się ją wyjąć.

228
00:27:35,860 --> 00:27:39,860
Za wcześnie wyrokować,
czy będzie sparaliżowany.

229
00:27:40,030 --> 00:27:44,160
Stracił dużo krwi.
Większość przed operacją.

230
00:27:44,200 --> 00:27:51,290
Niedobór tlenu uszkodził mózg
i spowodował śpiączkę.

231
00:27:51,460 --> 00:27:54,500
Jest jak roślina?

232
00:27:57,090 --> 00:28:02,760
- Cierpi?
- Nie cierpi. Poza tym trudno powiedzieć.

233
00:28:05,010 --> 00:28:06,930
Wyleczycie go?

234
00:28:07,100 --> 00:28:14,100
Możemy jedynie podtrzymywać go
przy życiu i podawać pokarm.

235
00:28:14,150 --> 00:28:18,320
Naszym jedynym
sprzymierzeńcem jest czas.

236
00:28:18,570 --> 00:28:21,530
Przykro mi, Shelly.

237
00:28:21,570 --> 00:28:24,160
Mnie też.

238
00:28:24,780 --> 00:28:30,700
- Wsadzą go do więzienia?
- Jest podejrzany o różne przestępstwa.

239
00:28:34,170 --> 00:28:37,590
I tak jest jakby w więzieniu.

240
00:28:37,630 --> 00:28:40,420
Zgadza się.

241
00:28:51,890 --> 00:28:55,270
Zaprowadzę cię do pokoju.

242
00:28:57,110 --> 00:29:00,230
Tom czeka na dole.
Odwiezie cię do domu.

243
00:29:12,700 --> 00:29:15,670
Słucham, posterunek w Twin Peaks.

244
00:29:15,830 --> 00:29:19,210
Mogę wiedzieć, kto mówi?

245
00:29:20,000 --> 00:29:25,880
Przykro mi, nie mogę nikogo połączyć
z szeryfem, jeśli nie znam nazwiska.

246
00:29:26,260 --> 00:29:28,970
Nie poznam?

247
00:29:29,390 --> 00:29:35,060
Przykro mi, nie połączę z szeryfem,
jeśli nie dowiem się, kto dzwoni.

248
00:29:37,730 --> 00:29:42,900
Strasznie mi przykro,
ale jestem zmuszona odłożyć słuchawkę.

249
00:30:03,760 --> 00:30:06,720
- Wezmę to.
- Nie.

250
00:30:06,880 --> 00:30:09,800
Mogę to zanieść.

251
00:30:10,350 --> 00:30:13,180
Tylko uważaj.

252
00:31:02,060 --> 00:31:06,070
- Dlaczego wyłączyłaś odkurzacz?
- Śliczne paluszki.

253
00:31:06,230 --> 00:31:08,150
Jakie paluszki?

254
00:31:08,740 --> 00:31:10,530
Przyniosłam lód.

255
00:31:11,200 --> 00:31:14,530
Frosty? Mój mały bałwanku.

256
00:31:14,580 --> 00:31:17,870
Nadciąga zimny front...

257
00:31:19,790 --> 00:31:23,460
Cześć, Emory. Pamiętasz mnie?

258
00:31:24,380 --> 00:31:27,920
Zdaje się, że napytałeś sobie biedy.

259
00:31:28,130 --> 00:31:32,890
Opowiem ci śliczną bajkę.
Jesteś gotów?

260
00:31:33,340 --> 00:31:37,680
Dawno temu żyła sobie dziewczynka
imieniem Czerwony Kapturek - to ja.

261
00:31:37,720 --> 00:31:40,440
Poznała złego wilka,
czyli ciebie.

262
00:31:40,600 --> 00:31:44,810
Spuściła mu lanie
i powiedziała wszystko tatusiowi.

263
00:31:44,860 --> 00:31:49,900
Powiedziała też policji i wstrętny stary wilk,
poszedł do więzienia na milion lat.

264
00:31:49,990 --> 00:31:52,950
- Czego chcesz?
- Powiesz mi wszystko, co wiesz, Emory.

265
00:31:52,990 --> 00:31:57,240
O stoisku perfumeryjnym, Laurze Palmer,
Ronette Pulaski i "Jednookim Jacku".

266
00:31:57,290 --> 00:32:00,910
- Zwariowałaś.
- Czyżby?

267
00:32:00,960 --> 00:32:05,040
Jestem Audrey Horne i zawsze
dostaję to, czego chcę. Zrozumiano?

268
00:32:05,080 --> 00:32:06,210
Zrozumiałem...

269
00:32:07,750 --> 00:32:09,710
Pracuję dla właściciela
"Jednookiego Jacka".

270
00:32:09,760 --> 00:32:12,680
- Czyli?
- Dla właściciela "Jednookiego Jacka".

271
00:32:12,720 --> 00:32:15,390
- Czyli?
- Dla twojego ojca.

272
00:32:15,430 --> 00:32:18,350
To wszystko jest jego.
Wszystko.

273
00:32:18,510 --> 00:32:20,980
Przysyłam tu dziewczyny
ze stoiska perfumeryjnego.

274
00:32:21,020 --> 00:32:23,100
Zwerbowałem Ronette i Laurę.

275
00:32:23,730 --> 00:32:27,730
- Laura tu bywała?
- Kiedyś się naćpała.

276
00:32:27,900 --> 00:32:30,860
Wyrzuciliśmy ją.
Więcej jej nie widziałem. Przysięgam.

277
00:32:31,030 --> 00:32:35,030
- Czy mój ojciec wiedział, że tu była?
- Tak.

278
00:32:35,200 --> 00:32:39,330
Pan Horne testuje wszystkie dziewczęta.

279
00:32:40,410 --> 00:32:43,370
- Wiedziała, że jest właścicielem?
- Czego?

280
00:32:43,540 --> 00:32:47,540
- "Jednookiego Jacka"!
- Tak. Chyba tak.

281
00:32:47,710 --> 00:32:52,050
Laura zawsze stawiała na swoim.
Rozumiesz?

282
00:32:52,920 --> 00:32:55,840
Zupełnie jak ty.

283
00:33:09,770 --> 00:33:12,530
Zmień stację.

284
00:33:16,030 --> 00:33:17,120
Tak lepiej.

285
00:33:23,330 --> 00:33:27,330
Dzwoniłem tam.
Podałem się za kuzyna Leo.

286
00:33:27,500 --> 00:33:31,500
Przysługuje mu renta, pod warunkiem,
że nie siedzi w więzieniu.

287
00:33:31,670 --> 00:33:34,590
To dużo pieniędzy.
5.000 $ miesięcznie.

288
00:33:34,760 --> 00:33:38,760
Ale tylko, jeśli będzie
siedzieć w domu.

289
00:33:38,930 --> 00:33:42,930
Nie obchodzą mnie pieniądze.
Nie chcę mieć Leo w domu.

290
00:33:43,730 --> 00:33:46,230
Leo nic nie kuma.

291
00:33:46,310 --> 00:33:50,360
Możemy zrobić z niego wieszak.

292
00:33:50,400 --> 00:33:53,360
Szeryf chce, żebym zeznawała.

293
00:33:53,530 --> 00:33:56,490
Nie zmuszą cię do obciążenia
własnego męża.

294
00:33:56,650 --> 00:34:02,450
Wystarczy trzymać Leo w domu.
A potem już tylko przyjmować czeki.

295
00:34:05,000 --> 00:34:09,830
Leo jest ci coś winien.
Zmusimy go, żeby zapłacił.

296
00:34:09,880 --> 00:34:13,960
Chcesz coś ładnego? Kupimy.
Chcesz gdzieś pojechać?

297
00:34:15,380 --> 00:34:19,050
Leo ci to postawi.
Rozumiesz?

298
00:34:22,680 --> 00:34:27,350
Byłoby fajnie nie martwić się
o pieniądze.

299
00:34:27,890 --> 00:34:30,860
- Mam wiele zaległych rachunków.
- Rachunki?

300
00:34:31,020 --> 00:34:34,980
Zapomnij.
Ja tu mówię o nowym życiu.

301
00:34:38,320 --> 00:34:40,780
Razem?

302
00:34:47,710 --> 00:34:50,580
A jak myślisz?

303
00:34:54,960 --> 00:34:59,840
Myślę, że wolę
wygodniejszą pozycję.

304
00:35:00,180 --> 00:35:03,850
Dlatego wziąłem samochód ojca.

305
00:35:08,520 --> 00:35:13,060
Bobby Briggs... zjem cię.

306
00:35:13,940 --> 00:35:18,860
- Obiecujesz?
- Sam się przekonaj.

307
00:35:26,450 --> 00:35:30,040
Diane, otrzymałem dziś złe wieści.

308
00:35:30,210 --> 00:35:33,330
Windom Earle zniknął.

309
00:35:34,340 --> 00:35:38,970
Zniknięcie dawnego partnera
bardzo mnie niepokoi.

310
00:35:40,590 --> 00:35:44,600
Dowiedziałem się też
o zaginięciu Audrey Horne.

311
00:35:44,760 --> 00:35:49,770
Nie przypuszczałem,
że jej nieobecność tak mnie poruszy.

312
00:35:49,980 --> 00:35:55,900
Złapałem się na myśleniu nie o śladach,
czy dowodach, lecz o jej uśmiechu.

313
00:36:00,400 --> 00:36:02,320
Kto tam?

314
00:36:02,490 --> 00:36:06,080
Major Briggs.

315
00:36:13,920 --> 00:36:16,590
Chwileczkę.

316
00:36:20,380 --> 00:36:24,340
- Majorze?
- Mogę wejść?

317
00:36:32,690 --> 00:36:36,690
- Mam dla pana wiadomość.
- Od kogo?

318
00:36:36,860 --> 00:36:41,280
Nie wolno mi zdradzać
szczegółów mojej pracy.

319
00:36:42,070 --> 00:36:45,030
Czasami dyskrecja mi ciąży.

320
00:36:45,200 --> 00:36:51,200
Biurokracja bazująca na tajemnicy
nieuchronnie zmierza do korupcji.

321
00:36:51,620 --> 00:36:56,880
Ale przysięgałem przestrzegać tych zasad
i wierzę, że przysięga to świętość.

322
00:36:56,920 --> 00:37:01,460
Też w to wierzę, jako człowiek
i urzędnik państwowy.

323
00:37:01,510 --> 00:37:03,510
Oto, co mogę wyjawić:

324
00:37:04,130 --> 00:37:11,890
Zajmuję się - między innymi - aparaturą
badającą odległe galaktyki.

325
00:37:13,310 --> 00:37:17,150
Otrzymujemy przekazy różnego typu.

326
00:37:17,480 --> 00:37:21,480
Kosmiczny śmietnik,
który dekodujemy i badamy.

327
00:37:21,650 --> 00:37:24,530
Wygląda to tak.

328
00:37:25,820 --> 00:37:29,830
Fale radiowe i bełkot.

329
00:37:32,040 --> 00:37:36,830
Aż do czwartkowej nocy.
A właściwie do piątku rano.

330
00:37:37,250 --> 00:37:40,840
Wtedy do mnie strzelano.

331
00:37:41,420 --> 00:37:44,380
Odczyt nas zaskoczył.

332
00:37:44,550 --> 00:37:48,260
Niezrozumiały bełkot i nagle...

333
00:37:50,810 --> 00:37:54,730
"Sowy nie są tym, czym się wydają".

334
00:38:03,320 --> 00:38:06,280
Dlaczego przyszedł pan z tym do mnie?

335
00:38:06,450 --> 00:38:10,410
Ponieważ później, rano pojawiło się...

336
00:38:10,570 --> 00:38:13,080
"Cooper"

337
00:38:19,960 --> 00:38:22,420
Boże...

338
00:38:27,340 --> 00:38:30,970
Dobrze. Spróbujmy jeszcze raz.

339
00:41:44,210 --> 00:41:47,500
- Co się stało?
- Nic.

340
00:41:52,550 --> 00:41:56,680
- Co się dzieje?
- Drżę. Przez ciebie.

341
00:42:08,650 --> 00:42:14,150
Dzwoni do ciebie jakiś Harold Smith.

342
00:42:14,190 --> 00:42:16,700
Odbiorę w korytarzu.

343
00:42:27,750 --> 00:42:32,550
Już, tato!

344
00:42:34,840 --> 00:42:37,720
Słucham?

345
00:42:37,760 --> 00:42:40,430
Pan Smith?

346
00:42:42,720 --> 00:42:45,640
Dostałam pańską.

347
00:42:47,940 --> 00:42:50,900
Chciałabym porozmawiać.

348
00:42:51,060 --> 00:42:54,020
Możemy się spotkać?

349
00:42:54,190 --> 00:42:56,440
Dobrze.

350
00:44:22,240 --> 00:44:27,280
Sowy nie są tym,
czym się wydają.

351
00:45:13,710 --> 00:45:14,580
Agent Cooper.

352
00:45:14,620 --> 00:45:18,630
- To ja, Audrey.
- Audrey? Gdzie jesteś?

353
00:45:18,670 --> 00:45:20,670
Dlaczego pana tu nie ma?

354
00:45:20,710 --> 00:45:23,010
To nie pora na wygłupy.

355
00:45:23,050 --> 00:45:26,550
Natychmiast wracaj do domu.

356
00:45:26,970 --> 00:45:30,930
Widziałam pana w smokingu.
Wyglądał pan jak gwiazdor filmowy.

357
00:45:30,970 --> 00:45:34,100
Jeśli masz jakieś kłopoty...

358
00:45:34,140 --> 00:45:38,900
Mam kłopoty, ale wracam do domu.

359
00:45:39,310 --> 00:45:41,820
Audrey?

360
00:45:44,070 --> 00:45:45,950
Kłopoty, panno Horne?

361
00:45:46,410 --> 00:45:48,990
Nie masz pojęcia,
co to są kłopoty.

362
00:45:49,030 --> 00:45:52,830
Przynajmniej na razie.

363
00:46:04,340 --> 00:46:07,430
Opracowanie: 
Jakub "Qbin" Jedynak

364
00:46:07,470 --> 00:46:10,350
Redakcja: Kraps 
kraps(at)poczta.onet.pl

365
00:46:10,390 --> 00:46:13,310
Polska strona o Davidzie Lynchu 
www.lynchland.pl

366
00:46:15,310 --> 00:46:18,310
.:: Napisy24 - Nowy Wymiar Napisów ::.
Napisy24.pl

