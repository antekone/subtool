1
00:00:28,890 --> 00:00:33,710
MIASTECZKO TWIN PEAKS

2
00:03:04,120 --> 00:03:05,650
Dobrze ci?

3
00:04:13,840 --> 00:04:15,390
Josie?

4
00:04:15,700 --> 00:04:17,060
Tak...

5
00:04:20,020 --> 00:04:21,580
to ja.

6
00:04:22,700 --> 00:04:24,190
Jestem tutaj.

7
00:04:27,870 --> 00:04:29,340
Jestem tu.

8
00:05:45,340 --> 00:05:48,610
- Obsługa.
- Otwarte.

9
00:05:53,570 --> 00:05:55,780
Proszę postawić na stole.

10
00:05:56,950 --> 00:05:57,910
Audrey?

11
00:05:58,220 --> 00:05:59,860
Dobrze spałeś?

12
00:05:59,940 --> 00:06:05,690
Staramy się w miarę możliwości
umilać gościom pobyt.

13
00:06:05,770 --> 00:06:09,630
Poważnie podchodzimy do spraw obsługi.

14
00:06:13,620 --> 00:06:14,670
Pamiętasz dziadka?

15
00:06:16,000 --> 00:06:20,210
To był bardzo mądry człowiek.
Wiesz, co mi kiedyś powiedział?

16
00:06:20,300 --> 00:06:23,060
Że biorąc młotek,
lepiej wziąć i gwoździe.

17
00:06:24,440 --> 00:06:26,020
Co to miało znaczyć?

18
00:06:26,230 --> 00:06:32,070
Że gdy znów tu wejdziesz,
bądź gotowa skończyć to, co zaczęłaś.

19
00:06:34,590 --> 00:06:36,110
Może już jestem.

20
00:06:36,700 --> 00:06:40,230
- To bądź sobą.
- Jestem.

21
00:06:42,900 --> 00:06:45,750
Tak, masz rację.

22
00:06:46,070 --> 00:06:48,500
Jesteś piękną i inteligentną kobietą.

23
00:06:51,010 --> 00:06:51,750
Co jeszcze?

24
00:06:54,310 --> 00:06:58,390
Masz w sobie mnóstwo czaru.

25
00:06:59,330 --> 00:07:04,720
Mam pomysł. Lot o zachodzie
i kolacja we dwoje.

26
00:07:05,860 --> 00:07:08,190
- Kiedy?
- Podczas lotu.

27
00:07:09,070 --> 00:07:12,670
Muszę iść do biblioteki i do ojca.

28
00:07:12,750 --> 00:07:14,110
Tak czy nie?

29
00:07:16,200 --> 00:07:17,150
Tak.

30
00:07:22,680 --> 00:07:25,850
I jeśli weźmiesz młotek,
pamiętaj o gwoździach.

31
00:07:39,420 --> 00:07:40,590
I co?

32
00:07:41,520 --> 00:07:45,520
Nic. Żąda widzenia z konsulem RPA.

33
00:07:45,610 --> 00:07:47,150
W Twin Peaks?

34
00:07:48,690 --> 00:07:51,900
Dlaczego chciał mnie zabić?

35
00:07:51,990 --> 00:07:53,960
Z zazdrości.

36
00:07:57,220 --> 00:08:01,120
- Dobrze, że wróciłeś.
- Muszę dojść do siebie.

37
00:08:01,200 --> 00:08:05,300
Wyglądasz, jakbyś wrócił
z daleka. Idź do domu.

38
00:08:05,380 --> 00:08:09,340
Wystarczy mi kawa i coś do jedzenia.

39
00:08:09,420 --> 00:08:11,590
Mam niezawodny lek na kaca.

40
00:08:11,680 --> 00:08:16,840
Wypij szybko szklankę lodowatego
soku pomidorowego z dwiema ostrygami.

41
00:08:16,920 --> 00:08:18,080
Działa cuda.

42
00:08:18,160 --> 00:08:22,810
Potem talerz nereczek
z orzechami i bekonem,

43
00:08:22,890 --> 00:08:29,410
a na koniec fura herbatników
z gęstym sosem z anchois...

44
00:08:29,500 --> 00:08:30,760
Przepraszam.

45
00:08:36,030 --> 00:08:37,190
Powinno podziałać.

46
00:08:41,460 --> 00:08:44,660
Bonsai. Doskonała miniatura.

47
00:08:45,550 --> 00:08:48,160
Woda sodowa. Najlepsza na trawienie.

48
00:08:52,780 --> 00:08:54,390
Pij na zdrowie.

49
00:08:55,970 --> 00:08:57,140
Dziękuję.

50
00:09:01,470 --> 00:09:03,890
Skąd się to wzięło?

51
00:09:04,900 --> 00:09:06,470
Przynieśli dziś rano.

52
00:09:13,890 --> 00:09:15,020
Od Josie...

53
00:09:17,290 --> 00:09:20,150
Earle był u mnie w domu.

54
00:09:23,180 --> 00:09:24,610
Windom Earle?

55
00:09:25,900 --> 00:09:27,770
- Kiedy?
- Wczoraj.

56
00:09:27,850 --> 00:09:29,550
Zostawił to Donnie.

57
00:09:31,760 --> 00:09:33,800
Jest coraz bliżej.

58
00:09:34,740 --> 00:09:38,110
"Skoczek na F3". Co teraz?

59
00:09:38,200 --> 00:09:41,780
- Opublikujemy nasz ruch.
- Dzięki, Lucy!

60
00:09:42,320 --> 00:09:43,830
Weź się w garść.

61
00:09:43,910 --> 00:09:48,530
Panowie, to ja, Gordon Cole.
Wybaczcie, że przeszkadzam.

62
00:09:48,610 --> 00:09:53,540
Przywiozłem utajnioną
część akt Windoma Earle'a.

63
00:09:54,250 --> 00:09:55,600
Z Bend?

64
00:09:55,690 --> 00:09:59,520
Byłem w Bend, w Oregonie.
Panuje tam straszne rozprzężenie.

65
00:10:02,080 --> 00:10:04,320
Jest Gordon Cole.

66
00:10:08,490 --> 00:10:11,010
To takie denerwujące.

67
00:10:11,100 --> 00:10:14,660
Młody Dale kantuje w grze!

68
00:10:15,750 --> 00:10:20,890
Gdy Earle dał nogę,
odkryli, że był na haloperydolu.

69
00:10:20,980 --> 00:10:23,300
To samo brał ten jednoręki, Gerard.

70
00:10:24,150 --> 00:10:27,040
- Słucham?
- Gerard to brał!

71
00:10:27,120 --> 00:10:32,600
Racja. Ale stwierdzili, że symulował,
tak jak podejrzewałeś.

72
00:10:32,680 --> 00:10:34,770
To podstęp godny szaleńca.

73
00:10:40,400 --> 00:10:42,740
Błękitna Księga?

74
00:10:42,820 --> 00:10:44,770
Co on w tym robił?

75
00:10:44,850 --> 00:10:48,730
Biuro wypożyczyło go
siłom powietrznym w '65.

76
00:10:48,810 --> 00:10:52,430
Raporty z tych dwóch lat
są ściśle tajne.

77
00:10:52,510 --> 00:10:55,410
Jak to? Earle badał UFO?

78
00:10:56,080 --> 00:11:00,960
Jak major Briggs.
Mamy tu ciekawe związki.

79
00:11:01,050 --> 00:11:03,880
Słowo związki przypomina mi kąski.

80
00:11:04,380 --> 00:11:07,730
Od myślenia zawsze wolałem jedzenie.

81
00:11:07,820 --> 00:11:11,230
Śniadanie to dobry pomysł.

82
00:11:11,320 --> 00:11:15,140
O, bonsai. Pamiętacie te filmy
o drugiej wojnie światowej?

83
00:11:18,040 --> 00:11:19,710
Niech to szlag!

84
00:11:24,690 --> 00:11:28,060
Miło pana widzieć, doktorze.
Zabierze pan szeryfa.

85
00:11:28,140 --> 00:11:31,110
- Zaczekamy na was.
- Chwilkę.

86
00:11:31,200 --> 00:11:33,470
Odkurz swój garnitur.

87
00:11:33,560 --> 00:11:37,620
Gdy w okolicy grasuje Earle,
chcemy cię mieć w drużynie.

88
00:11:37,700 --> 00:11:39,280
Co chcesz powiedzieć?

89
00:11:39,360 --> 00:11:45,030
Z przyjemnością oznajmiam ci,
że wracasz w szeregi FBI.

90
00:11:47,660 --> 00:11:52,920
Nowy nabytek, celny i skuteczny.
Smith & Wesson, 10 milimetrów,

91
00:11:53,010 --> 00:11:57,040
model 1076, z nierdzewnej stali.

92
00:11:57,120 --> 00:12:00,790
To piękna broń i jest twoja.

93
00:12:00,870 --> 00:12:02,210
Dziękuję.

94
00:12:02,300 --> 00:12:06,140
Znajdziemy Earle'a. Dościgniemy,

95
00:12:06,230 --> 00:12:08,900
schwytamy i zatrzymamy.

96
00:12:09,750 --> 00:12:11,390
Ruszajmy.

97
00:12:14,970 --> 00:12:16,920
Chodź, ja stawiam.

98
00:12:17,010 --> 00:12:18,530
Już idę.

99
00:12:21,490 --> 00:12:22,790
Leo...

100
00:12:27,170 --> 00:12:28,210
wybierz trzy karty.

101
00:12:35,010 --> 00:12:36,640
Jedna...

102
00:12:37,420 --> 00:12:38,770
druga...

103
00:12:40,220 --> 00:12:41,610
i trzecia.

104
00:12:43,630 --> 00:12:46,160
Królowa Donna.

105
00:12:48,490 --> 00:12:51,130
Królowa Audrey.

106
00:12:52,920 --> 00:12:55,240
Królowa Shelly. Dobrze...

107
00:12:56,860 --> 00:13:00,910
A teraz poszukaj króla.

108
00:13:09,300 --> 00:13:10,300
Mały Dale.

109
00:13:13,120 --> 00:13:15,510
Ale potrzebna nam czwarta królowa.

110
00:13:16,500 --> 00:13:17,720
Zaraz...

111
00:13:19,650 --> 00:13:23,810
Dama kier. Chwileczkę.

112
00:13:25,450 --> 00:13:27,150
Miss Twin Peaks.

113
00:13:27,860 --> 00:13:29,690
Co dostanie laureatka?

114
00:13:30,120 --> 00:13:31,740
Tuzin róż,

115
00:13:32,080 --> 00:13:35,310
stypendium w wybranym college'u,

116
00:13:35,390 --> 00:13:38,240
uznanie koleżanek.

117
00:13:38,320 --> 00:13:39,660
Tak, wiem.

118
00:13:41,260 --> 00:13:42,790
Czeka ją śmierć.

119
00:13:45,150 --> 00:13:47,210
Królewska egzekucja.

120
00:13:50,920 --> 00:13:52,820
A Cooper...

121
00:13:56,570 --> 00:13:58,430
ma miejsce na widowni.

122
00:14:52,200 --> 00:14:53,820
Musimy porozmawiać.

123
00:15:09,530 --> 00:15:10,750
Jak minął pobyt?

124
00:15:13,830 --> 00:15:15,370
Niewiarygodnie.

125
00:15:19,520 --> 00:15:22,310
- Jest Audrey Horne?
- Na zapleczu, zawołam ją.

126
00:15:24,190 --> 00:15:25,310
Cześć, Donna.

127
00:15:36,580 --> 00:15:38,530
- Możemy pogadać?
- Jasne.

128
00:15:40,570 --> 00:15:43,490
To zabrzmi dziwnie.

129
00:15:44,250 --> 00:15:44,910
Co?

130
00:15:45,410 --> 00:15:49,170
Wiesz, dlaczego moja mama
odwiedza twojego ojca?

131
00:15:50,140 --> 00:15:51,330
Nie bardzo.

132
00:15:53,780 --> 00:15:59,720
Wczoraj do niej przyszedł,
a dziś ona przyjechała do niego.

133
00:16:00,330 --> 00:16:03,350
Może pomaga przy akcji w Ghostwood.

134
00:16:03,430 --> 00:16:06,270
Nie, bo by mi powiedziała,
a nic nie wiem.

135
00:16:08,960 --> 00:16:10,650
Jest u niego?

136
00:16:11,160 --> 00:16:12,980
W jego biurze.

137
00:16:18,140 --> 00:16:21,820
Nie przyjmę ich. To twoje listy.

138
00:16:22,620 --> 00:16:28,180
- Napisane do ciebie.
- Nie chcę ich dłużej trzymać w domu.

139
00:16:28,970 --> 00:16:33,470
Masz je od 20 lat. Dlaczego teraz?

140
00:16:33,550 --> 00:16:36,500
O to samo chcę spytać. Dlaczego teraz?

141
00:16:36,590 --> 00:16:42,010
Rozdrapujemy tylko stare rany.
Mam tego dość.

142
00:16:49,200 --> 00:16:52,490
- Dlaczego mi nie wierzysz?
- Słowa to za mało.

143
00:16:52,570 --> 00:16:55,950
Mówią za mnie moje czyny.
Nie słuchasz?

144
00:16:56,040 --> 00:17:01,300
Od lat byłem w błędzie.
Staram się to naprawić.

145
00:17:02,160 --> 00:17:05,530
Chcąc coś naprawić,
tylko to pogarszasz.

146
00:17:05,610 --> 00:17:07,330
Wiem.

147
00:17:10,210 --> 00:17:15,300
Powinnaś być światłem mojego życia.

148
00:17:15,380 --> 00:17:16,850
Proszę.

149
00:17:19,990 --> 00:17:22,230
Nie objąłem cię od tamtej nocy.

150
00:17:25,190 --> 00:17:26,860
Chcę cię przytulić.

151
00:17:27,810 --> 00:17:29,410
Wrócić.

152
00:17:30,320 --> 00:17:33,090
- Gdybyś tylko...
- Przestań!

153
00:17:38,980 --> 00:17:40,120
Przepraszam.

154
00:17:47,000 --> 00:17:48,620
Powiedziałaś jej?

155
00:17:48,700 --> 00:17:54,770
Nie. I nigdy nie powiem,
a ty obiecaj mi to samo.

156
00:17:54,860 --> 00:17:57,240
Nie przychodź więcej.

157
00:17:58,700 --> 00:18:00,590
I trzymaj się od niej z daleka.

158
00:18:03,310 --> 00:18:04,660
Rozumiem.

159
00:18:05,680 --> 00:18:07,050
Mam nadzieję.

160
00:18:16,510 --> 00:18:18,550
Co tu jest grane?

161
00:18:19,890 --> 00:18:21,350
Nie wiem.

162
00:18:25,890 --> 00:18:28,370
Ale się dowiem.

163
00:18:29,750 --> 00:18:35,440
Najlepszy sposób na kaca, jaki znam,
to dużo surowego mięsa.

164
00:18:35,530 --> 00:18:39,710
Wbijasz na to jajko, do tego anchois,

165
00:18:40,120 --> 00:18:42,540
sos tabasco i Worcester.

166
00:18:43,120 --> 00:18:45,880
Możemy ci to zamówić.

167
00:18:48,900 --> 00:18:52,520
- A wiesz, na co ja mam ochotę?
- Na co?

168
00:18:52,600 --> 00:18:55,200
Na stek tak krwisty
jak rubiny od Tiffany'ego.

169
00:18:55,290 --> 00:18:57,220
Dobrze trafiłeś.

170
00:18:58,480 --> 00:18:59,800
Cieszę się.

171
00:19:01,880 --> 00:19:04,570
Rany, kto to jest?

172
00:19:06,910 --> 00:19:07,910
Shelly Johnson.

173
00:19:09,990 --> 00:19:11,080
Shelly Johnson!

174
00:19:15,770 --> 00:19:21,090
Co za uroda! Przypomina mi
ten posąg bez rąk.

175
00:19:22,030 --> 00:19:23,050
Wenus z Milo.

176
00:19:23,130 --> 00:19:26,240
Nazywa się Milo, ale mniejsza z tym.

177
00:19:26,320 --> 00:19:29,470
Przy takiej dziewczynie
chciałoby się znać francuski.

178
00:19:31,250 --> 00:19:37,080
Pozwól, że wypróbuję moją znajomość
barowego esperanto.

179
00:19:37,160 --> 00:19:38,470
Powodzenia.

180
00:19:41,970 --> 00:19:43,110
Witam.

181
00:19:43,440 --> 00:19:47,360
Mogę prosić o mocną, czarną kawę?

182
00:19:48,020 --> 00:19:52,800
A przy okazji zabawić panią
chwilą rozmowy?

183
00:19:54,540 --> 00:20:00,090
Nazywam się Gordon Cole.
Zobaczyłem panią stamtąd...

184
00:20:01,560 --> 00:20:07,300
i na widok pani urody
poczułem mrowienie w żołądku.

185
00:20:07,880 --> 00:20:09,760
Nie musi pan krzyczeć, dobrze słyszę.

186
00:20:10,740 --> 00:20:14,470
Usłyszałem to! Usłyszałem!

187
00:20:15,370 --> 00:20:17,100
Podać coś do kawy?

188
00:20:17,180 --> 00:20:19,020
Doskonale słyszę!

189
00:20:19,280 --> 00:20:21,290
Ja pana też. Proszę...

190
00:20:21,370 --> 00:20:23,610
Nie rozumie pani!

191
00:20:23,690 --> 00:20:27,280
Nie rozumie pani. Widzi to pani?

192
00:20:28,190 --> 00:20:31,300
Od 20 lat proszę wszystkich,
żeby mówili głośniej,

193
00:20:31,380 --> 00:20:36,040
ale jakimś cudem panią słyszę
doskonale. Proszę coś powiedzieć.

194
00:20:38,590 --> 00:20:40,080
Podać do kawy placek?

195
00:20:41,050 --> 00:20:43,540
Boże! Świetnie panią słyszę.

196
00:20:43,620 --> 00:20:47,340
To jakiś cud. Coś niezwykłego.

197
00:20:47,420 --> 00:20:49,720
- Czy cuda to coś złego?
- Słucham?

198
00:20:51,970 --> 00:20:54,550
Ten placek to cud.

199
00:20:55,290 --> 00:21:00,030
Czy pani z pieńkiem
może mówić głośniej?

200
00:21:01,380 --> 00:21:04,230
Mówiła o placku.

201
00:21:04,500 --> 00:21:07,530
Znów usłyszałem. Słyszałem.

202
00:21:09,290 --> 00:21:10,360
Chce pan placka?

203
00:21:10,870 --> 00:21:16,450
Ogromną porcję.
I szklankę wody, bo cały płonę.

204
00:21:42,290 --> 00:21:45,050
Na tym dodge'u siedzi chyba sikorka.

205
00:21:45,970 --> 00:21:48,650
Wygląda raczej na łuskowca.

206
00:21:50,050 --> 00:21:52,570
Nie. To sikora czarnogłowa.

207
00:21:56,940 --> 00:21:59,580
- Mocna czarna kawa?
- Tak jest.

208
00:22:03,680 --> 00:22:07,790
- Co byś poleciła na kaca?
- Abstynencję i modlitwę.

209
00:22:08,660 --> 00:22:11,680
- Słusznie.
- Dostanę kawy?

210
00:22:14,180 --> 00:22:15,470
Jak się miewasz?

211
00:22:17,100 --> 00:22:19,930
Dobrze. Choć dziwnie.

212
00:22:20,460 --> 00:22:23,760
Nie bardzo wiem, gdzie jestem.
To znaczy...

213
00:22:23,840 --> 00:22:28,150
wiem, ale dziwnie mi z tym.
W porządku.

214
00:22:30,860 --> 00:22:36,020
Długo nie byłam w obiegu
i zapomniałam o grzeczności.

215
00:22:36,100 --> 00:22:39,660
Zapytana, jak się miewam,
powinnam odpowiedzieć,

216
00:22:39,740 --> 00:22:41,870
że świetnie,
i spytać, jak ty się czujesz.

217
00:22:42,450 --> 00:22:44,210
Świetnie, dziękuję.

218
00:22:47,170 --> 00:22:50,460
- Masz mnie za dziwaczkę.
- Nie.

219
00:22:50,910 --> 00:22:54,430
Zgodnie z zasadami,
nawet gdybyś tak uważał,

220
00:22:54,520 --> 00:22:55,970
to byś mi nie powiedział, prawda?

221
00:22:56,260 --> 00:22:58,470
Powiedziałbym, gdybym tak myślał.

222
00:22:58,770 --> 00:23:02,640
- Więc tak nie uważasz?
- Wcale.

223
00:23:03,440 --> 00:23:06,300
Dobrze, że to wyjaśniliśmy.

224
00:23:07,300 --> 00:23:08,630
Co podać?

225
00:23:09,220 --> 00:23:13,480
Zapiekankę z kurczaka
i dużą szklankę mleka.

226
00:23:13,740 --> 00:23:16,150
Słusznie. Pełna wartość odżywcza.

227
00:23:16,910 --> 00:23:21,290
Indyk na pełnoziarnistym chlebie
z sałatą i majonezem.

228
00:23:21,370 --> 00:23:25,540
I opowiem ci kawał.
Idą po lodzie dwa pingwiny.

229
00:23:26,220 --> 00:23:30,850
Jeden mówi do drugiego:
"Wyglądasz, jakbyś był we fraku..."

230
00:23:32,190 --> 00:23:33,840
Słyszałem!

231
00:23:34,740 --> 00:23:36,590
Zaraz wrócę.

232
00:23:37,790 --> 00:23:39,220
Nie skończyłem.

233
00:23:41,580 --> 00:23:43,180
Od kiedy się w niej bujasz?

234
00:23:45,010 --> 00:23:46,640
Skąd ten pomysł?

235
00:23:49,020 --> 00:23:51,270
Chciałeś jej opowiedzieć kawał.

236
00:23:57,310 --> 00:23:59,430
I co na to drugi pingwin?

237
00:24:00,800 --> 00:24:04,990
Pierwszy mówi:
"Wyglądasz, jakbyś był we fraku".

238
00:24:05,070 --> 00:24:08,660
A tamten na to: "Może jestem".

239
00:24:12,590 --> 00:24:14,800
Nie mam pytań.

240
00:24:15,880 --> 00:24:19,450
- Dołożyć placka? Może cały?
- Z chęcią, panno Johnson.

241
00:24:19,540 --> 00:24:25,470
I proszę o papier i pióro.
Napiszę poemat o tym placku.

242
00:24:34,860 --> 00:24:38,280
- Byłeś w Sowiej Jaskini.
- Słucham?

243
00:24:39,310 --> 00:24:41,900
To wygląda jak rysunek z jaskini.

244
00:24:43,250 --> 00:24:44,630
Pokaż.

245
00:24:45,310 --> 00:24:47,540
Połączyłem tatuaże majora
i Pieńkowej Damy.

246
00:24:50,010 --> 00:24:52,420
W jaskini jest identyczny.

247
00:24:53,500 --> 00:24:56,800
Muszę zobaczyć tę Sowią Jaskinię.

248
00:25:16,510 --> 00:25:20,200
Kochana Donno.
Miałaś rację, musiałem wyjechać.

249
00:25:20,790 --> 00:25:23,360
W drodze wszystko jest inne.

250
00:25:24,320 --> 00:25:25,770
Lepsze.

251
00:25:26,800 --> 00:25:30,870
San Francisco jest wspaniałe.
Teraz ruszam do Meksyku.

252
00:25:31,530 --> 00:25:35,650
Wrócę z milionem opowieści, przyrzekam.

253
00:25:35,730 --> 00:25:38,000
Kocham cię, James.

254
00:25:48,220 --> 00:25:49,010
Od Jamesa?

255
00:25:50,960 --> 00:25:51,890
Co u niego?

256
00:25:52,610 --> 00:25:55,980
Wszystko dobrze. Zrobił to, co musiał.

257
00:25:57,380 --> 00:25:58,610
Wróci.

258
00:26:03,510 --> 00:26:06,660
- Mogę cię o coś spytać?
- Oczywiście.

259
00:26:07,130 --> 00:26:08,950
Skąd mama zna Bena Horne'a?

260
00:26:09,940 --> 00:26:13,120
Jeśli w ogóle, to bardzo słabo.

261
00:26:13,690 --> 00:26:17,440
Był tu wczoraj.
Rozmawiali, jak starzy znajomi.

262
00:26:17,520 --> 00:26:23,220
Mama udziela się społecznie.
Nawet Ben czasami pomaga.

263
00:26:23,520 --> 00:26:26,670
Dziś rano pojechała do niego do hotelu.

264
00:26:27,860 --> 00:26:31,600
- Na pewno w sprawie dotacji.
- Nie sądzę.

265
00:26:33,420 --> 00:26:38,610
Mówiła mi o tym przy śniadaniu.
Chodzi o ekologię.

266
00:26:40,720 --> 00:26:45,450
- Mnie nic nie powiedziała.
- Ale to właśnie to. Tak mówiła.

267
00:26:48,990 --> 00:26:50,580
Otworzę.

268
00:27:05,970 --> 00:27:09,170
To dla mamy. Bez bileciku.

269
00:27:11,080 --> 00:27:12,510
Wstawię do wody.

270
00:27:37,180 --> 00:27:41,320
- Najmocniej przepraszam.
- Nie słyszałam pana.

271
00:27:41,400 --> 00:27:46,920
Nie pozwalajcie, by wasz umysł
wędrował wraz ze stopami.

272
00:27:48,140 --> 00:27:50,680
Edward Perkins.
Jeszcze raz przepraszam.

273
00:27:51,170 --> 00:27:53,010
Audrey Horne. Miło mi.

274
00:27:53,850 --> 00:27:56,310
Czego szukasz w bibliotece?

275
00:27:56,560 --> 00:27:58,940
Danych o obywatelskim nieposłuszeństwie.

276
00:27:59,170 --> 00:28:04,360
- Sprzeciw leży w naturze młodych!
- Jest pan profesorem?

277
00:28:04,440 --> 00:28:09,310
Wykładowcą. Moja domena to poezja.

278
00:28:09,400 --> 00:28:12,160
Naprawdę? To może zna pan ten wiersz?

279
00:28:12,240 --> 00:28:15,120
Może. Pokaż.

280
00:28:19,490 --> 00:28:20,420
Albo przeczytaj.

281
00:28:23,540 --> 00:28:27,140
"Patrz! Całują góry niebo,
fale tulą się do siebie,

282
00:28:27,220 --> 00:28:31,180
żaden kwiat owocu nie da,
gdy nie ma drugiego.

283
00:28:31,260 --> 00:28:34,920
Światło słońca ziemię głaszcze,
księżyc pieści morze."

284
00:28:35,000 --> 00:28:41,770
"Wszystko się w naturze kocha,
a ty mnie nie możesz."

285
00:28:42,120 --> 00:28:46,110
- Zna pan to.
- Tak, to Shelley.

286
00:28:47,880 --> 00:28:48,490
Co?

287
00:28:49,650 --> 00:28:54,890
Gdy recytujesz Shelleya,

288
00:28:54,970 --> 00:28:58,290
wyglądasz jak królowa.

289
00:29:01,040 --> 00:29:02,120
Dziękuję.

290
00:29:04,150 --> 00:29:09,050
- Pójdę już. Miło było poznać.
- Może znów się spotkamy.

291
00:29:17,980 --> 00:29:20,480
WYBORY MISS TWIN PEAKS

292
00:29:27,930 --> 00:29:29,420
Startujesz?

293
00:29:29,510 --> 00:29:31,930
Nie. Ten świat jest dość dziwny

294
00:29:32,010 --> 00:29:35,830
bez paradowania po scenie
w kostiumie i na obcasach.

295
00:29:35,910 --> 00:29:38,080
W klasztorze tego nie robicie.

296
00:29:38,730 --> 00:29:40,040
Raczej nie.

297
00:29:41,590 --> 00:29:43,670
Jak to jest wrócić do cywilizacji?

298
00:29:45,150 --> 00:29:50,460
Nie zamykali nas tam na strychu.
Miałyśmy gazety i telewizję.

299
00:29:51,330 --> 00:29:54,300
A wszyscy traktują mnie,
jakbym wyszła na warunkowe.

300
00:29:55,710 --> 00:29:57,180
Przepraszam.

301
00:29:59,050 --> 00:30:01,930
Najdziwniejsi są mężczyźni.

302
00:30:03,220 --> 00:30:05,940
Żeby to czuć,
nie trzeba być w klasztorze.

303
00:30:07,820 --> 00:30:09,360
Jaki jest ten Dale Cooper?

304
00:30:10,130 --> 00:30:13,560
- Podoba ci się?
- Dlaczego?

305
00:30:13,650 --> 00:30:17,950
Sądząc po minie, jaką zrobił
na twój widok, masz duże szanse.

306
00:30:18,730 --> 00:30:22,320
Wcale mi się nie podoba.

307
00:30:48,910 --> 00:30:51,920
- Andy?
- Tak, Lucy?

308
00:30:52,910 --> 00:30:57,370
Dziękuję, że mi pomogłeś
w czasie tej awantury z kuną.

309
00:30:57,450 --> 00:31:00,950
Czego nie można powiedzieć
o niejakim Dicku.

310
00:31:01,530 --> 00:31:04,420
Utrzymanie porządku to mój zawód.

311
00:31:06,560 --> 00:31:08,750
Dokąd się wybieracie?

312
00:31:09,440 --> 00:31:11,040
Do jaskiń.

313
00:31:12,390 --> 00:31:14,920
Czy to niebezpieczne? Te jaskinie?

314
00:31:17,480 --> 00:31:19,050
Czasami.

315
00:31:19,620 --> 00:31:21,220
Obiecaj mi coś.

316
00:31:24,360 --> 00:31:29,440
Wiem, że jesteś silny i bardzo dzielny,

317
00:31:29,520 --> 00:31:33,780
ale przyrzeknij mi, że będziesz uważał.

318
00:31:34,950 --> 00:31:38,910
Będę. Masz moje słowo.

319
00:31:56,620 --> 00:31:59,860
- Jesteś gotów?
- Pete przysłał swój ruch.

320
00:32:01,430 --> 00:32:04,340
- Wystawiamy piona pod gońca?
- Wstrzymać się?

321
00:32:04,430 --> 00:32:10,180
Pete wie, co robi. Zbijając piona,
Earle straci gońca i przewagę.

322
00:32:10,270 --> 00:32:13,990
- Jak się czujesz?
- Jakby mi przebili głowę przez uszy.

323
00:32:14,070 --> 00:32:18,910
Wyprawa w chłód podziemi
będzie dobrym lekarstwem.

324
00:32:19,720 --> 00:32:23,340
- Cole da sobie radę?
- Nie idzie z nami.

325
00:32:23,430 --> 00:32:29,210
Shelly zabrała go do doktora
na badanie słuchu. Podstroiła go.

326
00:32:47,140 --> 00:32:48,430
Audrey?

327
00:32:49,570 --> 00:32:53,730
Gdy Jack Kennedy zamieszkał
w Białym Domu, wziął ze sobą brata.

328
00:32:53,810 --> 00:32:58,880
Wiedział, że Bobby powie mu prawdę,
nawet nieprzyjemną.

329
00:33:00,010 --> 00:33:04,410
Gdy wybuchła afera po desancie
w Zatoce Świń, stał przy nim.

330
00:33:04,490 --> 00:33:08,630
Kiedy Ruscy ustawili rakiety
na podwórku Fidela,

331
00:33:08,720 --> 00:33:12,130
Bobby niewzruszenie stał u boku brata.

332
00:33:13,360 --> 00:33:16,010
Jerry robi to dla ciebie?

333
00:33:16,090 --> 00:33:21,630
Szukam niczym nieskażonej prawdy.

334
00:33:21,710 --> 00:33:25,340
I uważam, że ty...

335
00:33:27,570 --> 00:33:30,500
jesteś do tego najlepsza.

336
00:33:31,350 --> 00:33:32,470
Ja?

337
00:33:36,260 --> 00:33:37,400
Audrey...

338
00:33:39,120 --> 00:33:42,660
wiem, że byłem kiepskim ojcem.

339
00:33:45,420 --> 00:33:47,330
Cholera!

340
00:33:47,410 --> 00:33:49,300
Kogo ja oszukuję?

341
00:33:49,380 --> 00:33:55,330
Kiedy byłem kimś lepszym
niż wredna gnida?

342
00:33:57,590 --> 00:34:00,940
- Może, gdy byłam mała...
- Właśnie.

343
00:34:01,710 --> 00:34:07,040
Ale się zmieniłem
i zamierzam być kimś lepszym.

344
00:34:07,120 --> 00:34:12,690
Ojcem, którego kiedyś
będziesz szanować.

345
00:34:16,240 --> 00:34:20,300
Gdy myślę o Laurze...

346
00:34:21,310 --> 00:34:23,880
i o błędach, które popełniłem...

347
00:34:24,940 --> 00:34:32,080
pragnę zapewnić rodzinie
szczęśliwe życie.

348
00:34:32,160 --> 00:34:33,690
Tobie i sobie.

349
00:34:36,860 --> 00:34:38,540
Pomożesz mi w tym?

350
00:34:40,120 --> 00:34:41,380
Proszę.

351
00:34:42,690 --> 00:34:44,920
Możesz na mnie liczyć.

352
00:34:46,260 --> 00:34:48,850
Cudownie.

353
00:34:50,440 --> 00:34:54,770
Pakuj się i jedź na lotnisko.
Za godzinę masz samolot.

354
00:34:54,860 --> 00:34:56,060
Samolot?

355
00:34:58,150 --> 00:35:02,110
John! Dobrze, że cię widzę.

356
00:35:04,960 --> 00:35:06,130
Tato...

357
00:35:06,990 --> 00:35:11,800
Lepiej nie zwlekaj.
Audrey leci za godzinę do Seattle.

358
00:35:11,880 --> 00:35:14,800
Rano ma spotkanie z obrońcami przyrody.

359
00:35:14,890 --> 00:35:19,090
Nasza kuna zyska rozgłos w całym kraju.

360
00:35:19,170 --> 00:35:20,250
Wspaniale.

361
00:35:21,010 --> 00:35:23,960
Nie wiem, czy mogę wszystko rzucić.

362
00:35:24,310 --> 00:35:29,720
To bardzo ważne: czas i miejsce.
Przywieź dobre wieści.

363
00:35:32,030 --> 00:35:33,710
Dobre wieści.

364
00:35:37,620 --> 00:35:39,010
Będę się zbierać.

365
00:35:50,360 --> 00:35:52,190
Bezpiecznej podróży.

366
00:36:05,080 --> 00:36:06,370
Jak się masz?

367
00:36:09,340 --> 00:36:15,120
Przepełniają mnie pozytywne uczucia.

368
00:36:16,180 --> 00:36:19,630
Rozświetla wewnętrzny blask,
jak choinkę.

369
00:36:20,100 --> 00:36:21,200
Ale...

370
00:36:22,370 --> 00:36:26,170
gdy wieczorem spoglądam w lustro...

371
00:36:27,000 --> 00:36:33,620
muszę się pogodzić z faktem,
że nie umiem być dobry.

372
00:36:33,700 --> 00:36:35,120
Dobrze zacząłeś.

373
00:36:35,880 --> 00:36:38,610
Jak ty to robisz?

374
00:36:38,690 --> 00:36:42,380
Myślisz, że się tego nauczę?

375
00:36:42,870 --> 00:36:47,050
Tak. Kieruj się sercem i mów prawdę.

376
00:36:48,760 --> 00:36:50,800
Mówić prawdę.

377
00:36:50,990 --> 00:36:52,430
Zacznij od najtrudniejszej.

378
00:36:53,500 --> 00:36:57,140
Mówić najtrudniejszą prawdę.
Świetne. Wspaniałe.

379
00:36:57,530 --> 00:37:04,020
Kwestia prawdy to, jak widzę,
podstawa prawości.

380
00:37:04,270 --> 00:37:05,230
Ben?

381
00:37:07,050 --> 00:37:09,010
Zakochuję się w twojej córce.

382
00:37:09,090 --> 00:37:13,110
- To przykład trudnej prawdy.
- To jest prawda.

383
00:37:15,190 --> 00:37:17,540
- Tak?
- Tak.

384
00:37:21,770 --> 00:37:22,970
John...

385
00:37:27,760 --> 00:37:31,070
Żyjemy w cudownym świecie.

386
00:37:44,570 --> 00:37:48,410
- Mocno się trzymaj.
- Dobrze, Andy.

387
00:37:48,490 --> 00:37:50,300
Przekładaj ręce.

388
00:37:51,170 --> 00:37:52,660
Już niedaleko.

389
00:37:52,750 --> 00:37:54,140
Nie patrz w dół.

390
00:37:54,220 --> 00:37:56,590
Trzymaj się i nie patrz w dół.

391
00:37:56,670 --> 00:37:57,870
Nie patrz!

392
00:38:01,190 --> 00:38:02,440
W porządku?

393
00:38:03,170 --> 00:38:04,390
Tak.

394
00:38:10,430 --> 00:38:12,380
Niesamowite.

395
00:38:13,480 --> 00:38:19,040
Kiedyś się tu bawiliśmy.
Udawaliśmy, że żyją tu złe duchy.

396
00:38:19,120 --> 00:38:21,130
Raczej piwosze.

397
00:38:21,210 --> 00:38:22,810
Gdzie jest symbol?

398
00:38:23,880 --> 00:38:25,010
Tam.

399
00:38:36,320 --> 00:38:37,740
Już niedaleko.

400
00:38:46,580 --> 00:38:47,750
Tutaj.

401
00:38:53,550 --> 00:39:00,220
Dwa symbole połączone
w jeden duży. Co to znaczy?

402
00:39:01,140 --> 00:39:03,810
Nie mam pojęcia.

403
00:39:03,890 --> 00:39:08,860
Tatuaże to pytanie,
a to chyba odpowiedź.

404
00:39:47,890 --> 00:39:51,490
Utkwił. Czekan się zaklinował.

405
00:39:57,080 --> 00:39:58,910
Boże...

406
00:40:17,550 --> 00:40:19,680
Niech to!

407
00:40:23,960 --> 00:40:24,810
Spójrzcie.

408
00:40:31,620 --> 00:40:33,520
Co ja narobiłem?

409
00:40:34,380 --> 00:40:40,450
Koledzy, naszym życiem
rządzi przypadek i los.

410
00:40:50,790 --> 00:40:53,920
To jakiś petroglif.

411
00:40:56,360 --> 00:40:58,690
Nie wiem, dokąd nas zaprowadzi,

412
00:40:58,780 --> 00:41:05,220
ale mam przeczucie, że będzie to
miejsce cudowne i dziwne.

413
00:41:40,100 --> 00:41:42,180
Witam. Czym mogę służyć?

414
00:41:43,500 --> 00:41:46,680
Napiłabym się czegoś,
ale nie wiem czego.

415
00:41:47,350 --> 00:41:48,770
Panie lubią rum.

416
00:41:49,320 --> 00:41:52,980
W takim razie proszę o rum z...

417
00:41:53,700 --> 00:41:54,480
Tonikiem?

418
00:41:57,610 --> 00:42:02,950
Diane, jest wtorek,
pięć po dziewiątej wieczorem.

419
00:42:03,030 --> 00:42:06,000
Właśnie wróciłem z Sowiej Jaskini.

420
00:42:06,080 --> 00:42:08,330
Jak tam trafiłem,
to dość skomplikowane.

421
00:42:08,410 --> 00:42:11,040
Zaczęło się od tatuaży...

422
00:42:21,130 --> 00:42:23,990
- Pierwszy stawia firma.
- Dziękuję.

423
00:42:24,070 --> 00:42:25,790
Cześć, Annie.

424
00:42:25,880 --> 00:42:29,360
- Chcesz czegoś?
- Mam już rum z tonikiem.

425
00:42:30,110 --> 00:42:33,480
Jedna z sióstr piła herbatę z rumem
i uważałam, że to ciekawe.

426
00:42:34,510 --> 00:42:35,810
To dla ciebie nowość?

427
00:42:37,130 --> 00:42:41,560
Wszystko mnie zaskakuje.

428
00:42:41,650 --> 00:42:46,190
Muzyka i ludzie, jak mówią, śmieją się,

429
00:42:46,270 --> 00:42:50,410
a niektórzy są zakochani.
To dla mnie jak obcy język.

430
00:42:50,710 --> 00:42:53,750
Znam go na tyle, by widzieć,
jak mało rozumiem.

431
00:42:54,610 --> 00:42:56,920
Chciałbym ujrzeć świat twoimi oczami.

432
00:42:57,890 --> 00:42:58,710
Dlaczego?

433
00:42:59,330 --> 00:43:02,260
Wiele rzeczy bym zmienił.

434
00:43:02,900 --> 00:43:04,180
Ja też.

435
00:43:18,900 --> 00:43:21,880
Kiedyś już poniosłam porażkę i...

436
00:43:27,500 --> 00:43:29,530
boję się powtórki.

437
00:43:31,210 --> 00:43:35,400
- Chcesz o tym porozmawiać?
- Jeszcze nie.

438
00:43:37,450 --> 00:43:38,540
Może pomogę.

439
00:43:40,540 --> 00:43:41,590
Potrafisz?

440
00:43:42,160 --> 00:43:43,550
Jeśli zechcesz.

441
00:43:45,080 --> 00:43:49,300
- Jestem okropnie uparta.
- Nie szkodzi.

442
00:43:50,770 --> 00:43:54,140
- Niektórzy mają mnie za dziwaczkę.
- Znam to.

443
00:43:57,760 --> 00:44:02,270
Nie obiecuję, że będę robiła to,
czego byś oczekiwał.

444
00:44:02,360 --> 00:44:04,190
Niczego nie oczekuję.

445
00:44:06,710 --> 00:44:09,410
Więc przyjmuję twoją propozycję.

446
00:44:11,080 --> 00:44:11,950
Dobrze.

447
00:44:12,850 --> 00:44:14,080
Cieszę się.

448
00:44:37,850 --> 00:44:39,900
No proszę...

449
00:44:41,640 --> 00:44:43,290
Co my tu mamy?

450
00:44:47,500 --> 00:44:49,450
Czy to możliwe?

451
00:44:52,680 --> 00:44:53,890
Tak!

452
00:44:56,880 --> 00:44:58,960
Odwrócony symbol!

453
00:45:04,080 --> 00:45:05,370
A teraz...

