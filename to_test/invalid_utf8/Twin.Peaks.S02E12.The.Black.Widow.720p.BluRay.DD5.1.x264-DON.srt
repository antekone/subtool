1
00:00:28,920 --> 00:00:33,660
MIASTECZKO TWIN PEAKS

2
00:02:02,340 --> 00:02:03,740
Panie Horne?

3
00:02:11,240 --> 00:02:12,580
Panie Horne?

4
00:02:16,570 --> 00:02:18,440
Sekretarka mnie wpuściła.

5
00:02:19,440 --> 00:02:20,860
Jestem Bobby Briggs.

6
00:02:21,360 --> 00:02:23,640
Wiesz, co jest w życiu ważne?

7
00:02:26,830 --> 00:02:28,150
Równowaga.

8
00:02:29,610 --> 00:02:30,970
Dystans.

9
00:02:32,570 --> 00:02:33,880
Symetria.

10
00:02:40,890 --> 00:02:42,010
Patrz.

11
00:02:44,500 --> 00:02:46,590
Piękna rzecz, prawda?

12
00:02:51,830 --> 00:02:55,080
Przesłuchał pan taśmę, którą przysłałem?

13
00:02:55,170 --> 00:02:56,810
Tak, słuchałem.

14
00:02:57,200 --> 00:03:01,610
I jestem szczerze zdumiony,
że Leo na to wpadł.

15
00:03:05,630 --> 00:03:07,200
Czego chcesz?

16
00:03:08,880 --> 00:03:12,240
Panie Horne,
darzę pana wielkim podziwem.

17
00:03:12,560 --> 00:03:15,900
Podziw należy się
poetom i mlecznym krowom.

18
00:03:15,990 --> 00:03:20,040
Nie bardzo rozumiem.

19
00:03:20,950 --> 00:03:22,130
Bobby...

20
00:03:25,720 --> 00:03:30,090
stoisz przed gigantycznym wieżowcem.

21
00:03:30,520 --> 00:03:34,100
Zawiłą konstrukcją, przebijającą chmury.

22
00:03:34,970 --> 00:03:37,600
Jakie pytanie ci się nasuwa?

23
00:03:46,020 --> 00:03:48,350
Pierwsze pytanie,
jakie sobie zadajesz, to:

24
00:03:48,430 --> 00:03:53,460
"Kto mieszka na szczycie?
Kto to jest i dlaczego?".

25
00:03:54,610 --> 00:03:56,230
Ja bym o to spytał.

26
00:03:58,500 --> 00:03:59,770
A ty?

27
00:04:02,710 --> 00:04:06,060
Jasne, ja też.

28
00:04:10,100 --> 00:04:11,200
Bobby...

29
00:04:12,290 --> 00:04:14,250
masz wielkie szczęście.

30
00:04:17,980 --> 00:04:22,990
Będziesz śledził Hanka Jenningsa.

31
00:04:23,890 --> 00:04:26,830
Masz wszystko dokumentować.

32
00:04:27,500 --> 00:04:31,740
Pokaż mi coś, o czym jeszcze nie wiem.

33
00:04:48,110 --> 00:04:53,180
- Obejrzę obydwa.
- Świetnie. Który najpierw?

34
00:05:13,660 --> 00:05:14,870
Co to takiego?

35
00:05:15,950 --> 00:05:17,970
Nie zamierzałam tego pokazywać.

36
00:05:18,060 --> 00:05:22,830
To Farma Zdechłego Psa
i nazwa jest bardzo trafna.

37
00:05:24,230 --> 00:05:25,330
Dlaczego?

38
00:05:25,420 --> 00:05:28,170
Nie wiadomo dlaczego.
Nikt nie zagrzewa tam miejsca.

39
00:05:29,010 --> 00:05:30,840
Kiedy tam pojedziemy?

40
00:05:35,330 --> 00:05:39,470
Przepraszam za spóźnienie,
ale mały tak się przejął wycieczką,

41
00:05:39,550 --> 00:05:42,390
że nie daje się wyciągnąć z samochodu.

42
00:05:42,470 --> 00:05:46,360
To jest pani Judy Swyne
z fundacji Pomocna Dłoń.

43
00:05:46,440 --> 00:05:49,610
- Zajmuję się Nickym.
- Bardzo mi miło.

44
00:05:49,690 --> 00:05:50,730
Usiądźmy.

45
00:05:56,450 --> 00:05:59,920
Właśnie zamierzałam uprzedzić
pańskich znajomych

46
00:06:00,010 --> 00:06:03,560
o najważniejszym,
co trzeba wiedzieć o Nickym.

47
00:06:03,640 --> 00:06:08,790
W swoim krótkim życiu
wiele się już wycierpiał,

48
00:06:08,870 --> 00:06:14,340
zupełnie jakby los się na niego uwziął.

49
00:06:15,270 --> 00:06:18,140
Wygląda na to, że życie go nie pieści.

50
00:06:18,220 --> 00:06:21,860
- Tak, w istocie.
- Biedactwo.

51
00:06:21,940 --> 00:06:23,380
Jest sierotą.

52
00:06:23,460 --> 00:06:25,810
Tak? Co się stało? Rodzice mu umarli?

53
00:06:26,830 --> 00:06:31,820
Słusznie główkujesz.
Los się na niego uwziął?

54
00:06:32,370 --> 00:06:34,030
Zmarli tragicznie.

55
00:06:34,680 --> 00:06:36,950
W jaki sposób?

56
00:06:37,440 --> 00:06:39,290
W tajemniczych okolicznościach.

57
00:06:39,370 --> 00:06:43,370
Ale władze sierocińca
nie ujawniły w jakich.

58
00:06:44,390 --> 00:06:46,830
Andy, jedziemy. Coś się stało w hotelu.

59
00:07:04,600 --> 00:07:06,360
To wygląda na zawał.

60
00:07:07,210 --> 00:07:13,770
"Pobudzona kobieta reaguje
w ten sposób, że skóra wokół jej..."

61
00:07:13,850 --> 00:07:15,440
Mój Boże!

62
00:07:15,520 --> 00:07:17,820
Najwyraźniej Dougie odszedł w butach.

63
00:07:18,470 --> 00:07:21,520
Miłosne wiersze Byrona,

64
00:07:21,600 --> 00:07:25,120
Kamasutra, Ladacznice i służące...

65
00:07:25,890 --> 00:07:27,710
A to widzieliście?

66
00:07:33,400 --> 00:07:35,790
Co tu się wyrabia?

67
00:07:44,450 --> 00:07:45,610
Doug...

68
00:07:47,670 --> 00:07:51,710
niestety wyszło na moje.

69
00:08:05,640 --> 00:08:09,050
Oto narzędzie zbrodni.

70
00:08:10,430 --> 00:08:13,820
Równie dobrze mogła mu wypalić w łeb!

71
00:08:22,290 --> 00:08:25,440
Nie umiał odmawiać kobietom.

72
00:08:33,250 --> 00:08:35,020
Stary dureń...

73
00:08:50,860 --> 00:08:55,170
Ty awanturnico!
Spłoniesz za to w piekle!

74
00:08:55,250 --> 00:08:59,710
- Proszę się uspokoić.
- Będę krzyczał, aż obudzę umarłych!

75
00:08:59,790 --> 00:09:02,210
Zabiła mi brata!

76
00:09:02,300 --> 00:09:04,470
To czarownica!

77
00:09:05,820 --> 00:09:10,700
- Ma rację, jestem przeklęta.
- Bzdury.

78
00:09:10,790 --> 00:09:14,020
Nie, naprawdę.

79
00:09:14,970 --> 00:09:19,370
Zaczęło się w liceum, na studniówce.

80
00:09:20,320 --> 00:09:23,090
Mój chłopak chciał mnie pocałować,

81
00:09:23,170 --> 00:09:26,740
a pierwszy raz był na randce
z nowym aparatem na zębach.

82
00:09:26,820 --> 00:09:30,010
Tak błyszczał w świetle księżyca...

83
00:09:32,850 --> 00:09:37,140
Coś trzasnęło i nie mógł zamknąć ust.

84
00:09:37,220 --> 00:09:43,760
W szpitalu musieli mu złamać
szczękę w trzech miejscach.

85
00:09:44,430 --> 00:09:47,140
Od tej pory było coraz gorzej.

86
00:09:47,530 --> 00:09:51,650
Klątwę można zdjąć.
Trochę się na tym znam.

87
00:09:52,150 --> 00:09:54,000
Jest pan szeryfem?

88
00:09:55,770 --> 00:10:01,290
Powiedzmy, że specjalistą
od trudnych spraw.

89
00:10:07,650 --> 00:10:12,300
Jest historia o pewnym trenerze,

90
00:10:12,380 --> 00:10:14,870
którego nazwisko zapomniałem.

91
00:10:14,950 --> 00:10:18,770
Za nic nie chciał dopuścić
do drużyny czarnych.

92
00:10:19,490 --> 00:10:22,710
Aż ktoś przyprowadził
najlepszego czarnego napastnika,

93
00:10:22,790 --> 00:10:25,000
jakiego widział w życiu.

94
00:10:25,080 --> 00:10:29,690
Gdy zobaczył, jak przebiegł 50 jardów,

95
00:10:29,770 --> 00:10:33,010
zawołał: "Patrzcie,
jak ten Indianin leci!".

96
00:10:33,560 --> 00:10:38,000
I uszanował wolę walki sportowca.

97
00:10:38,800 --> 00:10:40,020
Nadine...

98
00:10:41,500 --> 00:10:45,300
Dziś to samo można powiedzieć
o tej kobiecie...

99
00:10:45,390 --> 00:10:52,620
dziewczynie, bo też chce walczyć.
Ma moralne i konstytucyjne prawo.

100
00:10:54,060 --> 00:10:57,370
Poproszę tu Mike'a Nelsona.

101
00:11:03,250 --> 00:11:09,430
Nadine chce się sprawdzić,
walcząc z najlepszym z nas,

102
00:11:09,520 --> 00:11:13,430
mistrzem okręgu, Mikiem Nelsonem.

103
00:11:18,580 --> 00:11:20,230
Cześć, Mike.

104
00:11:21,980 --> 00:11:23,360
Połóż tu rękę...

105
00:11:24,070 --> 00:11:25,390
Nie, tutaj...

106
00:11:26,390 --> 00:11:28,820
A teraz pchaj, a ja spróbuję
nic ci nie zrobić.

107
00:11:28,910 --> 00:11:31,690
- Spotkamy się wieczorem?
- To się nazywa "krawat".

108
00:11:38,320 --> 00:11:40,670
- Ty głupia...
- Co mówisz?

109
00:11:47,640 --> 00:11:50,680
- Skręcisz mi kark.
- Chętnie cię popieszczę.

110
00:11:54,040 --> 00:11:56,730
Dziś wieczorem?

111
00:11:58,940 --> 00:11:59,760
Trenerze?

112
00:12:03,240 --> 00:12:03,980
Trenerze!

113
00:12:13,460 --> 00:12:16,310
Raz, dwa i trzy.

114
00:12:16,400 --> 00:12:18,280
Umówisz się ze mną?

115
00:12:35,360 --> 00:12:39,640
- Co ci jest?
- Dziewczyna mnie pobiła.

116
00:12:41,750 --> 00:12:46,100
- Co jej zrobiłeś?
- Nie żartuję. Pomóż mi.

117
00:12:46,180 --> 00:12:49,930
Nie dość, że mnie położyła
przy drużynie, to jeszcze na mnie leci.

118
00:12:50,010 --> 00:12:51,860
- Która to?
- Pani Hurley.

119
00:12:52,950 --> 00:12:54,300
Nadine?

120
00:12:55,570 --> 00:12:59,680
- Ciebie to bawi?
- Może tobie trzeba starszej kobiety.

121
00:12:59,760 --> 00:13:02,650
Nie chcę kogoś, kto mną rzuca.

122
00:13:02,730 --> 00:13:04,670
Co mnie do tego?

123
00:13:04,760 --> 00:13:08,000
Pogadaj z nią albo udaj,
że ze sobą chodzimy.

124
00:13:08,540 --> 00:13:10,430
Przeceniasz mnie.

125
00:13:10,520 --> 00:13:14,140
Zrób coś, bo skończę na wyciągu.

126
00:13:26,830 --> 00:13:30,530
- Słucham.
- Planuję mały bunt służby.

127
00:13:30,620 --> 00:13:34,660
Nazbieramy chrustu i podpalimy tę budę.

128
00:13:34,740 --> 00:13:40,960
Malcolm Sloane, brat Evelyn,
a poza tym szofer pana Marsha.

129
00:13:41,040 --> 00:13:42,660
Co z gablotą?

130
00:13:43,080 --> 00:13:46,420
- Mocno rozwaliła Jaguara?
- Da się naprawić.

131
00:13:47,080 --> 00:13:50,510
To wielka zaleta przedmiotów.

132
00:13:53,320 --> 00:13:58,770
Kiedy Jeffrey się w niej zakochał,
dał jej nowe życie.

133
00:14:00,920 --> 00:14:05,180
Ja dostałem twarzowy mundur...
i klucz do barku.

134
00:14:08,820 --> 00:14:12,910
Nie chcę wyjść na niewdzięcznika,
bo przy tym, co było dawniej,

135
00:14:12,990 --> 00:14:17,780
to jawna rozpusta.
Evelyn też się sporo nauczyła.

136
00:14:18,410 --> 00:14:21,540
Robienia uników, maskowania sińców,
wrednych numerów.

137
00:14:21,980 --> 00:14:23,450
O czym ty gadasz?

138
00:14:24,250 --> 00:14:26,520
Byłem ordynarny?

139
00:14:27,340 --> 00:14:30,290
Jeffrey leje ją regularnie
co dwa tygodnie,

140
00:14:30,370 --> 00:14:33,920
a ona mści się, coś mu niszcząc.

141
00:14:34,950 --> 00:14:37,030
To tylko nakręca spiralę.

142
00:14:37,110 --> 00:14:39,300
To twoja siostra, zrób coś.

143
00:14:41,970 --> 00:14:45,420
Panu Marshowi się nie sprzeciwia.

144
00:14:45,500 --> 00:14:47,840
Takie tu mamy zasady.

145
00:14:48,730 --> 00:14:51,070
Masz to przełknąć i odpuścić.

146
00:14:53,490 --> 00:14:54,970
Muszę spadać.

147
00:14:58,290 --> 00:14:59,540
Jestem ci wdzięczny.

148
00:15:00,780 --> 00:15:01,950
Naprawdę.

149
00:15:09,830 --> 00:15:12,340
NA SPRZEDAŻ

150
00:15:34,780 --> 00:15:39,310
Wciąż stoi. Jakby siłą przyzwyczajenia.

151
00:15:39,390 --> 00:15:44,720
To mocny fundament.
Farma Zdechłego Psa?

152
00:15:44,800 --> 00:15:51,180
Jest taka legenda, że ściągają tu
najlepsi i najgorsi ze świata.

153
00:15:51,270 --> 00:15:57,110
Większość odchodzi, a tylko ci
o czystym sercu rozumieją to miejsce.

154
00:15:57,200 --> 00:16:01,160
Reszta z nas z nim walczy.

155
00:16:02,030 --> 00:16:05,080
Ktoś to ostatnio oglądał?

156
00:16:05,170 --> 00:16:08,940
Chyba od roku nikogo nie było.

157
00:16:09,020 --> 00:16:14,770
Trzy samochody: jeep,
terenówka i luksusowy sedan.

158
00:16:20,490 --> 00:16:21,870
Otwarte...

159
00:16:29,650 --> 00:16:31,660
Ktoś tu był...

160
00:16:33,070 --> 00:16:35,190
najwyżej kilka godzin temu.

161
00:16:47,400 --> 00:16:49,010
Co pan znalazł?

162
00:16:52,990 --> 00:16:55,450
Zapomnieli, że nie ma bieżącej wody.

163
00:17:04,830 --> 00:17:06,420
Lek na przeczyszczenie.

164
00:17:36,790 --> 00:17:38,900
Więcej białego proszku.

165
00:17:45,650 --> 00:17:47,190
To kokaina.

166
00:17:49,480 --> 00:17:51,520
Trzeba zawiadomić szeryfa.

167
00:18:05,740 --> 00:18:07,720
Nicky, przestań, proszę.

168
00:18:16,080 --> 00:18:17,730
Przestań.

169
00:18:19,780 --> 00:18:21,740
Nic z tego nie rozumiem.

170
00:18:23,290 --> 00:18:25,860
Samochód jest po przeglądzie.

171
00:18:27,490 --> 00:18:29,370
Mówili, że jest w pełni sprawny.

172
00:18:34,390 --> 00:18:38,210
To jest takie skomplikowane...

173
00:18:42,290 --> 00:18:45,760
Proszę cię, wujek próbuje czytać.

174
00:18:56,320 --> 00:18:58,190
Nicholas, wysiadaj!

175
00:19:00,100 --> 00:19:04,300
Wysiadaj z samochodu, natychmiast!

176
00:19:05,600 --> 00:19:06,950
Słyszysz?

177
00:19:10,530 --> 00:19:12,630
Gniewasz się na mnie?

178
00:19:12,710 --> 00:19:16,460
Skądże, dlaczego miałbym się gniewać?

179
00:19:16,540 --> 00:19:18,130
Nastraszyłem cię?

180
00:19:44,890 --> 00:19:46,070
Wujku Dick!

181
00:19:48,290 --> 00:19:50,820
Nie martw się, nic mi nie jest.

182
00:19:50,910 --> 00:19:54,510
A gdybyś umarł? Nie umrzesz, prawda?

183
00:19:54,590 --> 00:19:57,680
Jasne, że nie. Nie gadaj głupstw.

184
00:20:04,390 --> 00:20:05,250
Harry?

185
00:20:05,950 --> 00:20:08,050
To pułkownik Riley.

186
00:20:08,130 --> 00:20:11,610
Prowadzi dochodzenie
w sprawie zniknięcia Briggsa.

187
00:20:11,700 --> 00:20:13,100
Witam.

188
00:20:13,930 --> 00:20:16,180
Był pan z nim,
gdy zniknął nam z radaru.

189
00:20:16,740 --> 00:20:18,310
Tak jest.

190
00:20:18,770 --> 00:20:22,420
Czy zauważył pan w okolicy
jakieś dzikie zwierzęta?

191
00:20:22,500 --> 00:20:25,770
- Zwierzęta?
- Ptaki albo sowy?

192
00:20:25,850 --> 00:20:27,030
Tak.

193
00:20:27,730 --> 00:20:31,740
Tuż przed zniknięciem majora
słyszałem sowę.

194
00:20:32,460 --> 00:20:34,240
Widział ją pan?

195
00:20:34,330 --> 00:20:37,890
Garland Briggs to mój przyjaciel.

196
00:20:37,980 --> 00:20:40,210
Byłoby lepiej, gdyby pan mówił wprost.

197
00:20:40,290 --> 00:20:45,630
Wiemy o antenach i dotyczących mnie
sygnałach z kosmosu.

198
00:20:46,170 --> 00:20:50,870
Wiecie to i owo, ale trochę to uściślę.

199
00:20:50,950 --> 00:20:53,880
Nasze anteny są wycelowane w kosmos,

200
00:20:53,960 --> 00:20:58,240
ale odebrana wiadomość,
którą przekazał panu Briggs,

201
00:20:58,770 --> 00:21:02,190
została wysłana z tego lasu.

202
00:21:02,440 --> 00:21:06,520
Do kogo - to już inna historia.

203
00:21:07,400 --> 00:21:11,200
Czy to ma coś wspólnego
z tak zwaną "Białą Chatą"?

204
00:21:12,410 --> 00:21:13,980
To tajne.

205
00:21:15,460 --> 00:21:18,620
Chętnie pomożemy, ale musi
nam pan powiedzieć więcej.

206
00:21:20,410 --> 00:21:26,150
Garland Briggs to najlepszy pilot,
jakiego znam.

207
00:21:26,230 --> 00:21:30,170
Ma zdolności, o jakich
większość może tylko marzyć.

208
00:21:30,840 --> 00:21:34,420
Powiem tylko, że jego zniknięcie

209
00:21:34,510 --> 00:21:39,240
ma dla bezpieczeństwa narodowego
takie znaczenie,

210
00:21:39,890 --> 00:21:43,100
że zimna wojna
to przy tym przeziębienie.

211
00:21:51,650 --> 00:21:56,080
Silnik działa, oś też jest w porządku.

212
00:21:58,080 --> 00:22:00,050
Gdzie się tego nauczyłeś?

213
00:22:01,490 --> 00:22:06,330
Od wujka Eda. Nie dorastam mu do pięt.

214
00:22:08,070 --> 00:22:12,540
Twój brat wspomniał mi
o twojej sytuacji...

215
00:22:12,620 --> 00:22:15,340
Pilnuj swoich spraw.

216
00:22:17,240 --> 00:22:18,680
Boisz się?

217
00:22:20,090 --> 00:22:21,640
Swojego męża?

218
00:22:22,790 --> 00:22:23,990
Powiedz.

219
00:22:25,110 --> 00:22:26,990
Nie ma o czym mówić.

220
00:22:29,580 --> 00:22:32,280
Wiem, czym jest samotność.

221
00:22:48,590 --> 00:22:50,820
Boisz się go, prawda?

222
00:22:53,260 --> 00:22:55,150
Dlaczego nie odejdziesz?

223
00:22:56,310 --> 00:22:57,840
To trudne.

224
00:23:09,740 --> 00:23:11,410
Boże, to on!

225
00:23:13,560 --> 00:23:15,570
Muszę iść.

226
00:23:19,090 --> 00:23:21,910
Nie jest tak źle, jak ci się wydaje.

227
00:23:49,190 --> 00:23:53,040
Kogo ja widzę? Tatko zmienił zdanie?

228
00:23:53,680 --> 00:23:56,740
Tatko podjął najlepszą decyzję roku.

229
00:23:56,820 --> 00:24:00,040
Tak? Mów, co zrobił?

230
00:24:00,820 --> 00:24:02,580
Przyjął mnie do roboty.

231
00:24:04,260 --> 00:24:06,290
Ty mały karierowiczu!

232
00:24:06,380 --> 00:24:10,400
Mam tu coś. Moje ostatnie zlecenie.

233
00:24:11,250 --> 00:24:13,980
Mogę ci jakoś pomóc?

234
00:24:14,060 --> 00:24:15,590
Może.

235
00:24:15,680 --> 00:24:20,320
Na przykład to oblać.

236
00:24:21,390 --> 00:24:23,410
Jak pokażę to szefowi.

237
00:24:23,490 --> 00:24:25,890
Wiesz, co pomyślałam?

238
00:24:26,620 --> 00:24:27,730
Powiedz.

239
00:24:28,260 --> 00:24:31,890
Powinniśmy się zastanowić

240
00:24:31,970 --> 00:24:35,860
nad wspólnym interesem.

241
00:24:44,160 --> 00:24:46,910
- To bardzo dobry pomysł.
- Dziękuję.

242
00:25:09,860 --> 00:25:11,190
Co słychać, panie Horne?

243
00:25:16,180 --> 00:25:19,820
Co pan właściwie robi?

244
00:25:20,890 --> 00:25:23,170
Gettysburg, dzień pierwszy.

245
00:25:24,840 --> 00:25:25,910
Południe ma przewagę.

246
00:25:28,000 --> 00:25:30,910
Widziałem Hanka i mam dla pana zdjęcia.

247
00:25:40,450 --> 00:25:44,030
Nie wiem, co knuje, ale nic dobrego.

248
00:25:44,120 --> 00:25:49,010
Nie znam tych facetów,
ale to nie kółko różańcowe.

249
00:25:49,370 --> 00:25:53,190
- Lubię cię.
- I wzajemnie.

250
00:25:53,270 --> 00:25:54,650
Ile ci płacę?

251
00:25:57,030 --> 00:26:00,230
Jeszcze o tym nie rozmawialiśmy.

252
00:26:00,990 --> 00:26:02,780
Uznaj to za pierwszą podwyżkę.

253
00:26:02,870 --> 00:26:07,000
Wpadnij jutro,
to pogadamy o pełnym etacie.

254
00:26:09,380 --> 00:26:13,720
Dzięki... Ben. Nie zawiodę cię.

255
00:26:26,270 --> 00:26:30,270
To nektar bogów, moja droga.

256
00:26:34,630 --> 00:26:37,040
Bąbelki.

257
00:26:41,090 --> 00:26:45,040
"Wino w usta wpływa...

258
00:26:46,090 --> 00:26:49,780
miłość się w oczach odzywa.

259
00:26:50,410 --> 00:26:52,790
Dotykam wargami kielicha

260
00:26:53,440 --> 00:26:56,180
i patrząc na ciebie, wzdycham."

261
00:26:57,750 --> 00:26:59,120
Dziękuję.

262
00:26:59,440 --> 00:27:02,710
- To takie liryczne.
- To Keats.

263
00:27:03,790 --> 00:27:06,040
Poeta.

264
00:27:07,070 --> 00:27:10,350
Tak, wiem.

265
00:27:10,640 --> 00:27:14,020
Znam inny toast, ale to limeryk.

266
00:27:14,100 --> 00:27:19,980
- Nie, nie musisz...
- "Była raz w Galway dama..."

267
00:27:20,060 --> 00:27:21,190
Josie!

268
00:27:26,030 --> 00:27:26,810
Słucham.

269
00:27:26,900 --> 00:27:29,740
Podaj zakąski.

270
00:27:32,480 --> 00:27:37,330
Musisz wiedzieć, że choć twoja
pozycja w tym domu jest inna,

271
00:27:37,770 --> 00:27:42,880
będę ci okazywać szacunek,
na jaki zasługujesz.

272
00:27:44,790 --> 00:27:49,430
Dziękuję, ja też się postaram.

273
00:27:51,030 --> 00:27:53,060
I włóż czepeczek.

274
00:27:59,470 --> 00:28:05,650
Nie jesteś dla niej za ostra?
Wciąż należy do rodziny.

275
00:28:06,290 --> 00:28:12,620
Przyłożyła rękę do zabójstwa Andrew
i próbowała mnie zniszczyć.

276
00:28:12,700 --> 00:28:14,950
Ma szczęście, że jej nie powiesiłam.

277
00:28:16,130 --> 00:28:18,640
Nie wierzę.

278
00:28:18,720 --> 00:28:21,150
Ja znam inną Josie.

279
00:28:21,990 --> 00:28:27,920
Chcę wznieść toast
za dwulicowość Bena Horne'a,

280
00:28:28,000 --> 00:28:34,780
zbrodnie Josie Packard
i za kobietę, która ich okpiła.

281
00:28:36,210 --> 00:28:38,890
Czyli za mnie.

282
00:28:41,810 --> 00:28:43,910
Twoje zdrowie.

283
00:28:48,410 --> 00:28:51,610
Mam w ręku ogólnokrajową gazetę.

284
00:28:51,700 --> 00:28:56,200
Moją odpowiedź na otwarcie
Windoma Earle'a kazałem wydrukować

285
00:28:56,280 --> 00:29:02,850
wśród prywatnych ogłoszeń
i wczoraj otrzymałem wiadomość.

286
00:29:03,660 --> 00:29:07,290
Bezbłędnie przewidział mój ruch.

287
00:29:08,620 --> 00:29:10,420
Igra ze mną.

288
00:29:12,470 --> 00:29:16,100
Ciekawe, gdzie jest i co zamierza.

289
00:29:19,660 --> 00:29:24,010
Dwa dni bez odznaki i broni
spędziłem najlepiej jak umiałem,

290
00:29:24,090 --> 00:29:28,920
nie marnując czasu
i oglądając nieruchomość,

291
00:29:29,000 --> 00:29:33,040
którą agentka miłosiernie
nazwała "obiecującą".

292
00:29:33,130 --> 00:29:36,070
Mimo wszystko można tam
zamieszkać i założyć rodzinę,

293
00:29:36,160 --> 00:29:39,440
co dotąd mi się nie udało,
ale nie tracę nadziei.

294
00:29:41,170 --> 00:29:48,010
Jednak jak wszędzie w Twin Peaks
tam także kryje się tajemnica,

295
00:29:48,090 --> 00:29:51,700
która może mieć związek
z moimi kłopotami

296
00:29:51,780 --> 00:29:54,770
i kokainą znalezioną
w moim samochodzie.

297
00:29:57,430 --> 00:30:00,050
Agent Hardy niedługo
przedstawi mi zarzuty

298
00:30:00,140 --> 00:30:05,190
i jeśli nie zdołam się wybronić,
czeka mnie długi wyrok.

299
00:30:21,030 --> 00:30:22,430
Dzień dobry.

300
00:30:30,310 --> 00:30:32,620
Ukradłam to dla pana.

301
00:30:33,630 --> 00:30:35,710
Może lepiej to zwróć.

302
00:30:36,620 --> 00:30:38,680
Najpierw niech pan tam zajrzy.

303
00:30:43,440 --> 00:30:44,770
I co znajdę?

304
00:30:45,230 --> 00:30:47,370
Ojciec dużo za nie zapłacił.

305
00:30:55,520 --> 00:30:57,320
Dobrze zrobiłam?

306
00:30:58,350 --> 00:31:02,830
Lepiej niż dobrze.
Może uratowałaś mi życie.

307
00:31:04,470 --> 00:31:05,770
Fajnie.

308
00:31:10,140 --> 00:31:11,880
To jesteśmy kwita?

309
00:31:19,410 --> 00:31:21,600
- Cześć, Coop.
- Wejdź.

310
00:31:24,940 --> 00:31:26,560
Przeszkadzam?

311
00:31:27,340 --> 00:31:32,150
Agentka Denise Bryson z wydziału
narkotykowego, a to Audrey Horne.

312
00:31:34,660 --> 00:31:36,990
Przyjmują kobiety?

313
00:31:37,070 --> 00:31:38,580
Powiedzmy.

314
00:31:40,170 --> 00:31:45,330
- Mamy sprawy służbowe.
- Nie jest pan zawieszony?

315
00:31:46,030 --> 00:31:47,000
Jestem.

316
00:31:47,670 --> 00:31:50,450
Dziękuję i dobrej nocy.

317
00:31:53,030 --> 00:31:57,010
Nawzajem, to drobiazg.

318
00:32:07,780 --> 00:32:11,800
To zdjęcia z opuszczonego domu
na skraju miasta.

319
00:32:11,890 --> 00:32:15,700
Jean Renault, Hank Jennings,
ojczym Normy Jennings

320
00:32:15,780 --> 00:32:19,760
i sierżant King z Królewskiej Konnej.
Byli tam dzisiaj.

321
00:32:19,850 --> 00:32:24,230
W kuchni znalazłem kokainę
i proszek, z którym ją mieszali.

322
00:32:25,450 --> 00:32:30,530
Porównaj to z próbkami z mojego wozu.
Myślę, że to to samo.

323
00:32:30,620 --> 00:32:32,840
To dobra wiadomość.

324
00:32:32,930 --> 00:32:38,050
Pomówmy o czymś ważniejszym.
Ile ona ma lat?

325
00:32:39,220 --> 00:32:42,570
Sądziłem, że dziewczyny
już cię nie interesują.

326
00:32:42,660 --> 00:32:47,680
Chodzę w kiecce, ale nogi wciąż
wkładam w majtki oddzielnie.

327
00:32:47,770 --> 00:32:49,260
Rozumiesz?

328
00:32:49,920 --> 00:32:51,240
Nie bardzo.

329
00:32:57,670 --> 00:33:01,180
Będziesz to jadł
czy tylko przepychał po talerzu?

330
00:33:02,240 --> 00:33:04,370
Nie jestem głodny.

331
00:33:04,450 --> 00:33:08,040
- Chcesz o tym pogadać?
- To cię nie zainteresuje.

332
00:33:10,060 --> 00:33:11,750
Rozmawialiśmy o wszystkim.

333
00:33:13,330 --> 00:33:15,210
I wciąż możemy.

334
00:33:16,790 --> 00:33:21,820
Pamiętasz, jak w dzieciństwie
snuliśmy wielkie plany,

335
00:33:21,910 --> 00:33:27,100
czekając, aż zaczniemy naprawdę żyć
i będziemy je realizować?

336
00:33:27,920 --> 00:33:32,770
Ale zanim się obejrzeliśmy,
jesteśmy już w połowie drogi

337
00:33:32,850 --> 00:33:36,230
i wszystkie tamte plany
nie są warte funta kłaków.

338
00:33:37,600 --> 00:33:40,610
Tak wygląda moje życie
i wcale go nie lubię.

339
00:33:48,590 --> 00:33:50,440
Można snuć nowe plany.

340
00:34:06,940 --> 00:34:08,070
Andrew.

341
00:34:10,150 --> 00:34:12,020
O co chodzi?

342
00:34:12,100 --> 00:34:15,440
- Zamienimy słówko?
- Jasne.

343
00:34:16,380 --> 00:34:19,700
- Chyba mamy problem.
- Jaki?

344
00:34:19,780 --> 00:34:23,220
- Chodzi o małego Nicky'ego.
- Tak?

345
00:34:23,300 --> 00:34:26,090
Jak by to ująć? On jest...

346
00:34:29,160 --> 00:34:33,920
Myślę, że Nicky, choć brzmi to
niewiarygodnie, to diabeł.

347
00:34:34,000 --> 00:34:35,680
Diabeł?

348
00:34:36,540 --> 00:34:39,770
A w najlepszym razie morderca.

349
00:34:39,850 --> 00:34:43,360
Trzeba sprawdzić,
co spotkało jego rodziców.

350
00:34:48,700 --> 00:34:51,850
Mam raport z sekcji.

351
00:34:52,130 --> 00:34:56,650
Dougie zmarł z przyczyn naturalnych,
na serce.

352
00:34:56,730 --> 00:34:58,940
Brak znamion przestępstwa.

353
00:35:01,010 --> 00:35:03,450
Badałeś go pod kątem czarów?

354
00:35:04,430 --> 00:35:09,260
Tego nie wykaże żadna autopsja.

355
00:35:12,520 --> 00:35:14,290
Pójdę do sądu.

356
00:35:14,370 --> 00:35:17,670
Nie możesz nikogo oskarżać,
bo nie popełniono przestępstwa.

357
00:35:18,130 --> 00:35:21,020
Zabiła go seksem.

358
00:35:22,960 --> 00:35:25,630
Czy to nie jest oczywiste?

359
00:35:25,710 --> 00:35:32,060
To był męczący dzień.
Idź do domu i odpocznij.

360
00:35:32,440 --> 00:35:34,870
Mam iść spać?

361
00:35:34,950 --> 00:35:40,610
Nie, póki ta ladacznica nie trafi
za kratki, gdzie jej miejsce.

362
00:35:40,690 --> 00:35:44,560
Wytoczę jej sprawę cywilną.

363
00:35:44,640 --> 00:35:48,310
Nie położy ręki na majątku Douga!

364
00:35:48,390 --> 00:35:54,240
Nie dam, póki żyję!
Ani złamanego centa! Nic!

365
00:36:01,210 --> 00:36:05,450
Harry, masz jeszcze
tę irlandzką whisky?

366
00:36:05,530 --> 00:36:08,090
Dolałbym jej do mleka
dla wdowy Milford.

367
00:36:10,140 --> 00:36:13,980
"Ona zawstydza świec jarzących blaski.

368
00:36:15,580 --> 00:36:21,560
Piękność jej wisi u nocnej opaski
jak drogi klejnot u uszu Etiopa..."

369
00:36:22,210 --> 00:36:28,280
"Nie tknęła ziemi wytworniejsza stopa..."

370
00:37:00,160 --> 00:37:02,960
Posterunek w Twin Peaks, mówi Lucy.

371
00:37:03,970 --> 00:37:05,550
Chwileczkę.

372
00:37:07,500 --> 00:37:09,020
Szeryfie Truman?

373
00:37:10,630 --> 00:37:11,890
Szeryfie?

374
00:37:20,690 --> 00:37:26,430
Szeryfie Truman,
telefon na trzeciej linii.

375
00:37:31,620 --> 00:37:33,390
Proszę zaczekać.

376
00:37:58,560 --> 00:38:05,140
Teraz będzie najlepsze. Spytałam:
"Po co według ciebie noszę majtki?".

377
00:38:10,390 --> 00:38:14,950
Albo to. Miałam kuzyna,
który był klaunem na rodeo.

378
00:38:15,030 --> 00:38:20,160
Wszyscy mieli strasznie wielkie konie

379
00:38:20,250 --> 00:38:24,460
i nie uwierzycie, do czego go zmusili.

380
00:38:24,550 --> 00:38:26,570
Kazali mu... Dziękuję.

381
00:38:26,650 --> 00:38:33,030
Musiał wyjść na środek
i rozebrać się do naga.

382
00:39:01,560 --> 00:39:02,180
Słucham panią?

383
00:39:03,680 --> 00:39:06,990
Panie Niles, nie chcę na pana naciskać,

384
00:39:07,070 --> 00:39:11,060
ale siedział pan
i nie muszę panu wiele wyjaśniać.

385
00:39:14,320 --> 00:39:15,510
O co chodzi?

386
00:39:15,600 --> 00:39:19,670
Choćby o złamanie warunków zwolnienia.

387
00:39:22,020 --> 00:39:23,790
To wystarczy.

388
00:39:24,970 --> 00:39:29,220
I jeśli nie będzie pan
współpracował, dopilnuję,

389
00:39:29,300 --> 00:39:33,460
żeby wrócił pan na dłużej.

390
00:39:34,850 --> 00:39:39,010
Nazywam się Ernie Niles
i przyznaję się do winy.

391
00:39:39,090 --> 00:39:42,590
Jestem winny, choć wbrew mojej woli.

392
00:39:42,680 --> 00:39:44,540
Nie interesują nas szczegóły.

393
00:39:44,620 --> 00:39:49,900
Grozili mi bronią, bili i straszyli.
To bydlaki, nic ich nie powstrzyma.

394
00:39:49,980 --> 00:39:53,240
- Panie Niles...
- Mam rozprowadzić ich narkotyki.

395
00:39:53,320 --> 00:39:58,630
Opierałem się, ale rodzina
jest dla mnie wszystkim.

396
00:39:58,710 --> 00:40:02,670
Gdy zagrozili mojej żonie, uległem.

397
00:40:02,750 --> 00:40:06,930
Bardzo mi przykro.
Postąpiłem źle, ale z miłości.

398
00:40:07,490 --> 00:40:11,200
Miłość ogłupia,
sprowadza na złą drogę...

399
00:40:11,280 --> 00:40:14,220
Ernie, zamknij się!

400
00:40:14,300 --> 00:40:18,930
Rozumiemy pana i współczujemy.
Dlatego rozmawiamy.

401
00:40:19,010 --> 00:40:23,220
- Naprawdę?
- Tak. Mów, co wiesz.

402
00:40:23,300 --> 00:40:28,460
Torturowali mnie i chodziło im o to...

403
00:40:28,550 --> 00:40:33,840
Proszę się ograniczyć
do sprawy narkotyków.

404
00:40:35,200 --> 00:40:41,180
Mam dla nich sprzedać
4 kilogramy kokainy.

405
00:40:41,260 --> 00:40:46,790
Muszę chronić rodzinę
i znaleźć jakiegoś kupca.

406
00:40:46,880 --> 00:40:51,350
- Znalazłeś?
- Nie znam nikogo takiego.

407
00:40:51,440 --> 00:40:52,800
Już znasz.

408
00:40:54,010 --> 00:40:57,530
Jutro przyjedzie z Seattle
poważny kupiec.

409
00:40:57,620 --> 00:41:00,950
Ustawisz spotkanie
na Farmie Zdechłego Psa.

410
00:41:04,270 --> 00:41:07,090
Jak go poznam?

411
00:41:08,820 --> 00:41:10,550
Patrzysz na niego.

412
00:41:11,210 --> 00:41:12,440
Na niego?

413
00:41:14,280 --> 00:41:18,300
Przepraszam. Mam słaby pęcherz.

414
00:41:47,010 --> 00:41:48,600
Nic jej nie będzie?

415
00:41:50,830 --> 00:41:55,620
Gdy pobił ją pierwszy raz,
poprzysiągłem straszną zemstę.

416
00:41:56,820 --> 00:42:01,190
Błagała, żebym mu darował,
i ustąpiłem dla dobra nas obojga.

417
00:42:01,760 --> 00:42:04,460
To było cztery lata temu.

418
00:42:06,860 --> 00:42:09,060
Kiedyś go zabiję.

419
00:42:10,500 --> 00:42:12,570
Potem niech się dzieje, co chce.

420
00:42:38,030 --> 00:42:40,480
Rany, mamo! Dostanę zawału!

421
00:42:40,570 --> 00:42:41,990
Przepraszam.

422
00:42:42,520 --> 00:42:45,110
Dlaczego siedzisz po ciemku?

423
00:42:47,330 --> 00:42:48,600
Tak sobie.

424
00:42:49,120 --> 00:42:50,700
Po prostu siedzę sobie.

425
00:42:51,610 --> 00:42:54,840
To poczytaj książkę. Wystraszyłaś mnie.

426
00:43:03,060 --> 00:43:04,010
Chodzi o tatę?

427
00:43:05,580 --> 00:43:07,380
Wiesz, że wróci.

428
00:43:07,460 --> 00:43:10,140
To tylko jakaś tajna sprawa.

429
00:43:11,190 --> 00:43:13,390
Tym razem nie mam pewności.

430
00:43:20,200 --> 00:43:26,060
Dwa tygodnie temu wpadłem
do baru i pogadałem z tatą.

431
00:43:27,290 --> 00:43:29,210
Śnił mu się...

432
00:43:30,720 --> 00:43:36,210
wielki dom z białego marmuru
i z mnóstwem świateł.

433
00:43:37,630 --> 00:43:41,130
Byliśmy tam obaj, tata i ja.

434
00:43:41,660 --> 00:43:45,840
Mówił o życiu w harmonii i radości,

435
00:43:46,360 --> 00:43:49,450
a potem o mojej przyszłości.

436
00:43:50,840 --> 00:43:52,340
Same dobre rzeczy.

437
00:43:54,930 --> 00:43:58,100
To było miłe.

438
00:43:59,400 --> 00:44:02,520
Twój ojciec
jest niezwykłym człowiekiem.

439
00:44:04,620 --> 00:44:07,610
Jest nieźle pokręcony...

440
00:44:08,890 --> 00:44:13,160
ale na pewno mądrzejszy
niż większość innych.

441
00:44:14,140 --> 00:44:21,170
Czasami, gdy śpię,
przeczesuje mi włosy palcami.

442
00:44:22,510 --> 00:44:24,920
Myśli, że o tym nie wiem.

443
00:44:36,380 --> 00:44:38,230
Długo mnie nie było?

444
00:44:38,990 --> 00:44:40,270
Dwa dni.

445
00:44:40,650 --> 00:44:42,440
Dziwne.

446
00:44:43,960 --> 00:44:45,650
Nie byłem pewien.

447
00:44:54,860 --> 00:44:57,870
Bobby, zgaś tego papierosa.

448
00:45:04,160 --> 00:45:07,960
I nalej mi drinka. Coś mocnego.

449
00:45:09,470 --> 00:45:10,710
Jasne.

450
00:45:18,230 --> 00:45:20,440
Wszystko będzie dobrze?

451
00:45:21,540 --> 00:45:25,520
Raczej nie, kochanie.

