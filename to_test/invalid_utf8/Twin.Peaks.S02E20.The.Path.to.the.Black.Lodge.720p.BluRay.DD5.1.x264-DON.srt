1
00:00:28,820 --> 00:00:33,750
MIASTECZKO TWIN PEAKS

2
00:02:38,170 --> 00:02:39,690
Rany...

3
00:02:40,380 --> 00:02:41,610
Rusty?

4
00:02:42,310 --> 00:02:44,270
Jak się nazywał?

5
00:02:45,960 --> 00:02:47,450
Tomasky.

6
00:02:48,620 --> 00:02:52,520
Wkręcił mnie do zespołu, jeżdżę z nimi.

7
00:02:52,600 --> 00:02:54,550
Co robicie w Twin Peaks?

8
00:02:55,540 --> 00:03:00,860
Mieliśmy grać w Knife River,
ale złapaliśmy gumę.

9
00:03:01,730 --> 00:03:02,610
I kogoś spotkaliście.

10
00:03:03,600 --> 00:03:09,340
Ten facet wyszedł z lasu,
jak jakaś Wielka Stopa.

11
00:03:11,290 --> 00:03:12,990
Był dziwnie ubrany.

12
00:03:13,080 --> 00:03:19,650
Pogadaliśmy, a potem powiedział,
że ma browar, gdybyśmy chcieli.

13
00:03:20,580 --> 00:03:25,960
No i Rusty z nim poszedł.
Więcej go nie widziałem.

14
00:03:27,490 --> 00:03:30,330
Musimy zawiadomić jego rodziców.

15
00:03:31,920 --> 00:03:33,970
Nienawidził ich.

16
00:03:34,800 --> 00:03:45,390
Mieszkał u wuja, w Moses Lake.
Mieliśmy jechać do Los Angeles...

17
00:04:02,380 --> 00:04:06,380
Zbił kolejnego piona,
ale nas o tym nie uprzedził.

18
00:04:07,200 --> 00:04:10,380
Earle zmienia zasady gry.

19
00:04:19,410 --> 00:04:23,060
- Dzień dobry.
- Czołem, szeryfie. Agencie...

20
00:04:23,140 --> 00:04:27,100
Hawk czeka w pana biurze
z kimś, kogo nie znam.

21
00:04:27,180 --> 00:04:30,780
Był bardzo przybity
i Hawk dał mu śniadanie.

22
00:04:32,980 --> 00:04:33,910
Andy?

23
00:04:39,710 --> 00:04:41,120
Słucham?

24
00:04:41,950 --> 00:04:44,420
Co wiesz o ratowaniu naszej planety?

25
00:04:44,680 --> 00:04:46,460
Wiem, że jest z nią kiepsko.

26
00:04:46,710 --> 00:04:50,630
Andy, biorę mój los w swoje ręce,

27
00:04:50,870 --> 00:04:54,580
bo nie mogę już znieść tej niepewności.

28
00:04:54,670 --> 00:04:58,140
Jutro jest Dzień T. Dzień Taty.

29
00:04:58,790 --> 00:05:04,140
Za 24 godziny wybiorę ojca
dla mojego dziecka.

30
00:05:04,230 --> 00:05:09,910
Posterunkowy Andy Brennan
czy ekspedient Richard Tremayne.

31
00:05:12,310 --> 00:05:15,740
Co ma do tego konkurs piękności?

32
00:05:17,750 --> 00:05:22,910
Przydadzą nam się pieniądze.
Ojciec zresztą też.

33
00:05:23,420 --> 00:05:27,150
Wygłoszę przemówienie
o ratowaniu planety.

34
00:05:27,240 --> 00:05:31,960
To wspaniały temat,
ale jeszcze nie wiem, co powiem.

35
00:05:34,210 --> 00:05:40,380
Muszą przestać wrzucać do jeziora
puszki po piwie, kiedy idą na ryby.

36
00:05:41,200 --> 00:05:42,270
Puszki...

37
00:05:44,400 --> 00:05:47,770
A styropian nie rozłoży się
do naszej śmierci.

38
00:05:49,550 --> 00:05:51,490
Naprawdę?

39
00:06:05,500 --> 00:06:08,350
- Audrey już jest?
- Nie.

40
00:06:08,430 --> 00:06:10,840
- Odzywała się?
- Nie.

41
00:06:11,770 --> 00:06:14,760
- Poślecie po moje bagaże?
- Oczywiście.

42
00:06:24,890 --> 00:06:27,210
Firmy ubezpieczeniowe
przysyłają tyle papierów,

43
00:06:27,290 --> 00:06:30,030
że od dźwigania można dostać zawału.

44
00:06:30,960 --> 00:06:32,570
Nic ci nie jest.

45
00:06:35,540 --> 00:06:38,030
Martwiłbym się raczej tym,
co nosisz w duszy.

46
00:06:40,350 --> 00:06:43,380
Masz powody, żeby mi nie wierzyć.

47
00:06:45,070 --> 00:06:49,770
Ale chcę dobra nas wszystkich.

48
00:06:49,850 --> 00:06:51,550
To daj spokój Eileen.

49
00:06:51,750 --> 00:06:53,350
Nie mogę.

50
00:06:54,050 --> 00:06:56,930
Póki żyję w kłamstwie.

51
00:06:57,420 --> 00:07:02,180
To nie takie proste. Wierzę ci...

52
00:07:03,110 --> 00:07:11,020
i cieszę się, że chcesz dobrze,
ale ty jesteś jak bomba zegarowa.

53
00:07:13,080 --> 00:07:16,530
Niszczenie czyjegoś życia
nie jest dobre.

54
00:07:16,610 --> 00:07:20,010
To złożony i trudny proces,

55
00:07:21,280 --> 00:07:22,960
ale nie mogę przestać.

56
00:07:25,030 --> 00:07:25,950
Przykro mi.

57
00:07:31,030 --> 00:07:33,300
Przepraszam. Jack.

58
00:07:34,270 --> 00:07:35,440
Wejdź.

59
00:07:36,250 --> 00:07:38,260
- Znasz Willa Haywarda?
- Tak.

60
00:07:38,820 --> 00:07:41,110
Ma pan uroczą córkę.

61
00:07:42,330 --> 00:07:45,140
Muszę wracać do pracy.

62
00:07:46,190 --> 00:07:49,700
Cieszę się, że pana widzę.

63
00:07:53,570 --> 00:07:56,830
Ben, bądź ostrożny.

64
00:08:02,590 --> 00:08:07,010
- Szukam Audrey.
- Wróci lada chwila.

65
00:08:07,870 --> 00:08:12,370
Wyjeżdżam. Już tankują mój samolot.

66
00:08:12,450 --> 00:08:17,120
Zadzwoń do niej, jak dolecisz tam,
dokąd tak się spieszysz.

67
00:08:17,930 --> 00:08:22,970
Mój przyjaciel, a nawet ktoś bliższy,
został zamordowany.

68
00:08:42,080 --> 00:08:43,860
Dziecko płci żeńskiej...

69
00:08:46,180 --> 00:08:48,680
Matka: Eileen Hayward...

70
00:08:49,670 --> 00:08:51,850
córka: Donna Marie...

71
00:08:53,480 --> 00:08:54,870
ojciec...

72
00:09:11,860 --> 00:09:14,860
- Donna, gdzie jesteś?
- O co chodzi?

73
00:09:14,940 --> 00:09:17,850
Dzwoni pan Hawk z policji.

74
00:09:17,930 --> 00:09:19,190
Już idę.

75
00:09:19,270 --> 00:09:21,510
Mówi, że to ważne.

76
00:09:36,230 --> 00:09:39,450
Mam dla pani kilka wiadomości.

77
00:09:39,530 --> 00:09:40,960
Wezmę to.

78
00:09:42,730 --> 00:09:43,960
Dziękuję.

79
00:09:46,140 --> 00:09:49,450
Agent Cooper
zaprasza cię na posterunek.

80
00:09:50,080 --> 00:09:52,790
Mam dużo pracy. Może po południu.

81
00:09:52,870 --> 00:09:54,680
Teraz, to pilne.

82
00:10:01,440 --> 00:10:06,160
A co z Ghostwood?
Jak cię znajdę? Potrzebuję czasu.

83
00:10:07,610 --> 00:10:11,240
Zginął odważny człowiek,
mój przyjaciel,

84
00:10:11,810 --> 00:10:13,920
i muszę go zastąpić.

85
00:10:14,000 --> 00:10:15,620
Już tu nie wrócisz?

86
00:10:19,000 --> 00:10:22,130
Niech szlag trafi lasy deszczowe!

87
00:10:22,220 --> 00:10:26,550
- Poradzisz sobie.
- Nie wiem. Jestem za słaby.

88
00:10:27,640 --> 00:10:30,720
Ben Horne za słaby? Nie wierzę.

89
00:10:31,050 --> 00:10:33,830
Czyń dobro?

90
00:10:33,920 --> 00:10:36,460
Wal najtrudniejszą prawdę?

91
00:10:36,550 --> 00:10:39,940
Staraj się. Tyle możesz.

92
00:10:43,460 --> 00:10:45,650
Szkoda, że nie ma Audrey.

93
00:10:50,480 --> 00:10:52,650
Dasz jej coś ode mnie?

94
00:10:54,970 --> 00:10:56,210
Powiedz...

95
00:10:58,240 --> 00:10:59,580
Po prostu jej to daj.

96
00:11:03,900 --> 00:11:05,180
Trzymaj się!

97
00:11:06,580 --> 00:11:11,200
Sprawdźcie, czy te symbole występują
w starożytnych kalendarzach rolniczych.

98
00:11:11,280 --> 00:11:14,290
- Szukajcie dziwnych słów.
- Tak jest.

99
00:11:17,820 --> 00:11:19,340
Dobra robota.

100
00:11:19,420 --> 00:11:20,610
Majorze.

101
00:11:20,690 --> 00:11:23,610
- Jak nam idzie?
- Jest pan skonany.

102
00:11:24,210 --> 00:11:27,910
Przez całą noc przeglądałem
akta projektu Błękitna Księga,

103
00:11:27,990 --> 00:11:31,490
szukając śladów Earle'a.
Teraz nie mogę spać.

104
00:11:31,820 --> 00:11:33,560
Co pan wygrzebał?

105
00:11:33,650 --> 00:11:36,650
Earle był najlepszy ze wszystkich.

106
00:11:36,730 --> 00:11:41,040
Ale gdy porzuciliśmy kosmos
na rzecz lasów wokół Twin Peaks,

107
00:11:41,120 --> 00:11:42,960
uległ fatalnej obsesji.

108
00:11:43,660 --> 00:11:47,550
Zazdrośnie strzegł zdobytych informacji,

109
00:11:47,630 --> 00:11:51,630
stał się samotnikiem,
wreszcie posunął się do przemocy.

110
00:11:51,710 --> 00:11:53,940
Odsunięto go od projektu.

111
00:11:54,020 --> 00:11:57,230
A to znalazłem w archiwach.

112
00:11:58,990 --> 00:12:03,670
Ci źli czarownicy,
dugpasi, jak ich nazywano,

113
00:12:03,750 --> 00:12:07,630
czcili zło dla niego samego.

114
00:12:07,720 --> 00:12:13,530
Wyrażali siebie w ciemności
i dla ciemności, bez wyraźnego motywu.

115
00:12:14,020 --> 00:12:18,610
Osiągnięcie czystości dawało im
wstęp do tajnego miejsca,

116
00:12:18,690 --> 00:12:23,180
gdzie kultywowano rytuały,
których nie sposób opisać,

117
00:12:23,260 --> 00:12:28,040
co miało wspomagać siłę zła.
To miejsce, gdzie rządzi zło,

118
00:12:28,120 --> 00:12:31,350
jest rzeczywiste
i dlatego można je znaleźć,

119
00:12:31,430 --> 00:12:35,010
wejść tam
i być może jakoś wykorzystać.

120
00:12:35,090 --> 00:12:41,020
Różnie je określali, ale najczęściej
używali nazwy Czarna Chata.

121
00:12:43,030 --> 00:12:47,310
Nie wierzycie mi? Myślicie,
że mi odbiło z przepracowania.

122
00:12:48,620 --> 00:12:49,860
Spadajcie.

123
00:12:57,170 --> 00:13:00,950
Panowie... gdy Windom Earle
pojawił się w Twin Peaks,

124
00:13:01,030 --> 00:13:06,880
sądziłem, że chce się na mnie
zemścić, ale byłem w błędzie.

125
00:13:07,600 --> 00:13:13,430
Ingeruje w życie bliskich mi osób
i morduje niewinnych ludzi.

126
00:13:13,520 --> 00:13:17,010
Zaangażował nas w śledztwo,
które miało być zasłoną dymną,

127
00:13:17,100 --> 00:13:21,830
co niespecjalnie lubię.
Te wszystkie działania to kamuflaż.

128
00:13:21,910 --> 00:13:27,110
Od początku chodziło mu
o coś innego – o Czarną Chatę.

129
00:13:28,040 --> 00:13:34,200
Musimy się dowiedzieć,
jakie ona ma tu znaczenie.

130
00:13:36,540 --> 00:13:38,580
Sądzisz, że to ma związek?

131
00:13:40,720 --> 00:13:42,000
On tak uważa.

132
00:13:50,250 --> 00:13:54,570
Musimy przejrzeć te akta
od deski do deski. Cappy?

133
00:13:54,650 --> 00:14:00,220
Wykop wszystko o tych dugpasach.
A pan musi się wyspać.

134
00:14:00,300 --> 00:14:04,360
Rozprostuję nogi. Przechadzka
po lesie oczyści mój umysł.

135
00:14:04,450 --> 00:14:07,610
- Tylko niech pan nie zabłądzi.
- Racja.

136
00:14:08,940 --> 00:14:10,060
Harry...

137
00:14:11,500 --> 00:14:12,980
do dzieła.

138
00:14:17,010 --> 00:14:21,160
Wiesz, Leo, że jedyne,
co odkrył Kolumb, to to, że zabłądził?

139
00:14:22,620 --> 00:14:25,670
Cooper z kolegami wciąż tego nie łapią.

140
00:14:26,620 --> 00:14:30,300
Wiem coś, o czym oni nie wiedzą.

141
00:14:30,380 --> 00:14:33,350
A ja wiem o czymś,
co Dale zaledwie podejrzewa.

142
00:14:34,590 --> 00:14:36,000
Projekt Błękitna Księga.

143
00:14:36,080 --> 00:14:39,340
Małe ludziki, głuptasy pełne optymizmu.

144
00:14:39,430 --> 00:14:43,350
Próbowali mnie zniechęcić,
a skutek był przeciwny.

145
00:14:43,430 --> 00:14:47,220
Zadałbym majorowi Briggsowi
parę pytań.

146
00:14:47,310 --> 00:14:51,560
Przepolerowałbym mu łysinę,
a nuż coś zaświta.

147
00:14:56,300 --> 00:15:00,770
Może i my rozprostujemy nogi?

148
00:15:00,850 --> 00:15:04,030
Urządzimy spotkanie
grupy z Błękitnej Księgi.

149
00:15:04,110 --> 00:15:09,330
Winko, ser, pogaduszki, anegdotki
z dawnych, dobrych czasów.

150
00:15:10,070 --> 00:15:14,210
Ciekawe, jak mógłbym ubarwić
nudny żywot Briggsa.

151
00:15:15,820 --> 00:15:20,180
Nie stercz jak słup. Gdybyś stał
na dworze, obsrałyby cię gołębie.

152
00:15:33,060 --> 00:15:35,490
"I tylko w ten sposób
możemy uratować drzewa.

153
00:15:35,570 --> 00:15:38,810
Trzeba skopać kilka tyłków
w obronie środowiska,

154
00:15:38,900 --> 00:15:41,460
zanim będzie za późno".

155
00:15:43,840 --> 00:15:45,950
Pięknie. Doskonale.

156
00:15:47,960 --> 00:15:50,520
Nie jestem pewien tych tyłków.

157
00:15:50,600 --> 00:15:57,090
Może lepiej będzie: "Stanąć
i walczyć w obronie środowiska"?

158
00:15:57,170 --> 00:15:57,800
Dużo lepiej.

159
00:16:00,110 --> 00:16:02,220
Zastanawiałem się...

160
00:16:02,730 --> 00:16:06,270
Tak, wiem.
Przemowa liczy się podwójnie.

161
00:16:06,350 --> 00:16:10,060
Nie o wyborach. Myślałem o nas.

162
00:16:14,710 --> 00:16:19,100
Wiem, że ostatnio
nie poświęcałem ci dość uwagi.

163
00:16:19,720 --> 00:16:24,990
Mam pracę u pana Horne'a,
chodzę w garniturze

164
00:16:25,080 --> 00:16:28,700
i nagle zacząłem
strasznie zadzierać nosa.

165
00:16:30,500 --> 00:16:31,540
Byłeś okropny.

166
00:16:37,770 --> 00:16:39,460
Brakuje mi ciebie.

167
00:16:41,240 --> 00:16:43,890
I tego, co razem robiliśmy.

168
00:16:46,590 --> 00:16:49,130
Gdy zobaczyłem, jak całujesz się
z tamtym facetem,

169
00:16:50,400 --> 00:16:55,820
coś we mnie pękło.
Jakby mózg mi się przewrócił

170
00:16:56,540 --> 00:17:02,450
i wreszcie zobaczyłem,
co jest dla mnie naprawdę ważne.

171
00:17:04,910 --> 00:17:06,600
Kocham cię...

172
00:17:08,340 --> 00:17:11,050
i chcę z tobą spędzać czas.

173
00:17:11,130 --> 00:17:16,840
Jeśli tylko tego chcesz,
to bardzo mi zależy...

174
00:17:20,810 --> 00:17:22,300
A mnie to nie?

175
00:17:31,550 --> 00:17:34,780
Szczęściarz ze mnie.

176
00:17:37,050 --> 00:17:42,510
Shelly, do ciebie! To agent Cooper.
Chce z tobą rozmawiać.

177
00:17:42,590 --> 00:17:45,310
Chodź, mówi, że to ważne!

178
00:18:04,680 --> 00:18:07,910
- Mam wspaniałe wieści.
- Jakie?

179
00:18:08,750 --> 00:18:13,600
To Norma Jennings i Richard Tremayne.

180
00:18:13,680 --> 00:18:16,890
Tremayne będzie trzecim sędzią.
Masz to jak w banku.

181
00:18:16,980 --> 00:18:21,230
Muszę mieć pewność. Kochanie,
ja chcę wiedzieć, że wygram.

182
00:18:21,320 --> 00:18:27,430
Mój Boże, on jest
z Wielkiej Brytanii albo z Bahamów.

183
00:18:27,520 --> 00:18:29,770
Ulegnie twojemu urokowi.

184
00:18:29,850 --> 00:18:35,430
Urządzimy to tak,
żebyście zostali sami.

185
00:18:36,010 --> 00:18:39,430
Włożysz suknię z rozcięciem...

186
00:18:40,220 --> 00:18:43,600
Takim jak stąd do Seattle.

187
00:18:45,390 --> 00:18:48,850
Potem nachylisz się ku niemu,

188
00:18:48,930 --> 00:18:52,520
a gdy zobaczy te wszystkie cuda

189
00:18:52,600 --> 00:18:57,080
i poczuje upojny zapach
francuskich perfum...

190
00:18:57,170 --> 00:18:58,590
i...

191
00:18:59,710 --> 00:19:05,670
utonie w głębinie
tych brązowych oczu...

192
00:19:09,830 --> 00:19:15,910
Dosyć! Przestań, ty lisico!
Dłużej tego nie zniosę.

193
00:19:15,990 --> 00:19:18,750
Weźmy cichy ślub.

194
00:19:19,090 --> 00:19:22,500
Dopiero kiedy wygram.

195
00:19:22,580 --> 00:19:26,550
Tylko wtedy za ciebie wyjdę.

196
00:19:29,310 --> 00:19:34,490
Jesteś taka sroga. Zaraz oszaleję.

197
00:19:36,010 --> 00:19:37,700
Chodź do mnie.

198
00:19:43,340 --> 00:19:46,240
Każda dostała fragment tego wiersza,

199
00:19:46,330 --> 00:19:49,670
w niezwykły sposób
i razem z zaproszeniem.

200
00:19:49,750 --> 00:19:52,430
Poszłyśmy tam,
ale nikt się nie pokazał.

201
00:19:53,960 --> 00:19:57,440
Czy ostatnio spotkałyście kogoś obcego?

202
00:19:57,520 --> 00:20:02,930
Kto wydał się wam dziwny,
sympatyczny albo zagadkowy?

203
00:20:04,360 --> 00:20:07,720
Jakiś mężczyzna
podawał się za przyjaciela ojca.

204
00:20:08,590 --> 00:20:13,380
Tego samego dnia ktoś dał mi
10 dolarów napiwku. To dziwne.

205
00:20:14,350 --> 00:20:18,660
Starszy pan w bibliotece
prosił o przeczytanie wiersza.

206
00:20:18,750 --> 00:20:20,250
A więc wszystkie.

207
00:20:21,440 --> 00:20:25,940
Shelly, przyjrzyj się.
Poznajesz to pismo?

208
00:20:30,280 --> 00:20:31,990
To Leo...

209
00:20:33,740 --> 00:20:39,380
Każda z was ma się dwa razy dziennie
kontaktować z szeryfem.

210
00:20:39,470 --> 00:20:43,110
O dziewiątej rano i wieczorem.

211
00:20:43,190 --> 00:20:46,470
Rodzice zawsze muszą wiedzieć,
gdzie jesteście.

212
00:20:47,380 --> 00:20:53,490
W drodze do szkoły czy do pracy,
nie możecie być same.

213
00:20:54,860 --> 00:21:00,320
Proszę, bądźcie bardzo ostrożne.
Grozi wam niebezpieczeństwo.

214
00:21:02,030 --> 00:21:03,680
Grozi nam wszystkim.

215
00:21:15,640 --> 00:21:21,140
Spodobaliby ci się dugpasi.
Dawni czarownicy wielbiący zło.

216
00:21:22,350 --> 00:21:27,370
Przypominali wyznawców Kali z Indii.
Tamci też byli nieźle zakręceni.

217
00:21:27,450 --> 00:21:29,920
Krew na śniadanie, krew na obiad...

218
00:21:32,530 --> 00:21:34,430
Stare, dobre czasy.

219
00:21:35,330 --> 00:21:38,100
- Shelly.
- Brawo, Leo.

220
00:21:38,530 --> 00:21:40,100
Pamiętasz.

221
00:21:40,680 --> 00:21:42,560
Obmierzła wywłoka.

222
00:21:44,340 --> 00:21:48,840
Tak między nami, twoja żonka
ma dość nowoczesne poglądy

223
00:21:48,930 --> 00:21:52,900
na wierność małżeńską. Ale to nic.

224
00:21:52,980 --> 00:21:54,910
Nadaje się na królową.

225
00:21:55,970 --> 00:21:59,290
Będziesz ją opłakiwał? Wątpię.

226
00:22:01,750 --> 00:22:07,510
Jej śmierć, Leo. Nagłe odejście.

227
00:22:08,200 --> 00:22:10,130
To bardzo proste.

228
00:22:10,210 --> 00:22:13,130
Jeśli zostanie Miss, zginie.

229
00:22:13,660 --> 00:22:15,240
Nie chciałbyś pomóc?

230
00:22:16,730 --> 00:22:17,180
Nie.

231
00:22:19,740 --> 00:22:22,180
Mój Boże, skąd to masz?

232
00:22:24,640 --> 00:22:29,170
Nie, Leo! Błagam, nie rób tego!
Zlituj się!

233
00:22:29,250 --> 00:22:30,230
Kocham Shelly!

234
00:22:53,510 --> 00:22:54,840
Biedny Leo.

235
00:22:56,820 --> 00:22:59,690
Miłość wszystkich ogłupia.

236
00:22:59,770 --> 00:23:04,930
Ale ty, jak ja,
poznasz wartość nienawiści.

237
00:23:32,010 --> 00:23:33,770
- Cześć, tato.
- Audrey!

238
00:23:35,110 --> 00:23:36,420
Witaj w domu.

239
00:23:38,250 --> 00:23:40,880
Wróciłam rano,
ale wezwał mnie agent Cooper.

240
00:23:40,970 --> 00:23:44,740
- Coś się stało?
- Jakiś świr przysłał mi wiersz.

241
00:23:45,660 --> 00:23:48,800
Dzwoniłam do Jacka. Wiesz, gdzie jest?

242
00:23:49,620 --> 00:23:54,590
Kiedy cię nie było,
wpadłem na fantastyczny pomysł.

243
00:23:54,680 --> 00:23:56,070
Jaki?

244
00:23:56,330 --> 00:24:00,950
Akcja obrony Ghostwood
potrzebuje rzecznika.

245
00:24:01,790 --> 00:24:08,160
Kogoś rozpoznawalnego,
kto zmobilizuje ludzi do działania.

246
00:24:09,180 --> 00:24:10,960
Gdzie jest Jack?

247
00:24:11,860 --> 00:24:13,930
Dasz radę.

248
00:24:15,130 --> 00:24:19,030
Jako Miss Twin Peaks i nasz rzecznik,

249
00:24:19,110 --> 00:24:24,220
mogłabyś poprowadzić publiczną debatę.

250
00:24:24,300 --> 00:24:26,820
Potrafisz.

251
00:24:26,900 --> 00:24:29,560
Tylko ty możesz ich przekonać.

252
00:24:30,070 --> 00:24:35,360
Tato... gdzie jest John Justice Wheeler?

253
00:24:38,140 --> 00:24:40,410
- Wyjechał.
- Dokąd?

254
00:24:43,860 --> 00:24:48,080
Musi zażegnać skutki
jakiejś tragedii w Brazylii.

255
00:24:49,600 --> 00:24:53,410
Zostawił dla ciebie list. Jest tutaj.

256
00:24:53,490 --> 00:24:54,590
Tato...

257
00:24:57,440 --> 00:25:00,670
- kiedy wyjechał?
- Parę minut temu, na lotnisko.

258
00:25:02,800 --> 00:25:06,310
- A co z wyborami miss?
- Pogadamy później, obiecuję.

259
00:25:13,680 --> 00:25:16,470
Josie, widzę twoją twarz...

260
00:25:16,560 --> 00:25:17,730
Randy!

261
00:25:20,340 --> 00:25:24,120
Nieważne. Ma pan samochód?

262
00:25:24,200 --> 00:25:27,920
- Jeździ jak marzenie.
- Świetnie, chodźmy.

263
00:25:32,030 --> 00:25:33,030
Szybciej!

264
00:25:38,050 --> 00:25:41,100
To symbol czasu.

265
00:25:42,160 --> 00:25:45,540
- To jasne.
- Czas na co?

266
00:25:46,180 --> 00:25:50,750
Nie wiem. Z początku myślałem,
że to jakiś szyld albo zaproszenie.

267
00:25:50,830 --> 00:25:52,300
Albo obydwa.

268
00:25:54,320 --> 00:25:55,660
A gdzie major?

269
00:25:56,700 --> 00:25:58,450
Powinien już być.

270
00:25:58,530 --> 00:26:02,250
Andy, zadzwoń do pani Briggs,
czy major jest w domu.

271
00:26:14,050 --> 00:26:18,840
- Co ci jest?
- Myślę o Annie Blackburn.

272
00:26:20,690 --> 00:26:23,380
Nie przypuszczałem,
że taki z ciebie marzyciel.

273
00:26:23,640 --> 00:26:26,370
Od rana dziwnie się czuję.

274
00:26:27,390 --> 00:26:31,500
Pracuję normalnie, jestem skupiony...

275
00:26:32,320 --> 00:26:37,350
a tu nagle, znienacka...
widzę ją i słyszę.

276
00:26:39,000 --> 00:26:45,920
Oczywiście staram się wziąć w karby,
ale ten obraz pozostaje.

277
00:26:46,940 --> 00:26:48,410
Czasami aż kręci mi się w głowie.

278
00:26:49,720 --> 00:26:52,020
To normalne u zakochanych.

279
00:26:52,110 --> 00:26:57,240
Objawy wskazywałyby na malarię,
ale nigdy nie czułem się lepiej.

280
00:26:59,380 --> 00:27:06,010
- Przepraszam, plotę bzdury.
- Wcale nie, szczęściarzu.

281
00:27:57,780 --> 00:27:59,940
Czołem, Wilbur!

282
00:28:18,240 --> 00:28:19,750
Kopę lat, Briggsy!

283
00:28:26,880 --> 00:28:30,630
Leo! Wreszcie znalazłeś
swoje powołanie!

284
00:28:55,130 --> 00:28:58,750
- Nie zaszkodzi spróbować.
- Może za rok.

285
00:28:59,480 --> 00:29:02,850
Cały świat czeka. Przeskocz tam.

286
00:29:02,930 --> 00:29:07,210
- Usłysz i zobacz drugą stronę.
- Święty Augustyn.

287
00:29:07,300 --> 00:29:09,100
- Kawy?
- Jasne.

288
00:29:23,590 --> 00:29:27,270
- Coś cię trapi.
- Tak.

289
00:29:27,360 --> 00:29:28,940
Chcesz pogadać?

290
00:29:31,840 --> 00:29:36,950
Prowadzę skomplikowane dochodzenie,
wymagające pełnej uwagi,

291
00:29:37,030 --> 00:29:39,690
a przez większość czasu myślę o tobie.

292
00:29:41,180 --> 00:29:46,060
Skądś to znam. Od rana
widzę cię w sadzonych jajkach.

293
00:29:48,850 --> 00:29:53,460
Niektórzy twierdzą, że to można
wyjaśnić naukowo: chemią.

294
00:29:53,960 --> 00:29:55,300
To prawda?

295
00:29:56,230 --> 00:29:57,400
Nie wiem.

296
00:29:58,090 --> 00:30:01,470
Nie potrafię tego ogarnąć.

297
00:30:03,930 --> 00:30:06,910
Próbowałam to ogarnąć przez pięć lat.

298
00:30:10,360 --> 00:30:11,780
Nie jest mi łatwo,

299
00:30:12,510 --> 00:30:16,430
ale wierzę w ciebie i w nas.
Bo to rozumiem.

300
00:30:16,520 --> 00:30:20,060
- Jesteśmy podobni.
- To pomaga.

301
00:30:20,140 --> 00:30:21,870
Za dużo myślimy.

302
00:30:23,080 --> 00:30:28,510
Nie postrzegamy natury jako takiej,
a ujętą w naszą siatkę pojęciową.

303
00:30:30,600 --> 00:30:32,060
Heisenberg.

304
00:30:36,740 --> 00:30:38,450
Pójdziemy na kręgle?

305
00:30:39,340 --> 00:30:41,020
A potańczyć?

306
00:30:41,100 --> 00:30:44,510
- Nie umiem.
- Nauczę cię, to łatwe.

307
00:30:44,600 --> 00:30:47,570
- Kiedy?
- Wieczorem.

308
00:31:17,790 --> 00:31:19,380
Jest! Gazu!

309
00:31:27,380 --> 00:31:29,550
Jack! Stój!

310
00:31:38,980 --> 00:31:41,190
Zwykłe "do widzenia" tu nie wystarczy.

311
00:31:52,800 --> 00:31:54,460
Kocham cię.

312
00:31:55,130 --> 00:31:57,010
Ja ciebie też.

313
00:31:59,490 --> 00:32:02,690
- Musisz lecieć?
- Tak.

314
00:32:03,620 --> 00:32:04,770
Kiedy wrócisz?

315
00:32:05,970 --> 00:32:07,520
Nie wiem.

316
00:32:08,990 --> 00:32:10,230
Jestem dziewicą.

317
00:32:12,060 --> 00:32:12,860
Co?

318
00:32:14,230 --> 00:32:16,400
Chcę się z tobą kochać.

319
00:32:18,150 --> 00:32:19,450
Tu i teraz?

320
00:32:20,700 --> 00:32:22,700
To twój samolot.

321
00:32:24,650 --> 00:32:26,110
Dzięki Bogu.

322
00:32:57,190 --> 00:33:01,810
Kiedy pierwszy raz zobaczyłeś
symbol w Sowiej Jaskini?

323
00:33:01,900 --> 00:33:05,030
Nie mogę ujawnić tej informacji.

324
00:33:06,150 --> 00:33:07,980
Kłamczuszek.

325
00:33:13,610 --> 00:33:16,630
Co oznacza ten obraz?

326
00:33:17,950 --> 00:33:21,720
Nie mogę ujawnić tej informacji.

327
00:33:23,950 --> 00:33:27,060
Uwielbiam wojskowych.

328
00:33:34,580 --> 00:33:38,710
Nazwa stolicy Karoliny Północnej?

329
00:33:38,790 --> 00:33:40,220
Raleigh.

330
00:33:42,600 --> 00:33:44,850
Dużo mi to da.

331
00:33:45,950 --> 00:33:50,080
Majorze, podziwiam pana niezłomność.

332
00:33:51,360 --> 00:33:56,000
Gdybym miał czas,
bawilibyśmy się w nieskończoność...

333
00:33:56,990 --> 00:33:58,350
ale...

334
00:34:00,280 --> 00:34:02,090
jestem niecierpliwy.

335
00:34:08,760 --> 00:34:09,810
Aport!

336
00:34:16,860 --> 00:34:20,030
- Nazwisko?
- Garland Briggs.

337
00:34:20,110 --> 00:34:24,950
Garlandzie, czego najbardziej się boisz?

338
00:34:25,850 --> 00:34:29,370
Ewentualności, że miłość nie wystarczy.

339
00:34:30,750 --> 00:34:33,010
Przestań, bo się rozbeczę.

340
00:34:33,770 --> 00:34:35,750
Ile waży twoja żona?

341
00:34:36,670 --> 00:34:38,930
52 kilogramy.

342
00:34:39,530 --> 00:34:41,250
Doskonale!

343
00:34:42,310 --> 00:34:46,000
To kiedy pierwszy raz widziałeś
ryty w Sowiej Jaskini?

344
00:34:48,320 --> 00:34:49,550
W snach.

345
00:34:50,680 --> 00:34:52,140
Co to za sny?

346
00:34:53,360 --> 00:34:57,400
Byłem na rybach z agentem Cooperem.

347
00:34:58,480 --> 00:35:02,300
Rozbłysło światło,
z nim przyszedł strażnik.

348
00:35:03,300 --> 00:35:07,030
Zostałem zabrany, a mój umysł...

349
00:35:08,180 --> 00:35:10,120
Ale poznaję symbole.

350
00:35:11,040 --> 00:35:14,040
Co oznaczają?

351
00:35:14,800 --> 00:35:16,750
W czasie, gdy...

352
00:35:16,830 --> 00:35:22,670
Jowisz spotka się z Saturnem,
zostaniesz zabrany.

353
00:35:32,340 --> 00:35:34,310
Garlandzie...

354
00:35:36,610 --> 00:35:39,960
liczyłem na dłuższą rozmowę.

355
00:35:40,750 --> 00:35:43,730
Leo, połóż majora do łóżka.

356
00:35:44,420 --> 00:35:46,670
Obawiam się, że odpłynął.

357
00:35:47,910 --> 00:35:53,490
Kiedy Jowisz spotka się z Saturnem...

358
00:35:59,420 --> 00:36:04,920
Jego pracownica, ta Jones,
przyniosła to zaraz po jego śmierci.

359
00:36:05,000 --> 00:36:11,510
Spadek po Eckhardtcie. Jakiś dowcip
albo coś cennego. Może tak?

360
00:36:13,170 --> 00:36:18,760
Nie. Każde z tych pudełek
to łamigłówka.

361
00:36:20,740 --> 00:36:26,280
To mnie doprowadza do szału.
Powiedz, że masz dobre wieści.

362
00:36:27,250 --> 00:36:32,360
Nasi inwestorzy aż się ślinią
od Paryża po Pekin.

363
00:36:32,450 --> 00:36:36,370
Będziesz miała swoje pole golfowe.
Obok hotelu Great Northern.

364
00:36:36,700 --> 00:36:39,710
Benjamin staje na głowie,
żeby nam przeszkodzić.

365
00:36:39,800 --> 00:36:40,790
Za późno.

366
00:36:41,070 --> 00:36:44,750
Kiedy jeszcze sam to planował,
usunął wszelkie przeszkody.

367
00:36:45,090 --> 00:36:48,380
Strefa ochronna, wpływ na środowisko...

368
00:36:48,470 --> 00:36:53,830
Był i jest swoim najgorszym wrogiem.

369
00:36:53,910 --> 00:36:59,890
- Jak zawsze. Co robisz?
- Nic. Wypróbuję kilka możliwości.

370
00:36:59,970 --> 00:37:02,350
- Daj mi...
- Nie!

371
00:37:04,740 --> 00:37:06,460
Urodziny Eckhardta...

372
00:37:11,940 --> 00:37:13,180
Moje...

373
00:37:18,190 --> 00:37:19,950
Dzień wręczenia prezentu...

374
00:37:25,370 --> 00:37:26,570
Dawaj!

375
00:37:31,550 --> 00:37:34,640
- Co za nudy!
- Otworzę to!

376
00:38:49,040 --> 00:38:51,290
Nie jestem na to gotowa.

377
00:38:51,380 --> 00:38:54,350
Pomyśl o tym jak o spacerze.

378
00:38:54,970 --> 00:38:58,950
Dwie osoby idą jak jedna.

379
00:39:00,690 --> 00:39:03,490
- Nie jest źle.
- Idź ze mną.

380
00:39:04,450 --> 00:39:06,740
- Pójdziemy razem.
- Dobrze.

381
00:39:11,590 --> 00:39:15,270
Mam to w genach.
Rodzice tańczyli boogie.

382
00:39:19,820 --> 00:39:21,200
Czujesz to.

383
00:39:27,840 --> 00:39:30,150
Nasze ciała ze sobą współgrają.

384
00:39:32,260 --> 00:39:34,680
Pomyślałam to samo.

385
00:39:34,760 --> 00:39:36,830
Jestem zbyt śmiały?

386
00:39:39,340 --> 00:39:42,590
- Chcę więcej niż pocałunki.
- Na przykład?

387
00:39:42,680 --> 00:39:43,970
- Chcę...
- Czy to działa?

388
00:39:46,240 --> 00:39:51,680
Witamy na finałowym wieczorze
naszego festynu

389
00:39:51,760 --> 00:39:57,000
i na wyborach Miss Twin Peaks...

390
00:40:01,010 --> 00:40:04,240
Czy to diabelstwo działa?

391
00:40:05,590 --> 00:40:12,520
Rozumiem, dlaczego się wahasz
i jesteś taki ostrożny.

392
00:40:13,170 --> 00:40:16,220
Zakonnica to dla wielu
bezradna kobieta,

393
00:40:16,300 --> 00:40:21,800
bojąca się wszelkich wzruszeń,
umartwiona i rozmodlona.

394
00:40:22,840 --> 00:40:28,230
Ale gdy mnie obejmujesz i całujesz...

395
00:40:29,110 --> 00:40:32,680
jestem bezpieczna i śmiała.

396
00:40:33,780 --> 00:40:40,760
Nie boję się tego,
co przy tobie czuję i czego chcę.

397
00:40:46,690 --> 00:40:48,830
To działa?

398
00:40:50,870 --> 00:40:55,810
- Zaczyna mnie to złościć.
- Dlaczego? Jest uroczy.

399
00:40:55,900 --> 00:40:57,210
Tak myślisz?

400
00:40:59,150 --> 00:41:01,130
Może jednak się zgłoszę.

401
00:41:02,320 --> 00:41:05,300
- Do konkursu?
- Czemu nie.

402
00:41:05,380 --> 00:41:11,230
Zajrzeć na tamtą stronę.
Można zacząć dużo gorzej.

403
00:41:11,780 --> 00:41:15,490
- To jak bajka.
- A ty jesteś królową.

404
00:42:19,320 --> 00:42:20,380
Co za dno.

405
00:42:20,700 --> 00:42:26,050
Wreszcie poznałam faceta moich marzeń,
a on musi lecieć do Brazylii.

406
00:42:27,240 --> 00:42:30,720
Przecież wróci. Obiecał.

407
00:42:31,160 --> 00:42:32,320
Tak...

408
00:42:33,740 --> 00:42:37,310
Obiecał mi też wycieczkę na ryby.

409
00:42:38,990 --> 00:42:40,360
Miłość jest do chrzanu.

410
00:42:41,430 --> 00:42:43,150
Na ryby?

411
00:42:43,840 --> 00:42:45,820
Nad Pearl Lake.

412
00:42:47,350 --> 00:42:49,740
Mam wędki w samochodzie.

413
00:42:51,140 --> 00:42:52,370
Tak?

414
00:42:56,620 --> 00:42:59,590
Są różne leki na złamane serce...

415
00:43:00,830 --> 00:43:05,340
ale nic nie dorówna
pstrągom skaczącym do księżyca.

416
00:43:08,840 --> 00:43:10,430
Jedziemy?

417
00:43:14,820 --> 00:43:16,390
Jasne.

418
00:43:27,860 --> 00:43:34,500
Jowisz się z Saturnem schodzi
i życie nam osłodzi...

419
00:43:36,910 --> 00:43:42,830
Planety, o których bredzi major,
to nie tylko odległe kule, a zegar.

420
00:43:42,920 --> 00:43:44,590
Zegar wskazujący czas.

421
00:43:44,960 --> 00:43:49,840
A czas... zbliża nas do tego
z każdym tyknięciem.

422
00:43:49,920 --> 00:43:53,760
Dale się nie mylił,
to jest zaproszenie i mówi,

423
00:43:53,840 --> 00:43:59,150
kiedy wszystko się zacznie.
Ale Dale nie wie, gdzie szukać.

424
00:44:01,020 --> 00:44:05,610
Bo ten rysunek na skale to nie tylko
zaproszenie, ale i mapa!

425
00:44:05,690 --> 00:44:07,900
Wskazuje Czarną Chatę!

426
00:44:24,750 --> 00:44:26,420
Coś jest nie tak.

427
00:44:27,500 --> 00:44:30,180
Dzieje się coś złego.

