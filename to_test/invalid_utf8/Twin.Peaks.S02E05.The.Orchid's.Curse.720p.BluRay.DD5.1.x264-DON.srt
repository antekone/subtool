1
00:01:55,280 --> 00:01:59,270
Dopasowanie: Bonaducci

2
00:01:59,280 --> 00:02:02,910
Diane, jest godzina 6:42.

3
00:02:03,450 --> 00:02:06,410
...w przybliżeniu.

4
00:02:06,580 --> 00:02:10,580
Śniło mi się, że żuję dużą,
niesmaczną gumę do żucia.

5
00:02:10,750 --> 00:02:16,840
Po przebudzeniu dotarło do mnie,
że gryzę swoją zatyczkę do ucha.

6
00:02:17,010 --> 00:02:19,970
Stąd ten nieprzyjemny smak.

7
00:02:20,140 --> 00:02:25,020
Chyba nie powinienem
pić wieczorem tyle kawy.

8
00:02:27,430 --> 00:02:30,400
Ból nadal mi dokucza,

9
00:02:30,560 --> 00:02:34,570
dlatego codziennie aplikuję sobie
dodatkowe 15 minut jogi.

10
00:02:34,730 --> 00:02:37,690
Na szczęście ćwiczenia
przynoszą ulgę,

11
00:02:37,860 --> 00:02:42,990
spychając ból w odległe
rejony świadomości.

12
00:02:45,160 --> 00:02:50,000
Dzisiaj zacznę
od stania na głowie.

13
00:02:56,630 --> 00:03:00,130
Diane, stoję na głowie.

14
00:03:02,890 --> 00:03:05,850
Mój umysł staje się chłonny.

15
00:03:06,010 --> 00:03:09,350
Zaczynam ogarniać dzisiejsze zadania.

16
00:03:09,390 --> 00:03:12,850
Wszystko się wyostrza.

17
00:03:28,950 --> 00:03:33,420
Olbrzym miał rację.
Zapomniałem o czymś.

18
00:03:37,300 --> 00:03:40,670
"Pojechałam na północ.
Może Jack da odpowiedź."

19
00:03:40,720 --> 00:03:43,800
"Pańska Audrey."

20
00:04:01,280 --> 00:04:04,240
Już myśleliśmy,
że zaginąłeś w lasach Kalispell.

21
00:04:04,410 --> 00:04:08,410
W sąsiedztwie Palmerów mieszkały
dwie emerytowane nauczycielki.

22
00:04:08,580 --> 00:04:11,540
Nie pamiętają żadnego
siwowłosego faceta.

23
00:04:11,710 --> 00:04:15,710
Wypiłem trzy filiżanki rumianku,
żeby się tego dowiedzieć.

24
00:04:15,880 --> 00:04:18,840
Wybaczysz na moment?

25
00:04:19,000 --> 00:04:21,970
Szukaj dalej jednorękiego,
musimy go mieć.

26
00:04:22,130 --> 00:04:25,090
Szeryfie, chciałam się pożegnać.

27
00:04:25,260 --> 00:04:29,260
Wybieram się na dwa dni do Tacomy
odwiedzić moją siostrę Gwen,

28
00:04:29,430 --> 00:04:33,440
jej męża Larry'ego i dziecko,
które urodziło mu się tydzień temu.

29
00:04:33,600 --> 00:04:37,610
To znaczy Gwen je urodziła,
Larry jest ojcem.

30
00:04:37,770 --> 00:04:40,730
Pamięta pan, to chłopiec...

31
00:04:40,900 --> 00:04:43,860
Kiedy przyjdzie dziewczyna do pomocy,
nauczę ją obsługiwać centralkę...

32
00:04:44,030 --> 00:04:46,990
- ... ekspres do kawy i...
- Nie trzeba.

33
00:04:47,160 --> 00:04:49,080
- Ale...
- Damy sobie radę.

34
00:04:49,620 --> 00:04:54,290
Już powinna tu być!
Mogę zostać i pokazać jej jak...

35
00:04:54,460 --> 00:04:58,080
Nie martw się - jedź.

36
00:04:58,630 --> 00:05:03,510
Czasem dziewczyny do pomocy
nie potrafią pomóc.

37
00:05:03,840 --> 00:05:07,930
- Szeryfie...
- Damy sobie radę.

38
00:05:09,050 --> 00:05:13,060
- Bezpiecznej podróży, Lucy.
- Dziękuję...

39
00:05:13,220 --> 00:05:17,230
- Cześć, Coop.
- Mamy jakieś wskazówki?

40
00:05:17,400 --> 00:05:22,610
Nie są nam potrzebne.
Już wiem, gdzie jest Audrey.

41
00:05:26,780 --> 00:05:32,870
To urządzenie umożliwi swobodne
przemieszczanie pana Johnsona.

42
00:05:33,040 --> 00:05:37,040
Pozwoli mu także
wydostać się na zewnątrz...

43
00:05:37,210 --> 00:05:40,170
gdzie będzie mógł...
kontemplować piękno przyrody.

44
00:05:40,340 --> 00:05:45,380
Panie Pinkle, mój kuzyn Leo
ma w pełni korzystać z życia.

45
00:05:45,550 --> 00:05:50,600
Właśnie po to tu jestem.
Spróbujmy jeszcze raz...

46
00:05:50,760 --> 00:05:54,770
Nie zamierza pan zamontować rampy?

47
00:05:54,930 --> 00:05:57,270
Tak, oczywiście.

48
00:05:57,310 --> 00:06:03,320
Już nawet przygotowałem deski.

49
00:06:03,360 --> 00:06:05,320
Jutro ją zamontuję.

50
00:06:05,360 --> 00:06:07,400
Słuszna uwaga.

51
00:06:07,450 --> 00:06:11,450
Teraz wypróbujmy.

52
00:06:16,830 --> 00:06:20,830
A to ci dopiero.
Jeszcze wczoraj działało bez zarzutu.

53
00:06:23,090 --> 00:06:25,130
Czasem trzeba stanowczo.

54
00:06:25,170 --> 00:06:32,050
U nas mawiają: do maszyny
jak do baby, trzeba mieć twardą rękę.

55
00:06:33,510 --> 00:06:36,470
Nic ci nie jest?

56
00:06:36,640 --> 00:06:39,140
Poczęstuj pana Pinkle czymś do picia!

57
00:06:39,190 --> 00:06:42,610
Może być lemoniada...

58
00:06:43,940 --> 00:06:47,030
To śmiertelna pułapka,
a my chcemy go żywego.

59
00:06:47,070 --> 00:06:50,030
Ubezpieczyciel nie sypie groszem.

60
00:06:50,200 --> 00:06:56,290
Po odjęciu kosztów zostaje to,
albo wózek inwalidzki.

61
00:06:56,450 --> 00:06:59,000
Bardzo pani dziękuję.

62
00:07:07,920 --> 00:07:12,970
- Mam spotkanie z adwokatem Leo.
- Przesłuchanie jest dziś?

63
00:07:13,140 --> 00:07:16,600
Spróbujmy jeszcze raz.

64
00:07:20,440 --> 00:07:23,270
Z nami w porządku?

65
00:07:33,570 --> 00:07:36,030
Chodźcie zobaczyć.

66
00:07:36,080 --> 00:07:37,370
Już działa.

67
00:07:37,990 --> 00:07:40,870
Chodźmy stąd.

68
00:07:44,420 --> 00:07:48,420
- Trafi do wyjścia.
- Bobby, udało się.

69
00:07:58,180 --> 00:08:00,680
Nie mogę sięgnąć wyłącznika!

70
00:08:12,320 --> 00:08:15,990
Pan Palmer nie przyznaje się

71
00:08:16,030 --> 00:08:18,790
do popełnienia morderstwa
pierwszego stopnia.

72
00:08:18,830 --> 00:08:21,790
Obrona wniosła o zwolnienie za kaucją.

73
00:08:21,960 --> 00:08:24,960
Dzisiejsze wstępne
przesłuchanie pozwoli orzec

74
00:08:25,000 --> 00:08:27,130
czy oskarżony zostanie zwolniony.

75
00:08:27,170 --> 00:08:29,090
Panie prokuratorze.

76
00:08:29,250 --> 00:08:33,260
W imieniu mieszkańców
stanu Waszyngton, domagam się,

77
00:08:33,300 --> 00:08:35,470
by nie przyznawać prawa do kaucji.

78
00:08:35,510 --> 00:08:40,350
Ze względu na:
po pierwsze - ciężar zbrodni,

79
00:08:40,720 --> 00:08:44,730
po drugie - premedytację,
z jaką została popełniona

80
00:08:44,890 --> 00:08:49,940
i po trzecie - ewidentny brak
równowagi psychicznej,

81
00:08:49,980 --> 00:08:54,320
jaki wykazuje od czasu śmierci córki.

82
00:08:55,320 --> 00:08:59,330
W imieniu obrony
głos zabierze szeryf Truman.

83
00:08:59,490 --> 00:09:02,450
Wysoki sądzie...

84
00:09:02,620 --> 00:09:06,620
Leland Palmer jest powszechnie
znanym, lubianym

85
00:09:06,790 --> 00:09:09,710
i szanowanym członkiem
tutejszej społeczności.

86
00:09:09,750 --> 00:09:11,840
Korzenie jego rodziny sięgają głęboko.

87
00:09:12,010 --> 00:09:17,050
Jego dziadek, Joshua Palmer, przybył tu
z rodziną ponad 75 lat temu.

88
00:09:17,220 --> 00:09:22,100
Wysoki sądzie,
nikt z nas nie jest świadom...

89
00:09:22,430 --> 00:09:26,900
...co czuje ojciec po stracie dziecka.

90
00:09:28,690 --> 00:09:31,650
Tyle mogę powiedzieć.

91
00:09:31,820 --> 00:09:33,740
Dziękuję, szeryfie.

92
00:09:35,950 --> 00:09:38,570
Może sprzedam to do gazety.

93
00:09:39,240 --> 00:09:43,330
Często drukują portrety z sali sądowej.

94
00:09:44,580 --> 00:09:47,420
Spróbuj lepiej uchwycić twarz.

95
00:09:47,460 --> 00:09:52,500
Ze względu na nieposzlakowaną opinię
oskarżonego i jego pozycję społeczną...

96
00:09:52,670 --> 00:09:57,380
- To tylko propozycja.
- Dziękuję, szeryfie.

97
00:09:57,880 --> 00:10:02,600
...oskarżony zostanie
zwolniony za kaucją.

98
00:10:03,100 --> 00:10:05,930
Lelandzie...

99
00:10:06,230 --> 00:10:10,230
Jak wiesz, poproszę cię teraz,
żebyś nie opuszczał miasta

100
00:10:10,400 --> 00:10:15,030
oraz informował szeryfa
o swoich planach.

101
00:10:17,700 --> 00:10:20,660
Sid. Weź kalendarz.

102
00:10:20,820 --> 00:10:26,790
Wyznaczymy panu Palmerowi
możliwie najbliższy termin rozprawy.

103
00:10:42,720 --> 00:10:43,760
Cześć

104
00:10:46,890 --> 00:10:49,520
Dziękuję.

105
00:10:56,280 --> 00:11:01,530
- Co dziś kryją te błękitne oczy?
- Mam propozycję.

106
00:11:02,530 --> 00:11:05,910
Podzielę się z tobą moimi wspomnieniami.

107
00:11:05,950 --> 00:11:08,080
Niech staną się częścią twojej powieści.

108
00:11:08,790 --> 00:11:13,630
W zamian dasz mi przeczytać
pamiętnik Laury.

109
00:11:16,090 --> 00:11:18,920
Intrygujące.

110
00:11:23,390 --> 00:11:26,770
Ja będę ci go czytać.

111
00:11:27,560 --> 00:11:31,560
Pamiętnik nie może opuścić tego pokoju.

112
00:11:31,730 --> 00:11:34,190
Zgoda.

113
00:11:45,280 --> 00:11:49,000
- Zaczniesz?
- Oczywiście.

114
00:11:49,450 --> 00:11:52,540
Dobiliśmy targu.

115
00:12:28,030 --> 00:12:31,000
Donna Hayward.

116
00:12:41,590 --> 00:12:44,260
Zaczynaj.

117
00:12:44,300 --> 00:12:49,760
Jestem z Twin Peaks. Tu się urodziłam.
Odbierał mnie mój ojciec.

118
00:12:49,810 --> 00:12:51,850
Jest lekarzem.

119
00:12:53,060 --> 00:12:56,440
A ty skąd pochodzisz?

120
00:12:59,320 --> 00:13:02,280
Dorastałem w Bostonie.

121
00:13:02,440 --> 00:13:06,030
Właściwie wśród książek.

122
00:13:11,830 --> 00:13:16,710
Są rzeczy, których
nie dowiesz się z książek.

123
00:13:17,210 --> 00:13:21,760
Są rzeczy, których
nie dowiesz się znikąd.

124
00:13:21,840 --> 00:13:27,930
Marzymy, że znajdziemy je u innych.

125
00:13:28,510 --> 00:13:30,970
Mów dalej.

126
00:13:31,010 --> 00:13:34,980
Czasem marzenia się spełniają.

127
00:13:39,980 --> 00:13:42,730
Moja kolej.

128
00:13:43,110 --> 00:13:48,570
- Co robisz?
- Może przeczytam to na zewnątrz...

129
00:13:49,370 --> 00:13:53,370
Czekaj... dopiero zaczęliśmy...

130
00:13:54,580 --> 00:13:58,830
Chodź ze mną.
Nie ma się czego bać.

131
00:13:59,790 --> 00:14:02,250
Chodź.

132
00:14:05,010 --> 00:14:08,390
Proszę... oddaj to...

133
00:14:23,780 --> 00:14:26,740
Przepraszam...

134
00:14:38,750 --> 00:14:43,800
Obrona przedstawi dowody.
Panie Racine.

135
00:14:44,210 --> 00:14:46,880
Oto dwa elektroencefalogramy.

136
00:14:46,920 --> 00:14:52,180
To zapis fal mojego mózgu
i dowodzi, że działa on normalnie.

137
00:14:52,220 --> 00:14:55,020
Sprzeciw wobec użycia
słowa "normalnie".

138
00:14:55,060 --> 00:14:58,060
Nie mija się z prawdą,
sprzeciw oddalony.

139
00:15:00,520 --> 00:15:04,400
A to zapis fal mózgowych
Leo Johnsona.

140
00:15:04,440 --> 00:15:08,650
Wykazuje jego minimalną aktywność.

141
00:15:12,530 --> 00:15:15,290
Przedłożone wcześniej
raporty medyczne stwierdzają,

142
00:15:15,330 --> 00:15:18,960
że pan Johnson cierpi
na rozległe uszkodzenia mózgu.

143
00:15:19,000 --> 00:15:24,960
W tej chwili nie ma możliwości
poddania go podstawowym testom.

144
00:15:25,250 --> 00:15:30,300
Proszę wziąć pod uwagę,
że sądzenie mojego klienta

145
00:15:30,340 --> 00:15:34,430
w tej sytuacji będzie jedynie
farsą systemu sprawiedliwości.

146
00:15:34,470 --> 00:15:40,230
Zaś ten nieszczęsny młodzieniec
nawet nie będzie tego świadom.

147
00:15:44,440 --> 00:15:47,690
Panie prokuratorze?

148
00:15:50,110 --> 00:15:53,070
Wiem, że Leo Johnson
nie jest w najlepszej formie.

149
00:15:53,240 --> 00:15:57,240
Na pozór absurdalnym jest
stawiać go przed sądem.

150
00:15:57,410 --> 00:16:01,920
Jednak zadaniem sądu
jest nie tylko ukarać przestępcę.

151
00:16:01,960 --> 00:16:06,250
Trzeba upewnić społeczność,
iż sprawiedliwości staje się zadość.

152
00:16:06,380 --> 00:16:09,130
Leo Johnson jest podejrzany
o wiele przestępstw.

153
00:16:09,170 --> 00:16:14,680
Zwłaszcza o zabójstwo Laury Palmer,
które wstrząsnęło całym miasteczkiem.

154
00:16:14,760 --> 00:16:18,220
- Dopóki Leo Johnson oddycha...
- W porządku. Już dobrze...

155
00:16:18,260 --> 00:16:21,230
- ... należy mu się proces...
- Wystarczy!

156
00:16:21,390 --> 00:16:25,400
- ... który uspokoi społeczeństwo!
- Wystarczy!

157
00:16:25,560 --> 00:16:29,440
Proszę siadać, panie Lodwick.

158
00:16:32,860 --> 00:16:35,950
Zrobimy teraz chwilę przerwy.

159
00:16:35,990 --> 00:16:39,490
Muszę rozważyć sprawę.

160
00:16:41,200 --> 00:16:47,040
Panie Cooper, szeryfie Truman,
czy zechcecie mi towarzyszyć?

161
00:16:49,380 --> 00:16:54,130
Sid, przyrządź nam trzy szklaneczki
ponczu Black Yukon.

162
00:16:54,180 --> 00:16:55,470
Tak, panie sędzio.

163
00:16:55,640 --> 00:16:59,760
Lodwick zapędził mnie w kozi róg.

164
00:17:00,850 --> 00:17:04,850
Nie przypuszczałem, że wyskoczy
z oskarżeniami o morderstwo.

165
00:17:04,940 --> 00:17:08,650
Cooper, czy Leo Johnson
to ten, którego pan szuka?

166
00:17:08,690 --> 00:17:11,230
Nie sądzę.

167
00:17:11,280 --> 00:17:15,030
Harry, co mówią na mieście?

168
00:17:15,450 --> 00:17:18,410
Chcą procesu, czy linczu?

169
00:17:18,580 --> 00:17:21,540
Chcą, aby winny stanął przed sądem.

170
00:17:21,700 --> 00:17:24,660
Więc nie potrzeba pokazówki.

171
00:17:24,830 --> 00:17:28,840
Ten nieszczęśnik jest jak warzywo.

172
00:17:29,000 --> 00:17:30,920
Jakieś sugestie, Cooper?

173
00:17:31,090 --> 00:17:36,130
Myślę, że zabójca
zostanie w końcu schwytany.

174
00:17:36,300 --> 00:17:40,930
Uważajcie na to.
Momentalnie zwala z nóg.

175
00:17:52,980 --> 00:17:59,200
Mam zamiar uznać Leo za niezdolnego
do stawienia się przed sądem.

176
00:18:00,280 --> 00:18:05,870
Niech go zabiorą do domu,
kiedy to tylko będzie możliwe.

177
00:18:07,580 --> 00:18:12,630
Przekażesz tę wieść młodej damie?

178
00:18:12,800 --> 00:18:15,880
Dzięki, Clinton.

179
00:18:19,050 --> 00:18:23,060
- Cooper, od jak dawna pan tu jest?
- Od 12 dni.

180
00:18:23,220 --> 00:18:27,690
Nie będą go sądzić.
Leo wraca do domu.

181
00:18:28,440 --> 00:18:33,150
Dam panu radę:
niech pan uważa na te lasy.

182
00:18:33,650 --> 00:18:36,740
Są przepiękne...

183
00:18:37,820 --> 00:18:40,160
...ale bardzo dziwne.

184
00:18:40,200 --> 00:18:42,830
Dziękuję.

185
00:19:00,760 --> 00:19:03,720
Chcesz czegoś, Nadine?

186
00:19:04,510 --> 00:19:08,270
- Kim jesteś?
- Ja?

187
00:19:10,150 --> 00:19:14,480
- Chodzisz do naszego liceum?
- Tak.

188
00:19:15,360 --> 00:19:19,360
Nie poznałam cię.
Jesteś z innej klasy?

189
00:19:19,530 --> 00:19:21,780
Usiądź, skarbie,
przyniosę coś do picia.

190
00:19:21,820 --> 00:19:24,950
Sama sobie wezmę.

191
00:19:26,830 --> 00:19:30,040
Gdzie mama i tata?

192
00:19:30,710 --> 00:19:33,420
Wyjechali.

193
00:19:42,470 --> 00:19:46,470
Doktor kazał przytakiwać.

194
00:19:46,640 --> 00:19:49,600
Może niech ją zbada doktor Jacoby.

195
00:19:49,770 --> 00:19:54,360
Wyjechał na Hawaje.
Odpoczywa po zawale.

196
00:19:59,150 --> 00:20:02,570
Samo odeszło.

197
00:20:17,920 --> 00:20:20,590
Przyszedł pan Tojamura.

198
00:20:20,630 --> 00:20:22,970
Kto?

199
00:20:27,810 --> 00:20:30,600
Pan wybaczy...

200
00:20:30,640 --> 00:20:34,440
- Czy pan był umówiony, panie...?
- Tojamura.

201
00:20:35,060 --> 00:20:39,530
Reprezentuję inwestorów z Azji.

202
00:20:39,820 --> 00:20:47,330
Mamy dla was korzystniejszą ofertę
dotyczącą projektu Ghostwood.

203
00:20:48,160 --> 00:20:52,160
Zostałem upoważniony
przez inwestorów z Tokio,

204
00:20:52,330 --> 00:20:56,330
aby natychmiast podjąć
odpowiednie kroki.

205
00:20:56,500 --> 00:21:02,590
Interesujące - niestety za moment
mam bardzo ważną naradę...

206
00:21:02,760 --> 00:21:06,760
- Tu są prospekty.
- Może innym razem.

207
00:21:06,930 --> 00:21:09,930
A to przedsmak.

208
00:21:17,360 --> 00:21:19,860
Pięć...

209
00:21:20,480 --> 00:21:23,320
...milionów.

210
00:21:46,220 --> 00:21:50,850
- Moi ludzie przejrzą plany.
- Liczymy na to.

211
00:21:50,890 --> 00:21:55,350
Arigato, Tojamura-san.

212
00:22:07,410 --> 00:22:10,280
Cooper idzie.

213
00:22:12,620 --> 00:22:15,580
Telefon z Kanady, panie Horne.

214
00:22:15,750 --> 00:22:18,170
- To oni?
- Tak.

215
00:22:18,210 --> 00:22:21,040
W samą porę.

216
00:22:27,220 --> 00:22:28,390
Benjamin Horne.

217
00:22:28,430 --> 00:22:30,300
Jesteś gotowy?

218
00:22:30,350 --> 00:22:32,850
Audrey.

219
00:22:33,470 --> 00:22:34,890
Chcę rozmawiać z córką.

220
00:22:34,930 --> 00:22:37,600
To zaczeka do wieczora.
Słuchaj.

221
00:22:37,640 --> 00:22:42,320
Tuż za granicą, 8 km od Grand Fork,
przy drodze do Castlegar,

222
00:22:42,360 --> 00:22:44,900
stoi bar "Kolumbijczyk".

223
00:22:44,940 --> 00:22:47,900
Na tyłach znajdziesz nieczynne
wesołe miasteczko.

224
00:22:48,070 --> 00:22:53,330
Podejdź do karuzeli,
zostaw walizkę przy koniu bez łba.

225
00:22:53,370 --> 00:22:56,540
O północy. Sam.

226
00:22:56,580 --> 00:23:00,210
Wyślę mojego człowieka...

227
00:23:07,880 --> 00:23:13,260
Banknoty z jednej serii,
zgodnie z pana instrukcjami.

228
00:23:15,180 --> 00:23:19,890
Życie mojej córki
jest w pańskich rękach.

229
00:23:28,740 --> 00:23:32,620
Proszę czekać przy telefonie.

230
00:23:39,160 --> 00:23:44,210
Idź za nim.
Dopilnuj, by dotarł z pieniędzmi.

231
00:23:44,380 --> 00:23:49,720
- Przyprowadź Audrey.
- Cooper jej nie przyprowadzi?

232
00:23:50,630 --> 00:23:54,640
Cooper nie wróci.
Poradzisz sobie.

233
00:23:54,800 --> 00:23:58,890
Wróć z Audrey i z walizką.

234
00:24:06,730 --> 00:24:10,570
Pięć milionów dolarów.

235
00:24:10,610 --> 00:24:13,280
Niezła dniówka.

236
00:24:21,410 --> 00:24:25,420
Jak już wejdziesz do domu Harolda,
nie trać czasu.

237
00:24:25,590 --> 00:24:27,500
Zaczynam się bać.

238
00:24:27,670 --> 00:24:30,630
Pamiętnik Laury jest w skrytce w regale,

239
00:24:30,800 --> 00:24:33,760
za rzędem sztucznych książek.

240
00:24:33,930 --> 00:24:40,020
Po prawej stronie znajdziesz
gałkę otwierającą skrytkę.

241
00:24:41,230 --> 00:24:46,270
Zeszyt Laury jest mniejszy od innych
i ma czerwoną okładkę,

242
00:24:46,440 --> 00:24:50,440
- Jak wyciągniesz go z domu?
- Nie z domu, tylko z dużego pokoju.

243
00:24:50,610 --> 00:24:55,660
Dam ci znak latarką przez okno.
Drzwi będą otwarte.

244
00:24:55,820 --> 00:24:59,330
Myślałam, że go lubisz.

245
00:25:00,000 --> 00:25:02,460
Lubię.

246
00:25:12,510 --> 00:25:15,720
Cooper, dawaj walizkę.

247
00:25:15,760 --> 00:25:18,390
Biorę ją...

248
00:25:22,930 --> 00:25:25,900
Co z Benem Horne?

249
00:25:26,060 --> 00:25:30,070
Zrzeknie się kontroli nad
"Jednookim Jackiem". Lokal będzie twój.

250
00:25:30,230 --> 00:25:33,190
Po tym, jak zabijemy mu córkę?

251
00:25:33,360 --> 00:25:36,320
Kiedy jesz dobry stek,

252
00:25:36,490 --> 00:25:39,450
nie zastanawiasz się,
jak trafił na twój talerz.

253
00:25:39,620 --> 00:25:43,620
Tu chodzi o twój interes.

254
00:25:43,790 --> 00:25:46,920
Jak ją załatwisz?

255
00:26:02,560 --> 00:26:06,690
- Dziewczyna śpi?
- Jak niemowlę.

256
00:26:08,810 --> 00:26:12,190
Chodź tu do mnie.

257
00:26:15,070 --> 00:26:19,280
W pokoju obok
dziewczyna śni o rybkach,

258
00:26:19,370 --> 00:26:23,370
i o splątanych wodorostach
na dnie jeziora.

259
00:26:23,410 --> 00:26:27,540
Co z Blackie?
Tak długo już czekam.

260
00:26:27,580 --> 00:26:30,590
Dziś się zabawimy.

261
00:26:34,880 --> 00:26:37,760
Pocałuj mnie.

262
00:26:39,470 --> 00:26:42,060
Dobrze.

263
00:26:42,100 --> 00:26:43,680
Do widzenia.

264
00:26:53,650 --> 00:26:55,570
Laboratorium.

265
00:26:55,740 --> 00:26:58,360
Dzwonię w sprawie wyników.

266
00:26:58,410 --> 00:27:00,370
Doktor Hayward kazał mi zadzwonić...

267
00:27:00,410 --> 00:27:04,740
- Co to za badanie?
- Badanie?

268
00:27:05,120 --> 00:27:07,830
To było...

269
00:27:08,040 --> 00:27:12,250
...badanie spermy.
Brennan, Andy.

270
00:27:12,420 --> 00:27:15,380
- Nie słyszę pana.
- Przepraszam.

271
00:27:15,550 --> 00:27:20,590
- Brennan, Andy.
- Diagnoza brzmi: oligospermia.

272
00:27:20,760 --> 00:27:24,760
Proszę powtórzyć.
Chciałbym sobie zapisać.

273
00:27:24,930 --> 00:27:28,310
Oligospermia.

274
00:27:31,980 --> 00:27:35,320
To brzmi strasznie!
Ja to mam?

275
00:27:35,360 --> 00:27:39,360
- Nie, miał pan.
- Miałem?

276
00:27:39,530 --> 00:27:43,280
"Za mało plemników".

277
00:27:44,740 --> 00:27:48,750
- Świetnie, proszę pana.
- Już wyzdrowiałem?

278
00:27:48,910 --> 00:27:52,920
Doktor powiedział...

279
00:27:53,080 --> 00:27:58,260
"Nie jedna kijanka, ale cały staw".

280
00:27:58,300 --> 00:28:01,260
- Właśnie. Rozumie pan?
- Oczywiście.

281
00:28:01,430 --> 00:28:06,810
- Nie chodzi o kijanki, tylko o...
- Przepraszam, mam drugi telefon.

282
00:28:06,850 --> 00:28:11,650
Dziękuję.
Bardzo dziękuję.

283
00:28:11,770 --> 00:28:14,770
Mam pełen staw!

284
00:28:15,940 --> 00:28:18,940
Mam pełen staw.

285
00:28:24,280 --> 00:28:26,990
Nie pytaj.

286
00:28:27,410 --> 00:28:30,370
Na czym skończyliśmy?

287
00:28:30,540 --> 00:28:34,540
Tu jest bar i pokój gościnny, kasyno,
a tu wybierają panienki.

288
00:28:34,710 --> 00:28:39,920
- A to pokój Blackie?
- Przekonamy się na miejscu.

289
00:28:42,800 --> 00:28:46,390
Jednoręki zatrzymał się
w motelu przy autostradzie nr 9.

290
00:28:46,430 --> 00:28:49,560
Od wczoraj nikt go nie widział.

291
00:28:49,600 --> 00:28:52,270
Pokój wygląda na zamieszkany.

292
00:28:52,440 --> 00:28:54,230
Znalazłem to.

293
00:28:54,810 --> 00:28:57,480
To samo lekarstwo, co poprzednio.

294
00:28:57,650 --> 00:29:00,610
Dziwny, ostry zapach.

295
00:29:00,780 --> 00:29:04,780
Wciąż czekamy na wyniki od Alberta.
Dobra robota, Hawk.

296
00:29:06,740 --> 00:29:10,700
Zobaczymy się rano.

297
00:29:23,720 --> 00:29:26,680
Podejdziemy od strony lasu,
tylnym wejściem.

298
00:29:26,850 --> 00:29:29,810
Stamtąd schody prowadzą do pokoi.

299
00:29:29,970 --> 00:29:33,980
Myślę, że w jednym z nich
trzymają Audrey.

300
00:29:34,140 --> 00:29:36,900
Ilu mają ochroniarzy?

301
00:29:36,940 --> 00:29:40,030
Kilku w kasynie,
ale pewnie jest ich więcej.

302
00:29:42,320 --> 00:29:45,410
Ruszajmy.

303
00:29:50,830 --> 00:29:53,540
- Dobranoc, Andy.
- Do jutra, Andy.

304
00:29:53,580 --> 00:29:56,540
Dobra robota.

305
00:30:02,300 --> 00:30:05,180
Gwen i Larry.

306
00:30:12,720 --> 00:30:15,690
- Klinika aborcyjna Adams.
- Cześć, jest Lucy...

307
00:30:15,850 --> 00:30:19,940
- Co?
- Klinika aborcyjna Adams.

308
00:30:21,070 --> 00:30:23,900
Boże święty!

309
00:30:33,370 --> 00:30:36,000
Poproszę kawę na wynos.

310
00:30:39,000 --> 00:30:41,670
Dużą kawę.

311
00:30:41,710 --> 00:30:45,720
- Widziałaś się dziś z Donną?
- Nie. Niestety. A ty?

312
00:30:45,880 --> 00:30:49,890
- Nie. Po co ci ta kawa?
- Kawa?

313
00:30:50,050 --> 00:30:52,510
Dla wujka Lelanda.

314
00:30:53,060 --> 00:30:55,350
Nie macie kawy w domu?

315
00:30:56,310 --> 00:31:00,310
Nie mogę teraz rozmawiać.
Muszę iść.

316
00:31:01,020 --> 00:31:03,110
Musisz iść? Dokąd?

317
00:31:03,150 --> 00:31:06,190
Do domu. Cześć.

318
00:31:35,560 --> 00:31:39,350
Donna Hayward, 7 marca...

319
00:31:39,520 --> 00:31:43,360
...wtorek. Spotkanie drugie.

320
00:31:44,730 --> 00:31:47,860
Drogi pamiętniku.

321
00:31:49,950 --> 00:31:53,830
To było dawno temu. Może być?

322
00:31:54,120 --> 00:31:59,870
Miałam wtedy 13, może 14 lat.

323
00:32:02,130 --> 00:32:06,130
Założyłyśmy z Laurą najkrótsze
i najbardziej obcisłe spódnice.

324
00:32:06,210 --> 00:32:10,840
Zbyt obcisłe. Laura mnie namówiła.

325
00:32:12,890 --> 00:32:16,890
Wybrałyśmy się do Roadhouse
na spotkanie z chłopcami.

326
00:32:17,060 --> 00:32:20,060
Nazywali się...

327
00:32:20,640 --> 00:32:25,860
Josh, Rick i Tim.

328
00:32:28,110 --> 00:32:31,320
Mieli po 20 lat...

329
00:32:33,320 --> 00:32:36,830
...i byli dla nas mili.

330
00:32:47,920 --> 00:32:51,930
Czułyśmy się przy nich starsze.

331
00:32:54,180 --> 00:32:59,220
Rick zapytał, czy chcemy z nimi iść
na zabawę i Laura się zgodziła.

332
00:32:59,390 --> 00:33:04,440
Poczułam nagle,
jak żołądek podchodzi mi do gardła.

333
00:33:04,600 --> 00:33:10,570
Ale kiedy Laura wsiadła
do furgonetki Ricka, zrobiłam to samo.

334
00:33:10,860 --> 00:33:13,950
Rzeka w lesie...

335
00:33:15,030 --> 00:33:19,790
Chyba była pełnia.
Wszędzie blada poświata.

336
00:33:23,370 --> 00:33:26,840
Laura zaczyna tańczyć.

337
00:33:31,720 --> 00:33:39,390
Porusza biodrami... do tyłu i w przód.

338
00:33:43,180 --> 00:33:49,190
Rick zaczyna klaskać,
ale Tim tylko patrzy.

339
00:33:49,440 --> 00:33:52,900
To mnie złości.

340
00:33:53,610 --> 00:33:57,490
Mówię: "Chodźmy się popluskać!"

341
00:34:00,910 --> 00:34:04,040
Rozbieramy się...

342
00:34:06,120 --> 00:34:12,090
...i wiem, że oni patrzą.
Laura całuje się z Joshem i Rickiem.

343
00:34:12,380 --> 00:34:16,970
Nie wiem, co robić,
więc idę pływać.

344
00:34:20,720 --> 00:34:24,940
Chciałabym uciec, ale nie uciekam.

345
00:34:26,980 --> 00:34:30,360
Tim podpływa do mnie.

346
00:34:32,190 --> 00:34:35,570
Całuje mnie w rękę...

347
00:34:37,410 --> 00:34:40,240
...i w usta.

348
00:34:46,790 --> 00:34:50,670
Wciąż pamiętam ten pocałunek.

349
00:34:52,000 --> 00:34:56,130
Jego wargi były ciepłe i słodkie.

350
00:34:58,260 --> 00:35:01,760
Serce wali mi jak młot.

351
00:35:02,430 --> 00:35:06,770
Mówi coś do mnie, ale nie słyszę go.

352
00:35:08,690 --> 00:35:12,150
Tylko ten pocałunek...

353
00:35:13,900 --> 00:35:16,900
Nigdy więcej go nie widziałam.

354
00:35:18,030 --> 00:35:22,490
Wtedy pierwszy raz byłam zakochana.

355
00:35:28,500 --> 00:35:30,960
To wszystko.

356
00:35:31,000 --> 00:35:34,000
To było piękne.

357
00:36:10,460 --> 00:36:13,420
Zajmę się nim.

358
00:36:50,210 --> 00:36:54,170
[ TU POWSTANIE ŁAŹNIA WŁOSKA ]
[ PRZEPRASZAMY ZA BAŁAGAN ]

359
00:37:57,860 --> 00:38:01,110
Poczekaj tu.

360
00:38:04,110 --> 00:38:07,490
Nie, to ślepy zaułek.

361
00:38:23,920 --> 00:38:27,140
- Dwoje.
- Audrey?

362
00:38:39,560 --> 00:38:42,940
W gruncie rzeczy
to nie taka straszna śmierć.

363
00:38:42,980 --> 00:38:45,650
O nie, cudowna.

364
00:38:45,820 --> 00:38:49,910
- Jest sama?
- Jest z nią Nancy.

365
00:38:51,030 --> 00:38:55,040
Co takiego ma ona, czego nie mam ja?

366
00:38:55,460 --> 00:38:58,040
Młodość.

367
00:39:15,480 --> 00:39:20,110
Ludzie myślą, że orchidee
rosną tylko w tropikach.

368
00:39:20,150 --> 00:39:25,530
A naprawdę mogą rosnąć wszędzie.
To tylko kwestia wilgoci i oświetlenia.

369
00:39:25,690 --> 00:39:27,110
Przepiękna.

370
00:39:28,820 --> 00:39:30,160
Spójrz.

371
00:39:32,990 --> 00:39:38,040
Najważniejsze to zachować
odpowiednią wilgotność.

372
00:39:38,210 --> 00:39:42,210
Nadmiar wody sprowadza choroby.

373
00:39:42,380 --> 00:39:45,340
Trzy działki kielicha i trzy płatki.

374
00:39:45,510 --> 00:39:49,510
Środkowy płatek ma kształt dolnej wargi

375
00:39:49,680 --> 00:39:52,640
nazywa się „labellum”.

376
00:39:52,800 --> 00:39:55,810
Jaki delikatny.

377
00:39:58,020 --> 00:40:02,770
Służy za lądowisko
dla zapylających owadów.

378
00:40:02,810 --> 00:40:05,730
Jakie to romantyczne.

379
00:40:37,640 --> 00:40:41,350
Przepraszam cię na moment.

380
00:41:17,260 --> 00:41:20,230
Chciałbym widzieć się
z Audrey Horne.

381
00:41:20,640 --> 00:41:24,150
- Chyba jest zajęta.
- Zobaczymy.

382
00:41:44,040 --> 00:41:47,000
To nie był mój pomysł.

383
00:41:47,880 --> 00:41:50,300
Dla nich jesteśmy tylko przynętą.

384
00:41:50,340 --> 00:41:52,760
Zamknij się!

385
00:41:53,510 --> 00:41:56,970
Audrey, słyszysz mnie?

386
00:42:07,730 --> 00:42:12,740
- Modliłam się...
- Zabiorę cię stąd.

387
00:42:35,340 --> 00:42:39,550
Pocałuj mnie na pożegnanie.

388
00:42:40,060 --> 00:42:43,890
Jeśli tak, to nie ma sprawy.

389
00:43:25,930 --> 00:43:29,270
Rzuć broń.
Powoli.

390
00:43:35,320 --> 00:43:38,200
Odwróćcie się.

391
00:43:54,090 --> 00:43:58,970
Dobrze, że nie potraficie
utrzymać tajemnicy.

392
00:44:00,340 --> 00:44:03,100
Dobry rzut.

393
00:44:11,810 --> 00:44:17,400
Była strzelanina.
Truman i Cooper wychodzą z pana córką.

394
00:44:20,160 --> 00:44:23,280
Któż to może być?

395
00:44:24,330 --> 00:44:27,580
Prokurator Lodwick.

396
00:44:43,720 --> 00:44:48,480
- Co się stało?
- Nic. Przestraszyłeś mnie.

397
00:44:55,190 --> 00:44:59,780
Chodź.
Opowiedz mi o tej orchidei.

398
00:45:17,090 --> 00:45:20,840
Haroldzie, nie rozumiesz...

399
00:45:24,390 --> 00:45:26,850
Maddy!

400
00:45:27,890 --> 00:45:30,390
Czekaj!

401
00:45:34,150 --> 00:45:37,360
Szukacie tajemnic?

402
00:45:38,320 --> 00:45:41,400
O to chodziło?

403
00:45:43,530 --> 00:45:46,910
Więc może wam pomogę.

404
00:45:47,700 --> 00:45:51,000
Wiecie, co to jest
tajemnica ostateczna?

405
00:45:51,040 --> 00:45:53,540
Chcecie ją poznać?

406
00:45:53,580 --> 00:45:56,710
Laura ją znała...

407
00:46:00,800 --> 00:46:03,510
Znała swojego zabójcę.

408
00:46:18,440 --> 00:46:21,320
Tekst polski i opracowanie
Jakub "Qbin" Jedynak

409
00:46:21,340 --> 00:46:25,340
Dopasowanie: Bonaducci

