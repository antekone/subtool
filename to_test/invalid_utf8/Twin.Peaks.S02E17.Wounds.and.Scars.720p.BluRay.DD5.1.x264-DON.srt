1
00:00:28,870 --> 00:00:33,560
MIASTECZKO TWIN PEAKS

2
00:02:45,170 --> 00:02:48,470
Śniadanie od Normy. Specjalne.

3
00:02:52,540 --> 00:02:54,380
Kochana Norma.

4
00:02:55,240 --> 00:02:57,890
Może później, dzięki.

5
00:03:00,560 --> 00:03:02,730
Co słychać na posterunku?

6
00:03:03,060 --> 00:03:08,070
Pochłania nas partia szachów.
Facet nie umie się bawić.

7
00:03:10,550 --> 00:03:13,020
Na razie się trzymamy.

8
00:03:14,070 --> 00:03:18,820
Poradzicie sobie z sami.
To zwyczajne miasteczko.

9
00:03:21,230 --> 00:03:22,740
Takie było.

10
00:03:25,580 --> 00:03:29,540
Chyba świat nas dogonił.

11
00:03:53,300 --> 00:03:54,560
Annie!

12
00:04:02,110 --> 00:04:04,740
Shelly, to jest moja siostra, Annie.

13
00:04:04,820 --> 00:04:08,070
Witamy. Wszystko
mi o tobie opowiedziała.

14
00:04:10,380 --> 00:04:13,710
- Wszystko?
- Shelly jest jak rodzina.

15
00:04:14,210 --> 00:04:16,970
W naszej to marna rekomendacja.

16
00:04:17,670 --> 00:04:20,590
Jak ci się układa z mamą?

17
00:04:20,670 --> 00:04:25,650
Nic nie mówmy albo udajmy,
że dobrze. Tak wolę.

18
00:04:25,740 --> 00:04:28,870
Ja też. Dobrze, że jesteś.

19
00:04:29,680 --> 00:04:32,200
Dziwnie się czuję
w prawdziwym świecie.

20
00:04:32,280 --> 00:04:35,140
Prawie zapomniałam,
do czego są pieniądze.

21
00:04:35,220 --> 00:04:37,910
W klasztorze gramy w bingo na żetony.

22
00:04:39,780 --> 00:04:42,500
Nie traktuj mnie ulgowo.

23
00:04:43,180 --> 00:04:46,590
Spokojnie,
będziesz harować aż padniesz.

24
00:04:51,030 --> 00:04:52,050
Niedoścignione.

25
00:05:14,910 --> 00:05:17,080
Harry jest załamany.

26
00:05:18,800 --> 00:05:19,660
Zjadł cokolwiek?

27
00:05:23,220 --> 00:05:29,250
- Kiedy może tu wrócić?
- Zobaczymy. Mogę pomóc?

28
00:05:30,460 --> 00:05:32,130
To ja powinienem spytać ciebie.

29
00:05:32,430 --> 00:05:34,400
Jesteś starszy stopniem.

30
00:05:34,490 --> 00:05:37,880
Niech zostanie po staremu.

31
00:05:37,960 --> 00:05:40,310
Nie cierpię papierów.

32
00:05:41,030 --> 00:05:44,460
Tu jest gorzej niż u nas.

33
00:05:44,950 --> 00:05:47,550
Eckhardt, Josie...

34
00:05:48,580 --> 00:05:50,560
Mam raport z sekcji.

35
00:05:50,640 --> 00:05:56,270
Doktor nie ustalił przyczyny zgonu,
ciało ważyło 29 kilogramów.

36
00:05:57,590 --> 00:05:58,370
Jak to możliwe?

37
00:05:59,510 --> 00:06:03,620
Nie wiem. Może to ma
jakiś związek z moją wizją.

38
00:06:04,120 --> 00:06:08,260
Może lepiej nie drażnić duchów.

39
00:06:09,620 --> 00:06:10,670
Jest coś o Earle'u?

40
00:06:12,370 --> 00:06:14,090
Na razie nie.

41
00:06:14,660 --> 00:06:16,740
Czekamy na odpowiedź.

42
00:06:31,990 --> 00:06:34,160
- �apcie.
- Kapcie.

43
00:06:38,450 --> 00:06:43,900
Wiesz, Leo, nie doceniasz,
jak kojące jest życie na wsi,

44
00:06:43,990 --> 00:06:48,490
póki sam go... nie zakosztujesz.

45
00:06:49,630 --> 00:06:51,670
- Fajka?
- Wspaniale.

46
00:06:54,130 --> 00:07:00,700
A nawet jeśli już byłeś na wsi,
to gdy sobie to wyobrażasz,

47
00:07:01,340 --> 00:07:08,070
obraz jest niedoskonały.
Wyobrażenia zawsze są.

48
00:07:08,820 --> 00:07:10,180
Mam rację?

49
00:07:11,410 --> 00:07:14,450
Możesz mi przytaknąć.

50
00:07:15,720 --> 00:07:18,790
Twoje milczenie jest jak księga.

51
00:07:19,410 --> 00:07:25,580
Jeśli nie księga, to przynajmniej
całkiem pokaźny rozdział.

52
00:07:29,360 --> 00:07:32,670
Zobaczmy, co nasz biedny Cooper
wymyślił tym razem.

53
00:07:48,050 --> 00:07:50,030
To nie ruch.

54
00:07:51,070 --> 00:07:52,720
To podstęp.

55
00:07:54,270 --> 00:07:56,830
Gra na pata.

56
00:07:59,090 --> 00:08:02,740
Cooper nie ma zielonego pojęcia
o grze na pata!

57
00:08:05,220 --> 00:08:06,750
Ktoś mu pomaga.

58
00:08:12,300 --> 00:08:16,810
Nie toleruję ludzi,
którzy łamią zasady.

59
00:08:17,920 --> 00:08:20,660
Którzy nie trzymają się reguł!

60
00:08:24,850 --> 00:08:26,380
Wielu...

61
00:08:28,030 --> 00:08:30,390
tego pożałuje.

62
00:09:07,040 --> 00:09:07,600
Rozumiecie?

63
00:09:11,070 --> 00:09:12,370
Moje panie...

64
00:09:13,460 --> 00:09:16,670
muszę przyznać, że obie

65
00:09:16,750 --> 00:09:20,320
wspaniale oddajecie

66
00:09:20,400 --> 00:09:24,040
nieskazitelność natury.

67
00:09:24,860 --> 00:09:26,080
Papierosa?

68
00:09:26,970 --> 00:09:30,290
Wolę mieć czyste płuca.

69
00:09:31,120 --> 00:09:34,000
Sprawność to potężne bóstwo.

70
00:09:37,160 --> 00:09:39,410
- Rozmawiał pan z Pinkle'em?
- Z kim?

71
00:09:39,490 --> 00:09:44,670
Ma wykład o zagrożonej
kunie świerkowej.

72
00:09:44,750 --> 00:09:48,280
Uroczo. Co to jest? Jakiś szop?

73
00:09:48,360 --> 00:09:50,140
�asica.

74
00:09:51,470 --> 00:09:55,640
- Świetnie. Macie współpracować.
- Chyba śnisz.

75
00:09:59,880 --> 00:10:00,940
Witam.

76
00:10:01,740 --> 00:10:04,110
Widzę, że wiosłujesz pod prąd.

77
00:10:04,190 --> 00:10:06,520
Słucham pana, panie Wheeler.

78
00:10:06,600 --> 00:10:09,510
Panno Horne,
ta rozmowa przy obiedzie...

79
00:10:09,600 --> 00:10:13,480
- Byłam niegrzeczna.
- To moja wina.

80
00:10:14,870 --> 00:10:20,040
Ale potrzebujemy pomocy
i nikt nie zrobi tego lepiej niż pan.

81
00:10:24,620 --> 00:10:26,380
O czym my właściwie mówimy?

82
00:10:26,470 --> 00:10:30,640
Przepraszamy się nawzajem
i czegoś żałujemy. Pójdziemy gdzieś?

83
00:10:33,070 --> 00:10:36,190
- Pytam nie w porę?
- Skądże.

84
00:10:37,140 --> 00:10:38,190
Może być piknik?

85
00:10:40,450 --> 00:10:44,530
- Trzeba coś wziąć: koszyk, koc...
- Jedzenie.

86
00:10:46,160 --> 00:10:49,740
- Nie umiem gotować.
- Ktoś w kuchni umie.

87
00:10:51,520 --> 00:10:54,410
Pokażę ludziom,
jak wygląda kuna świerkowa.

88
00:10:54,500 --> 00:10:56,880
Doskonale to rozumiem, panie Dinkle.

89
00:10:56,960 --> 00:10:58,300
Nazywam się Pinkle.

90
00:10:58,380 --> 00:11:03,770
Mówię tylko, że pokazywanie
wypchanego zwierzaka,

91
00:11:03,860 --> 00:11:07,060
by w ekologicznym proteście
reprezentował zagrożony gatunek,

92
00:11:07,150 --> 00:11:09,100
to podstawowa sprzeczność.

93
00:11:10,880 --> 00:11:12,770
Masz rację.

94
00:11:17,020 --> 00:11:17,810
Harry?

95
00:11:20,430 --> 00:11:21,400
Cześć, Coop.

96
00:11:22,150 --> 00:11:24,400
Z Interpolu przyszły akta Josie.

97
00:11:26,560 --> 00:11:27,770
Nie jestem ciekaw.

98
00:11:27,850 --> 00:11:31,890
Prócz zabójstwa Eckhardta, próby
zabicia mnie i zabójstwa Jonathana...

99
00:11:31,970 --> 00:11:35,870
- Sprawy są zamknięte.
- ...jest poszukiwana w Hongkongu.

100
00:11:36,130 --> 00:11:37,640
Nie chcę tego słuchać.

101
00:11:37,730 --> 00:11:40,000
Dwa razy była zatrzymana
za prostytucję.

102
00:11:52,470 --> 00:11:57,310
Wiedz, że była zatwardziałą
kryminalistką i morderczynią.

103
00:12:00,310 --> 00:12:01,580
Wyjdź stąd.

104
00:12:02,510 --> 00:12:04,440
Wiem, że ci niełatwo.

105
00:12:05,090 --> 00:12:06,730
Wyjdź.

106
00:12:09,090 --> 00:12:11,660
Wynoś się stąd!

107
00:12:32,670 --> 00:12:34,140
Było otwarte.

108
00:12:34,750 --> 00:12:36,670
Wiejskie nawyki.

109
00:12:37,320 --> 00:12:38,980
Jesteśmy ufni.

110
00:12:40,550 --> 00:12:44,610
Nazywam się Jones.
Byłam asystentką pana Eckhardta.

111
00:12:48,780 --> 00:12:50,020
Dziękuję.

112
00:13:01,110 --> 00:13:04,500
Doglądam transportu
jego zwłok do Hongkongu.

113
00:13:07,500 --> 00:13:11,880
To tragedia. A on ją naprawdę kochał.

114
00:13:12,920 --> 00:13:14,180
Jak my wszyscy.

115
00:13:15,390 --> 00:13:17,430
Pochowają ich obok siebie.

116
00:13:18,980 --> 00:13:21,850
Będą się nawzajem pilnować.

117
00:13:22,470 --> 00:13:24,650
Ta gorycz musi mieć przyczyny.

118
00:13:25,550 --> 00:13:28,640
Proszę to uznać za sceptycyzm.

119
00:13:29,660 --> 00:13:32,950
Po co naprawdę pani przyszła?

120
00:13:35,310 --> 00:13:38,830
Mam dla pani prezent. Można?

121
00:13:39,150 --> 00:13:40,870
Raczej nie.

122
00:13:47,480 --> 00:13:48,840
Powoli.

123
00:14:03,160 --> 00:14:04,580
Od Thomasa.

124
00:14:06,420 --> 00:14:09,690
Zostało mi jeszcze parę spraw,
a wieczorem wyjeżdżam.

125
00:14:11,400 --> 00:14:13,650
Powodzenia, pani Martell.

126
00:14:28,890 --> 00:14:33,210
Zastałem Willa albo Eileen Hayward?

127
00:14:34,050 --> 00:14:36,390
Nie ma ich. Mogę jakoś pomóc?

128
00:14:37,130 --> 00:14:42,250
Jestem doktor Gerald Craig,
kolega Willa ze studiów.

129
00:14:42,330 --> 00:14:44,890
Jadę na zjazd do Spokane

130
00:14:44,980 --> 00:14:49,610
i chciałem zrobić im niespodziankę.

131
00:14:49,910 --> 00:14:53,320
- Z którą z córek rozmawiam?
- Z Donną.

132
00:14:55,450 --> 00:14:57,450
Może pan wejdzie?

133
00:14:58,170 --> 00:15:00,080
Bardzo chętnie.

134
00:15:14,980 --> 00:15:18,620
Podać panu coś do picia?
Może wody gazowanej?

135
00:15:18,700 --> 00:15:20,890
Nie, dziękuję.

136
00:15:21,550 --> 00:15:24,490
To lokalna gazetka?

137
00:15:24,570 --> 00:15:26,410
Niewielka, jak miasteczko.

138
00:15:26,490 --> 00:15:31,960
Nie skreślaj małych miast,
póki nie zamieszkasz w wielkim.

139
00:15:32,040 --> 00:15:37,140
Wydaje mi się, że Will znalazł tu sobie
własny kawałek nieba.

140
00:15:41,110 --> 00:15:44,850
- Masz dwie siostry?
- Tak, są młodsze.

141
00:15:44,930 --> 00:15:47,210
I obie takie ładne jak ty?

142
00:15:48,100 --> 00:15:49,400
Nie wiem.

143
00:15:53,280 --> 00:16:00,640
Często siadywałem z twoim tatą
i snuliśmy plany na życie.

144
00:16:01,530 --> 00:16:06,880
I chyba on doszedł bliżej
wyznaczonego celu niż ja.

145
00:16:09,230 --> 00:16:11,750
- Jesteś w liceum?
- Niestety.

146
00:16:11,840 --> 00:16:17,360
Myślałem tak samo.
W liceum bywa ciężko.

147
00:16:17,450 --> 00:16:20,100
Nie masz pojęcia,
co chcesz robić w życiu,

148
00:16:20,180 --> 00:16:23,960
i wydaje ci się to
całkiem niepotrzebne.

149
00:16:24,040 --> 00:16:25,440
To prawda.

150
00:16:26,380 --> 00:16:28,770
Nie martw się, wszystko się ułoży.

151
00:16:28,860 --> 00:16:33,960
A teraz ciesz się
całym absurdem sytuacji.

152
00:16:35,570 --> 00:16:39,860
Mam prezencik dla twojego taty.

153
00:16:39,940 --> 00:16:45,240
Obiecasz, że go nie otworzysz?

154
00:16:45,320 --> 00:16:46,440
Oczywiście.

155
00:16:47,620 --> 00:16:51,860
Skoro o szkole mowa,
to Will i ja skończyliśmy ją

156
00:16:51,940 --> 00:16:57,560
dokładnie 30 lat temu,
więc słabo go pamiętam.

157
00:16:58,090 --> 00:17:00,270
Będę już zmykał.

158
00:17:00,900 --> 00:17:04,220
Tam się zatrzymam.

159
00:17:05,520 --> 00:17:09,500
Mam nadzieję, że się jeszcze spotkamy.

160
00:17:09,580 --> 00:17:10,720
Ja też.

161
00:17:52,050 --> 00:17:53,460
Jak ci idzie?

162
00:17:54,200 --> 00:17:59,440
Zbadałem wszystkie patowe partie,
jakie kiedykolwiek opisano

163
00:18:00,090 --> 00:18:04,280
i oprócz tego
kilka z tych mniej znanych,

164
00:18:04,360 --> 00:18:08,500
o których nie piszą w książkach,
ale to na nic.

165
00:18:08,580 --> 00:18:11,470
Nie było na świecie takiej partii,

166
00:18:11,560 --> 00:18:15,350
w której nie padłoby
co najmniej kilka figur.

167
00:18:16,050 --> 00:18:20,890
Klasyczny Herbstman:
kończysz z sześcioma.

168
00:18:20,970 --> 00:18:23,160
Mogę to poprawić,

169
00:18:23,240 --> 00:18:27,450
ale nawet jeśli ograniczę straty
do połowy i skończę z dwunastoma,

170
00:18:28,500 --> 00:18:32,750
to zginie sześć osób.

171
00:18:34,620 --> 00:18:35,890
Postaraj się.

172
00:18:36,770 --> 00:18:41,090
Earle to geniusz,
ale nie grzeszy cierpliwością.

173
00:18:41,180 --> 00:18:44,780
Nie chce pionków, mierzy najwyżej.

174
00:18:44,860 --> 00:18:48,710
Chroń figury, a zwłaszcza królową,
to go rozdrażnimy.

175
00:18:48,790 --> 00:18:50,970
Tak nie możesz.

176
00:18:52,020 --> 00:18:53,100
Moi uczniowie.

177
00:18:53,660 --> 00:18:58,520
Panie Martell, Andy przestawił skoczka
i nie zrobił zawijasa.

178
00:18:58,600 --> 00:19:01,230
Nie musisz go robić,
masz tylko możliwość.

179
00:19:01,550 --> 00:19:07,490
Skoczek musi zrobić zawijasa.

180
00:19:07,810 --> 00:19:10,990
- Za każdym razem?
- To przywilej.

181
00:19:11,230 --> 00:19:14,250
Żadna inna figura tak nie może.

182
00:19:15,130 --> 00:19:17,070
Dobrze.

183
00:19:20,190 --> 00:19:24,370
Niektórzy wcale nie wiedzą
tak dużo, jak im się wydaje.

184
00:19:26,480 --> 00:19:27,050
Szach.

185
00:19:37,390 --> 00:19:40,220
Uznaliśmy, że powinniśmy przyjść.

186
00:19:46,430 --> 00:19:52,570
Jak pan wie, znak na mojej szyi
pojawił się podczas zaginięcia.

187
00:19:52,650 --> 00:19:55,580
Oczywiście nie wiem,
jak do niego doszło.

188
00:19:56,280 --> 00:20:01,580
- Pani go dziś zobaczyła.
- Pieniek zauważył. Pamiętam.

189
00:20:01,960 --> 00:20:03,340
Co pani pamięta?

190
00:20:04,820 --> 00:20:07,050
Proszę spojrzeć na moją łydkę.

191
00:20:20,070 --> 00:20:26,890
Miałam siedem lat. Poszłam do lasu,
a gdy wróciłam, powiedzieli,

192
00:20:26,970 --> 00:20:30,150
że przepadłam na całą dobę.

193
00:20:30,810 --> 00:20:33,570
Pamiętam tylko błysk światła,

194
00:20:34,500 --> 00:20:37,450
a na mojej nodze pojawił się ten znak.

195
00:20:37,530 --> 00:20:40,110
Wszyscy widzieliśmy światło.

196
00:20:41,190 --> 00:20:44,880
I słyszeliśmy sowę.

197
00:20:46,120 --> 00:20:47,960
Tak, pamiętam.

198
00:20:48,440 --> 00:20:53,260
Widziałam to samo i słyszałam
jeszcze tylko jeden raz.

199
00:20:53,820 --> 00:20:56,460
Tuż przed śmiercią mojego męża.

200
00:20:57,150 --> 00:20:58,450
W pożarze.

201
00:21:07,390 --> 00:21:11,540
- Z czymś mi się to kojarzy.
- Tak.

202
00:21:12,100 --> 00:21:13,260
Z czym?

203
00:21:15,810 --> 00:21:17,490
Nie wiem.

204
00:21:20,520 --> 00:21:26,280
Nie chcę na prerii leżeć

205
00:21:26,360 --> 00:21:32,260
Gdzie wyją kojoty i wicher wieje

206
00:21:32,860 --> 00:21:38,220
W płytkim grobie, bez nadziei

207
00:21:38,310 --> 00:21:44,690
Nie chcę na pustej prerii leżeć

208
00:21:49,430 --> 00:21:51,360
Jeszcze nikt mi nigdy nie śpiewał.

209
00:21:52,920 --> 00:21:55,320
Raz czy dwa na pewno.

210
00:21:57,000 --> 00:21:59,440
Nie budzę takich uczuć.

211
00:22:01,110 --> 00:22:03,210
Chłopcy się mnie boją.

212
00:22:04,020 --> 00:22:08,380
Nie wiedzą, jaka jesteś naprawdę.

213
00:22:09,830 --> 00:22:12,430
Mnie chyba nikt nie zna.

214
00:22:14,880 --> 00:22:16,770
To jak ostrzeżenie.

215
00:22:17,970 --> 00:22:19,180
Wiem.

216
00:22:19,680 --> 00:22:22,270
Jeśli masz kogoś...

217
00:22:25,900 --> 00:22:29,810
Był ktoś, ale to minęło.

218
00:22:31,080 --> 00:22:32,320
Nie ma nikogo.

219
00:22:34,610 --> 00:22:36,860
Co chcesz jeszcze wiedzieć?

220
00:22:39,950 --> 00:22:41,950
Znasz więcej kowbojskich sztuczek?

221
00:22:43,350 --> 00:22:46,920
Jeśli wzięłaś arkan,
mogę złapać jakąś krowę.

222
00:22:48,510 --> 00:22:50,280
Tu ich nie ma.

223
00:22:50,370 --> 00:22:53,670
To psa, szopa, ptaka albo kunę.

224
00:23:21,400 --> 00:23:24,330
- Ktoś was odwiedził.
- Kto?

225
00:23:24,410 --> 00:23:28,620
Twój kolega ze studiów, Craig.

226
00:23:29,220 --> 00:23:31,210
Gerald Craig?

227
00:23:32,110 --> 00:23:33,340
To niemożliwe.

228
00:23:34,340 --> 00:23:37,780
Ależ tak. Był w drodze
na zjazd w Spokane.

229
00:23:37,860 --> 00:23:39,130
Wszystko o mnie wiedział.

230
00:23:46,970 --> 00:23:49,470
Zostawił swój numer i to.

231
00:23:55,040 --> 00:23:57,520
Mieszkałem z Geraldem w akademiku.

232
00:23:58,260 --> 00:24:03,810
Utonął na wycieczce po Snake River.
Byłem tam, próbowałem go ratować.

233
00:24:07,380 --> 00:24:08,990
To cmentarz.

234
00:24:17,260 --> 00:24:20,450
"Skoczek na F3".

235
00:24:25,310 --> 00:24:29,690
Ten człowiek jest niebezpieczny.
Nigdy więcej go nie wpuszczaj.

236
00:24:30,490 --> 00:24:32,260
Zaniosę to Cooperowi.

237
00:24:41,510 --> 00:24:44,790
Nadine, chcę powiedzieć, że....

238
00:24:48,150 --> 00:24:50,570
skoro spotykasz się z Mikiem...

239
00:24:50,660 --> 00:24:53,660
Jest tak cudnie, że aż wstyd.

240
00:24:54,840 --> 00:24:58,690
A skoro i ja kogoś mam...

241
00:25:00,400 --> 00:25:02,550
Nie wiem, czy to zrozumiesz...

242
00:25:02,630 --> 00:25:05,170
Oczywiście, że rozumiem.

243
00:25:05,780 --> 00:25:08,440
To nie jest mały kryzys.

244
00:25:09,100 --> 00:25:10,720
Nadine...

245
00:25:11,800 --> 00:25:14,930
wiesz, co sugeruje Ed?

246
00:25:15,010 --> 00:25:22,010
To oczywiste. Nie omawiamy
teorii względności, tylko zrywamy.

247
00:25:22,100 --> 00:25:24,560
Ostateczne zerwanie.

248
00:25:26,360 --> 00:25:30,050
W zasadzie o to chodzi...

249
00:25:30,140 --> 00:25:32,970
Ale to trochę poważniejsze.

250
00:25:33,060 --> 00:25:35,990
Jesteś taki zasadniczy.

251
00:25:37,040 --> 00:25:43,230
Jesteśmy w takim wieku,
nie róbmy z tego afery.

252
00:25:43,310 --> 00:25:44,610
Doktorze...

253
00:25:46,530 --> 00:25:51,030
Nie licz na żadne zaklęcia.

254
00:25:51,110 --> 00:25:55,950
To przypomina gojenie się rany.

255
00:25:56,470 --> 00:26:02,280
Wróci do rzeczywistości,
gdy poczuje się bezpiecznie.

256
00:26:02,360 --> 00:26:04,070
Czyli kiedy?

257
00:26:04,710 --> 00:26:09,680
Nie wiem. Rana jest dość duża.

258
00:26:11,120 --> 00:26:17,160
Nie życzę sobie
żadnych awantur z Mikiem.

259
00:26:17,250 --> 00:26:19,750
Nie chcę ataków zazdrości.

260
00:26:20,260 --> 00:26:24,170
No, może raz.

261
00:26:24,250 --> 00:26:25,630
Poddaję się.

262
00:26:28,680 --> 00:26:30,030
Nadine...

263
00:26:31,350 --> 00:26:35,000
Ed ma na myśli rozwód.

264
00:26:39,300 --> 00:26:43,500
Chyba oślepłam na lewe oko.

265
00:27:35,000 --> 00:27:35,910
Shelly?

266
00:27:40,510 --> 00:27:42,040
Widziałaś to?

267
00:27:42,950 --> 00:27:44,880
Miss Twin Peaks.

268
00:27:48,490 --> 00:27:49,840
Chyba żartujesz.

269
00:27:50,340 --> 00:27:52,470
Jest nagroda i stypendium.

270
00:27:53,370 --> 00:27:55,680
- A ty masz duże szanse.
- Nie wydaje mi się.

271
00:27:55,870 --> 00:27:58,980
To proste, palniesz mówkę
i odpowiesz na kilka pytań.

272
00:28:00,520 --> 00:28:05,690
Miss baru, co zrobiłabyś
w sprawie pokoju na świecie?

273
00:28:08,260 --> 00:28:11,080
Zebrałabym w krąg
przywódców wszystkich państw

274
00:28:11,160 --> 00:28:14,420
i kazałabym im podać sobie ręce.

275
00:28:14,510 --> 00:28:17,330
Wtedy nie da się zaciskać pięści.

276
00:28:17,730 --> 00:28:20,580
- Wygłupiasz się.
- Mam zamówienie.

277
00:28:32,240 --> 00:28:33,050
Pański hamburger.

278
00:28:34,990 --> 00:28:36,060
Tańczy pani?

279
00:28:37,660 --> 00:28:40,600
Szefowa namawia mnie
na udział w wyborach miss.

280
00:28:41,180 --> 00:28:46,980
Żartuje pani, ale nie brak pani urody.
Niech pani startuje.

281
00:28:48,240 --> 00:28:52,200
- Nie uważam się za ładną.
- To wychodzi z duszy.

282
00:29:08,620 --> 00:29:10,190
Podać coś do picia?

283
00:29:10,270 --> 00:29:12,120
Tak, poproszę...

284
00:29:12,780 --> 00:29:15,600
o filiżankę mocnej, czarnej kawy.

285
00:29:16,910 --> 00:29:17,970
Już się robi.

286
00:29:26,890 --> 00:29:28,190
Pani jest siostrą Normy.

287
00:29:28,600 --> 00:29:31,110
Annie. Skąd pan wie?

288
00:29:31,760 --> 00:29:33,950
Dale Cooper, z tutejszej policji.

289
00:29:35,660 --> 00:29:38,620
- Ma pan sporo pracy.
- Rzeczywiście.

290
00:29:41,570 --> 00:29:46,540
- Zostanie pani na dłużej?
- Może na bardzo długo.

291
00:29:47,870 --> 00:29:50,920
- Ze mną też tak było.
- Chyba to panu służy.

292
00:29:52,110 --> 00:29:53,260
To miasto umie zauroczyć.

293
00:30:07,280 --> 00:30:08,480
Zaparzyłam dość mocną.

294
00:30:24,510 --> 00:30:25,750
Jest w sam raz.

295
00:30:42,050 --> 00:30:44,480
Mamy kłopoty w Księgarni.

296
00:30:46,070 --> 00:30:48,420
Agencie Cooper, Hawk, szeryf oszalał.

297
00:30:48,500 --> 00:30:53,120
Porozwalał, co się dało,
i nie wiem, co robić.

298
00:30:53,210 --> 00:30:54,590
W porządku.

299
00:31:02,330 --> 00:31:06,960
Co słychać, posterunkowy Dale?

300
00:31:08,500 --> 00:31:11,460
No cóż... Sytuacja jest skomplikowana.

301
00:31:12,610 --> 00:31:16,880
Prawo ma wielką zaletę.
Nie oddycha i nie można go zabić!

302
00:31:25,100 --> 00:31:26,240
Mam pomysł.

303
00:31:27,990 --> 00:31:29,820
Oddaj mi broń.

304
00:31:37,810 --> 00:31:43,040
Nigdy w życiu
nie oddałem nikomu broni!

305
00:31:45,200 --> 00:31:47,640
Kiedyś trzeba zacząć.

306
00:31:52,130 --> 00:31:54,180
Wiesz, czego jeszcze nie zrobiłem?

307
00:31:57,710 --> 00:31:59,690
Nigdy nie popłynąłem za morze.

308
00:32:02,770 --> 00:32:04,650
Nie byłem w Chinach.

309
00:32:08,590 --> 00:32:10,460
Sama do mnie przyszła.

310
00:32:12,100 --> 00:32:14,410
I świat stał się lepszy.

311
00:32:16,060 --> 00:32:19,510
Wszystko było lepsze.

312
00:32:26,790 --> 00:32:30,420
Twoje życie wciąż należy do ciebie.
Nie zabrała go ze sobą.

313
00:32:30,500 --> 00:32:35,750
Powinienem ją stąd zabrać.
Wywieźć gdzieś daleko!

314
00:32:40,480 --> 00:32:42,130
Kochałem ją.

315
00:32:45,330 --> 00:32:47,530
Nie musiała umierać.

316
00:33:05,220 --> 00:33:06,550
Nie rozumiem tego.

317
00:33:08,950 --> 00:33:12,020
Nic nie rozumiem.

318
00:33:14,120 --> 00:33:15,780
Jak wszyscy.

319
00:33:22,470 --> 00:33:26,050
KSI�GARNIA

320
00:33:32,500 --> 00:33:34,680
Niech ktoś go pilnuje.

321
00:33:34,770 --> 00:33:37,170
Nigdy go takim nie widziałem.

322
00:33:37,250 --> 00:33:40,070
To jakby pójść
w swoje ulubione miejsce

323
00:33:40,150 --> 00:33:42,650
i znaleźć dziurę w miejscu jeziora.

324
00:33:43,480 --> 00:33:45,230
Josie miała moc.

325
00:33:46,400 --> 00:33:50,170
Ten, kto nie kocha łatwo,
kocha za bardzo.

326
00:33:50,970 --> 00:33:53,870
- To dobry człowiek.
- Najlepszy.

327
00:33:55,530 --> 00:33:56,910
Mówcie dalej.

328
00:34:01,180 --> 00:34:06,340
Apartament nowożeńców
dla państwa Hinkmanów?

329
00:34:06,430 --> 00:34:08,310
Zgadza się.

330
00:34:09,040 --> 00:34:14,760
Jesteśmy z Bozeman,
ale żona chciała obejrzeć okolicę.

331
00:34:15,340 --> 00:34:18,150
A okolicę macie tu bardzo malowniczą.

332
00:34:19,050 --> 00:34:21,410
- Państwo na dłużej?
- Za krótko.

333
00:34:21,500 --> 00:34:26,840
Nie, tylko do jutra. Potem jedziemy...

334
00:34:26,930 --> 00:34:29,570
- na ryby.
- Dobre.

335
00:34:29,650 --> 00:34:34,200
Chcemy już iść na górę,
ja i moja... pani Hinkman.

336
00:34:34,290 --> 00:34:35,870
Mike, to ty?

337
00:34:37,640 --> 00:34:38,960
Susan...

338
00:34:40,970 --> 00:34:42,010
Jak się masz?

339
00:34:43,220 --> 00:34:44,340
Dobrze.

340
00:34:45,720 --> 00:34:47,840
Do zobaczenia w szkole.

341
00:34:52,330 --> 00:34:54,080
Prosimy o pokój!

342
00:34:55,790 --> 00:34:57,730
Sami weźmiemy bagaże.

343
00:35:03,340 --> 00:35:06,600
Przede wszystkim
chcę wszystkim podziękować

344
00:35:06,680 --> 00:35:10,770
za niezwykle liczne przybycie.

345
00:35:10,850 --> 00:35:14,490
To bardzo budujące widzieć,
że tak wiele osób

346
00:35:15,290 --> 00:35:18,340
poważnie myśli o naszym środowisku.

347
00:35:19,200 --> 00:35:24,370
Akcja "Zatrzymać Ghostwood"
to nasz wspólny wysiłek,

348
00:35:24,880 --> 00:35:28,030
mający na celu
uniemożliwienie inwestycji,

349
00:35:28,120 --> 00:35:33,920
która zmieni cudowne lasy
w koszmarny park rozrywki,

350
00:35:34,010 --> 00:35:39,560
niszcząc przy tym nietknięte
od setek lat ostoje zwierząt.

351
00:35:39,650 --> 00:35:46,180
Małe światy, będące sanktuariami
wielu zagrożonych gatunków.

352
00:35:46,930 --> 00:35:52,580
Choćby naszej małej kuny świerkowej.

353
00:35:54,350 --> 00:35:56,120
Zanim przejdziemy do rzeczy...

354
00:35:57,990 --> 00:36:04,500
pragnę przypomnieć,
że ekologia to nie żaden luksus.

355
00:36:04,580 --> 00:36:08,030
Nie zajmuje się tylko tym, co ładne.

356
00:36:08,120 --> 00:36:11,210
To nauka o przetrwaniu.

357
00:36:12,460 --> 00:36:19,040
O tym, czy wszyscy
wyjdziemy z tego cało. I tyle.

358
00:36:20,680 --> 00:36:27,200
Ale żeby całkowicie
nie pozbawiać się przyjemności,

359
00:36:27,280 --> 00:36:31,260
zapraszam na pokaz mody.

360
00:36:32,500 --> 00:36:37,000
Jeśli odniosą państwo wrażenie,
że nasi modele wyglądają znajomo,

361
00:36:37,090 --> 00:36:40,180
to dlatego że są
mieszkańcami naszego miasta,

362
00:36:40,260 --> 00:36:42,720
którzy poświęcili nam swój czas.

363
00:36:42,800 --> 00:36:48,170
Zapraszam naszego gospodarza,
Richarda Tremayne.

364
00:36:53,820 --> 00:36:57,260
Dziękuję. Cóż za publiczność.

365
00:36:57,340 --> 00:37:00,470
Nasza mała Lucy ma na sobie urocze...

366
00:37:01,220 --> 00:37:06,010
połączenie ciepłej, północnej wygody
i południowej beztroski.

367
00:37:06,100 --> 00:37:10,830
Elegancki wełniany żakiet
na czerwonym tartanie,

368
00:37:11,480 --> 00:37:13,610
z odrobiną groszków.

369
00:37:14,910 --> 00:37:16,730
Piękne, prawda?

370
00:37:16,820 --> 00:37:21,530
Do tego czarna jak noc,
wąska spódniczka, mówiąca wprost:

371
00:37:21,610 --> 00:37:24,540
"Świecie, oto jestem!".

372
00:37:34,310 --> 00:37:40,510
Pan Brennan jest ubrany w golf
w kolorze strażackiej czerwieni

373
00:37:40,590 --> 00:37:44,250
i wełnianą kurtkę w kratkę.

374
00:37:44,330 --> 00:37:49,390
Stroju dopełniają kraciaste bryczesy.

375
00:37:49,480 --> 00:37:51,400
To strój dla mężczyzny,

376
00:37:51,480 --> 00:37:57,330
który chce wyglądać elegancko
podczas codziennego wypoczynku

377
00:37:57,410 --> 00:38:00,900
albo gdy wybiera się
na konną przejażdżkę w nieznane.

378
00:38:03,290 --> 00:38:04,380
Dziękuję, Andy.

379
00:38:13,800 --> 00:38:19,080
W tym tempie nie zdążymy.
Spróbujmy trochę go pogonić.

380
00:38:19,170 --> 00:38:21,650
- Spociłem się.
- Nic dziwnego.

381
00:38:21,730 --> 00:38:25,240
Czuję się w tej wełnie jak owca.

382
00:38:25,320 --> 00:38:30,280
A pan Tremayne
napala się na te dziewczyny.

383
00:38:30,850 --> 00:38:32,110
Idź.

384
00:38:34,600 --> 00:38:41,700
- To chyba nasz przyjaciel sekwoi.
- Catherine! Miło, że przyszłaś.

385
00:38:41,790 --> 00:38:44,870
Skarbie, kogo ty chcesz nabrać?

386
00:38:45,480 --> 00:38:50,040
Jestem stuprocentowo szczery.

387
00:38:50,120 --> 00:38:53,180
Daj spokój. Do kogo ta mowa?

388
00:38:54,260 --> 00:38:56,560
Mamy te same metody,

389
00:38:57,390 --> 00:39:00,620
a ciebie zawsze łatwo było przejrzeć.

390
00:39:00,700 --> 00:39:02,180
Ale to...

391
00:39:02,930 --> 00:39:04,680
Catherine...

392
00:39:05,110 --> 00:39:11,060
Czy kiedykolwiek przeżyłaś coś,
co całkiem cię odmieniło?

393
00:39:11,140 --> 00:39:15,360
Daruj sobie bzdury
o odrodzeniu i mów wprost.

394
00:39:15,440 --> 00:39:20,580
Nie zdołasz mi przeszkodzić,
więc co zamierzasz osiągnąć?

395
00:39:23,080 --> 00:39:27,900
Pierwszą rysę na najbardziej
zatwardziałym sumieniu

396
00:39:27,990 --> 00:39:30,350
na Północnym Zachodzie.

397
00:39:30,440 --> 00:39:33,880
Mam nadzieję, że cię zmienię.

398
00:39:38,750 --> 00:39:42,320
Znam twoje plany co do Ghostwood.

399
00:39:42,400 --> 00:39:46,890
Przyznaję, że kiedyś myślałem
dokładnie tak samo.

400
00:39:47,500 --> 00:39:52,570
I nieważne, ile interesów zrobiłem
i ile zbiłem na tym kasy,

401
00:39:52,660 --> 00:39:58,190
w duszy wciąż byłem
beznadziejną szumowiną.

402
00:39:59,500 --> 00:40:03,740
Teraz się przekonałem,
że jedyne, co daje radość życia

403
00:40:04,050 --> 00:40:07,910
i prawdziwą satysfakcję, to dawanie.

404
00:40:09,200 --> 00:40:13,710
Nie czekaj z tym odkryciem,
aż spoczniesz na łożu śmierci.

405
00:40:13,950 --> 00:40:19,130
Boże... ty chyba naprawdę tak myślisz.

406
00:40:20,740 --> 00:40:21,940
Tak.

407
00:40:22,640 --> 00:40:27,330
Dla ciebie, siebie i dla przyszłości.

408
00:40:31,150 --> 00:40:40,160
Skorzystaj z okazji, by odrzucić
megalomanię i egocentryzm,

409
00:40:40,240 --> 00:40:44,810
wystawiając czek na pokaźną sumkę.

410
00:41:02,270 --> 00:41:05,320
A teraz przedstawimy państwu
zwierzaka, którego dom

411
00:41:05,400 --> 00:41:09,270
jest zagrożony przez planowaną
budowę w Ghostwood.

412
00:41:09,350 --> 00:41:11,260
Panie Pinkle?

413
00:41:14,340 --> 00:41:17,780
Dziękuję, Dick. Dziękuję.

414
00:41:20,850 --> 00:41:24,030
Witam, co słychać?

415
00:41:28,530 --> 00:41:29,870
To dobrze.

416
00:41:30,810 --> 00:41:34,830
Proszę państwa,
mam ze sobą zwierzaka,

417
00:41:34,910 --> 00:41:39,790
który nie wie,
jak straszny czeka go los.

418
00:41:39,880 --> 00:41:45,560
Jest bezradny w oszalałym świecie.
W oszalałym...

419
00:41:47,870 --> 00:41:51,750
Żeby nie przedłużać...

420
00:41:54,470 --> 00:42:00,990
przedstawiam państwu kunę świerkową.

421
00:42:02,500 --> 00:42:10,190
Należy do rodziny łasicowatych.
Nie jest agresywna wobec ludzi

422
00:42:10,270 --> 00:42:18,960
i ma śliczne futerko, a na dodatek
kontroluje liczebność gryzoni.

423
00:42:19,600 --> 00:42:28,190
Niestety są z natury ciekawskie
i bardzo często wpadają w pułapki.

424
00:42:28,270 --> 00:42:32,560
Pociągają je też
jasne, lśniące przedmioty.

425
00:42:32,650 --> 00:42:34,360
Jak mój gors.

426
00:42:36,200 --> 00:42:40,660
To zadziwiające,
że uwielbiają pewne zapachy,

427
00:42:40,750 --> 00:42:44,360
jak na przykład tanią wodę kolońską.

428
00:42:44,450 --> 00:42:47,850
Co to? Chyba chce ci dać buziaka.

429
00:42:47,930 --> 00:42:49,510
Panie Pinkle...

430
00:42:54,030 --> 00:42:56,610
- To nic takiego, pocałujcie się.
- Co?

431
00:42:57,480 --> 00:42:59,710
No, śmiało.

432
00:42:59,790 --> 00:43:02,110
Cześć, malutki...

433
00:43:14,410 --> 00:43:19,480
Bez paniki, jest niegroźna!
I poważnie zagrożona!

434
00:43:32,980 --> 00:43:34,200
Proszę o spokój!

435
00:43:36,660 --> 00:43:39,770
Prosimy pozostać na miejscach.

436
00:43:42,360 --> 00:43:44,540
- W porządku?
- Tak.

437
00:43:45,410 --> 00:43:48,950
- To tylko mała kuna.
- Pomożemy im?

438
00:43:49,040 --> 00:43:52,070
Po co? Trochę ruchu dobrze im zrobi.

439
00:43:53,980 --> 00:43:56,940
- Przyszedłeś na pokaz?
- Nie. Po ciebie.

