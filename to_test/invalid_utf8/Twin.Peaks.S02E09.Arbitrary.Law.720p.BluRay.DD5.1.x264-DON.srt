1
00:00:28,920 --> 00:00:33,570
MIASTECZKO TWIN PEAKS

2
00:02:35,010 --> 00:02:38,080
To robota tej samej bestii,
która zabiła Laurę.

3
00:02:38,730 --> 00:02:43,050
Jest wiadomość: litera O
pod paznokciem Maddie.

4
00:02:43,140 --> 00:02:47,240
- W dłoni miała zwierzęcą sierść.
- Jaką?

5
00:02:47,330 --> 00:02:50,280
Z białego lisa, nasyconą formaldehydem.

6
00:02:50,370 --> 00:02:51,630
Zwierzak był wypchany.

7
00:02:52,830 --> 00:02:56,130
Zadzwonię do Lelanda
po adres rodziców Maddie.

8
00:02:56,220 --> 00:02:58,110
Jeszcze nie.

9
00:02:58,200 --> 00:03:00,910
- Daj mi 24 godziny.
- Na co?

10
00:03:00,990 --> 00:03:04,240
- Aż to skończę.
- Cooper.

11
00:03:07,090 --> 00:03:09,000
Mała uwaga...

12
00:03:10,940 --> 00:03:13,380
Nie wiem, do czego to prowadzi,

13
00:03:13,470 --> 00:03:17,710
ale tylko ty potrafisz to rozgryźć.

14
00:03:18,060 --> 00:03:24,130
Podążaj za wizjami,
stań na krawędzi wulkanu i zatańcz,

15
00:03:24,410 --> 00:03:27,130
tylko złap tego bydlaka,
zanim znów to zrobi.

16
00:03:33,320 --> 00:03:35,370
Nie wiem, od czego zacząć.

17
00:03:35,450 --> 00:03:39,190
Znalazłeś ścieżkę. Nie pytaj,
dokąd wiedzie, tylko nią podążaj.

18
00:03:50,570 --> 00:03:52,670
Myślałam, że nie przyjdziesz.

19
00:03:53,650 --> 00:03:55,450
Chciałem się przejechać.

20
00:03:55,660 --> 00:03:58,870
Silnik brzmiał jak anielski chór.

21
00:03:58,950 --> 00:04:02,410
- O czym śpiewał?
- A jak myślisz?

22
00:04:02,500 --> 00:04:06,110
O zeszłej nocy? Bo ja bym mogła.

23
00:04:06,190 --> 00:04:08,160
Mam coś dla ciebie.

24
00:04:12,990 --> 00:04:14,660
Otwórz.

25
00:04:21,000 --> 00:04:23,140
Nie znałem rozmiaru.

26
00:04:23,230 --> 00:04:26,070
Dziewczyna w sklepie
miała duże dłonie,

27
00:04:26,700 --> 00:04:29,640
ale myślę...

28
00:04:30,500 --> 00:04:31,990
Jak ulał.

29
00:04:36,180 --> 00:04:39,830
- Nie powinniśmy się rozstawać.
- Naprawdę?

30
00:04:40,400 --> 00:04:42,000
Jeśli chcesz.

31
00:04:43,360 --> 00:04:45,250
Bardzo.

32
00:05:00,970 --> 00:05:03,480
Skąd wzięłaś te jajka?

33
00:05:03,800 --> 00:05:06,440
Czyli ci nie smakują?

34
00:05:06,690 --> 00:05:08,690
Zaraz zwymiotuję.

35
00:05:11,930 --> 00:05:14,830
Nie możesz powiedzieć nic miłego?

36
00:05:14,910 --> 00:05:18,120
Starałam się. Wciąż się staram,

37
00:05:18,490 --> 00:05:21,120
ale tobie zawsze mało.

38
00:05:21,200 --> 00:05:24,790
Nie podoba ci się nic, co zrobię.

39
00:05:25,370 --> 00:05:28,340
Nieprawda. Jadłospisy są urocze.

40
00:05:29,320 --> 00:05:31,250
Żartuję.

41
00:05:31,330 --> 00:05:34,050
Ale coś ci podpowiem.

42
00:05:34,370 --> 00:05:43,640
Najlepszy jest omlet z białą
cielęcą kiełbasą i smardzami.

43
00:05:44,180 --> 00:05:47,480
Dzięki. Skoczę po smardze.

44
00:06:05,450 --> 00:06:07,260
Co powiedziałeś?

45
00:06:08,500 --> 00:06:11,810
Nic. Cześć, Donna.

46
00:06:12,560 --> 00:06:14,240
Cześć, James.

47
00:06:14,330 --> 00:06:16,780
Co mówiłeś?

48
00:06:16,870 --> 00:06:19,430
- To po francusku.
- Wiem. Powiedz.

49
00:06:19,670 --> 00:06:21,680
O co chodzi?

50
00:06:29,360 --> 00:06:30,440
Znasz panią Tremond?

51
00:06:31,170 --> 00:06:33,150
- Kogo?
- Panią Tremond.

52
00:06:33,410 --> 00:06:35,690
Jej wnuk chce być magikiem.
On to powiedział.

53
00:06:36,270 --> 00:06:37,860
Nie, pan Smith.

54
00:06:40,510 --> 00:06:43,410
- Harold?
- Zostawił list.

55
00:06:45,360 --> 00:06:49,330
To znaczy: "Jestem samotną duszą".

56
00:06:50,860 --> 00:06:52,550
Idę do agenta Coopera.

57
00:06:55,470 --> 00:06:57,740
Co jest grane?

58
00:07:07,030 --> 00:07:10,960
To pani Tremond
wskazała mi Harolda Smitha.

59
00:07:11,050 --> 00:07:16,660
To słowa jej wnuka.
I z samobójczego listu Harolda.

60
00:07:16,750 --> 00:07:18,930
- Może Harold też to usłyszał.
- Nie.

61
00:07:19,020 --> 00:07:21,010
W tych słowach zawarł swój świat.

62
00:07:21,090 --> 00:07:25,830
Ten list to wiadomość.
I prowadzi nas tutaj.

63
00:07:28,590 --> 00:07:32,550
Jest stara. Jeśli wnuka nie ma,
trzeba poczekać.

64
00:07:35,130 --> 00:07:37,340
Słucham państwa?

65
00:07:37,610 --> 00:07:41,170
- Zastaliśmy panią Tremond?
- To ja.

66
00:07:41,580 --> 00:07:43,470
Więc rozmawiałam z pani matką.

67
00:07:44,280 --> 00:07:48,010
Z matką? Nie żyje od trzech lat.
Mieszkam tu sama.

68
00:07:48,700 --> 00:07:52,470
Była tu. Rozmawiałam
z nią i z jej wnukiem.

69
00:07:52,560 --> 00:07:56,090
- Nie mam dzieci.
- Byli tutaj. Oboje.

70
00:07:56,180 --> 00:07:58,490
- Donna, lepiej już chodźmy.
- Donna?

71
00:07:58,730 --> 00:08:01,570
- Jesteś Donna Hayward?
- Tak.

72
00:08:01,930 --> 00:08:03,780
Chwileczkę.

73
00:08:04,230 --> 00:08:08,370
- Widzę ją pierwszy raz w życiu.
- Ale dostarczasz tu posiłki.

74
00:08:08,450 --> 00:08:12,290
Na drugi dzień po śmierci Harolda
znalazłam to wśród listów.

75
00:08:12,370 --> 00:08:15,250
Chciałam zwrócić. Jest do ciebie.

76
00:08:18,610 --> 00:08:20,240
Dziękuję.

77
00:08:23,350 --> 00:08:26,820
- To pismo Harolda.
- Otwórz.

78
00:08:35,870 --> 00:08:39,190
To z pamiętnika Laury. Jedna kartka.

79
00:08:39,440 --> 00:08:40,690
Czytaj.

80
00:08:44,050 --> 00:08:45,780
"22 lutego.

81
00:08:46,160 --> 00:08:48,820
Miałam w nocy dziwny sen.

82
00:08:48,910 --> 00:08:52,430
Byłam w czerwonym pokoju
z ubranym na czerwono karłem,

83
00:08:52,520 --> 00:08:56,370
a w fotelu siedział starszy człowiek.
Próbowałam z nim rozmawiać

84
00:08:56,450 --> 00:09:00,430
i powiedzieć mu, kim jest Bob,
żeby mi pomógł.

85
00:09:00,510 --> 00:09:03,690
Moje słowa były zwolnione i dziwne.

86
00:09:03,780 --> 00:09:06,450
Trudno było mi mówić.

87
00:09:06,540 --> 00:09:10,050
Wstałam i podeszłam do niego,

88
00:09:10,130 --> 00:09:14,030
pochyliłam się
i wyszeptałam mu to do ucha.

89
00:09:16,930 --> 00:09:21,510
Boba trzeba powstrzymać.
Boi się tylko jednego człowieka.

90
00:09:21,590 --> 00:09:23,580
Sam mi mówił.

91
00:09:23,660 --> 00:09:26,380
To jakiś Mike.

92
00:09:26,460 --> 00:09:29,510
Ciekawe, czy to Mike był w moim śnie.

93
00:09:29,590 --> 00:09:33,670
Choć to był sen, może mnie usłyszał.

94
00:09:33,750 --> 00:09:36,880
Bo na jawie nikt mi nie uwierzy.

95
00:09:38,200 --> 00:09:39,920
23 lutego.

96
00:09:40,230 --> 00:09:42,250
Dzisiejszej nocy umrę.

97
00:09:44,430 --> 00:09:49,190
Wiem, że muszę,
bo tylko tak ucieknę przed Bobem.

98
00:09:49,280 --> 00:09:51,980
Tylko tak wyrwę go z duszy.

99
00:09:52,810 --> 00:09:55,950
Pragnie mnie, czuję żar jego ognia.

100
00:09:56,030 --> 00:09:58,730
Ale gdy umrę,
już mnie więcej nie skrzywdzi".

101
00:10:04,710 --> 00:10:07,260
Oboje mieliśmy ten sam sen.

102
00:10:07,350 --> 00:10:10,540
- To niemożliwe.
- A jednak.

103
00:10:14,530 --> 00:10:16,920
Odwieź Donnę do domu.

104
00:10:17,000 --> 00:10:19,150
Jadę do Gerarda.

105
00:10:34,020 --> 00:10:38,480
Jest odwodniony i źle oddycha.
Musi dostać leki.

106
00:10:38,560 --> 00:10:40,940
Mike, odezwij się.

107
00:10:41,030 --> 00:10:42,900
Może umrzeć!

108
00:10:42,990 --> 00:10:44,580
Mike?

109
00:10:50,340 --> 00:10:52,370
Bob znów zabił.

110
00:10:53,810 --> 00:10:56,540
Muszę coś ustalić, pomóż mi.

111
00:10:58,190 --> 00:11:01,620
Bob był w moim śnie.
Była tam także Laura i ty.

112
00:11:02,490 --> 00:11:04,820
W noc przed śmiercią
Laura śniła o mnie.

113
00:11:04,910 --> 00:11:09,590
Mieliśmy ten sam sen.
Muszę to wreszcie rozgryźć.

114
00:11:09,840 --> 00:11:14,430
Gdy Bob i ja wspólnie zabijaliśmy,

115
00:11:15,240 --> 00:11:18,820
łączyła nas doskonała więź...

116
00:11:18,910 --> 00:11:24,360
pragnienia, satysfakcja, złoty krąg...

117
00:11:25,210 --> 00:11:26,520
Złoty krąg...

118
00:11:29,850 --> 00:11:32,970
Obrączka.

119
00:11:33,050 --> 00:11:35,030
Oddałem ją olbrzymowi.

120
00:11:35,480 --> 00:11:39,660
- Znamy go.
- Więc istnieje?

121
00:11:40,370 --> 00:11:43,280
Tak jak ja.

122
00:11:44,220 --> 00:11:48,040
Pomoże ci znaleźć Boba.

123
00:11:48,120 --> 00:11:51,670
- Jak?
- Musisz go poprosić.

124
00:11:52,390 --> 00:11:53,630
Jak to zrobić?

125
00:11:54,850 --> 00:12:00,660
Znasz wszystkie wskazówki.

126
00:12:02,410 --> 00:12:07,240
Odpowiedź nie kryje się tutaj.

127
00:12:08,270 --> 00:12:11,290
Jest tu.

128
00:12:11,380 --> 00:12:12,700
Nie rozumiem.

129
00:12:14,240 --> 00:12:17,560
Taka odpowiedzialność...

130
00:12:45,350 --> 00:12:48,440
Wiem o panu.

131
00:12:50,190 --> 00:12:53,020
Mleko szybko stygnie,

132
00:12:53,100 --> 00:12:56,580
ale teraz już się rozgrzewa.

133
00:13:00,270 --> 00:13:02,750
Rozgrzewa się...

134
00:13:13,590 --> 00:13:15,980
Zabieramy wszystko z szuflad.

135
00:13:16,070 --> 00:13:18,410
Mamy biling telefoniczny.

136
00:13:18,490 --> 00:13:20,540
Spójrz, to noc śmierci Laury.

137
00:13:20,630 --> 00:13:22,810
Leland miał rację.

138
00:13:22,890 --> 00:13:26,400
Ben stąd do niej dzwonił.

139
00:13:26,490 --> 00:13:28,110
Biały lis.

140
00:13:28,200 --> 00:13:31,990
Biały, martwy i wypchany.
Maddie tu była.

141
00:13:32,070 --> 00:13:35,790
Ben ją zabił i wywiózł pod wodospad.

142
00:13:35,880 --> 00:13:39,400
Zabił i Laurę, i Maddie.

143
00:13:39,490 --> 00:13:43,460
Madelaine Ferguson
zginęła między 22-gą a północą.

144
00:13:43,540 --> 00:13:46,240
Pasuje. Zgarnęliśmy Bena po północy.

145
00:13:46,330 --> 00:13:48,490
Dokładniej nie umiem.

146
00:13:49,730 --> 00:13:51,940
Wynik badania krwi.

147
00:13:59,970 --> 00:14:02,100
Naprawię to w parę minut.

148
00:14:02,500 --> 00:14:08,520
To wina czujników. Wyreguluję je,
żeby was nie zalało.

149
00:14:09,060 --> 00:14:11,150
Pomówmy o moim dziecku.

150
00:14:11,450 --> 00:14:14,230
- Naszym.
- Może.

151
00:14:14,310 --> 00:14:16,650
Co "może"?

152
00:14:16,730 --> 00:14:19,700
Może jest twoje, a może nie.

153
00:14:20,080 --> 00:14:21,700
Dick?

154
00:14:21,780 --> 00:14:23,910
Boże! On jest ojcem?

155
00:14:24,100 --> 00:14:27,050
Najpierw myślałam, że ty,

156
00:14:27,140 --> 00:14:29,580
ale mówiłeś, że nie masz plemników.

157
00:14:31,530 --> 00:14:36,170
Wtedy uznałam, że to Dick,
a teraz plemniki wróciły.

158
00:14:36,260 --> 00:14:39,680
Czyli że szanse są pół na pół.

159
00:14:41,790 --> 00:14:44,670
Andy, dokąd idziesz?

160
00:14:44,760 --> 00:14:46,910
Co robisz?

161
00:14:49,110 --> 00:14:51,450
Dokąd dzwonisz?

162
00:14:51,540 --> 00:14:54,560
Z Richardem Tremaynem,
dział mody męskiej.

163
00:14:54,640 --> 00:14:57,020
Zaczekam.

164
00:14:57,110 --> 00:14:59,160
Nie zrobisz mu krzywdy?

165
00:15:01,610 --> 00:15:04,240
Tu Andy Brennan.

166
00:15:04,330 --> 00:15:06,340
Musimy pogadać.

167
00:15:07,370 --> 00:15:10,370
Jeśli znajdziesz chwilę.

168
00:15:23,220 --> 00:15:25,530
Pan Tojamura.

169
00:15:26,010 --> 00:15:28,940
Co za miłe odwiedziny.

170
00:15:29,020 --> 00:15:30,810
Zamknęli pana.

171
00:15:32,190 --> 00:15:34,160
Tak...

172
00:15:34,240 --> 00:15:37,530
ale zapewniam,
że to chwilowe niedogodności.

173
00:15:38,410 --> 00:15:40,990
Mam dokumenty do podpisu.

174
00:15:42,160 --> 00:15:44,350
Kontrakt na Ghostwood.

175
00:15:46,430 --> 00:15:48,520
Panie Tojamura...

176
00:15:48,600 --> 00:15:52,090
obawiam się, że mamy kłopot.

177
00:15:52,850 --> 00:15:53,890
Kłopot?

178
00:15:53,980 --> 00:15:58,340
Nieprzewidziane trudności
natury prawnej oraz...

179
00:15:59,940 --> 00:16:03,890
Nie mogę teraz zawrzeć tej umowy.

180
00:16:03,980 --> 00:16:07,730
Proszę mi zatem zwrócić pięć milionów.

181
00:16:10,200 --> 00:16:14,190
Bardzo bym chciał.
Tak jak podpisać kontrakt.

182
00:16:15,880 --> 00:16:17,890
Panie Tojamura...

183
00:16:18,680 --> 00:16:22,570
jak pan widzi, siedzę w celi.

184
00:16:23,790 --> 00:16:25,860
Fałszywie oskarżony.

185
00:16:27,300 --> 00:16:34,340
Mój genialny brat
szuka mi lepszego adwokata.

186
00:16:34,430 --> 00:16:37,470
Dopóki nie wypowie się prawo

187
00:16:37,550 --> 00:16:43,890
i nie odetchnę powietrzem wolności...

188
00:16:45,970 --> 00:16:49,270
proszę o wyrozumiałość.

189
00:16:50,880 --> 00:16:56,320
W mojej duszy zapadła ciemna noc.

190
00:16:57,040 --> 00:16:59,640
Muszę w niej odnaleźć...

191
00:17:00,510 --> 00:17:04,660
mały płomyczek nadziei.

192
00:17:05,660 --> 00:17:08,330
Przepraszam. Ponoszą mnie emocje.

193
00:17:23,380 --> 00:17:24,880
Catherine?

194
00:17:25,300 --> 00:17:27,980
Benjaminie Horne...

195
00:17:28,070 --> 00:17:32,330
ty obmierzły szczurze.

196
00:17:32,420 --> 00:17:38,650
Zmienię w piekło to, co ci zostało
z tego nędznego życia.

197
00:17:38,730 --> 00:17:41,550
Catherine!

198
00:17:43,150 --> 00:17:45,510
Boże!

199
00:17:45,600 --> 00:17:47,620
To ty!

200
00:17:54,400 --> 00:18:01,380
Powiedz szeryfowi, że spędziliśmy razem
noc, kiedy zginęła Laura.

201
00:18:03,620 --> 00:18:07,260
Mam cię błagać? Chcesz? To będę błagał.

202
00:18:07,920 --> 00:18:11,350
Przepiszesz na mnie tartak i Ghostwood?

203
00:18:12,820 --> 00:18:14,020
Natychmiast.

204
00:18:18,300 --> 00:18:22,070
Świetne przebranie! Genialne!

205
00:18:23,570 --> 00:18:30,070
Podpisane... i doręczone.

206
00:18:30,630 --> 00:18:34,790
Teraz powiesz szeryfowi? Powiesz?

207
00:18:35,500 --> 00:18:37,500
Zastanowię się.

208
00:18:37,710 --> 00:18:41,000
Catherine, zaczekaj.

209
00:18:42,320 --> 00:18:45,760
- Przecież to prawda.
- Och, Ben...

210
00:18:45,980 --> 00:18:49,040
Przez całe dorosłe życie
okłamywaliśmy się,

211
00:18:49,130 --> 00:18:51,730
więc po co to psuć, mówiąc prawdę?

212
00:19:03,450 --> 00:19:07,560
Donna! Jak miło, że wpadłaś.

213
00:19:08,270 --> 00:19:12,350
Od tak dawna nie mogliśmy
spokojnie porozmawiać.

214
00:19:12,440 --> 00:19:14,910
Chcesz lemoniady?

215
00:19:15,130 --> 00:19:19,080
Nie, dziękuję. Spieszę się.

216
00:19:19,160 --> 00:19:24,670
To dla Maddie. Nagraliśmy z Jamesem
piosenkę i to kopia dla niej.

217
00:19:25,060 --> 00:19:28,720
Jesteś kochana. Mówiła o tym

218
00:19:28,920 --> 00:19:31,740
i że doskonale się przy tym bawiła.

219
00:19:31,820 --> 00:19:35,890
Mógłby pan jej to wysłać?

220
00:19:35,980 --> 00:19:38,460
To żaden kłopot.

221
00:19:42,870 --> 00:19:44,660
Coś nie tak?

222
00:19:44,750 --> 00:19:48,610
Nie... tylko okulary.

223
00:19:51,450 --> 00:19:53,240
To Laury, mam je od Maddie.

224
00:19:54,360 --> 00:19:56,670
Dlatego je poznałem.

225
00:20:12,570 --> 00:20:13,590
Panie Palmer...

226
00:20:15,040 --> 00:20:18,510
wiedział pan o tajnym pamiętniku Laury?

227
00:20:19,200 --> 00:20:20,710
O dzienniku? Tak.

228
00:20:20,790 --> 00:20:23,480
Był w jej pokoju.

229
00:20:24,590 --> 00:20:28,760
Ten zabrała policja, ale był inny.

230
00:20:28,850 --> 00:20:31,660
Nawet ja nie wiedziałam, a pan?

231
00:20:33,240 --> 00:20:34,200
Nie.

232
00:20:34,740 --> 00:20:37,030
Dała go Haroldowi Smithowi.

233
00:20:38,850 --> 00:20:42,090
Zabił się parę dni temu.

234
00:20:42,180 --> 00:20:45,810
- Pamiętnik był u niego.
- Boże!

235
00:20:46,510 --> 00:20:48,700
Nie wiedziałem.

236
00:20:49,950 --> 00:20:52,590
Chciałabym wiedzieć, co w nim jest.

237
00:20:52,900 --> 00:20:55,130
Wciąż myślę o Laurze.

238
00:21:00,900 --> 00:21:02,470
Przepraszam.

239
00:21:08,310 --> 00:21:11,110
Tak, Beth. Jak się masz?

240
00:21:12,140 --> 00:21:14,200
Od wczoraj nie.

241
00:21:14,670 --> 00:21:17,700
Skądże. Sam ją odwiozłem na autobus.

242
00:21:20,700 --> 00:21:24,620
To się na pewno jakoś wyjaśni.

243
00:21:24,710 --> 00:21:28,820
Oczywiście, zadzwonię za godzinkę.

244
00:21:28,910 --> 00:21:30,000
Cześć.

245
00:21:39,130 --> 00:21:40,220
Chodzi o Maddie?

246
00:21:41,710 --> 00:21:44,180
Dziwna sprawa.

247
00:21:44,270 --> 00:21:45,600
Nie dotarła do domu.

248
00:21:46,090 --> 00:21:47,650
Boże!

249
00:21:54,500 --> 00:21:57,410
Nie martw się.

250
00:21:58,650 --> 00:22:00,830
Przyniosę lemoniady,

251
00:22:01,220 --> 00:22:04,250
usiądziemy, zastanowimy się

252
00:22:04,650 --> 00:22:07,210
i na pewno coś wymyślimy.

253
00:22:07,880 --> 00:22:09,770
Wspólnie.

254
00:22:49,380 --> 00:22:54,090
Czekała na nas, a ja myślałam,
że przecież ją odwiedzę.

255
00:22:54,180 --> 00:22:56,280
Do Missoula nie jest daleko.

256
00:22:56,370 --> 00:23:00,140
Za bardzo bierzesz to sobie do serca.

257
00:23:00,230 --> 00:23:02,740
Nic jej nie jest.

258
00:23:04,160 --> 00:23:07,850
Wiem, jak ci poprawić humor.
To bardzo proste.

259
00:23:08,420 --> 00:23:10,470
Mnie zawsze pomaga.

260
00:23:54,120 --> 00:23:56,200
Mogę cię prosić do tańca?

261
00:24:37,790 --> 00:24:39,710
Zaczekaj tutaj.

262
00:24:48,390 --> 00:24:49,930
Harry?

263
00:24:50,020 --> 00:24:53,660
- Musisz nam pomóc.
- O co chodzi?

264
00:24:53,750 --> 00:24:56,820
- Doszło do kolejnego morderstwa.
- Boże!

265
00:24:56,900 --> 00:24:59,250
Nie powiem jak i dlaczego,
ale musisz nam pomóc.

266
00:24:59,340 --> 00:25:01,630
- I to zaraz.
- Oczywiście.

267
00:25:01,720 --> 00:25:03,700
Wezmę płaszcz.

268
00:25:35,710 --> 00:25:37,800
Co jest? O co chodzi?

269
00:25:38,470 --> 00:25:41,690
- O Maddie.
- Co z nią?

270
00:25:41,780 --> 00:25:43,010
Nie żyje.

271
00:25:43,780 --> 00:25:47,380
Zabił ją ten sam morderca co Laurę.

272
00:25:51,630 --> 00:25:54,510
Słyszałam, jak pan Palmer
rozmawiał z jej matką.

273
00:25:54,600 --> 00:25:58,990
Nie dojechała do domu.
Wtedy przyszedł szeryf Truman.

274
00:25:59,080 --> 00:26:01,180
- Mogliśmy jej pomóc.
- Jak?

275
00:26:01,260 --> 00:26:02,710
- Mogliśmy pomóc.
- Jak?!

276
00:26:02,800 --> 00:26:04,640
Nie wiem!

277
00:26:06,920 --> 00:26:09,740
- To jest złe.
- Co?

278
00:26:10,540 --> 00:26:12,080
My?

279
00:26:15,250 --> 00:26:19,420
- Muszę iść.
- To nie nasza wina, zostań!

280
00:26:19,510 --> 00:26:24,390
To bez znaczenia, jak wszystko,
co robimy. Nic się nie zmieni.

281
00:26:24,470 --> 00:26:29,130
My jesteśmy szczęśliwi,
a świat się wali.

282
00:26:31,090 --> 00:26:32,350
Nie odchodź!

283
00:26:34,500 --> 00:26:36,520
Nie zostawiaj mnie!

284
00:27:21,620 --> 00:27:23,210
Harry.

285
00:27:24,930 --> 00:27:26,260
Lelandzie.

286
00:27:26,350 --> 00:27:29,660
Po co tu przyszliśmy?

287
00:27:30,920 --> 00:27:33,090
Czy to jakieś spotkanie?

288
00:27:33,180 --> 00:27:34,810
Tak.

289
00:27:34,900 --> 00:27:36,760
Z mordercą?

290
00:27:36,840 --> 00:27:39,730
Nie wiem. Może.

291
00:27:43,430 --> 00:27:45,230
Ed, dobrze że jesteś.

292
00:27:45,320 --> 00:27:50,930
Poprzestawiajmy meble,
na środku ma być pusto.

293
00:27:52,340 --> 00:27:53,830
Oczywiście.

294
00:28:04,510 --> 00:28:07,980
No proszę, wszyscy w komplecie.

295
00:28:10,490 --> 00:28:14,530
Bobby, przewieź tu Leo.
Musicie mnie dobrze słyszeć.

296
00:28:14,970 --> 00:28:16,980
Hawk, dorób sobie.

297
00:28:24,620 --> 00:28:27,630
Panowie, dwa dni temu młoda dziewczyna

298
00:28:27,710 --> 00:28:32,340
została zamordowana przez,
jak sądzę, zabójcę Laury Palmer.

299
00:28:33,260 --> 00:28:37,680
Podejrzewam, że sprawca jest w tej sali.

300
00:28:38,420 --> 00:28:43,850
Jako agent FBI szukam
prostych odpowiedzi na trudne pytania.

301
00:28:44,090 --> 00:28:47,770
Ścigając zabójcę Laury,
odwoływałem się do przepisów,

302
00:28:47,860 --> 00:28:54,140
technik dedukcyjnych, tybetańskich,
instynktu i szczęścia.

303
00:28:54,220 --> 00:28:59,260
Teraz zastosuję coś,
co z braku lepszego słowa

304
00:28:59,340 --> 00:29:02,210
nazwę magią.

305
00:29:06,620 --> 00:29:11,650
A my mamy odprawiać tybetańskie modły?

306
00:29:12,570 --> 00:29:16,050
Moim zdaniem idzie doskonale, prawda?

307
00:29:17,570 --> 00:29:19,230
Co teraz?

308
00:29:20,210 --> 00:29:22,800
Nie jestem pewien.

309
00:29:23,720 --> 00:29:26,190
Kogoś brakuje.

310
00:29:33,700 --> 00:29:36,240
Przepraszamy.

311
00:29:36,570 --> 00:29:39,080
Majorze, w samą porę.

312
00:29:39,500 --> 00:29:41,440
Jechałem do domu,

313
00:29:41,530 --> 00:29:44,360
gdy zatrzymał mnie ten miły dżentelmen,

314
00:29:44,450 --> 00:29:47,090
prosząc, żebym go tu przywiózł.

315
00:30:01,640 --> 00:30:05,600
Znam tę gumę. Żułem ją w dzieciństwie.

316
00:30:05,680 --> 00:30:08,020
Lubiłem ją najbardziej na świecie.

317
00:30:08,290 --> 00:30:13,530
Pana ulubiona guma
wróci w wielkim stylu.

318
00:30:30,000 --> 00:30:32,410
Zabił mnie...

319
00:30:32,490 --> 00:30:35,090
mój ojciec.

320
00:31:19,470 --> 00:31:21,050
Panie Horne,

321
00:31:21,360 --> 00:31:23,930
jedziemy na posterunek.

322
00:31:25,560 --> 00:31:28,680
Radzę zabrać Lelanda Palmera
jako adwokata.

323
00:32:17,280 --> 00:32:19,860
Zejdźmy do pokoju przesłuchań.

324
00:32:24,270 --> 00:32:27,180
- Przedstawicie mu zarzuty?
- Tak.

325
00:32:27,260 --> 00:32:30,260
Więc wystąpię o zwolnienie za kaucją.

326
00:32:30,340 --> 00:32:32,560
Na wstępnej rozprawie.

327
00:32:58,380 --> 00:32:59,570
Nie martw się, Ben.

328
00:33:17,160 --> 00:33:19,740
Hawk, pan Horne jest wolny.

329
00:33:19,830 --> 00:33:21,800
- Leland?
- To nie jest Leland.

330
00:33:24,790 --> 00:33:29,160
- Skąd wiedziałeś?
- Laura powiedziała mi we śnie.

331
00:33:29,250 --> 00:33:31,310
Przydałyby się mocniejsze dowody.

332
00:33:31,400 --> 00:33:33,600
Na przykład przyznanie się?

333
00:33:35,830 --> 00:33:37,610
Ma pan prawo do adwokata.

334
00:33:37,690 --> 00:33:41,810
Jeśli pana na niego nie stać,
zostanie przydzielony z urzędu.

335
00:33:57,210 --> 00:34:00,090
Pewnie macie jakieś pytania.

336
00:34:01,180 --> 00:34:03,340
Zabiłeś Laurę Palmer?

337
00:34:09,790 --> 00:34:10,930
To znaczy "tak".

338
00:34:11,690 --> 00:34:14,840
- A Madelaine Ferguson?
- Jak myślisz?

339
00:34:14,920 --> 00:34:16,180
- Pytam.
- Że co?

340
00:34:16,270 --> 00:34:21,090
- Odpowiadaj.
- Chorobcia! Chyba tak.

341
00:34:21,180 --> 00:34:23,090
Mam słabość do noży.

342
00:34:23,180 --> 00:34:27,950
Pamiętasz, co cię spotkało
w Pittsburghu? Co, Cooper?

343
00:34:29,890 --> 00:34:34,100
Och, Lelandzie, Lelandzie...

344
00:34:34,190 --> 00:34:36,000
Dobry był z ciebie wehikuł,

345
00:34:36,350 --> 00:34:38,370
fajnie się jechało,

346
00:34:38,460 --> 00:34:45,090
ale pełno w tobie dziur. Pora na złom!

347
00:34:45,400 --> 00:34:47,010
Leland wiedział, co zrobiłeś?

348
00:34:47,460 --> 00:34:54,170
To kawał durnia z dziurą w miejscu,
gdzie kiedyś miał sumienie.

349
00:34:54,250 --> 00:34:58,210
Gdy odchodzę, zrywam sznurki.

350
00:34:58,300 --> 00:35:03,870
Pilnujcie Lelanda.
Pilnujcie go, choć niedługo.

351
00:35:13,690 --> 00:35:15,000
To mi wystarczy.

352
00:35:37,870 --> 00:35:39,600
Dick?

353
00:35:39,680 --> 00:35:42,180
Proszę ze mną.

354
00:35:49,090 --> 00:35:50,950
Zrobimy tak.

355
00:35:51,170 --> 00:35:53,620
Urodzę to dziecko.

356
00:35:55,020 --> 00:35:56,700
To nie podlega dyskusji.

357
00:35:57,500 --> 00:36:00,670
- Rozumiem.
- Cicho, niech mówi.

358
00:36:01,770 --> 00:36:02,790
Masz ogień?

359
00:36:07,780 --> 00:36:09,360
Nie!

360
00:36:11,340 --> 00:36:13,140
Na czym stanęło?

361
00:36:14,830 --> 00:36:17,030
Jest tylko jeden sposób.

362
00:36:17,120 --> 00:36:20,960
Można ustalić ojcostwo po grupie krwi.

363
00:36:21,050 --> 00:36:24,480
Może to ty, a może ty.

364
00:36:25,360 --> 00:36:29,290
Ale badanie można zrobić,
dopiero gdy dziecko się urodzi,

365
00:36:29,370 --> 00:36:35,140
a do tego czasu oczekuję
wsparcia ze strony was obu.

366
00:36:35,230 --> 00:36:37,240
Jak sobie życzysz.

367
00:36:37,950 --> 00:36:39,410
To twoje dziecko.

368
00:36:39,780 --> 00:36:41,460
Dziękuję.

369
00:36:52,580 --> 00:36:54,670
Analiza krwi Bena Horne'a go wyklucza.

370
00:36:54,760 --> 00:36:58,140
Odpowiedź miałem przed nosem
od samego początku.

371
00:36:58,220 --> 00:37:00,930
Co robił karzeł w moim śnie?

372
00:37:03,470 --> 00:37:04,520
Tańczył.

373
00:37:04,870 --> 00:37:08,390
Leland też tańczy od śmierci Laury.

374
00:37:08,480 --> 00:37:11,620
Wiedzieliśmy,
że morderca, Bob, jest siwy.

375
00:37:11,710 --> 00:37:14,530
Gdy Leland zabił Jacques'a,
osiwiał w jedną noc.

376
00:37:15,070 --> 00:37:19,270
Powiedział, że w dzieciństwie
miał sąsiada o nazwisku Robertson.

377
00:37:19,360 --> 00:37:21,870
Mike nazwał opętanych jego dziećmi.

378
00:37:22,170 --> 00:37:25,030
Robertson – syn Roberta.

379
00:37:25,110 --> 00:37:31,380
Litery pod paznokciami: R-O-B-T,
to wskazówka, podpis demona.

380
00:37:32,070 --> 00:37:33,220
Dlaczego ją zabił?

381
00:37:33,580 --> 00:37:37,210
Laura pisała w pamiętniku o Bobie,
Leland go znalazł i wyrwał kartki.

382
00:37:37,290 --> 00:37:38,640
Ona wiedziała, że ją ściga.

383
00:37:38,890 --> 00:37:43,120
To Leland powiedział,
że Ben dzwonił do niej tamtej nocy.

384
00:37:43,210 --> 00:37:45,310
To on był tym trzecim
pod chatą Jacques'a.

385
00:37:45,630 --> 00:37:49,520
Zabrał dziewczyny do wagonu.
To była jego krew, nie Bena.

386
00:37:50,330 --> 00:37:51,970
A dlaczego Maddie?

387
00:37:52,060 --> 00:37:54,890
Przypominała mu Laurę.
Miała wrócić do domu.

388
00:37:54,970 --> 00:37:58,170
Nie mógł znieść rozstania,
chciał to znów przeżyć

389
00:37:58,250 --> 00:38:01,240
albo bał się, bo wiedziała,
że mordercą jest Bob.

390
00:38:02,150 --> 00:38:08,920
Ale ten Bob nie jest prawdziwy.
Leland oszalał, prawda?

391
00:38:09,400 --> 00:38:12,660
Wśród przyszłych czasów minionych

392
00:38:12,750 --> 00:38:16,000
czarodziej zbiec pragnie.

393
00:38:16,080 --> 00:38:19,300
Przez otchłań ciemną,
między dwoma światami...

394
00:38:19,390 --> 00:38:22,990
Ogniu! Krocz ze mną!

395
00:38:24,650 --> 00:38:27,750
Złapię was do mojego wora.

396
00:38:27,830 --> 00:38:34,110
Zwariował stary – tak myślicie.
Lecz znów zabiję! Zobaczycie!

397
00:39:28,440 --> 00:39:29,790
Wezwijcie lekarza.

398
00:39:31,790 --> 00:39:34,120
Boże! Laura!

399
00:39:34,860 --> 00:39:38,230
Zabiłem ją! Mój Boże!

400
00:39:38,310 --> 00:39:40,180
Zabiłem własną córkę!

401
00:39:40,860 --> 00:39:44,220
To wszystko ja. Wybacz mi, Boże!

402
00:39:53,370 --> 00:39:55,600
Byłem wtedy mały.

403
00:39:56,410 --> 00:39:58,880
Przyśnił mi się

404
00:39:58,960 --> 00:40:01,810
i chciał się ze mną bawić.

405
00:40:02,740 --> 00:40:05,790
Otworzył mnie, ja go zaprosiłem

406
00:40:05,870 --> 00:40:08,240
i wszedł we mnie.

407
00:40:08,320 --> 00:40:14,330
- Wszedł w ciebie?
- Nie wiem, co robił, gdy tam był.

408
00:40:14,410 --> 00:40:16,980
Gdy wychodził,

409
00:40:17,060 --> 00:40:19,310
nic nie pamiętałem.

410
00:40:19,960 --> 00:40:24,850
Zmuszał mnie
do robienia strasznych rzeczy.

411
00:40:24,930 --> 00:40:27,740
Chciał odbierać życie.

412
00:40:27,830 --> 00:40:34,440
Pragnął innych,
których wykorzystywałby jak mnie.

413
00:40:34,920 --> 00:40:36,580
Chciał Laury.

414
00:40:36,670 --> 00:40:39,280
Pragnęli jej. Chcieli ją mieć,

415
00:40:39,540 --> 00:40:43,430
ale była silna i walczyła.

416
00:40:43,510 --> 00:40:45,660
Nie wpuszczała go.

417
00:40:46,400 --> 00:40:48,620
Boże!

418
00:40:48,710 --> 00:40:52,210
Kazali mi zabić Teresę!

419
00:40:54,210 --> 00:40:57,090
Grozili...

420
00:40:57,690 --> 00:41:00,610
że jeśli nie oddam im Laury,

421
00:41:00,690 --> 00:41:03,430
ją też będę musiał zabić.

422
00:41:04,400 --> 00:41:06,880
Ale ich nie dopuszczała.

423
00:41:08,850 --> 00:41:13,720
Powiedziała, że prędzej umrze.

424
00:41:14,790 --> 00:41:17,740
I kazali mi ją zabić.

425
00:41:19,000 --> 00:41:22,850
Boże, miej litość!

426
00:41:23,740 --> 00:41:26,110
Co ja zrobiłem?

427
00:41:26,910 --> 00:41:29,260
Co ja zrobiłem?

428
00:41:30,030 --> 00:41:32,110
Boże!

429
00:41:32,200 --> 00:41:34,510
Kochałem ją!

430
00:41:34,600 --> 00:41:38,420
Kochałem z całego serca!

431
00:41:39,380 --> 00:41:42,430
Mój aniołku, przebacz mi...

432
00:41:55,430 --> 00:41:59,640
Lelandzie, nadszedł czas,
byś odszukał ścieżkę.

433
00:41:59,730 --> 00:42:03,430
Twoja dusza stanie twarzą w twarz
z jasnym światłem

434
00:42:03,520 --> 00:42:07,070
i doświadczysz jego realności,

435
00:42:07,150 --> 00:42:10,580
gdzie wszystko jest
jak bezkresne i bezchmurne niebo,

436
00:42:11,150 --> 00:42:15,880
a nieskalany intelekt
przypomina przejrzystą próżnię,

437
00:42:15,960 --> 00:42:19,620
bez granic ani środka.

438
00:42:19,710 --> 00:42:22,220
Musisz się określić

439
00:42:22,830 --> 00:42:25,230
i pozostać w tym stanie.

440
00:42:27,260 --> 00:42:30,200
Szukaj światła, Lelandzie.

441
00:42:30,290 --> 00:42:32,270
Znajdź światło.

442
00:42:33,060 --> 00:42:34,780
Widzę je.

443
00:42:35,990 --> 00:42:40,230
Idź ku światłu.

444
00:42:40,320 --> 00:42:43,800
Widzę ją.

445
00:42:44,550 --> 00:42:47,360
Jest tam.

446
00:42:47,450 --> 00:42:49,720
Idź do światła.

447
00:42:50,520 --> 00:42:53,260
Jest piękna.

448
00:42:53,350 --> 00:42:55,370
Do światła.

449
00:42:57,600 --> 00:42:59,490
Laura...

450
00:43:00,270 --> 00:43:02,330
Nie bój się.

451
00:43:59,280 --> 00:44:02,100
Całkiem postradał zmysły.

452
00:44:02,180 --> 00:44:03,400
Tak myślisz?

453
00:44:03,690 --> 00:44:10,410
Ludzie widują Boba w wizjach,
jak Laura, Maddie i Sara Palmer.

454
00:44:10,860 --> 00:44:15,870
Są rzeczy na niebie i ziemi,
o których nie śniło się filozofom.

455
00:44:19,990 --> 00:44:23,180
Spędziłem w tych lasach
niemal całe życie.

456
00:44:24,430 --> 00:44:27,310
Widziałem różne dziwy,
ale to już przesada.

457
00:44:27,390 --> 00:44:30,800
Nie mogę w to uwierzyć.

458
00:44:31,490 --> 00:44:33,580
�atwiej by�oby ci uwierzy� w to,

459
00:44:33,670 --> 00:44:38,950
że ktoś jest w stanie
zgwałcić i zabić własną córkę?

460
00:44:39,030 --> 00:44:40,850
Nie.

461
00:44:41,620 --> 00:44:46,450
Tak potworne zło w tym pięknym świecie.

462
00:44:48,110 --> 00:44:50,950
Czy to ważne, skąd się bierze?

463
00:44:51,590 --> 00:44:55,200
Tak. Bo musimy to zatrzymać.

464
00:45:02,860 --> 00:45:07,630
Może ten "Bob" to zło, które czynimy.

465
00:45:09,020 --> 00:45:12,960
- Nieważne, jak je nazwiemy.
- Być może.

466
00:45:14,900 --> 00:45:21,420
Ale jeśli on istnieje,
mieliśmy go i nam uciekł...

467
00:45:22,470 --> 00:45:24,310
to gdzie teraz jest?

