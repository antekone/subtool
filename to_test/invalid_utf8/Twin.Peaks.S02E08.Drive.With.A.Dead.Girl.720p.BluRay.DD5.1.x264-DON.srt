1
00:00:28,830 --> 00:00:33,580
MIASTECZKO TWIN PEAKS

2
00:02:58,500 --> 00:03:02,390
Cześć, dzieciaki. Wejdźcie.

3
00:03:02,470 --> 00:03:04,690
Chcieliśmy się pożegnać z Maddie.

4
00:03:04,780 --> 00:03:06,950
Minęliście się.

5
00:03:07,690 --> 00:03:10,700
20 minut temu
podrzuciłem ją na autobus.

6
00:03:11,120 --> 00:03:12,450
- Wyjechała?
- Tak.

7
00:03:13,510 --> 00:03:15,660
Czekała na was wczoraj.

8
00:03:18,530 --> 00:03:19,630
Mówiła coś?

9
00:03:20,270 --> 00:03:23,340
Powiedziałbym,
że była nieco zawiedziona.

10
00:03:24,480 --> 00:03:26,470
Lelandzie!

11
00:03:26,560 --> 00:03:28,380
Kochanie!

12
00:03:28,730 --> 00:03:30,050
Przepraszam.

13
00:03:45,720 --> 00:03:48,700
Możecie do niej napisać.

14
00:03:49,180 --> 00:03:51,240
Na pewno się ucieszy.

15
00:03:52,950 --> 00:03:56,510
- Nie będziemy przeszkadzać.
- Nic podobnego.

16
00:03:56,600 --> 00:03:58,710
Donna, pozdrów rodziców.

17
00:03:59,500 --> 00:04:02,430
- Do widzenia.
- Cześć.

18
00:04:03,390 --> 00:04:05,490
Trzymajcie się.

19
00:04:19,090 --> 00:04:20,020
Tak, skarbie?

20
00:04:20,110 --> 00:04:23,910
Zapiszesz nas na wieczorek
z Glennem Millerem?

21
00:04:24,000 --> 00:04:26,190
Nie ma obawy, nie zapomnę.

22
00:04:53,320 --> 00:04:55,260
Pa, kochanie!

23
00:05:48,100 --> 00:05:50,570
Gdzieś ty był tak długo?

24
00:05:51,770 --> 00:05:56,430
Zmiany daty, strefy czasowe...
Sam nie wiem, gdzie jestem.

25
00:05:58,870 --> 00:06:00,830
Okropnie wyglądasz.

26
00:06:13,930 --> 00:06:18,760
Skoro oskarżają o morderstwo
mojego rodzonego brata,

27
00:06:18,850 --> 00:06:23,480
sam zajmę się tą sprawą.
Możemy zaczynać.

28
00:06:25,000 --> 00:06:26,990
Zabiłeś ją?

29
00:06:28,610 --> 00:06:31,690
- Na miłość boską!
- Masz rację.

30
00:06:31,900 --> 00:06:36,170
Ostatnie, co powinien
znać adwokat, to prawda.

31
00:06:39,710 --> 00:06:40,950
Wyciągniesz mnie stąd?

32
00:06:41,890 --> 00:06:46,540
Bez przedstawienia zarzutów
mogą cię trzymać 24 godziny. Czy 48?

33
00:06:48,520 --> 00:06:50,680
Jerry...

34
00:06:50,770 --> 00:06:52,910
mam kłopoty.

35
00:06:53,000 --> 00:06:55,870
Panuję nad sytuacją.

36
00:06:55,950 --> 00:07:00,670
To gdzie byłeś w noc zabójstwa Laury?

37
00:07:02,640 --> 00:07:06,330
- Z Catherine.
- Kiepski wybór.

38
00:07:06,420 --> 00:07:08,650
Ale to prawda.

39
00:07:08,730 --> 00:07:10,720
Rany...

40
00:07:10,800 --> 00:07:15,850
Zostało z niej coś?
Kawałek kości albo zęby?

41
00:07:16,820 --> 00:07:21,530
Jakiś dokumencik
potwierdzający twoją wersję?

42
00:07:27,270 --> 00:07:29,740
Ja się załamię.

43
00:07:41,520 --> 00:07:42,550
Piętrowe prycze!

44
00:07:47,830 --> 00:07:49,850
Pamiętasz nasz dawny pokój?

45
00:07:50,440 --> 00:07:54,520
Ja na górze, ty na dole...

46
00:07:54,750 --> 00:07:56,570
i Louise Dombrowski,

47
00:07:56,660 --> 00:08:00,320
tańcząca z latarką
na szmacianym dywaniku...

48
00:09:16,370 --> 00:09:19,280
Co się z nami porobiło?

49
00:09:24,360 --> 00:09:30,380
Za pierwszym razem myśleli,
że to pęcherz i zbadali mi mocz.

50
00:09:30,470 --> 00:09:34,580
To było zakażenie.
W życiu nie miałam zastrzału.

51
00:09:34,660 --> 00:09:39,660
Pamiętasz, jak moczyłam palec w sodzie?

52
00:09:39,750 --> 00:09:45,060
Przeszło, ale potem dostałam skurczów,
jakbym zjadła coś niestrawnego...

53
00:09:45,140 --> 00:09:47,020
Widziałeś Andy'ego?

54
00:09:47,110 --> 00:09:51,320
Jesteś tym rdzennym,
o którym tyle słyszałam?

55
00:09:51,410 --> 00:09:53,340
Sokole Oko?

56
00:09:53,430 --> 00:09:56,450
Hawk. Siostra?

57
00:09:56,540 --> 00:10:00,130
Tak, to moja siostra, Gwen. A to Hawk.

58
00:10:00,220 --> 00:10:05,770
Ależ wy musicie nienawidzić białych
za to, co zrobiliśmy.

59
00:10:05,860 --> 00:10:08,910
Mam wśród białych wielu przyjaciół.

60
00:10:11,510 --> 00:10:14,290
Cicho, malutki. Mój żołądek...

61
00:10:14,550 --> 00:10:18,150
Jest trzy po dziesiątej,
hotel Great Northern.

62
00:10:18,240 --> 00:10:23,120
Przyszedłem tu z szeryfem
za wskazówką jednorękiego.

63
00:10:23,200 --> 00:10:27,680
W innych czasach i kulturze
byłby jasnowidzem albo szamanem,

64
00:10:27,760 --> 00:10:31,290
a tu sprzedaje buty i żyje w cieniu.

65
00:10:46,860 --> 00:10:48,780
To Leland Palmer.

66
00:10:58,670 --> 00:11:01,670
Wie o zatrzymaniu Bena Horne'a?

67
00:11:01,760 --> 00:11:03,560
Raczej nie.

68
00:11:04,390 --> 00:11:06,880
Lepiej ty mu to powiedz.

69
00:11:15,760 --> 00:11:19,100
Witam panów. Trzeba się rozerwać.

70
00:11:20,390 --> 00:11:23,940
- Dobrze wyglądasz, Lelandzie.
- Mówcie mi Fred.

71
00:11:26,090 --> 00:11:29,630
Przepraszam...

72
00:11:29,720 --> 00:11:32,780
ale nie chcę,
żebyś się dowiedział od innych.

73
00:11:32,870 --> 00:11:35,700
Aresztowaliśmy Bena Horne'a
w związku ze sprawą Laury.

74
00:11:36,580 --> 00:11:40,220
Na razie jest tylko podejrzany.

75
00:11:40,300 --> 00:11:42,980
- Ben?
- Tak.

76
00:11:43,590 --> 00:11:46,070
To jakaś pomyłka.

77
00:11:46,150 --> 00:11:47,870
Ben?

78
00:11:48,740 --> 00:11:51,890
- Myślałem, że Jacques Renault.
- Nie.

79
00:11:55,910 --> 00:11:58,730
- Macie mocne dowody?
- Tak.

80
00:11:59,050 --> 00:12:01,050
Boże...

81
00:12:01,830 --> 00:12:03,930
Dobrze się pan czuje?

82
00:12:09,610 --> 00:12:12,740
- Zdajmy się na prawo.
- Słusznie.

83
00:12:14,160 --> 00:12:15,770
Dziękuję, Harry.

84
00:12:16,630 --> 00:12:18,160
Agencie Cooper.

85
00:12:45,730 --> 00:12:47,860
Dołączę do ciebie.

86
00:13:08,790 --> 00:13:10,490
Lelandzie?

87
00:13:10,580 --> 00:13:12,150
Tak?

88
00:13:13,490 --> 00:13:16,850
Jeśli pan sobie przypomni,
czy tamtej nocy

89
00:13:16,930 --> 00:13:20,430
pan Horne czymś pana zaskoczył,
proszę dać mi znać.

90
00:13:20,520 --> 00:13:23,310
Oczywiście.

91
00:13:25,820 --> 00:13:27,400
Dziękuję.

92
00:13:43,840 --> 00:13:45,590
Wszystko gra?

93
00:13:45,670 --> 00:13:47,450
Nie jestem pewien.

94
00:14:21,430 --> 00:14:23,620
Nie będzie bolało.

95
00:14:25,490 --> 00:14:28,780
Złożę skargę na brutalne
traktowanie mojego klienta.

96
00:14:28,860 --> 00:14:31,200
Tyle zrobił dla tego miasta.

97
00:14:31,290 --> 00:14:32,590
Proszę się nie ruszać.

98
00:14:41,740 --> 00:14:45,430
- Za dwie godziny będą wyniki.
- Dziękuję.

99
00:14:45,520 --> 00:14:50,410
Panowie, domagam się albo zwolnienia,
albo oskarżenia klienta.

100
00:14:50,500 --> 00:14:53,960
Odkąd pracuję w zawodzie,
nie spotkałem się jeszcze

101
00:14:54,050 --> 00:14:59,060
z tak drastycznym przypadkiem
łamania praw konstytucyjnych.

102
00:14:59,140 --> 00:15:03,310
Jeremy Horne,
Uniwersytet Gonzaga, rocznik '74,

103
00:15:03,390 --> 00:15:06,330
ukończył studia z ostatnim
wynikiem ze 143 kolegów,

104
00:15:06,890 --> 00:15:10,220
licencja za trzecim podejściem,
pozbawiony praw zawodowych

105
00:15:10,310 --> 00:15:13,800
w Illinois, na Florydzie,
Alasce i w Massachusetts.

106
00:15:15,590 --> 00:15:17,720
To nie mój proces.

107
00:15:18,060 --> 00:15:19,430
Siądźmy.

108
00:15:22,040 --> 00:15:23,350
Wie pan, co to jest?

109
00:15:26,220 --> 00:15:27,890
Książka.

110
00:15:27,980 --> 00:15:29,770
Przyjrzyjcie się.

111
00:15:38,000 --> 00:15:39,610
Pamiętnik.

112
00:15:40,430 --> 00:15:42,280
Wiemy o Jednookim Jacku.

113
00:15:43,270 --> 00:15:45,150
Laura też wiedziała.

114
00:15:47,930 --> 00:15:51,300
"Pewnego dnia powiem
wszystkim o Benie Hornie.

115
00:15:51,390 --> 00:15:53,750
Powiem im, kim jest naprawdę".

116
00:15:56,190 --> 00:15:58,130
Nie miała tej szansy.

117
00:16:02,970 --> 00:16:05,100
No, Ben!

118
00:16:05,190 --> 00:16:08,170
Jesteśmy dorośli.

119
00:16:08,260 --> 00:16:13,800
Przy takiej dziewczynie jak Laura
można stracić głowę.

120
00:16:13,890 --> 00:16:19,000
Może być potem groźna
dla firmy i rodziny.

121
00:16:19,080 --> 00:16:21,450
Przesadził pan.

122
00:16:21,970 --> 00:16:24,660
Może nie ma pan nic do ukrycia.

123
00:16:25,540 --> 00:16:27,830
Choć to temu przeczy.

124
00:16:27,910 --> 00:16:31,500
Nie wolno wam tak do mnie mówić, jasne?

125
00:16:32,860 --> 00:16:38,510
Uspokój się. Chciałbym chwilę
porozmawiać z bratem... z klientem.

126
00:16:40,220 --> 00:16:41,990
Jeśli można.

127
00:16:42,760 --> 00:16:43,970
Proszę.

128
00:16:50,610 --> 00:16:53,250
Ben...

129
00:16:53,330 --> 00:16:55,750
jeśli nadal będziesz się tak zachowywał,

130
00:16:55,840 --> 00:16:58,400
zaprzepaścisz
wszelkie możliwości obrony.

131
00:16:58,480 --> 00:17:00,910
To co robić?

132
00:17:02,140 --> 00:17:04,650
Twoje alibi się upiekło,

133
00:17:04,730 --> 00:17:07,090
miałeś motyw,

134
00:17:07,180 --> 00:17:11,240
a twoja krew może cię powiązać z Laurą.

135
00:17:17,200 --> 00:17:23,110
Jako twój prawnik, przyjaciel i brat...

136
00:17:24,160 --> 00:17:29,560
radzę ci, znajdź lepszego adwokata.

137
00:17:35,360 --> 00:17:39,360
Masz ostatnią szansę, Leo.
Nie zawiedź mnie.

138
00:17:39,450 --> 00:17:41,120
Tartak?

139
00:17:49,060 --> 00:17:53,010
Masz ostatnią szansę, Leo.
Nie zawiedź mnie.

140
00:17:53,100 --> 00:17:54,810
Tartak?

141
00:17:54,890 --> 00:17:57,480
Nie kombinuj. Śledczy ma zobaczyć

142
00:17:57,570 --> 00:18:02,340
szyld "PODPALENIE"
dwumetrowej wysokości.

143
00:18:02,430 --> 00:18:04,650
Tak jak się umówiliśmy?

144
00:18:08,290 --> 00:18:12,190
Głosy Bena Horne'a...

145
00:18:12,280 --> 00:18:16,150
i Leo Johnsona... Ben i Leo.

146
00:18:20,330 --> 00:18:23,420
Do rąk własnych pana Horne'a.

147
00:18:23,500 --> 00:18:25,200
MUSIMY POGADAĆ!

148
00:18:28,080 --> 00:18:29,520
Bobby?

149
00:18:29,600 --> 00:18:32,330
Boże! Bobby?

150
00:18:38,140 --> 00:18:39,880
Znów to samo.

151
00:18:41,080 --> 00:18:43,440
Tym razem ty sprzątasz.

152
00:18:50,040 --> 00:18:52,070
Co to?

153
00:18:52,160 --> 00:18:55,550
Inwestycja.

154
00:18:55,640 --> 00:18:57,680
To znaczy?

155
00:18:57,770 --> 00:19:02,760
Skoro nasza dojna krowa
zmieniła się w studnię bez dna,

156
00:19:02,850 --> 00:19:06,990
postanowiłem zająć się interesami.

157
00:19:07,080 --> 00:19:10,290
- Będziesz domokrążcą?
- Nie.

158
00:19:10,380 --> 00:19:14,270
Myślę o czymś poważniejszym.

159
00:19:14,360 --> 00:19:15,960
- Naprawdę?
- Tak.

160
00:19:16,050 --> 00:19:18,680
Wspaniale.

161
00:19:18,760 --> 00:19:23,710
Dam ci, co zechcesz.

162
00:19:25,170 --> 00:19:28,520
Niańkę na pełny etat.

163
00:19:31,370 --> 00:19:34,430
Może francuską pokojówkę?

164
00:19:47,320 --> 00:19:49,450
- Cześć, Normo.
- Mama?

165
00:19:50,000 --> 00:19:52,280
To prawdziwe ziemniaki, nie z proszku?

166
00:19:53,190 --> 00:19:54,200
Prawdziwe.

167
00:19:55,320 --> 00:19:56,860
Przepraszam.

168
00:19:59,140 --> 00:20:02,190
Zrobiłaś mi niespodziankę.

169
00:20:02,280 --> 00:20:03,950
Dobre.

170
00:20:04,040 --> 00:20:06,730
Hank czegoś się nauczył w pudle.

171
00:20:06,820 --> 00:20:08,510
- Gdzie twój uroczy mąż?
- Hank?

172
00:20:08,590 --> 00:20:11,340
- Nie ma go.
- To widzę.

173
00:20:12,540 --> 00:20:16,020
Rozumiem sarkazm, ale pozwól, że spytam.

174
00:20:16,290 --> 00:20:18,670
Co tu robisz?

175
00:20:18,760 --> 00:20:21,560
Chcę cię przedstawić mojemu mężowi.

176
00:20:23,090 --> 00:20:24,230
Mężowi?

177
00:20:24,510 --> 00:20:27,730
Ernie, to jest Norma.

178
00:20:27,810 --> 00:20:30,110
Dzień dobry.

179
00:20:30,930 --> 00:20:33,220
To podróż poślubna.

180
00:20:33,310 --> 00:20:36,830
Mógłbym prosić o filiżankę kawy?

181
00:20:40,640 --> 00:20:42,670
- Miła dziewczyna.
- Kochana.

182
00:20:42,950 --> 00:20:45,450
Ernie jest finansistą.

183
00:20:46,090 --> 00:20:49,020
Ślicznie przystroiłaś lokal.

184
00:20:49,110 --> 00:20:51,260
Kwiaty dodają uroku.

185
00:20:52,010 --> 00:20:54,930
Pięknie dziękuję.

186
00:20:55,740 --> 00:21:00,110
To wspaniale, że uroda przechodzi
z pokolenia na pokolenie.

187
00:21:05,540 --> 00:21:08,730
�adnie ci w tym fartuszku.

188
00:21:09,440 --> 00:21:12,360
Wciąż masz doskonałą figurę.

189
00:21:12,450 --> 00:21:15,780
Nie chcę być niegrzeczna,
ale długo zostaniecie?

190
00:21:16,790 --> 00:21:20,620
Parę dni. Ernie chce się rozejrzeć.

191
00:21:20,710 --> 00:21:22,250
Bo co?

192
00:21:23,820 --> 00:21:25,540
Szkoda, że nie wiedziałam.

193
00:21:25,620 --> 00:21:30,490
Nie chciałam ci robić kłopotu.
Znasz mnie.

194
00:21:30,570 --> 00:21:32,430
O co chodzi?

195
00:21:32,510 --> 00:21:33,990
O nic.

196
00:21:34,080 --> 00:21:37,600
Tylko przyjeżdża krytyk kulinarny,
a nie mam nikogo do pomocy.

197
00:21:37,940 --> 00:21:39,730
Krytyk!

198
00:21:40,630 --> 00:21:44,400
To dlatego tak tu ładnie.

199
00:21:44,480 --> 00:21:48,440
Normo, żałuję,
że nie zostaniemy dłużej.

200
00:21:49,180 --> 00:21:54,160
Możemy jechać do hotelu?
Czeka na mnie faks z Tokio.

201
00:21:54,920 --> 00:21:58,160
Będziemy w Great Northern.
Szkoda, że nie zastałam Hanka.

202
00:21:58,250 --> 00:22:00,490
Później zadzwonię.

203
00:22:02,170 --> 00:22:04,020
Było mi bardzo miło.

204
00:22:14,500 --> 00:22:16,590
1000 $ NA HUSTON

205
00:22:29,810 --> 00:22:32,690
Jest blisko.

206
00:22:35,750 --> 00:22:38,010
Siostro...

207
00:22:38,100 --> 00:22:40,730
mogę prosić o szklankę wody?

208
00:23:00,930 --> 00:23:02,720
Przepraszam.

209
00:23:22,450 --> 00:23:24,060
Cześć.

210
00:23:25,280 --> 00:23:27,220
Chyba się spóźniłem.

211
00:23:27,820 --> 00:23:31,250
48 godzin to nie spóźnienie,
a zaginięcie.

212
00:23:31,340 --> 00:23:34,730
Przepraszam.
Musiałem załatwić pewną sprawę.

213
00:23:34,810 --> 00:23:37,010
Sprawę! Tu masz swoje sprawy.

214
00:23:37,260 --> 00:23:41,260
I masz tu być, chcesz czy nie.

215
00:23:42,300 --> 00:23:45,980
Nie czaruj, bo mam ochotę cię wylać.

216
00:23:46,810 --> 00:23:48,770
Nie mów tak, póki mnie nie wysłuchasz.

217
00:23:50,370 --> 00:23:52,670
Kiedyś narozrabiałem...

218
00:23:53,420 --> 00:23:55,700
i teraz niektórzy...

219
00:23:56,910 --> 00:24:01,400
których dawniej znałem,
chcą mnie załatwić.

220
00:24:01,480 --> 00:24:04,750
Gdy się stawiałem, było jeszcze gorzej.

221
00:24:05,420 --> 00:24:09,580
Próbowałem uciekać,
ale zawsze ktoś był szybszy.

222
00:24:11,500 --> 00:24:14,850
Bywa, że nie da się żyć po swojemu.

223
00:24:15,730 --> 00:24:21,390
Czasami trzeba się zatrzymać
i przeczekać burzę.

224
00:24:23,360 --> 00:24:25,810
Nie chciałem cię martwić.

225
00:24:27,900 --> 00:24:30,870
Próbuję tylko utrzymać się
na powierzchni.

226
00:24:36,020 --> 00:24:39,010
Następnym razem poproś o pomoc.

227
00:24:42,470 --> 00:24:43,700
Poproszę.

228
00:24:44,370 --> 00:24:47,120
Wrócił syn marnotrawny.

229
00:24:52,120 --> 00:24:55,340
- Świetnie wyglądasz.
- Dzięki.

230
00:24:56,360 --> 00:24:57,460
Jak było w pace?

231
00:25:00,010 --> 00:25:01,380
Długo by gadać.

232
00:25:02,240 --> 00:25:04,680
Opowiesz przy kolacji. Mam nowego męża.

233
00:25:06,750 --> 00:25:08,740
- W hotelu, o wpół do dziewiątej.
- Z przyjemnością.

234
00:25:08,820 --> 00:25:13,900
- Nie wiem, czy mogę...
- Damy radę. Będziemy punktualnie.

235
00:25:21,150 --> 00:25:24,030
- Harry?
- Cześć, Pete.

236
00:25:24,310 --> 00:25:26,180
Co tam masz?

237
00:25:26,970 --> 00:25:28,850
Dzięcioła.

238
00:25:29,810 --> 00:25:32,120
To chyba smugoszyi.

239
00:25:35,320 --> 00:25:36,920
Popatrz.

240
00:25:39,870 --> 00:25:42,930
Dzięcioł smugoszyi...

241
00:25:44,230 --> 00:25:46,490
Niezwykłe.

242
00:25:53,680 --> 00:25:56,860
Josie wyjechała.

243
00:25:57,990 --> 00:26:00,030
Wiem.

244
00:26:00,120 --> 00:26:04,200
Wróciłem po pracy i znalazłem
kartkę na kuchennym stole.

245
00:26:04,290 --> 00:26:07,850
Żadnego wyjaśnienia ani pożegnania,

246
00:26:07,940 --> 00:26:10,160
tylko kartka.

247
00:26:10,250 --> 00:26:13,490
Sprzedała tartak Horne'owi.

248
00:26:13,570 --> 00:26:16,660
Niezupełnie.

249
00:26:16,750 --> 00:26:19,130
Nie musisz mi tego tłumaczyć.

250
00:26:23,260 --> 00:26:25,520
Kochałem ją.

251
00:26:28,390 --> 00:26:30,810
Wykrztusiłem to.

252
00:26:30,900 --> 00:26:33,210
Nie gniewaj się.

253
00:26:34,260 --> 00:26:36,550
Też ją kochałem.

254
00:26:38,430 --> 00:26:42,440
Stałem tu i patrzyłem,
jak asystent wynosi za nią walizki.

255
00:26:43,120 --> 00:26:44,450
Asystent?

256
00:26:44,860 --> 00:26:46,910
Ten Azjata. Nie znałem go.

257
00:26:48,590 --> 00:26:54,040
- Jak wyglądał?
- Średniego wzrostu, z kucykiem.

258
00:26:54,130 --> 00:26:56,290
To jej kuzyn, Jonathan.

259
00:26:57,000 --> 00:27:00,750
Mówiła, że asystent, pan Lee.

260
00:27:02,730 --> 00:27:04,850
Pan Lee?

261
00:27:07,150 --> 00:27:09,460
Harry...

262
00:27:09,540 --> 00:27:12,180
mam złe przeczucia.

263
00:27:13,850 --> 00:27:17,060
- Ja też.
- Harry!

264
00:27:17,300 --> 00:27:20,690
Cześć, Pete. Gerard zniknął.
Ogłuszył strażnika.

265
00:27:21,200 --> 00:27:23,530
Przepraszam, Pete.

266
00:27:30,280 --> 00:27:32,160
Daleko nie ucieknie.

267
00:27:35,360 --> 00:27:37,830
Czołem, panowie.

268
00:27:43,720 --> 00:27:45,600
Lucy?

269
00:28:00,670 --> 00:28:01,810
Co?

270
00:28:27,960 --> 00:28:32,390
Nie bój się. Nas, bandziorów,
zamykają w klatkach.

271
00:28:32,780 --> 00:28:34,810
Mam dla ciebie wiadomość.

272
00:28:35,700 --> 00:28:39,290
Pozdrowienia od chłopaków z tartaku?

273
00:28:46,310 --> 00:28:48,840
Posłuchamy muzyczki?

274
00:28:50,980 --> 00:28:52,570
Witaj, Benjaminie.

275
00:28:54,360 --> 00:28:57,870
Wybacz, że nie wpadłam z wizytą,

276
00:28:57,950 --> 00:29:01,730
ale mam awersję do aresztów.

277
00:29:01,820 --> 00:29:04,210
- Często wspominam...
- Żyje...

278
00:29:06,050 --> 00:29:07,880
...naszą wspólną noc...

279
00:29:08,540 --> 00:29:11,170
pełną miłosnych uniesień.

280
00:29:11,450 --> 00:29:12,720
Kiedy to było?

281
00:29:12,810 --> 00:29:17,960
Tak dawno, że wydaje się to jak sen...

282
00:29:18,690 --> 00:29:23,220
Tak, pamiętam.
Tej nocy zginęła Laura Palmer.

283
00:29:24,620 --> 00:29:30,830
Pamięć jest taka zawodna.
A może mi się to śniło?

284
00:29:30,910 --> 00:29:34,920
Niczego już nie jestem pewna.

285
00:29:35,590 --> 00:29:38,740
Czego chce? Zapłacę jej. Czego chce?

286
00:29:39,320 --> 00:29:41,060
Zaraz usłyszysz.

287
00:29:41,150 --> 00:29:45,260
W zamian za moje zeznanie
przepiszesz na mnie tartak

288
00:29:45,340 --> 00:29:48,220
i inwestycję w Ghostwood.

289
00:29:48,310 --> 00:29:52,680
Pomyślę, czy zostawić ci hotel.

290
00:29:53,770 --> 00:30:00,140
Mój przedstawiciel odwiedzi cię
z dokumentami w ciągu doby.

291
00:30:00,230 --> 00:30:02,910
Jeśli odmówisz,

292
00:30:03,000 --> 00:30:06,350
będziesz musiał skorzystać

293
00:30:06,440 --> 00:30:11,320
z jednej z wielu ofert
federalnego więziennictwa.

294
00:30:13,370 --> 00:30:16,320
Miło było znów cię spotkać.

295
00:30:19,240 --> 00:30:21,370
Wrobiła mnie.

296
00:30:22,230 --> 00:30:24,580
Ech, ta Catherine...

297
00:30:28,740 --> 00:30:30,800
Wystawiła...

298
00:30:31,700 --> 00:30:35,120
Dobra jest, co?

299
00:30:54,190 --> 00:30:57,780
Dopadnę cię, ty zdziro!

300
00:31:17,420 --> 00:31:19,730
Wystawiła mnie.

301
00:32:00,790 --> 00:32:04,930
Czy w tutejszych szkołach
uczą przepisów drogowych?

302
00:32:08,130 --> 00:32:09,690
Mamy wagarowicza. Uwaga!

303
00:32:46,670 --> 00:32:49,700
Agencie Cooper, Harry, przepraszam.

304
00:32:49,780 --> 00:32:52,400
- Nic wam nie jest?
- Tak, a co z tobą?

305
00:32:52,620 --> 00:32:55,040
Wszystko w porządku.

306
00:32:55,120 --> 00:33:00,290
Jechałem do klubu, wypróbować nowe kije

307
00:33:00,380 --> 00:33:03,560
i przypomniał mi się Ben.

308
00:33:03,640 --> 00:33:05,540
Chyba się zamyśliłem.

309
00:33:05,620 --> 00:33:08,500
Siadając za kółkiem,
lepiej zostawić troski w domu.

310
00:33:08,820 --> 00:33:11,540
Ma pan rację, przepraszam.

311
00:33:13,560 --> 00:33:17,270
Coś sobie przypomniałem. Pytał pan...

312
00:33:17,350 --> 00:33:20,100
o noc, gdy zginęła Laura.

313
00:33:20,180 --> 00:33:25,470
Pracowałem z Benem do późna.
Koło dziesiątej wyszedł zadzwonić.

314
00:33:25,550 --> 00:33:30,710
Nie wiem do kogo, ale mówił
podniesionym głosem, był zły.

315
00:33:30,790 --> 00:33:35,110
Wspominał o jakimś notatniku.

316
00:33:36,030 --> 00:33:39,150
- O notatniku?
- Chyba tak.

317
00:33:39,230 --> 00:33:41,310
- Może pamiętniku?
- Możliwe.

318
00:33:41,400 --> 00:33:46,950
Szeryfie, mówi Lucy. Jest pan tam?
Mam pilną wiadomość od Hawka.

319
00:33:47,870 --> 00:33:52,170
- Grywa pan w golfa?
- Owszem. Lubię go za precyzję.

320
00:33:52,260 --> 00:33:58,320
Chętnie zaproszę pana na partyjkę.
Obejrzy pan nowe kije?

321
00:34:00,030 --> 00:34:01,560
Dobrze.

322
00:34:06,170 --> 00:34:08,100
Cooper!

323
00:34:08,320 --> 00:34:11,100
Znaleźli Gerarda przy wodospadzie.

324
00:34:11,420 --> 00:34:14,560
Chodź, czekają na nas.

325
00:34:15,630 --> 00:34:18,740
Jedźmy. Zobaczę je innym razem.

326
00:34:18,820 --> 00:34:22,360
Oczywiście.
Zawsze chętnie służę pomocą.

327
00:34:22,810 --> 00:34:24,300
Dziękuję.

328
00:35:01,820 --> 00:35:07,560
Biedny Andy. Miałam to samo
na początku ciąży.

329
00:35:07,650 --> 00:35:11,700
Poszłam do sklepu i łup!
Padłam w dziale warzywnym.

330
00:35:12,210 --> 00:35:19,830
Leżałam poobijana w owocach,
a ludziska gapili się jak na trupa.

331
00:35:20,250 --> 00:35:23,840
Wszyscy chcą twojego nieszczęścia.

332
00:35:27,510 --> 00:35:29,840
Chodzi o moje plemniki.

333
00:35:30,220 --> 00:35:33,270
Plemniki? Facetom tylko jedno w głowie!

334
00:35:33,810 --> 00:35:38,280
Kiedy urodziłam Carla
i położyli mi go na piersi,

335
00:35:38,370 --> 00:35:44,930
spojrzałam i zobaczyłam
to maleństwo, pomyślałam:

336
00:35:45,020 --> 00:35:48,780
"Boże, tego nam trzeba...

337
00:35:48,860 --> 00:35:51,620
Następny producent spermy.

338
00:35:51,820 --> 00:35:54,450
Strzeżcie się, kobiety!".

339
00:35:56,720 --> 00:35:58,040
Serio.

340
00:36:01,650 --> 00:36:06,500
Zbadałem się dwa razy.
Najpierw wszystkie były martwe.

341
00:36:06,590 --> 00:36:11,630
Jasne! Ile razy ja to słyszałam!

342
00:36:12,540 --> 00:36:17,060
A potem skakały
jak łososie w strumieniu.

343
00:36:17,140 --> 00:36:21,350
Bo wy zawsze myślicie o tarle!

344
00:36:22,030 --> 00:36:25,230
- Cicho.
- Zamknij się, Gwen.

345
00:36:33,430 --> 00:36:37,440
Gdy powiedziałaś mi o dziecku,
nie sądziłem, że jest moje,

346
00:36:37,520 --> 00:36:41,590
bo myślałem, że nie mogę go mieć.

347
00:36:43,630 --> 00:36:48,370
Ale wyzdrowiałem. Informacja
była błędna i jestem ojcem.

348
00:36:50,720 --> 00:36:53,200
O rany...

349
00:36:54,410 --> 00:36:59,980
- Nie jestem?
- Chodź, malutki.

350
00:37:00,060 --> 00:37:04,150
Popatrz na niedobrą ciocię Lucy.

351
00:37:05,620 --> 00:37:11,280
Moje ty słodkie cudeńko.

352
00:37:23,590 --> 00:37:26,060
Był blisko.

353
00:37:28,380 --> 00:37:30,500
Boba tu nie ma.

354
00:37:30,590 --> 00:37:33,410
Szeryfie?

355
00:37:34,250 --> 00:37:37,430
Ma nam pan coś do powiedzenia?

356
00:37:39,120 --> 00:37:40,810
Pięknie.

357
00:37:41,340 --> 00:37:44,670
Gapi się na mnie jak pies na herbatnik.

358
00:37:44,750 --> 00:37:48,490
Bob był bardzo blisko.

359
00:37:48,580 --> 00:37:52,580
Jaki Bob? Nie znam żadnego Boba, a ty?

360
00:37:52,670 --> 00:37:57,600
Bez urazy, ale ten facet
ma nierówno pod sufitem.

361
00:37:57,690 --> 00:38:02,540
Minęły 24 godziny. Oskarżcie
mojego klienta albo wypuśćcie.

362
00:38:06,120 --> 00:38:10,840
Panie Horne, aresztuję pana
za zabójstwo Laury Palmer.

363
00:38:13,730 --> 00:38:14,760
Brawo, Jerry.

364
00:38:15,090 --> 00:38:18,650
Hawk, zabierz pana Gerarda do hotelu.

365
00:38:18,740 --> 00:38:22,470
Zabijcie okna gwoździami
i zostaw dwóch ludzi.

366
00:38:22,560 --> 00:38:25,850
Ty tępy drwalu! Jesteś tu skończony!

367
00:38:28,420 --> 00:38:29,980
Harry.

368
00:38:31,230 --> 00:38:33,220
Wyjdźmy.

369
00:38:39,170 --> 00:38:42,380
Chyba dzielimy skórę na niedźwiedziu.

370
00:38:42,470 --> 00:38:43,720
Nie rozumiem.

371
00:38:43,810 --> 00:38:46,380
Nie sądzę, by to on zabił Laurę.

372
00:38:46,470 --> 00:38:49,110
- Co?
- Trzeba było go zwolnić.

373
00:38:49,200 --> 00:38:50,750
Cooper...

374
00:38:50,830 --> 00:38:54,490
cały czas cię popierałem,
ale mam dość tych czarów,

375
00:38:54,580 --> 00:39:00,330
snów, wizji, karłów, olbrzymów,
Tybetu i całej reszty.

376
00:39:00,410 --> 00:39:03,710
Mamy dowody przeciwko Horne'owi

377
00:39:03,790 --> 00:39:05,930
i muszę go zamknąć.

378
00:39:06,630 --> 00:39:12,210
Masz rację, to twoje podwórko.
Czasami się zapominam.

379
00:39:19,920 --> 00:39:24,950
�oso� jest �wie�y i soczysty,
choć trochę przesmażony.

380
00:39:25,470 --> 00:39:27,870
- Mój też.
- W sam raz.

381
00:39:30,010 --> 00:39:34,070
- Przepraszam was na chwilę.
- Pójdę z tobą.

382
00:39:35,090 --> 00:39:36,980
Wracajcie zaraz.

383
00:39:54,840 --> 00:39:57,070
Nic im nie mów, dobra?

384
00:39:58,300 --> 00:40:02,950
Jeśli Vivian dowie się,
że siedziałem, to mnie spławi.

385
00:40:03,700 --> 00:40:04,990
Ufarbowałeś włosy.

386
00:40:07,320 --> 00:40:09,320
Ernie Niles...

387
00:40:09,410 --> 00:40:11,630
Ernie "Profesor" Niles...

388
00:40:13,270 --> 00:40:15,420
- Kiedy wyszedłeś?
- Dwa tygodnie temu.

389
00:40:15,630 --> 00:40:17,420
Ale mówię ci...

390
00:40:17,500 --> 00:40:21,380
ostatnie pół roku bez twojego
towarzystwa to było piekło.

391
00:40:21,470 --> 00:40:26,540
Nie było z kim pójść do biblioteki
ani popracować w warsztacie.

392
00:40:28,910 --> 00:40:31,010
Co ty z nią kombinujesz?

393
00:40:31,100 --> 00:40:32,760
Nic.

394
00:40:32,850 --> 00:40:36,190
Nie mamy przed sobą tajemnic.
Ona śpi na forsie.

395
00:40:37,430 --> 00:40:41,780
Poznaliśmy się na kweście
Republikanów i wzięliśmy ślub.

396
00:40:43,350 --> 00:40:46,660
Musiałeś ją nieźle zbajerować.

397
00:40:47,050 --> 00:40:52,960
Teraz staram się mówić
tylko prawdę. To terapia.

398
00:40:55,320 --> 00:40:58,090
A Vivian chce mi
powierzyć swoje finanse.

399
00:40:59,800 --> 00:41:02,470
Przedstawisz jej swojego lichwiarza?

400
00:41:02,700 --> 00:41:07,020
Skończyłem z tym. Z hazardem też.

401
00:41:07,100 --> 00:41:10,140
Chodzę na mityngi. I do kościoła.

402
00:41:10,520 --> 00:41:14,350
A jak ci strzeli do głowy
gwizdnąć forsę na parafialny piknik?

403
00:41:14,430 --> 00:41:19,780
W życiu nie okradłem kościoła.
Narobiłem tylko długów.

404
00:41:19,870 --> 00:41:25,420
To choroba. Odpokutowałem swoje
i zaczynam od nowa.

405
00:41:27,070 --> 00:41:30,230
- Naprawdę nic nie wie?
- Nie.

406
00:41:31,040 --> 00:41:33,670
To dobrze...

407
00:41:34,640 --> 00:41:38,680
bo dla mnie rodzina
to najważniejsza sprawa na świecie.

408
00:41:38,770 --> 00:41:44,760
Dla niej trzeba być gotowym
na wszystko. Zgadzasz się?

409
00:41:46,320 --> 00:41:49,580
Tak, w zupełności.

410
00:41:50,880 --> 00:41:53,020
Jeszcze pogadamy.

411
00:41:53,110 --> 00:41:57,570
Jak wtedy na spacerniaku:
dwaj koledzy spod celi.

412
00:41:58,270 --> 00:42:01,220
- Jesteśmy.
- Drogie panie.

413
00:42:05,630 --> 00:42:12,800
Wypijmy za zdrowie
nowożeńców i ich szczęście.

414
00:42:12,880 --> 00:42:15,050
Dziękuję, jesteś kochany.

415
00:42:15,130 --> 00:42:18,040
Za Erniego i Vivian.

416
00:42:19,160 --> 00:42:22,020
Ich przyszłe sukcesy

417
00:42:22,100 --> 00:42:24,590
i wszelką pomyślność.

418
00:42:25,810 --> 00:42:27,590
To urocze.

419
00:42:57,610 --> 00:43:03,080
Jest 23:05, jestem
w swoim pokoju w hotelu.

420
00:43:03,160 --> 00:43:06,160
Niebo jest bezgwiezdne,

421
00:43:06,240 --> 00:43:08,610
Ben Horne siedzi,

422
00:43:09,340 --> 00:43:11,230
a trop jest wyraźniejszy.

423
00:43:11,310 --> 00:43:16,580
Jestem blisko, ale ostatnie kroki
zawsze są najtrudniejsze.

424
00:43:37,150 --> 00:43:40,490
- Możemy porozmawiać?
- Proszę.

425
00:43:41,710 --> 00:43:45,470
- Tutaj pan oberwał?
- Tak.

426
00:43:48,560 --> 00:43:49,800
Aresztowaliście go?

427
00:43:52,490 --> 00:43:53,140
Tak.

428
00:43:55,230 --> 00:43:56,640
Zrobił to?

429
00:43:57,290 --> 00:43:59,870
O tym zdecyduje sąd.

430
00:43:59,960 --> 00:44:01,310
A jak pan myśli?

431
00:44:02,440 --> 00:44:05,190
To nie ma znaczenia.

432
00:44:05,280 --> 00:44:07,740
To przez to, co powiedziałam?

433
00:44:08,240 --> 00:44:09,360
Częściowo.

434
00:44:10,220 --> 00:44:12,080
Ale trochę tak?

435
00:44:14,930 --> 00:44:19,830
A ja tylko chciałam, żeby mnie kochał.

436
00:44:20,990 --> 00:44:22,500
Na pewno tak jest.

437
00:44:24,340 --> 00:44:27,510
- Wstydzi się mnie.
- Wcale nie.

438
00:44:38,500 --> 00:44:43,200
Gdy byłam w Jednookim Jacku,
nikomu nie pozwoliłam...

439
00:44:43,290 --> 00:44:46,390
Nie musisz nic mówić.

440
00:44:46,480 --> 00:44:49,960
- Musi pan wiedzieć.
- Wiem.

441
00:44:58,000 --> 00:44:59,860
Muszę odebrać.

442
00:45:08,680 --> 00:45:11,340
Cooper. Gdzie?

443
00:45:13,940 --> 00:45:15,980
Kiedy?

444
00:45:16,070 --> 00:45:19,280
- Już jadę.
- Co się stało?

445
00:45:19,360 --> 00:45:21,830
- Idź do siebie i zarygluj drzwi.
- Dlaczego?

446
00:45:21,920 --> 00:45:24,490
Rób, co mówię.

447
00:45:24,580 --> 00:45:26,260
Dobrze.

448
00:46:14,620 --> 00:46:15,700
To Maddie Ferguson.

