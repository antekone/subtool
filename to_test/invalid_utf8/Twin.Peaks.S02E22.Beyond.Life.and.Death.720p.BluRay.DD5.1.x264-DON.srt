1
00:00:28,980 --> 00:00:33,770
MIASTECZKO TWIN PEAKS

2
00:01:42,800 --> 00:01:45,070
Bałaś się, kotku?

3
00:01:46,370 --> 00:01:50,970
Stałam na scenie w Roadhouse,
gdy nagle przygasło światło.

4
00:01:51,060 --> 00:01:54,980
Pomyślałam: "Światło gaśnie!".

5
00:01:55,070 --> 00:01:56,700
I zgasło.

6
00:01:57,640 --> 00:02:00,580
Zrobiło się ciemno, a ja myślałam:

7
00:02:00,660 --> 00:02:04,910
"Jak ciemno!
Nie dogadasz się nawet na migi".

8
00:02:04,990 --> 00:02:10,810
A gdyby tak zgasło w szpitalu,
a my bylibyśmy w windzie?

9
00:02:11,450 --> 00:02:14,680
Wtedy pomógłbym ci urodzić w windzie,

10
00:02:14,770 --> 00:02:17,560
na oczach Boga i wszystkich.

11
00:02:25,350 --> 00:02:26,920
Kocham cię.

12
00:02:44,090 --> 00:02:48,550
Szukają Earle'a w trzech okręgach.
Przepadł bez śladu.

13
00:02:48,630 --> 00:02:52,530
Jedyna nadzieja, że mapa nam go wskaże.

14
00:02:56,170 --> 00:02:57,750
Olbrzym...

15
00:02:58,340 --> 00:02:59,740
karzeł...

16
00:03:00,880 --> 00:03:01,980
ogień...

17
00:03:02,820 --> 00:03:05,240
ogniu, krocz ze mną...

18
00:03:10,070 --> 00:03:13,500
Ogniu, krocz ze mną...

19
00:03:16,440 --> 00:03:19,440
Zgłaszam kradzież.

20
00:03:23,360 --> 00:03:27,130
Pieńkowa Dama ukradła mój samochód.

21
00:03:30,120 --> 00:03:34,600
Rocznik '68, niebieski dodge pickup.

22
00:03:35,210 --> 00:03:39,640
Goniłem ją, ale skręciła do lasu.

23
00:03:42,460 --> 00:03:48,120
Pieńkowa Dama nie ukradła ci
samochodu. Zaraz tu będzie.

24
00:03:50,310 --> 00:03:53,680
Las, Harry.

25
00:03:53,760 --> 00:03:55,700
Ghostwood.

26
00:03:56,010 --> 00:03:57,570
Ghostwood...

27
00:04:02,020 --> 00:04:06,870
Miałem tam 12 pstrągów.

28
00:04:08,520 --> 00:04:10,500
Chwila.

29
00:04:11,160 --> 00:04:16,180
Krąg dwunastu platanów.
Gaj Glastonbury.

30
00:04:18,480 --> 00:04:21,730
Tam znalazłem zakrwawiony ręcznik
i kartki z pamiętnika.

31
00:04:24,060 --> 00:04:28,810
Legendarne miejsce spoczynku
Króla Artura. Glastonbury.

32
00:04:28,890 --> 00:04:33,200
Król Artur leży w Anglii.

33
00:04:37,680 --> 00:04:39,590
O ile wiem.

34
00:04:45,460 --> 00:04:47,250
To Pieńkowa Dama.

35
00:04:52,700 --> 00:04:55,980
Gdzie mój samochód?

36
00:04:56,060 --> 00:04:59,390
Pete... to Windom Earle go ukradł.

37
00:05:03,460 --> 00:05:05,140
Przyniosłam olej.

38
00:05:05,220 --> 00:05:06,580
Dziękuję.

39
00:05:07,210 --> 00:05:10,730
Dałbym głowę, że to ona.

40
00:05:12,210 --> 00:05:18,030
Margaret, co pani mąż
mówił o tym oleju?

41
00:05:19,470 --> 00:05:22,450
Przyniósł go pewnej nocy,
niedługo przed śmiercią.

42
00:05:22,540 --> 00:05:28,210
Powiedział, że ten olej otwiera wrota.

43
00:05:30,150 --> 00:05:32,230
Ciekawe, prawda?

44
00:05:41,710 --> 00:05:45,610
To jest to. Spalony olej silnikowy.

45
00:05:46,190 --> 00:05:48,880
Hawk, wprowadź Ronette Pulaski.

46
00:05:54,990 --> 00:05:56,950
Dziękujemy, że przyszłaś.

47
00:05:58,510 --> 00:06:00,710
Poznajesz ten zapach?

48
00:06:10,460 --> 00:06:11,890
Tak...

49
00:06:13,370 --> 00:06:16,240
Z nocy, gdy zginęła Laura.

50
00:06:32,810 --> 00:06:35,620
Gaj Glastonbury.

51
00:06:37,310 --> 00:06:39,540
Jestem Windom Earle.

52
00:06:44,740 --> 00:06:49,320
Spójrz. 12 tęczowych pstrągów.

53
00:06:50,700 --> 00:06:54,530
- Zabij mnie! Na co czekasz?
- Mamy jeszcze czas.

54
00:06:54,620 --> 00:06:57,440
Ale cieszy mnie ta niecierpliwość.

55
00:07:10,930 --> 00:07:14,540
Ich kości nad czeluścią Szeolu.
Do Ciebie, Panie,

56
00:07:14,620 --> 00:07:18,120
zwracam moje oczy,
do Ciebie się uciekam.

57
00:07:18,200 --> 00:07:21,780
Strzeż mnie od sidła,
które zastawili na mnie.

58
00:07:21,860 --> 00:07:25,080
Niechaj występni wpadną
w Twoje sieci, gdy ja ujdę cało...

59
00:07:27,860 --> 00:07:28,760
Co tu robimy?

60
00:07:37,330 --> 00:07:42,570
Jesteśmy umówieni na końcu świata.

61
00:07:45,470 --> 00:07:47,650
- Przyjdzie po mnie.
- Nie.

62
00:07:49,340 --> 00:07:52,110
- Dlaczego pan mi to robi?
- Tak samo było poprzednio,

63
00:07:52,200 --> 00:07:54,250
gdy zakochał się w mojej żonie.

64
00:07:54,340 --> 00:07:57,310
Doprowadziłem go na krawędź.

65
00:07:58,030 --> 00:08:02,240
Chodź! Wejdź do kręgu!

66
00:08:06,150 --> 00:08:09,380
Tamci nie umarli.

67
00:08:09,470 --> 00:08:14,360
Ich ręce spotkają się z naszymi.

68
00:08:20,360 --> 00:08:22,630
Już mi nie uciekniesz.

69
00:08:25,100 --> 00:08:27,810
Nie z tego kręgu drzew.

70
00:08:30,210 --> 00:08:32,260
Pójdziesz ze mną.

71
00:09:03,250 --> 00:09:09,470
Okład na 20 minut i 20 minut przerwy.
To zmniejszy obrzęk.

72
00:09:09,550 --> 00:09:13,340
Weźcie aspirynę i zadzwońcie rano.

73
00:09:15,590 --> 00:09:16,960
Dzięki, doktorze.

74
00:09:20,930 --> 00:09:24,910
Bardzo cię boli, kochanie?
Tak się o ciebie bałem.

75
00:09:25,340 --> 00:09:30,530
Przyznaję, czasami wątpiłem,
czy do siebie pasujemy,

76
00:09:30,610 --> 00:09:33,240
ale gdy zobaczyłem,
jak spada na ciebie ten worek...

77
00:09:34,460 --> 00:09:35,950
wiedziałem...

78
00:09:37,300 --> 00:09:39,610
że dla ciebie zrobię wszystko.

79
00:09:40,710 --> 00:09:42,420
A potem dostałem w głowę.

80
00:09:47,040 --> 00:09:48,530
Kocham cię.

81
00:09:53,740 --> 00:09:57,900
Wyznałem to i cieszę się.

82
00:10:05,610 --> 00:10:06,890
Coś ty za jeden?

83
00:10:07,920 --> 00:10:09,320
Mike...

84
00:10:11,090 --> 00:10:12,050
Ty...

85
00:10:15,710 --> 00:10:18,720
nazywasz się Mike?

86
00:10:20,030 --> 00:10:22,090
Co robisz w moim domu?

87
00:10:24,400 --> 00:10:27,230
- Kazałaś mi się wprowadzić.
- Nie!

88
00:10:28,090 --> 00:10:30,470
Wynoś się!

89
00:10:30,990 --> 00:10:34,270
Ed, niech on wyjdzie!

90
00:10:35,550 --> 00:10:37,770
I ona tu jest?

91
00:10:42,690 --> 00:10:45,160
Tak się nie robi.

92
00:10:47,740 --> 00:10:49,450
Proszę...

93
00:11:00,990 --> 00:11:05,620
Gdzie moje prowadnice? Gdzie one są?

94
00:11:05,710 --> 00:11:09,930
- Ile masz lat?
- Co to za głupie pytanie?

95
00:11:10,010 --> 00:11:12,770
Odpowiedz! Ile masz lat?

96
00:11:12,860 --> 00:11:15,440
35, kretynie!

97
00:11:25,590 --> 00:11:27,050
Przepraszam, Ed.

98
00:11:28,880 --> 00:11:31,740
Chyba powinienem się wycofać.

99
00:11:54,510 --> 00:11:58,310
- Proszę, wysłuchaj mnie.
- Dość słyszałam!

100
00:11:58,390 --> 00:12:00,520
Pozwól mi się wytłumaczyć!

101
00:12:00,600 --> 00:12:02,060
Zostawcie mnie!

102
00:12:02,620 --> 00:12:05,900
To nie wina twoich rodziców,
tylko moja.

103
00:12:06,200 --> 00:12:09,450
Rodziców? To znaczy czyja?

104
00:12:10,070 --> 00:12:14,780
Chciałem zrobić coś dobrego.
Być dobry.

105
00:12:15,370 --> 00:12:19,400
To wspaniałe uczucie wyznać prawdę.

106
00:12:21,390 --> 00:12:23,500
Po tylu latach.

107
00:12:36,890 --> 00:12:39,350
Ostrzegałem. Wynoś się z mojego domu!

108
00:12:42,950 --> 00:12:47,480
Wybaczysz mi to, co wam zrobiłem?

109
00:12:55,720 --> 00:12:59,700
Sylvio, miałaś zostać w domu.

110
00:13:00,040 --> 00:13:03,000
Co ty robisz z tą rodziną?

111
00:13:03,470 --> 00:13:07,840
Tato... Ty jesteś moim tatą.
Ty nim jesteś.

112
00:13:12,160 --> 00:13:13,590
Tatusiu...

113
00:13:14,140 --> 00:13:16,550
Zostaw nas w spokoju!

114
00:13:48,480 --> 00:13:50,460
Wiedziałem.

115
00:13:51,150 --> 00:13:53,170
To klucz od skrytki.

116
00:13:57,900 --> 00:14:00,320
Nie połapią się.

117
00:14:13,960 --> 00:14:15,360
Dobranoc.

118
00:14:47,910 --> 00:14:49,040
Harry...

119
00:14:50,570 --> 00:14:51,790
tędy.

120
00:15:30,690 --> 00:15:32,850
Muszę pójść sam.

121
00:15:33,750 --> 00:15:35,060
Dlaczego?

122
00:16:50,280 --> 00:16:52,280
Platany.

123
00:17:13,110 --> 00:17:15,950
Olej otwiera wrota...

124
00:17:23,000 --> 00:17:24,410
To tutaj.

125
00:18:19,940 --> 00:18:21,550
Boże...

126
00:18:39,340 --> 00:18:47,180
Pod platanem...

127
00:19:33,900 --> 00:19:40,750
Zobaczę ciebie

128
00:19:40,830 --> 00:19:45,510
A ty ujrzysz mnie


129
00:19:45,600 --> 00:19:50,960
Dostrzegę cię wśród gałęzi drzew

130
00:19:51,040 --> 00:20:01,380
Gnących się pod niebem

131
00:20:02,160 --> 00:20:08,720
Ujrzę cię wśród drzew

132
00:20:09,970 --> 00:20:17,560
Gdy pod drzewem stanę

133
00:20:19,310 --> 00:20:31,560
Zobaczę cię pod platanem

134
00:21:00,210 --> 00:21:01,980
Szeryfie?

135
00:21:05,840 --> 00:21:07,450
Szeryfie Truman!

136
00:21:10,830 --> 00:21:13,720
Andy! Jestem tutaj!

137
00:21:53,090 --> 00:21:56,860
Jest tam od 10 godzin.

138
00:22:03,970 --> 00:22:06,470
Przynieść termos kawy?

139
00:22:11,600 --> 00:22:12,860
Tak.

140
00:22:16,370 --> 00:22:19,470
I coś na śniadanie?

141
00:22:25,000 --> 00:22:26,240
Tak.

142
00:22:30,780 --> 00:22:32,640
A deser?

143
00:22:38,290 --> 00:22:39,510
Też.

144
00:22:42,680 --> 00:22:44,490
Placka?

145
00:23:07,070 --> 00:23:10,640
- Dzień dobry, panie Mibbler.
- Audrey Horne!

146
00:23:10,720 --> 00:23:15,400
Mam dziś dobry dzień.

147
00:23:20,440 --> 00:23:23,500
Czy mogę coś dla pani zrobić?

148
00:23:23,580 --> 00:23:27,210
Proszę zadzwonić do Gazety Miejskiej.

149
00:23:27,600 --> 00:23:29,740
To znaczy...

150
00:23:31,670 --> 00:23:35,710
Do Dwayna Milforda Juniora, wydawcy.

151
00:23:38,770 --> 00:23:42,300
Czyli muszę zadzwonić.

152
00:23:49,020 --> 00:23:49,940
Tak...

153
00:23:51,320 --> 00:23:57,520
Proszę powiedzieć, że Audrey Horne
przykuła się do furty skarbca,

154
00:23:57,600 --> 00:24:02,280
w proteście przeciw finansowaniu
budowy w Ghostwood.

155
00:24:02,880 --> 00:24:06,790
I że zamierzam tu zostać,
aż rada miejska zwoła posiedzenie

156
00:24:06,870 --> 00:24:14,090
na temat wpływu inwestycji
w Ghostwood na środowisko.

157
00:24:15,100 --> 00:24:16,840
Tak, panienko.

158
00:24:17,230 --> 00:24:19,050
A mogę prosić o szklankę wody?

159
00:25:06,830 --> 00:25:08,280
Dziękuję.

160
00:25:38,100 --> 00:25:43,240
Pan... To pan!

161
00:25:43,870 --> 00:25:49,040
- Dell Mibbler, zdrów jak ja.
- On żyje.

162
00:25:49,120 --> 00:25:55,150
A pogrzeb? Te wszystkie kwiaty
i chór chłopięcy?

163
00:25:55,230 --> 00:26:00,080
Liczę na twoją pomoc. Wygląda znajomo?

164
00:26:00,170 --> 00:26:05,260
Tak, to klucz do naszej skrytki.

165
00:26:05,340 --> 00:26:06,550
Można?

166
00:26:06,640 --> 00:26:14,800
Oczywiście. Ale może być mały kłopot.

167
00:26:27,270 --> 00:26:28,680
Cześć, Pete.

168
00:26:29,910 --> 00:26:33,480
Dlaczego przykułaś się
do furty skarbca?

169
00:26:33,560 --> 00:26:35,540
W ramach protestu.

170
00:26:38,180 --> 00:26:43,270
"Nie rozprawiaj o prawości,
tylko taki bądź". Marek Aureliusz.

171
00:26:43,350 --> 00:26:47,730
To godne pochwały.
Nie widzę tu problemu.

172
00:26:47,820 --> 00:26:51,310
- Można?
- Oczywiście.

173
00:27:00,350 --> 00:27:04,280
Zadzwonił pan do gazety? I do szeryfa.

174
00:27:04,360 --> 00:27:06,820
Proszę spytać o agenta Coopera.

175
00:27:13,470 --> 00:27:16,210
Proszę o klucz.

176
00:28:03,650 --> 00:28:05,400
Chłopiec? To chłopiec!

177
00:28:16,810 --> 00:28:19,590
Dotarliśmy do kresu długiej drogi.

178
00:28:19,670 --> 00:28:21,320
Oto on.

179
00:28:24,550 --> 00:28:26,920
Mam cię, Andrew.
Pozdrowienia. Thomas.

180
00:29:05,590 --> 00:29:06,590
Shelly...

181
00:29:08,280 --> 00:29:10,170
powinniśmy się pobrać.

182
00:29:14,110 --> 00:29:17,660
A Leo? Wciąż noszę obrączkę.

183
00:29:27,510 --> 00:29:31,530
Co się stało, Heidi?
Przysnęłaś nad parówką?

184
00:29:32,290 --> 00:29:34,110
Samochód nie chciał zapalić.

185
00:29:35,580 --> 00:29:38,990
- Trudno rozruszać staruszka?
- Znowu.

186
00:29:41,060 --> 00:29:43,870
Myślałem, że Niemcy są punktualni.

187
00:29:55,430 --> 00:29:58,560
Leo ma pewnie ubaw po pachy.

188
00:30:05,190 --> 00:30:08,200
Miała pani rację. Jest major.

189
00:30:16,040 --> 00:30:23,180
Majorze Briggs, przepraszamy,
ale Sarah ma dla pana wiadomość.

190
00:30:36,380 --> 00:30:42,910
Jestem w Czarnej Chacie
z Windomem Earle'em.

191
00:30:55,550 --> 00:31:02,880
Czekam na ciebie.

192
00:31:27,010 --> 00:31:35,990
Kiedy znów mnie zobaczysz,
to nie będę ja.

193
00:31:47,130 --> 00:31:51,750
To jest poczekalnia.

194
00:31:58,390 --> 00:32:02,620
Napijesz się kawy?

195
00:32:05,830 --> 00:32:10,560
Są tu twoi znajomi.

196
00:32:26,680 --> 00:32:31,040
Witam, agencie Cooper.

197
00:32:44,840 --> 00:32:50,750
Zobaczymy się znów za 25 lat,

198
00:32:50,940 --> 00:32:52,980
a tymczasem...

199
00:33:36,900 --> 00:33:38,570
Kawa.

200
00:34:14,270 --> 00:34:20,400
Jeden i ten sam.

201
00:35:25,140 --> 00:35:31,180
Brawo, Bob!

202
00:35:34,340 --> 00:35:38,120
Ogniu, krocz ze mną.

203
00:36:55,530 --> 00:36:57,520
Nie tędy.

204
00:37:29,060 --> 00:37:33,410
Następna znajoma.

205
00:37:54,480 --> 00:37:57,170
Jestem Maddy.

206
00:37:58,010 --> 00:38:02,570
Opiekuj się moją kuzynką.

207
00:38:53,850 --> 00:38:57,480
Mroczny bliźniak!

208
00:39:06,630 --> 00:39:09,110
A tymczasem...

209
00:40:32,350 --> 00:40:33,770
Caroline...

210
00:40:45,120 --> 00:40:46,450
Annie?

211
00:41:34,120 --> 00:41:36,170
Dale...

212
00:41:36,250 --> 00:41:42,260
widziałam twarz... mojego zabójcy.

213
00:41:43,640 --> 00:41:44,850
Annie...

214
00:41:47,400 --> 00:41:49,560
twarz twojego zabójcy?

215
00:41:50,190 --> 00:41:52,310
To był mój mąż.

216
00:41:56,190 --> 00:41:57,810
Kim jest Annie?

217
00:41:59,790 --> 00:42:02,520
To ja.

218
00:42:04,540 --> 00:42:05,660
To ja.

219
00:42:10,220 --> 00:42:11,370
Caroline...

220
00:42:12,260 --> 00:42:17,670
Mylisz się. Ja żyję.

221
00:42:38,630 --> 00:42:40,900
Dale Cooper.

222
00:43:10,120 --> 00:43:17,530
Oddaj mi duszę,
to daruję Annie życie.

223
00:43:18,010 --> 00:43:20,110
Zgoda.

224
00:43:39,970 --> 00:43:42,460
Cicho!

225
00:43:44,640 --> 00:43:47,660
Milcz!

226
00:43:51,900 --> 00:43:55,310
Odejdź.

227
00:43:58,250 --> 00:44:02,960
On się myli.

228
00:44:04,400 --> 00:44:16,020
Nie może żądać twojej duszy.

229
00:44:16,550 --> 00:44:23,670
Ja wezmę jego.

230
00:45:20,750 --> 00:45:25,650
Nikogo nie zabiłem.

231
00:47:14,550 --> 00:47:16,260
Obudził się.

232
00:47:21,310 --> 00:47:23,230
Nie spałem.

233
00:47:25,440 --> 00:47:27,300
A Annie? Co z nią?

234
00:47:27,380 --> 00:47:31,200
Wyjdzie z tego, jest w szpitalu.

235
00:47:37,070 --> 00:47:39,210
Muszę umyć zęby.

236
00:47:40,680 --> 00:47:41,880
Jasne.

237
00:47:44,100 --> 00:47:45,440
Pomogę.

238
00:47:46,460 --> 00:47:47,750
Ostrożnie.

239
00:48:00,800 --> 00:48:02,350
Muszę umyć zęby.

240
00:48:03,210 --> 00:48:04,600
Słusznie.

241
00:49:11,820 --> 00:49:13,480
Nic ci nie jest?

242
00:49:17,210 --> 00:49:18,930
Co z Annie?

243
00:49:19,010 --> 00:49:20,440
Co z Annie?

244
00:49:20,530 --> 00:49:21,990
Co z Annie?

245
00:49:24,210 --> 00:49:26,840
Co z Annie?

