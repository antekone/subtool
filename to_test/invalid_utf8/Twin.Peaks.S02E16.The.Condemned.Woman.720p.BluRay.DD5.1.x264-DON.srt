1
00:00:28,860 --> 00:00:33,560
MIASTECZKO TWIN PEAKS

2
00:01:37,000 --> 00:01:40,730
Słuchaj uważnie, Dale.

3
00:01:40,810 --> 00:01:43,200
Czas na twój ruch.

4
00:01:43,880 --> 00:01:46,980
I przyłóż się, dobrze?

5
00:01:47,060 --> 00:01:50,270
Zauważyłem w twoim rozumowaniu
pewną niespójność,

6
00:01:50,360 --> 00:01:55,170
jakbyś nie był w pełni skupiony
na szachownicy.

7
00:01:55,250 --> 00:02:01,540
Może to upośledzać nie tylko
myślenie, ale i przewidywanie.

8
00:02:01,620 --> 00:02:06,830
To groźne w każdej partii,
a w tym konkretnym przypadku

9
00:02:06,920 --> 00:02:11,560
grozi katastrofą, bo gramy o życie.

10
00:02:12,800 --> 00:02:15,700
Co już bez wątpienia zrozumiałeś.

11
00:02:17,740 --> 00:02:20,610
Zamieść swój ruch
w jutrzejszej gazecie

12
00:02:20,700 --> 00:02:22,630
albo cię w tym wyręczę.

13
00:02:23,470 --> 00:02:27,870
Wszystko jasne.
Nie spuszczę cię z oka.

14
00:02:27,950 --> 00:02:30,950
Gdyby tego chciał, już bym nie żył.

15
00:02:35,690 --> 00:02:37,290
Lepiej wezwać Pete'a.

16
00:02:37,780 --> 00:02:38,700
Racja.

17
00:02:40,100 --> 00:02:42,880
Lucy, zadzwoń do Pete'a Martella,
niech zaraz przyjdzie.

18
00:02:42,960 --> 00:02:48,050
A potem do gazety i spytaj o termin
składania jutrzejszych ogłoszeń.

19
00:02:48,980 --> 00:02:51,910
Gazeta i Pete.
Załatwię to alfabetycznie.

20
00:02:58,660 --> 00:03:00,060
Była piękna.

21
00:03:03,780 --> 00:03:05,690
Tylko ją jedną kochałem.

22
00:03:14,120 --> 00:03:17,510
Powiedz Harry'emu, że zaraz będę.

23
00:03:39,340 --> 00:03:40,350
Peter.

24
00:03:41,260 --> 00:03:42,860
Sól i pieprz.

25
00:03:43,580 --> 00:03:44,920
Już się robi.

26
00:03:48,330 --> 00:03:51,230
Budzicie w sobie to, co najgorsze.

27
00:03:51,310 --> 00:03:55,280
- Pete to książę!
- Raczej dworski błazen.

28
00:03:59,070 --> 00:04:01,600
Muszę się zwijać.

29
00:04:11,980 --> 00:04:16,960
Żegnajcie, słodcy Packardowie.

30
00:04:23,110 --> 00:04:25,050
Catherine...

31
00:04:25,130 --> 00:04:28,820
wciąż mam nadzieję, że czas uciszy

32
00:04:29,070 --> 00:04:31,600
tę szalejącą w tobie nawałnicę.

33
00:04:32,060 --> 00:04:37,330
Kiedy patrzę na wasze wygłupy,
wszystko się we mnie burzy.

34
00:04:37,780 --> 00:04:39,960
Co się dzieje z Ghostwood?

35
00:04:40,480 --> 00:04:43,580
Jestem umówiony z inwestorami,
jutro lecę do Paryża.

36
00:04:45,450 --> 00:04:46,340
Mamy gości!

37
00:04:48,040 --> 00:04:50,060
Kochana Josie!

38
00:04:55,490 --> 00:04:57,100
Wróciłem. Nie tęskniłaś?

39
00:05:02,590 --> 00:05:04,380
Zaskoczyłem ją.

40
00:05:06,330 --> 00:05:09,610
- Biedulka.
- Ale urocza.

41
00:05:09,950 --> 00:05:12,550
Już niedługo.

42
00:05:22,780 --> 00:05:26,040
Sprawca nie zostawił śladów

43
00:05:26,630 --> 00:05:28,300
Proszę.

44
00:05:30,320 --> 00:05:33,300
- Cześć.
- Daruj sobie.

45
00:05:34,350 --> 00:05:37,310
Co jest? Wstałeś dziś lewą nogą?

46
00:05:37,870 --> 00:05:40,270
Złamałem warunki, to mnie wsadź.

47
00:05:40,870 --> 00:05:43,170
Przeszedłeś przez granicę,

48
00:05:43,880 --> 00:05:48,080
robiłeś interesy z handlarzami
narkotyków i usiłowałeś kogoś zabić.

49
00:05:49,530 --> 00:05:52,110
Oskarżam cię o próbę
zabójstwa Leo Johnsona.

50
00:05:54,870 --> 00:06:00,310
Lepiej uznać, że tamtego wieczoru
byłem w barze, ale jeśli chcesz...

51
00:06:00,390 --> 00:06:03,960
Widziano cię tam z bronią w ręku.

52
00:06:05,250 --> 00:06:11,240
Najwyraźniej nie żartujesz,
więc mam propozycję.

53
00:06:11,920 --> 00:06:18,170
Mogę ci pomóc ująć i skazać
zabójcę Andrew Packarda.

54
00:06:18,710 --> 00:06:22,520
Hank, świadek oskarżenia.

55
00:06:23,890 --> 00:06:25,680
Nic z tego.

56
00:06:25,760 --> 00:06:32,270
Więc wolisz próbę zabójstwa Leo
od mordercy Andrew Packarda?

57
00:06:32,360 --> 00:06:36,670
- Jak to wyjaśnisz wyborcom?
- Skończ z tym.

58
00:06:36,760 --> 00:06:41,030
Trudno ci będzie.
Zwłaszcza gdy się dowiedzą,

59
00:06:41,120 --> 00:06:44,120
że sypiasz z kobietą
odpowiedzialną za jego śmierć.

60
00:06:52,850 --> 00:06:54,090
Przepraszam.

61
00:06:54,660 --> 00:06:57,930
Zabierz go stąd. Jazda!

62
00:07:02,310 --> 00:07:03,670
Dawaj!

63
00:07:10,300 --> 00:07:15,380
Tę kulkę wyjęliśmy z ciebie,
a tę z czaszki tamtego trupa.

64
00:07:15,460 --> 00:07:19,660
Ta sama broń i sprawca. Zgarnijmy ją.

65
00:07:19,750 --> 00:07:21,360
Bez pośpiechu.

66
00:07:22,460 --> 00:07:27,820
Głupio ci przymknąć ukochaną kolegi,
ale cię podziurawiła.

67
00:07:29,420 --> 00:07:31,280
Jestem obiektywny.

68
00:07:31,820 --> 00:07:34,800
- A wynik badania rękawiczek?
- Po piątej.

69
00:07:34,890 --> 00:07:39,110
Nie złość się, ale wszyscy wokół niej
padają na ołowicę.

70
00:07:39,200 --> 00:07:40,790
Jest zabójcza.

71
00:07:40,870 --> 00:07:44,710
Pomówię z nią, może sama się przyzna.

72
00:07:45,940 --> 00:07:48,060
Albo wyhoduje skrzydła i odfrunie.

73
00:08:05,390 --> 00:08:10,530
To są jednorazowi goście.
Wyślemy do nich zaproszenia.

74
00:08:10,610 --> 00:08:13,650
Twój entuzjazm mnie rozczula,

75
00:08:13,730 --> 00:08:20,090
ale przekonasz się, że obcisły sweterek
i bezczelność to za mało.

76
00:08:21,000 --> 00:08:25,710
Tylko się uczę, nie odbiorę ci pracy.

77
00:08:25,790 --> 00:08:28,170
Polecam obsługę pokoi.

78
00:08:29,900 --> 00:08:31,720
To przyszło dziś rano.

79
00:08:33,200 --> 00:08:36,510
Życzę szczęścia, przyda ci się.

80
00:08:54,380 --> 00:08:55,630
Cześć.

81
00:08:56,360 --> 00:08:58,380
Mieszkam w 215.

82
00:08:58,460 --> 00:08:59,840
Winszuję.

83
00:09:01,380 --> 00:09:03,840
Przysłali mnie tu z recepcji.

84
00:09:04,830 --> 00:09:06,600
Czym mogę służyć?

85
00:09:06,680 --> 00:09:10,460
- Myślałem, że tu pracujesz.
- Bo pracuję.

86
00:09:11,460 --> 00:09:13,940
Widzi pan?

87
00:09:14,460 --> 00:09:17,440
Widzę... Audrey.

88
00:09:21,230 --> 00:09:22,280
Horne?

89
00:09:22,860 --> 00:09:24,650
Do usług.

90
00:09:24,730 --> 00:09:29,980
Wyślecie kogoś na lotnisko?
Bagaż nie zmieścił się w samochodzie.

91
00:09:30,070 --> 00:09:32,500
- Który to lot?
- Prywatny.

92
00:09:33,880 --> 00:09:35,750
Mój samolot wpada w oko.

93
00:09:38,740 --> 00:09:41,380
Coś jeszcze, panie Rockefeller?

94
00:09:42,640 --> 00:09:44,510
Dziękuję.

95
00:09:48,760 --> 00:09:52,560
Mam twoje zdjęcie w bawarskiej
sukience i z mysimi ogonkami.

96
00:09:52,640 --> 00:09:54,810
Coś pięknego.

97
00:09:56,980 --> 00:09:58,480
Nie noszę warkoczyków.

98
00:09:58,950 --> 00:10:03,910
Wtedy nosiłaś.
To było tutaj, w jadalni.

99
00:10:06,990 --> 00:10:08,770
Widzę cię, gdy zamknę oczy.

100
00:10:10,040 --> 00:10:13,330
Audrey Horne jako Heidi.

101
00:10:18,070 --> 00:10:21,150
Niezapomniany widok.

102
00:10:26,100 --> 00:10:27,500
Miałam dziesięć lat...

103
00:10:55,060 --> 00:10:56,080
"Ratuj ukochanego.

104
00:10:56,170 --> 00:10:59,960
Przyjdź o wpół do dziesiątej
do Roadhouse na zlot aniołów".

105
00:11:11,860 --> 00:11:13,150
Nadine?

106
00:11:18,090 --> 00:11:20,310
Nie jesteś w szkole?

107
00:11:20,790 --> 00:11:22,400
Źle się czujesz?

108
00:11:22,880 --> 00:11:23,890
Przestań.

109
00:11:26,900 --> 00:11:28,220
Co?

110
00:11:29,050 --> 00:11:31,850
Eddie... musimy porozmawiać.

111
00:11:34,930 --> 00:11:37,960
Mike i ja kochamy się.

112
00:11:42,880 --> 00:11:45,130
- Kochacie się?
- Chyba cię nie zraniłam?

113
00:11:47,610 --> 00:11:51,060
Za nic nie chciałabym, żebyś cierpiał.

114
00:11:51,140 --> 00:11:53,600
Musisz mi uwierzyć.

115
00:11:54,350 --> 00:11:58,390
Kiedy byliśmy na zawodach,

116
00:11:58,980 --> 00:12:03,940
przeżyliśmy cudowną noc.

117
00:12:05,060 --> 00:12:06,190
Całą?

118
00:12:09,460 --> 00:12:11,240
Wy z Normą też to robiliście.

119
00:12:13,510 --> 00:12:14,930
Racja.

120
00:12:17,740 --> 00:12:18,910
Co to oznacza?

121
00:12:24,590 --> 00:12:28,820
Spójrzmy prawdzie w oczy.

122
00:12:30,650 --> 00:12:32,800
Rozstańmy się.

123
00:12:37,960 --> 00:12:39,870
Tak mi przykro...

124
00:12:48,860 --> 00:12:52,930
Josie, mów szczerze,
co się wydarzyło w Seattle.

125
00:12:53,010 --> 00:12:56,910
Już mówiłam. Znów muszę powtarzać?

126
00:12:57,450 --> 00:13:03,500
Uciekłam Jonathanowi na lotnisku
i nie wiem, co się z nim stało.

127
00:13:04,040 --> 00:13:09,230
Wiesz, że mogę cię aresztować?
Przyszedłem jako przyjaciel Harry'ego.

128
00:13:09,910 --> 00:13:15,440
Nie wiem, co ty do niego czujesz,
ale on oddał ci serce.

129
00:13:16,000 --> 00:13:19,280
Masz szansę się wytłumaczyć.

130
00:13:19,360 --> 00:13:20,780
Proszę odejść.

131
00:13:29,770 --> 00:13:32,920
To koniec, nie masz już wyjścia.

132
00:13:37,220 --> 00:13:40,430
Bądź o dziewiątej...

133
00:13:40,510 --> 00:13:42,250
albo ja po ciebie przyjdę.

134
00:13:53,920 --> 00:13:58,680
Widziałam agenta Coopera.
Wpadł z wizytą?

135
00:14:06,080 --> 00:14:09,010
Fatalnie wyglądasz.

136
00:14:10,670 --> 00:14:12,670
Wszystko w porządku?

137
00:14:12,760 --> 00:14:14,190
Źle się czuję.

138
00:14:14,270 --> 00:14:16,390
Przykro mi.

139
00:14:16,470 --> 00:14:18,960
Żałuję, że nie mam lepszych wieści.

140
00:14:19,040 --> 00:14:21,600
Rozmawiałam o tobie
z panem Eckhardtem.

141
00:14:21,690 --> 00:14:26,250
Nie jest bez serca,
ale chce cię widzieć.

142
00:14:27,630 --> 00:14:28,890
Wieczorem.

143
00:14:29,950 --> 00:14:31,790
Zabije mnie.

144
00:14:31,880 --> 00:14:38,480
Ja bardziej obawiałabym się
jego reakcji na wieść, że Andrew żyje.

145
00:14:38,560 --> 00:14:41,260
Nie uzna, że go zdradziłaś?

146
00:14:41,970 --> 00:14:47,580
Możesz powiedzieć, że nie,
skoro i tak myślałaś, że zginął.

147
00:14:49,050 --> 00:14:50,390
Nie...

148
00:14:53,630 --> 00:14:55,090
Sama nie wiem.

149
00:14:58,280 --> 00:15:00,610
Pomożesz mi?

150
00:15:00,690 --> 00:15:02,370
Bo oszaleję.

151
00:15:06,590 --> 00:15:10,960
W końcu i tak
musisz się z nim spotkać...

152
00:15:12,250 --> 00:15:14,130
i powiedzieć prawdę.

153
00:15:16,230 --> 00:15:17,820
Albo mów, co chcesz.

154
00:15:24,380 --> 00:15:27,790
Tylko módl się, żeby uwierzył.

155
00:15:29,040 --> 00:15:30,520
Tutaj są!

156
00:16:06,620 --> 00:16:09,210
Nie odbiera, pewnie już idzie.

157
00:16:09,290 --> 00:16:13,490
- Dziękuję. Przyłączysz się?
- Ja?

158
00:16:13,570 --> 00:16:20,700
Tak. Zebrania rad nadzorczych
to zazwyczaj spotkania

159
00:16:20,780 --> 00:16:25,650
ludzi myślących wyłącznie o zyskach,

160
00:16:26,470 --> 00:16:27,740
ale dziś...

161
00:16:28,670 --> 00:16:31,830
dziś będzie zupełnie inaczej.

162
00:16:33,240 --> 00:16:37,690
Witam. Cześć, Ben.

163
00:16:40,680 --> 00:16:42,560
Jakże się cieszę!

164
00:16:42,640 --> 00:16:45,330
- Miałem rozmowę.
- Zajęli się tobą?

165
00:16:45,410 --> 00:16:48,470
Na medal.
Bagaże już są. Dzięki, Audrey.

166
00:16:50,570 --> 00:16:53,100
- Spotkaliście się?
- Przelotnie.

167
00:16:54,070 --> 00:16:55,990
To John Justice Wheeler,

168
00:16:56,710 --> 00:16:59,700
dawniej budowlaniec, ale się wybił.

169
00:16:59,780 --> 00:17:00,530
To miło.

170
00:17:01,860 --> 00:17:03,160
Pamiętasz mojego brata, Jerry'ego.

171
00:17:06,270 --> 00:17:13,300
- A to mój asystent, Bob Briggs.
- Bardzo mi miło.

172
00:17:13,380 --> 00:17:15,160
Może usiądźmy.

173
00:17:18,670 --> 00:17:22,010
Pozwoliłem sobie zaprosić
pana Wheelera na naradę.

174
00:17:23,250 --> 00:17:27,640
Kiedyś w niego zainwestowałem,

175
00:17:27,720 --> 00:17:31,430
a on zmienił ochłapy w fortunę.

176
00:17:33,070 --> 00:17:38,640
Zawsze w niego wierzyłem,

177
00:17:39,500 --> 00:17:43,400
jeszcze gdy jako zwykły urwis
szwendał się po naszym miasteczku.

178
00:17:43,480 --> 00:17:47,740
Poprosiłem go,
żeby teraz on uwierzył we mnie.

179
00:17:48,960 --> 00:17:52,000
To ciężka próba
dla mojej przyzwoitości.

180
00:17:55,470 --> 00:18:02,140
Przyznaję, że firma Horne'ów
przeżywa ciężkie chwile.

181
00:18:02,220 --> 00:18:07,640
Tartak i ziemia w Ghostwood
są dziś wyłączną własnością Catherine.

182
00:18:07,720 --> 00:18:12,690
Chociaż tartak
i przedtem należał do niej.

183
00:18:13,800 --> 00:18:16,520
W obliczu takich przeciwności

184
00:18:17,310 --> 00:18:21,860
i bez perspektyw na sukces,
co nam zostaje?

185
00:18:23,420 --> 00:18:25,200
Duch walki.

186
00:18:31,300 --> 00:18:37,540
Co najcenniejszego może dać
człowiek innym ludziom?

187
00:18:41,470 --> 00:18:42,920
Przyszłość.

188
00:18:46,410 --> 00:18:48,020
A ja daję wam...

189
00:18:51,700 --> 00:18:54,890
kunę świerkową.

190
00:18:55,630 --> 00:18:59,610
W stanie Waszyngton
występuje tylko w naszym okręgu

191
00:19:00,700 --> 00:19:02,530
i jest gatunkiem zagrożonym.

192
00:19:02,810 --> 00:19:04,810
Ma przechlapane.

193
00:19:06,080 --> 00:19:12,930
Według raportu środowiskowego
ostatnie, które tu jeszcze żyją,

194
00:19:13,010 --> 00:19:18,330
zginą po zrealizowaniu
inwestycji w Ghostwood.

195
00:19:18,700 --> 00:19:22,460
Chce pan ratować kuny?

196
00:19:22,820 --> 00:19:24,240
Nie, Bobby...

197
00:19:25,770 --> 00:19:27,490
nie tylko kuny...

198
00:19:29,430 --> 00:19:31,940
ale i nasz styl życia.

199
00:19:33,180 --> 00:19:38,560
Chcę, by Twin Peaks pozostało oazą

200
00:19:39,180 --> 00:19:42,980
w świecie powszechnej zagłady.

201
00:19:43,280 --> 00:19:46,200
Zablokujemy budowę Catherine,

202
00:19:46,280 --> 00:19:50,280
póki los nie odwróci się
na naszą korzyść. Genialne.

203
00:19:54,080 --> 00:20:01,480
Będę walczył z tą budową
gdzie się da i jak tylko się da.

204
00:20:03,750 --> 00:20:11,820
Niedługo wszyscy
będą tu mówić o kunach.

205
00:20:14,880 --> 00:20:16,310
A co potem?

206
00:20:19,270 --> 00:20:20,360
Później...

207
00:20:21,300 --> 00:20:23,140
zamierzam ubiegać się...

208
00:20:24,110 --> 00:20:25,840
o wybór do senatu.

209
00:20:35,000 --> 00:20:36,230
Dziękuję.

210
00:20:46,500 --> 00:20:49,550
To naprawdę nie kłopot,
miejsca jest dość.

211
00:20:50,040 --> 00:20:53,070
Przyjedź najbliższym autobusem.
Będę czekać.

212
00:20:54,450 --> 00:20:58,490
Kochanie, nie płacz,
wszystko będzie dobrze.

213
00:20:59,260 --> 00:21:00,620
Niedługo się zobaczymy.

214
00:21:01,550 --> 00:21:04,310
Ja też cię kocham. Pa.

215
00:21:06,580 --> 00:21:09,400
- Twoja siostra przyjeżdża?
- Jutro.

216
00:21:11,150 --> 00:21:13,550
Jak ją wyciągnęłaś z klasztoru?

217
00:21:14,610 --> 00:21:17,060
Przecież to nie więzienie.

218
00:21:17,150 --> 00:21:20,670
Prawie. Nie mają
telewizji ani chłopaków.

219
00:21:22,030 --> 00:21:27,190
Już dawno myślałam, że Annie
urodziła się w złych czasach.

220
00:21:27,690 --> 00:21:30,040
Zakon dobrze jej służy.

221
00:21:31,270 --> 00:21:33,680
Nie wyobrażam jej sobie
w naszym świecie.

222
00:21:36,070 --> 00:21:37,630
Coś do ciebie.

223
00:21:38,290 --> 00:21:41,380
- Od kogo?
- Od tego, kto tu siedział.

224
00:21:46,360 --> 00:21:52,680
Patrz: "Całują góry,
fale tulą, kwiat, słońce, księżyc,

225
00:21:52,760 --> 00:21:54,950
wszystko, nie możesz..."
To bez sensu.

226
00:21:57,660 --> 00:21:58,630
"Ratuj ukochanego.

227
00:21:58,710 --> 00:22:02,250
Przyjdź o wpół do dziesiątej
do Roadhouse na zlot aniołów."

228
00:22:02,800 --> 00:22:06,750
Brzmi ciekawie, ale groźnie.

229
00:22:08,700 --> 00:22:10,500
Kto to przysłał?

230
00:22:25,980 --> 00:22:31,990
Normo, kocham cię od 20 lat
i co noc o tobie marzę.

231
00:22:32,070 --> 00:22:34,300
Musimy być razem.

232
00:22:34,380 --> 00:22:35,960
Wyjdziesz za mnie?

233
00:22:37,830 --> 00:22:41,880
Zasługujemy na szczęście. Nareszcie.

234
00:23:16,650 --> 00:23:20,650
Tuliłem ją całą noc

235
00:23:20,730 --> 00:23:25,340
By nie przemokła we mgle

236
00:23:25,420 --> 00:23:27,380
Dobra robota.

237
00:23:27,870 --> 00:23:29,570
Brawo.

238
00:23:31,720 --> 00:23:34,500
Jeszcze kilka i coś przekąsimy.

239
00:23:35,100 --> 00:23:42,840
"Leć ku mej piersi, okraś ją
w barwy jesieni i o miłości mów."

240
00:23:44,480 --> 00:23:48,640
Leo, masz braki w wykształceniu.
Weźmy choćby

241
00:23:48,720 --> 00:23:53,200
umiejętność przeżycia w dziczy,
przyjacielu.

242
00:23:53,280 --> 00:23:58,490
Przyda ci się mój praktyczny kurs.

243
00:23:58,570 --> 00:24:00,880
Mam nadzieję, że uważasz.

244
00:24:05,170 --> 00:24:06,540
Natura jest okrutna.

245
00:24:07,780 --> 00:24:09,500
To także lekcja.

246
00:24:29,040 --> 00:24:30,400
Cześć, skarbie.

247
00:24:31,630 --> 00:24:33,150
Jak się czujesz?

248
00:24:34,830 --> 00:24:35,900
Świetnie...

249
00:24:36,860 --> 00:24:38,340
bo tu jesteś.

250
00:24:42,210 --> 00:24:44,580
Przyszłam cię prosić o rozwód.

251
00:24:46,690 --> 00:24:53,330
Właściwie cię nie winię.
Dałaś mi szansę, a ja zawaliłem.

252
00:24:54,610 --> 00:24:57,660
Nie wiem, dlaczego sam siebie niszczę,

253
00:24:57,740 --> 00:25:02,510
ale naprawdę chcę wyjść na prostą.

254
00:25:02,860 --> 00:25:04,940
Muszę nad sobą popracować.

255
00:25:05,030 --> 00:25:08,870
Postanowiłem iść na terapię.

256
00:25:10,350 --> 00:25:12,350
Nie chcę już taki być.

257
00:25:14,940 --> 00:25:16,710
To bardzo ciekawe...

258
00:25:18,210 --> 00:25:20,110
ale ja chcę żyć.

259
00:25:24,180 --> 00:25:25,280
Tak.

260
00:25:27,030 --> 00:25:28,590
Wiem.

261
00:25:29,390 --> 00:25:31,140
Chcę twojego szczęścia.

262
00:25:32,090 --> 00:25:33,370
Dziękuję.

263
00:25:36,940 --> 00:25:39,140
Proszę o ostatnią przysługę.

264
00:25:40,390 --> 00:25:42,090
Pomóż mi stąd wyjść.

265
00:25:44,100 --> 00:25:47,190
Zdechnę, jeśli mnie odstawią do pudła.

266
00:25:47,270 --> 00:25:51,390
Powiedz szeryfowi, że byłem
w barze, gdy postrzelili Leo.

267
00:25:51,470 --> 00:25:54,310
Jechałem do domu.
Możesz to poświadczyć.

268
00:25:54,950 --> 00:25:59,360
- Dość tych kłamstw.
- Uratujesz mi życie.

269
00:26:00,010 --> 00:26:01,690
Nie.

270
00:26:04,750 --> 00:26:06,480
Zabijasz mnie.

271
00:26:07,510 --> 00:26:09,910
Nie waż się zwalać tego na mnie!

272
00:26:10,550 --> 00:26:13,750
To Ed? Do niego uciekasz?

273
00:26:18,820 --> 00:26:19,860
Dobra...

274
00:26:21,400 --> 00:26:27,590
Zróbmy tak: w zamian za alibi
dostaniesz rozwód.

275
00:26:27,880 --> 00:26:30,320
Nie przyszłam pertraktować.

276
00:26:30,920 --> 00:26:32,470
To koniec.

277
00:26:32,790 --> 00:26:33,980
Dobra.

278
00:26:36,760 --> 00:26:37,710
Idź.

279
00:26:40,930 --> 00:26:42,440
Bądź jego dziwką.

280
00:26:45,360 --> 00:26:49,570
Wolę to, niż być twoją żoną!

281
00:27:00,330 --> 00:27:04,250
Są na to cztery, a może pięć sposobów.

282
00:27:04,330 --> 00:27:06,730
Każdy ma zalety.

283
00:27:06,820 --> 00:27:13,400
Gdy Capablanca grał z Laskerem
w Sankt Petersburgu w 1914...

284
00:27:13,480 --> 00:27:18,100
- Mamy tylko pięć minut.
- No tak.

285
00:27:21,700 --> 00:27:24,560
Zadzwoń do redakcji,
żeby nie zamykali numeru.

286
00:27:24,640 --> 00:27:26,960
- Potrzebujemy czasu.
- Dzwonię.

287
00:27:29,830 --> 00:27:30,990
Załatwione.

288
00:27:32,150 --> 00:27:35,670
Gwarantuję kilka nieprzespanych nocy.

289
00:27:35,750 --> 00:27:38,350
Byle tylko niczego nie zbił.

290
00:27:38,430 --> 00:27:41,870
Nie da rady. Dopiero
w piątym albo szóstym ruchu.

291
00:27:41,950 --> 00:27:45,410
Może zabić mimo to, z wściekłości.

292
00:27:45,490 --> 00:27:50,080
Nie sądzę. Earle stosuje się
do swoich zasad.

293
00:27:50,160 --> 00:27:53,140
Nie słyszałem, żeby ktoś
mordował według zasad.

294
00:27:58,610 --> 00:27:59,780
Świetnie, Pete.

295
00:28:05,470 --> 00:28:07,090
Powiem wprost.

296
00:28:08,270 --> 00:28:12,710
Na rękawiczkach Josie
były ślady prochu.

297
00:28:12,790 --> 00:28:16,430
W Seattle mają świadka,
który widział Josie,

298
00:28:16,510 --> 00:28:21,240
jak wysiadła z samochodu denata.
Przymknij ją, bo pomyślę...

299
00:28:21,890 --> 00:28:23,340
Załatwię to.

300
00:28:35,050 --> 00:28:37,060
Już załatwiłeś.

301
00:28:48,880 --> 00:28:49,900
Proszę.

302
00:29:02,550 --> 00:29:06,840
Zakładam, że wciąż lubisz szampana.
Wypiliśmy niejedną butelkę.

303
00:29:09,930 --> 00:29:11,310
Za początki...

304
00:29:12,570 --> 00:29:15,510
za zakończenia
i umiejętność ich rozróżniania.

305
00:29:18,580 --> 00:29:20,100
Przykro mi.

306
00:29:22,170 --> 00:29:26,450
Najpierw cię znienawidziłem,
to zrozumiałe.

307
00:29:32,160 --> 00:29:37,230
Gdy gniew minął,
przypomniałem sobie, że Eckhardt

308
00:29:37,310 --> 00:29:41,440
umie przekonać do wszystkiego,
a to był jego pomysł.

309
00:29:41,520 --> 00:29:42,920
Tak.

310
00:29:44,000 --> 00:29:45,690
Zmusił mnie.

311
00:29:46,590 --> 00:29:51,790
Powiedział, że mnie nie kochałeś
i ożeniłeś się ze mną na złość.

312
00:29:51,870 --> 00:29:53,680
Nieprawda, bardzo cię kochałem.

313
00:29:55,340 --> 00:29:58,070
W przeciwieństwie do ciebie.

314
00:29:58,840 --> 00:30:02,390
Nie okłamujmy się więcej.

315
00:30:03,210 --> 00:30:07,170
Zrobiłaś, co miałaś zrobić,
i teraz za to płacisz.

316
00:30:08,000 --> 00:30:10,280
Każde działanie ma jakieś skutki.

317
00:30:12,170 --> 00:30:13,510
Policja jest na twoim tropie

318
00:30:13,590 --> 00:30:17,620
i jeśli nic nie zrobisz,
będziesz dziś spać w areszcie.

319
00:30:17,700 --> 00:30:20,230
Co mam zrobić? Pomóż mi.

320
00:30:20,320 --> 00:30:22,120
Idź do Eckhardta.

321
00:30:24,320 --> 00:30:27,380
Może jednak
jesteście sobie przeznaczeni.

322
00:30:28,690 --> 00:30:30,730
On cię kocha.

323
00:30:33,280 --> 00:30:36,930
Nie wie, że żyję,
i może cię stąd wywieźć.

324
00:30:39,490 --> 00:30:41,040
Idź do niego.

325
00:30:43,900 --> 00:30:44,870
Natychmiast.

326
00:30:53,590 --> 00:30:55,220
Już się nie zobaczymy.

327
00:31:15,440 --> 00:31:18,000
- Cześć.
- Cześć.

328
00:31:19,280 --> 00:31:20,590
Jak tu ładnie.

329
00:31:21,640 --> 00:31:24,620
Chciałam pojechać gdzieś,
gdzie jeszcze nie byliśmy.

330
00:31:25,620 --> 00:31:26,760
Chodź.

331
00:31:29,780 --> 00:31:31,820
Jak poszło na policji?

332
00:31:31,910 --> 00:31:35,120
Zadawali milion pytań,
na które nie znam odpowiedzi.

333
00:31:35,200 --> 00:31:37,540
Ale wystarczyło im to,
co powiedziałem.

334
00:31:39,050 --> 00:31:42,060
Evelyn stanie przed sądem,
a ja będę świadkiem.

335
00:31:44,770 --> 00:31:47,830
- Zdjęłaś pierścionek.
- Wiem o was.

336
00:31:51,840 --> 00:31:54,130
Masz prawo mnie znienawidzić.

337
00:31:56,560 --> 00:32:01,110
Wiem, co czułeś, bo czułam to samo.

338
00:32:01,190 --> 00:32:03,350
Evelyn to wykorzystała.

339
00:32:04,210 --> 00:32:06,840
Źle zrobiłem, powinienem to wiedzieć.

340
00:32:09,540 --> 00:32:14,650
Masz prawo fatalnie się czuć,
ale mnie nie musisz żałować.

341
00:32:16,050 --> 00:32:17,990
Gdyby można zacząć od nowa.

342
00:32:19,710 --> 00:32:21,490
Wróć ze mną.

343
00:32:22,340 --> 00:32:23,950
Teraz nie mogę.

344
00:32:29,980 --> 00:32:32,920
To prawda, jedź.

345
00:32:36,590 --> 00:32:38,170
A co z tobą?

346
00:32:38,600 --> 00:32:40,530
Nie martw się o mnie.

347
00:32:41,970 --> 00:32:45,060
Byłam świadkiem strasznych wydarzeń.

348
00:32:45,760 --> 00:32:48,320
Teraz tęsknię za czymś dobrym.

349
00:32:51,100 --> 00:32:54,870
Jedź i nie spiesz się z powrotem.

350
00:32:55,400 --> 00:32:57,190
Kocham cię.

351
00:32:59,590 --> 00:33:01,890
Będę okropnie tęsknić.

352
00:33:03,520 --> 00:33:06,830
Wyjedziesz, ale wrócisz
z mnóstwem wspomnień.

353
00:33:08,150 --> 00:33:11,930
I nie o Laurze, Maddie ani Evelyn.

354
00:33:13,810 --> 00:33:15,340
A ja tu będę.

355
00:33:16,960 --> 00:33:18,510
Jedź ze mną.

356
00:33:18,790 --> 00:33:19,970
Nie.

357
00:33:23,420 --> 00:33:26,410
W takim razie obiecuję, że wrócę.

358
00:34:03,360 --> 00:34:04,870
Idę!

359
00:34:06,870 --> 00:34:07,950
Idę.

360
00:34:16,870 --> 00:34:21,790
- Muszę się zobaczyć z Josie.
- Wejdź, ale chyba jej nie ma.

361
00:34:21,870 --> 00:34:25,430
To dziwne.
Wzięła mój wóz bez uprzedzenia.

362
00:34:25,510 --> 00:34:30,500
- Wybierała się do Great Northern.
- Tak? Sporo ze sobą wzięła.

363
00:34:30,580 --> 00:34:35,060
Nie wiem, dokąd pojechała,
ale chyba na dłużej.

364
00:34:35,140 --> 00:34:36,750
Mówiła coś jeszcze?

365
00:34:38,040 --> 00:34:41,460
Że spotka się z dawnym znajomym.

366
00:34:41,930 --> 00:34:43,010
Z kim?

367
00:34:46,420 --> 00:34:51,430
Wszystkim nam trudno
pogodzić się z prawdą o Josie.

368
00:34:55,520 --> 00:34:56,810
Kto to jest?

369
00:34:58,590 --> 00:35:01,190
Thomas Eckhardt.

370
00:35:03,490 --> 00:35:04,670
Biedak.

371
00:35:18,290 --> 00:35:20,050
Na dół.

372
00:35:26,590 --> 00:35:27,840
Thomas.

373
00:35:31,670 --> 00:35:33,120
Co za spotkanie.

374
00:35:35,620 --> 00:35:37,890
Nie wierzę w duchy.

375
00:35:38,100 --> 00:35:44,040
Wielka szkoda.
Powrót straszliwej zjawy zza grobu.

376
00:35:44,120 --> 00:35:46,310
Zasłużyłeś, żeby cię straszyć.

377
00:35:47,600 --> 00:35:49,750
Przypatrz mi się.

378
00:35:50,600 --> 00:35:52,950
Ja żyję!

379
00:35:54,640 --> 00:35:55,770
Dlaczego?

380
00:35:56,460 --> 00:35:58,430
To chyba jasne.

381
00:36:00,240 --> 00:36:01,250
Josie.

382
00:36:02,490 --> 00:36:07,600
Ostrzegła mnie. Nie mogłaby
patrzeć na śmierć męża.

383
00:36:08,080 --> 00:36:11,850
Albo raczej sądziła,
że coś na tym zyska.

384
00:36:12,950 --> 00:36:14,310
Zdradziła mnie.

385
00:36:15,710 --> 00:36:18,590
Obaj dobrze to znamy.

386
00:36:18,680 --> 00:36:21,730
Josie należy do mnie.

387
00:36:24,130 --> 00:36:27,020
Uważaj, bo pęknie ci serce.

388
00:36:28,180 --> 00:36:31,560
Na szczęście już jej nie chcę.

389
00:36:33,000 --> 00:36:37,360
Za to Josie bardzo łatwo
gubi serduszko.

390
00:36:37,440 --> 00:36:40,300
Choćby z tutejszym szeryfem.

391
00:36:40,380 --> 00:36:43,250
- To już załatwiłem.
- Nie wątpię.

392
00:36:44,370 --> 00:36:50,800
Czasami się zastanawiam,
czy nasza Josie nie robi tego celowo,

393
00:36:51,180 --> 00:36:52,460
a ty?

394
00:36:54,910 --> 00:36:58,230
Jako dawny przyjaciel...

395
00:36:58,310 --> 00:37:00,440
przyszedłem cię ostrzec.

396
00:37:01,960 --> 00:37:04,110
Josie wróci, bądź ostrożny.

397
00:37:05,780 --> 00:37:08,400
Zawsze jestem.

398
00:37:10,520 --> 00:37:12,370
To parter.

399
00:37:13,770 --> 00:37:19,000
Jadę do garażu.
Moje zmartwychwstanie to tajemnica.

400
00:37:25,400 --> 00:37:26,560
Thomas...

401
00:37:29,770 --> 00:37:30,340
do widzenia.

402
00:37:39,900 --> 00:37:42,510
Muszę się sporo nauczyć. Jack...

403
00:37:44,200 --> 00:37:46,490
udziel mi lekcji.

404
00:37:46,570 --> 00:37:50,400
Uznaj mnie za otwartą księgę,

405
00:37:50,480 --> 00:37:54,350
której strony masz zapisać.

406
00:37:57,980 --> 00:37:59,260
Dobrze się czujesz?

407
00:38:01,210 --> 00:38:02,780
Panie Wheeler...

408
00:38:04,190 --> 00:38:06,040
co pan właściwie robi?

409
00:38:06,120 --> 00:38:08,210
Proszę, mów mi Jack.

410
00:38:09,530 --> 00:38:11,720
Wykupuję bankrutujące firmy,

411
00:38:11,810 --> 00:38:16,210
inwestuję w nie, stawiam na nogi,
a potem sprzedaję.

412
00:38:16,290 --> 00:38:22,850
Z pokaźnym zyskiem. Ale gdy już
przestaną szkodzić środowisku.

413
00:38:22,940 --> 00:38:27,850
Gdy Jack skończy,
odpady są przetwarzane, powietrze...

414
00:38:32,370 --> 00:38:33,750
powietrze...

415
00:38:35,390 --> 00:38:39,020
czystsze, a ludzie szczęśliwsi.

416
00:38:39,560 --> 00:38:43,350
Robisz ze mnie Mikołaja,
a to czysty interes.

417
00:38:48,550 --> 00:38:54,250
Przepraszam, ale kucharz chciał
zadźgać Jerry'ego. Zaraz wrócę.

418
00:38:54,800 --> 00:38:55,970
Słuchaj uważnie.

419
00:39:03,460 --> 00:39:07,910
Panie Wheeler, zbankrutowaliśmy
czy tylko padamy?

420
00:39:08,000 --> 00:39:10,610
Pomagam przyjacielowi i mów mi Jack.

421
00:39:11,110 --> 00:39:13,780
Kuny są liczniejsze
niż przyjaciele taty.

422
00:39:15,560 --> 00:39:18,570
Powiedzmy, że kiedyś nimi byliśmy.

423
00:39:19,300 --> 00:39:21,760
I teraz nas pan uratuje.

424
00:39:21,850 --> 00:39:25,250
Jest pan Mikołajem. Wywieszę skarpetę.

425
00:39:27,830 --> 00:39:30,340
Niezbyt mnie lubisz.

426
00:39:33,780 --> 00:39:35,670
Nie mam powodu lubić.

427
00:39:36,280 --> 00:39:37,960
A gdybyś miała?

428
00:39:39,730 --> 00:39:45,430
Powiedziałabym, że Horne'owie radzili
sobie dłużej, niż się panu wydaje,

429
00:39:45,800 --> 00:39:50,850
i choć dziś jesteśmy w dołku,
tak będzie i w przyszłości.

430
00:39:57,560 --> 00:39:58,400
Nie wątpię.

431
00:40:03,530 --> 00:40:05,430
Gdzie pan bywał?

432
00:40:06,090 --> 00:40:11,960
Kiedy nie ratował pan wdów i sierot
i nie poprawiał pan świata?

433
00:40:14,260 --> 00:40:15,630
Na krańcach Ziemi.

434
00:40:17,220 --> 00:40:19,550
Jest naprawdę wspaniała.

435
00:40:22,920 --> 00:40:26,130
Ale miło wrócić do domu.

436
00:40:30,230 --> 00:40:32,360
Mam dopiero 18 lat.

437
00:40:33,920 --> 00:40:36,780
A co ma piernik do wiatraka?

438
00:40:37,920 --> 00:40:39,140
Nic.

439
00:40:42,980 --> 00:40:46,140
Już późno. Jestem umówiona.
Przekaże pan to tacie?

440
00:40:46,220 --> 00:40:49,290
- Jasne.
- Na razie.

441
00:40:56,090 --> 00:40:58,090
Do widzenia, Jack.

442
00:41:01,460 --> 00:41:03,160
Cześć, Audrey.

443
00:41:21,370 --> 00:41:24,900
- Co słychać?
- W porządku, a u ciebie?

444
00:41:25,420 --> 00:41:26,360
Do chrzanu.

445
00:41:28,440 --> 00:41:30,110
Czekasz na Jamesa?

446
00:41:30,780 --> 00:41:32,240
Nie.

447
00:41:32,330 --> 00:41:35,210
Dostałam jakiś pokręcony list.

448
00:41:35,290 --> 00:41:39,220
Ktoś chce, żebym tam przyszła. Dziwne.

449
00:41:40,180 --> 00:41:42,480
To spójrz, co ja mam.

450
00:41:44,200 --> 00:41:46,300
Chyba coś nas łączy.

451
00:41:48,420 --> 00:41:49,560
Pasuje.

452
00:41:51,120 --> 00:41:55,680
"Patrz! Całują góry niebo,
fale tulą się do siebie,

453
00:41:55,760 --> 00:41:59,920
żaden kwiat owocu nie da,
gdy nie ma drugiego.

454
00:42:00,000 --> 00:42:04,260
Światło słońca ziemię głaszcze,
księżyc pieści morze.

455
00:42:04,350 --> 00:42:08,300
Wszystko się w naturze kocha,
a ty mnie nie możesz."

456
00:42:10,060 --> 00:42:12,040
- Dziwne...
- Co najmniej.

457
00:42:32,800 --> 00:42:35,410
Cooper. Tak, Catherine?

458
00:42:35,990 --> 00:42:38,480
Nie, właśnie miałem sprawdzić.

459
00:42:39,590 --> 00:42:40,730
Jest tutaj?

460
00:42:42,040 --> 00:42:43,870
U Thomasa Eckhardta...

461
00:42:45,320 --> 00:42:46,560
Rozumiem.

462
00:43:01,020 --> 00:43:02,080
Puszczaj!

463
00:43:03,710 --> 00:43:04,710
Kocham cię!

464
00:43:04,790 --> 00:43:07,290
- Nie!
- Proszę!

465
00:43:07,370 --> 00:43:08,870
Nie dotykaj mnie!

466
00:43:35,620 --> 00:43:36,540
Ani kroku.

467
00:43:45,560 --> 00:43:47,180
Chciał mnie zabić.

468
00:43:47,600 --> 00:43:50,660
To samo powiesz o mnie?

469
00:43:52,250 --> 00:43:56,150
A Jonathan? Też tego próbował?

470
00:43:57,900 --> 00:44:01,730
- Przyjechał po mnie.
- Dlaczego do mnie strzelałaś?

471
00:44:02,630 --> 00:44:04,490
Bo pan tu przyjechał.

472
00:44:05,120 --> 00:44:07,560
Wiedziałam, że do tego dojdzie.

473
00:44:07,640 --> 00:44:10,490
Nie pójdę do więzienia. Nie mogę.

474
00:44:13,040 --> 00:44:14,370
Rzuć broń.

475
00:44:17,800 --> 00:44:19,420
Rzuć broń!

476
00:44:21,430 --> 00:44:24,690
Przebacz mi, nie chciałam cię zranić.

477
00:44:49,690 --> 00:44:51,030
Nie żyje...

478
00:45:10,310 --> 00:45:14,400
Coop, co się stało z martwą Josie?!

