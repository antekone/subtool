1
00:00:28,890 --> 00:00:33,660
MIASTECZKO TWIN PEAKS

2
00:01:34,980 --> 00:01:39,030
Noc czy dzień,
wystarczy zejść do tej groty

3
00:01:39,110 --> 00:01:44,380
- i już nie widać żadnej różnicy.
- Pewnie dlatego się nie zmienia.

4
00:01:44,460 --> 00:01:47,050
Nie spieszcie się. Uważajcie pod nogi.

5
00:01:47,130 --> 00:01:49,480
Teraz tam, w prawo.

6
00:01:51,000 --> 00:01:54,200
Harry, tu ktoś był.

7
00:01:54,280 --> 00:01:56,630
I odwalił za nas robotę.

8
00:01:56,710 --> 00:01:57,940
Boże.

9
00:02:03,480 --> 00:02:06,390
Te same ślady
widziałem przy rozdzielni.

10
00:02:07,910 --> 00:02:11,970
Ciężar na lewej stopie,
uszkodzony obcas.

11
00:02:12,480 --> 00:02:13,790
Windom Earle.

12
00:02:14,430 --> 00:02:19,640
- Co on tu robił?
- Aż strach pomyśleć.

13
00:02:22,190 --> 00:02:26,810
Andy, będę potrzebował
dokładnej kopii tego rytu.

14
00:02:26,900 --> 00:02:29,580
Harry, zawiadom majora Briggsa.

15
00:02:29,660 --> 00:02:32,480
- Ma przyjść na posterunek?
- Coś mamy.

16
00:02:48,810 --> 00:02:54,460
Dawno temu był sobie
zakątek pełen dobra...

17
00:02:55,850 --> 00:02:58,100
zwany Białą Chatą.

18
00:02:58,790 --> 00:03:03,430
Małe sarenki dokazywały tam
wśród śmiechu wesołych duchów.

19
00:03:04,120 --> 00:03:07,680
Powietrze przesycała
niewinność i radość.

20
00:03:08,330 --> 00:03:12,000
A gdy padał deszcz,
z nieba lał się słodki nektar,

21
00:03:12,080 --> 00:03:18,470
napełniający serca pragnieniem
życia w prawdzie i doskonałości.

22
00:03:20,210 --> 00:03:22,670
Innymi słowy,
było to odrażające miejsce,

23
00:03:22,760 --> 00:03:25,820
przesiąknięte odorem wszelkich cnót,

24
00:03:25,900 --> 00:03:29,030
wypełnione po brzegi modłami matek,

25
00:03:29,120 --> 00:03:31,080
miauczeniem niemowlaków

26
00:03:31,160 --> 00:03:35,360
i bredzeniem młodych i starych głupków,
czyniących dobro bez powodu.

27
00:03:36,870 --> 00:03:39,050
Ale na szczęście nasza opowieść

28
00:03:39,130 --> 00:03:43,540
nie kończy się w tym koszmarnym,
przesłodzonym grajdołku.

29
00:03:43,630 --> 00:03:47,460
Bo jest też inne miejsce,
przeciwieństwo poprzedniego.

30
00:03:47,540 --> 00:03:51,030
To królestwo niewyobrażalnej potęgi,

31
00:03:51,120 --> 00:03:55,220
pełne ciemnych mocy
i mrocznych tajemnic.

32
00:03:55,300 --> 00:03:59,760
Żadne modlitwy nie mają wstępu
do tej ponurej czeluści.

33
00:03:59,840 --> 00:04:04,100
Tutejsze duchy nie dbają
o dobre uczynki. Jest im obojętne,

34
00:04:04,190 --> 00:04:08,340
czy obedrą cię z ciała do kości,
czy życzą ci miłego dnia.

35
00:04:08,420 --> 00:04:13,940
Kto by zdołał okiełznać duchy
tej krainy zdławionych krzyków

36
00:04:14,020 --> 00:04:18,280
i złamanych serc,
otrzymałby władzę tak potężną,

37
00:04:18,360 --> 00:04:23,960
że byłby w stanie przerobić
cały świat wedle swego życzenia.

38
00:04:26,570 --> 00:04:31,010
Miejsce, o którym mówię,
jest znane jako Czarna Chata.

39
00:04:31,610 --> 00:04:34,120
I zamierzam je znaleźć.

40
00:04:36,710 --> 00:04:41,170
Fajowa historyjka,
ale obiecałeś mi browar.

41
00:04:41,250 --> 00:04:43,320
Miała być impreza.

42
00:04:43,410 --> 00:04:49,720
Biała Chata, Czarna Chata...
Co mnie to obchodzi?

43
00:04:49,800 --> 00:04:55,220
Cierpliwości. Wszystko w swoim czasie.

44
00:05:19,250 --> 00:05:26,300
Już nigdy nie zobaczę pewnie
dziewczyny tak jak Josie pięknej.

45
00:05:27,530 --> 00:05:34,440
Kiedy w pokoju się zjawiała,
kwiaty od razu...

46
00:05:35,940 --> 00:05:41,750
Zjawiała, konały...

47
00:05:41,830 --> 00:05:45,390
znikały, padały...

48
00:05:47,830 --> 00:05:52,280
Już nigdy nie zobaczę pewnie
dziewczyny tak jak Josie pięknej.

49
00:05:52,360 --> 00:05:58,490
Kiedy w pokoju się zjawiała,
kwiaty od razu rozkwitały.

50
00:06:00,390 --> 00:06:01,300
Josie...

51
00:06:01,380 --> 00:06:05,500
Przestań się mazgaić
i pomóż mi z tym pudełkiem.

52
00:06:07,870 --> 00:06:10,750
Co dokładnie miałbym zrobić?

53
00:06:11,450 --> 00:06:15,100
Otworzyć. Próbuję od paru dni.

54
00:06:15,190 --> 00:06:17,870
Thomas Eckhardt
zostawił mi je w spadku.

55
00:06:17,960 --> 00:06:20,220
Chcę je otworzyć, i to zaraz.

56
00:06:21,180 --> 00:06:25,640
Pokaż mi je. Masz klucz?

57
00:06:26,670 --> 00:06:31,190
Gdybym miała, już bym je otworzyła.

58
00:06:33,890 --> 00:06:36,510
Nie ma nawet dziurki.

59
00:06:38,140 --> 00:06:41,010
To łamigłówka.

60
00:06:41,810 --> 00:06:46,900
Widziałem takie w sklepie
z pamiątkami na Guam.

61
00:06:46,980 --> 00:06:51,020
Byłem tam na wycieczce
z bliźniakami Doolittle.

62
00:06:51,110 --> 00:06:57,510
Małpy pokazywały niesamowite
sztuczki, a Dale, ten wyższy...

63
00:06:57,590 --> 00:07:01,530
Gdy się zastanowić,
to byli bardzo różni...

64
00:07:04,370 --> 00:07:05,890
Przepraszam.

65
00:07:06,440 --> 00:07:12,060
Trzeba to odpowiednio poprzestawiać.

66
00:07:12,140 --> 00:07:14,080
Długo to potrwa?

67
00:07:16,970 --> 00:07:20,070
Może parę lat.

68
00:07:24,430 --> 00:07:28,070
Shel, sporo myślałem o przyszłości.

69
00:07:28,150 --> 00:07:30,420
I co daje przewagę w świecie.

70
00:07:31,430 --> 00:07:34,710
Czytam, oglądam telewizję i już wiem.

71
00:07:36,210 --> 00:07:38,260
Znam tajemnicę sukcesu.

72
00:07:39,590 --> 00:07:43,420
Piękni ludzie mają wszystko.

73
00:07:43,500 --> 00:07:44,920
Sama pomyśl.

74
00:07:45,010 --> 00:07:49,550
Kiedy jakaś ładna blondynka
poszła na krzesło elektryczne?

75
00:07:51,850 --> 00:07:54,690
Ty jesteś piękna.

76
00:07:55,770 --> 00:08:00,260
I powinniśmy przystąpić do tego spisku.

77
00:08:00,340 --> 00:08:02,450
Wydasz mnie za senatora?

78
00:08:03,650 --> 00:08:04,950
Zobacz.

79
00:08:05,560 --> 00:08:08,620
Miss Twin Peaks.

80
00:08:08,710 --> 00:08:12,040
Kiedy zdobędziesz koronę,
świat będzie nasz.

81
00:08:13,430 --> 00:08:18,170
- Śnij dalej, ja wracam do pracy.
- Nie kłóć się ze mną.

82
00:08:18,670 --> 00:08:20,930
Ja jestem szefem, jasne?

83
00:08:23,560 --> 00:08:25,220
Zgłoś się jeszcze dziś.

84
00:08:33,680 --> 00:08:38,230
Chciałabym, żebyś coś dla mnie zrobił.

85
00:08:40,110 --> 00:08:44,080
Wszystko, czemu podoła umysł i ciało.

86
00:08:45,290 --> 00:08:49,020
Od paru dni ci to udowadniam.

87
00:08:51,570 --> 00:08:53,130
Mów, o co chodzi.

88
00:08:57,460 --> 00:09:00,080
Chcę wygrać tytuł Miss Twin Peaks.

89
00:09:00,160 --> 00:09:01,720
Mów, o co chodzi.

90
00:09:04,190 --> 00:09:07,470
Chcę wygrać tytuł Miss Twin Peaks!

91
00:09:09,450 --> 00:09:13,780
Oczywiście. Wygłosiłem w życiu
niejedno przemówienie.

92
00:09:13,860 --> 00:09:16,060
Wszystkiego cię nauczę.

93
00:09:16,700 --> 00:09:20,980
Kochanie, nie o to mi chodzi.

94
00:09:21,060 --> 00:09:25,410
Jesteś sędzią. Możesz to załatwić.

95
00:09:28,500 --> 00:09:32,330
Ale... to nie w porządku.

96
00:09:33,570 --> 00:09:37,840
Skarbie... to tylko dowód miłości.

97
00:09:42,300 --> 00:09:46,430
Dla ciebie zrobię wszystko,
wiesz o tym.

98
00:09:47,720 --> 00:09:51,040
Tak, kochany. Wiem.

99
00:09:59,170 --> 00:10:00,530
Dzień dobry.

100
00:10:02,180 --> 00:10:02,820
Na śniadanie?

101
00:10:03,370 --> 00:10:06,040
Mam czterech głodnych stróżów prawa.

102
00:10:06,130 --> 00:10:11,290
Prosimy o pączki i kawę.
Dwie czarne, dwie białe, bez cukru.

103
00:10:11,960 --> 00:10:14,250
- Już się robi.
- Annie?

104
00:10:16,050 --> 00:10:20,380
Przy okazji, wybrałabyś się
po południu na wycieczkę?

105
00:10:22,090 --> 00:10:23,530
Na wycieczkę?

106
00:10:24,380 --> 00:10:25,600
Chętnie.

107
00:10:27,240 --> 00:10:28,470
Cudownie.

108
00:10:31,430 --> 00:10:35,140
Gdy z tobą rozmawiam,
czuję łaskotanie w palcach i żołądku.

109
00:10:35,990 --> 00:10:37,190
To ciekawe.

110
00:10:38,190 --> 00:10:40,110
I to nie od kawy.

111
00:10:41,420 --> 00:10:43,010
Przyniosę pączki.

112
00:10:50,900 --> 00:10:55,150
Shelly... jesteś piękna.

113
00:10:55,720 --> 00:10:57,570
Wykorzystaj to.

114
00:11:01,020 --> 00:11:05,530
"Wszystko się w naturze kocha,
a ty mnie nie możesz."

115
00:11:05,620 --> 00:11:06,510
Słucham?

116
00:11:08,930 --> 00:11:11,650
Ktoś mi przysłał wiersz.

117
00:11:13,810 --> 00:11:15,520
Musisz mi go pokazać.

118
00:11:21,170 --> 00:11:24,660
Był w trzech kawałkach,
dla Donny, Audrey i dla mnie.

119
00:11:31,800 --> 00:11:33,240
Muszę to wziąć.

120
00:11:38,130 --> 00:11:41,210
Tuzin pączków i cztery kawy:
dwie czarne i dwie białe, bez cukru.

121
00:11:42,070 --> 00:11:43,410
Dziękuję.

122
00:11:43,490 --> 00:11:46,170
- Agencie Cooper?
- Tak?

123
00:11:47,940 --> 00:11:49,180
Dziś po południu?

124
00:11:50,010 --> 00:11:53,080
Przyjdę po ciebie o czwartej.

125
00:12:06,640 --> 00:12:13,320
"Góry całują, fale tulą, kwiat..."

126
00:12:14,330 --> 00:12:15,790
Co to takiego?

127
00:12:17,030 --> 00:12:22,160
Wiersz. W trzech częściach,
wysłanych do Shelly Johnson,

128
00:12:22,240 --> 00:12:26,130
Audrey Horne i Donny Hayward
przez Windoma Earle'a.

129
00:12:26,920 --> 00:12:28,920
Skontaktował się z nimi trzema?

130
00:12:30,730 --> 00:12:31,900
Jesteś pewien?

131
00:12:33,880 --> 00:12:38,960
"Patrz! Całują góry niebo,
fale tulą się do siebie,

132
00:12:39,040 --> 00:12:42,830
żaden kwiat owocu nie da,
gdy nie ma drugiego."

133
00:12:45,130 --> 00:12:47,110
Wysłałem ten wiersz Caroline.

134
00:12:48,730 --> 00:12:50,950
Oby tylko się z nami drażnił.

135
00:12:51,740 --> 00:12:56,200
Czerpie perwersyjną przyjemność
z niszczenia cudzego życia.

136
00:12:57,300 --> 00:13:00,790
Trzeba spytać dziewczyny, co pamiętają.

137
00:13:05,530 --> 00:13:07,240
List Donny Hayward.

138
00:13:11,180 --> 00:13:16,650
Audrey jest w Seattle, wraca jutro.
Major Briggs czeka w sali odpraw.

139
00:13:17,370 --> 00:13:18,450
Hawk?

140
00:13:20,150 --> 00:13:24,030
Przynieś raport z zatrzymania Leo.

141
00:13:29,060 --> 00:13:30,150
Co robisz?

142
00:13:32,710 --> 00:13:35,410
Układam łamigłówkę.

143
00:13:36,180 --> 00:13:37,500
Posterunkowy Brennan?

144
00:13:38,040 --> 00:13:42,460
O ile pamiętam, ta linia
powinna biec w dół, nie w bok.

145
00:13:43,340 --> 00:13:44,960
Skąd pan wie?

146
00:13:45,990 --> 00:13:48,880
Majorze, dziękuję za przybycie.

147
00:13:52,480 --> 00:13:57,430
Może nam pan pomóc, ale nie umiem
powiedzieć jak ani dlaczego.

148
00:13:57,970 --> 00:13:59,030
Słucham.

149
00:13:59,660 --> 00:14:03,640
Prowadzimy dochodzenie w trzech
pozornie niezwiązanych sprawach:

150
00:14:04,570 --> 00:14:07,380
zniknięcia Leo,

151
00:14:07,470 --> 00:14:11,420
pojawienia się mojego byłego partnera
i mordercy, Windoma Earle'a,

152
00:14:11,500 --> 00:14:15,090
oraz odkrycia naskalnych rytów
w Sowiej Jaskini.

153
00:14:15,170 --> 00:14:20,510
Logika podpowiada,
by badać te sprawy indywidualnie,

154
00:14:20,600 --> 00:14:22,720
ale ja sądzę inaczej.

155
00:14:22,810 --> 00:14:28,370
Nie są rozdzielne,
a stanowią wersy jednej piosenki,

156
00:14:28,450 --> 00:14:32,960
której nie znam, ale ją wyczuwam.

157
00:14:33,040 --> 00:14:36,340
- I to mi wystarcza.
- Jak miałbym pomóc?

158
00:14:37,480 --> 00:14:42,720
Muszę wiedzieć wszystko o udziale
Earle'a w projekcie Błękitna Księga.

159
00:14:42,810 --> 00:14:45,940
Po moim zniknięciu
odebrano mi uprawnienia.

160
00:14:46,470 --> 00:14:49,270
Wraz z dostępem do akt projektu?

161
00:14:50,150 --> 00:14:52,660
Nie w tym rzecz.

162
00:14:53,350 --> 00:14:58,360
To kwestia poważnych
dylematów moralnych.

163
00:14:59,910 --> 00:15:01,620
Rozumiem.

164
00:15:02,220 --> 00:15:06,330
Czy te informacje zapobiegną
kolejnym zabójstwom?

165
00:15:06,750 --> 00:15:07,920
Z pewnością.

166
00:15:10,990 --> 00:15:14,840
To jest kopia rytów z Sowiej Jaskini?

167
00:15:20,460 --> 00:15:23,680
- Przypomina to panu coś?
- Śniłem o tym.

168
00:15:24,700 --> 00:15:28,310
Albo gdzieś widziałem.

169
00:15:52,350 --> 00:15:54,160
Zrobię, o co pan prosi.

170
00:15:56,660 --> 00:16:00,190
- Raport ze sprawy Leo Johnsona.
- Dziękuję.

171
00:16:04,090 --> 00:16:10,080
Odniesiony uraz
wpływa na sprawność ręki,

172
00:16:10,160 --> 00:16:13,060
ale charakter pisma
zbytnio się nie zmienia.

173
00:16:25,600 --> 00:16:29,350
Wiersz przysłany przez Earle'a

174
00:16:29,430 --> 00:16:31,930
przepisał Leo.

175
00:16:42,000 --> 00:16:43,220
Panie Horne.

176
00:16:45,480 --> 00:16:50,580
Panie Horne, do degustacji win
zostało parę godzin,

177
00:16:50,660 --> 00:16:54,250
a nie mogę znaleźć pańskiej córki.

178
00:16:54,330 --> 00:16:56,320
Jest dziś w Seattle.

179
00:16:56,740 --> 00:17:01,530
Ale w recepcji panu pomogą.

180
00:17:01,620 --> 00:17:04,530
W recepcji. Wspaniale.

181
00:17:05,110 --> 00:17:06,900
Dziękuję.

182
00:17:06,980 --> 00:17:08,490
Panie...

183
00:17:09,160 --> 00:17:11,250
Richard Tremayne.

184
00:17:11,330 --> 00:17:14,960
- Konfekcja męska.
- Tak.

185
00:17:16,060 --> 00:17:18,660
Co z nosem?

186
00:17:18,740 --> 00:17:20,710
Nie ma o czym mówić.

187
00:17:20,790 --> 00:17:25,260
To drobiazg, gdy można
się przysłużyć większemu dobru.

188
00:17:28,130 --> 00:17:31,060
Pokryjemy koszty leczenia.

189
00:17:32,610 --> 00:17:33,770
Dziękuję.

190
00:17:34,500 --> 00:17:40,790
Mogę też liczyć na odszkodowanie?

191
00:17:44,760 --> 00:17:45,700
Nie ma sprawy.

192
00:17:47,060 --> 00:17:48,850
Wspaniale.

193
00:17:49,430 --> 00:17:51,380
Zawiadomię mojego prawnika.

194
00:17:55,440 --> 00:17:58,050
"Zawiadomię prawnika".

195
00:17:59,990 --> 00:18:06,880
Czasami trudno się oprzeć pokusie
i nie postąpić wrednie.

196
00:18:20,280 --> 00:18:23,530
Pij, przyjacielu. Świętujemy!

197
00:18:25,690 --> 00:18:28,910
Nie mogę ruszyć ręką.

198
00:18:30,800 --> 00:18:34,800
Spędziłeś swe krótkie życie
w zapomnieniu,

199
00:18:34,880 --> 00:18:40,510
ale wreszcie staniesz w blasku świateł.

200
00:18:40,600 --> 00:18:42,760
Zazdroszczę ci.

201
00:18:43,340 --> 00:18:46,020
Wyruszasz w podróż
poza granice wyobraźni.

202
00:18:46,100 --> 00:18:48,780
Kto wie, dokąd cię zawiedzie...

203
00:18:48,870 --> 00:18:50,690
Mam dość.

204
00:18:50,780 --> 00:18:54,070
Idziemy na jakąś paradę?

205
00:18:54,990 --> 00:18:57,750
Nie, kolego. Niezupełnie.

206
00:18:57,840 --> 00:19:01,360
Fajnie, że mogłem pomóc i w ogóle,

207
00:19:01,680 --> 00:19:04,210
ale jak z tego wyjdę?

208
00:19:04,300 --> 00:19:05,510
Nie wyjdziesz.

209
00:19:06,390 --> 00:19:07,550
Leo...

210
00:19:08,380 --> 00:19:09,960
podaj strzałę.

211
00:19:13,220 --> 00:19:14,520
Strzałę?

212
00:19:15,720 --> 00:19:17,630
Chwilunia.

213
00:19:18,640 --> 00:19:21,460
Nie lubię takich żartów.

214
00:19:23,920 --> 00:19:25,920
Leo, strzała.

215
00:19:28,940 --> 00:19:30,140
Nie...

216
00:19:36,090 --> 00:19:39,770
Nie kop go prądem.
Jak się teraz napiję?

217
00:19:42,590 --> 00:19:44,400
Boli go.

218
00:19:47,600 --> 00:19:50,820
Podaj mi strzałę!

219
00:20:06,690 --> 00:20:08,530
Dziękuję.

220
00:20:13,440 --> 00:20:15,770
Po co ci to?

221
00:20:17,060 --> 00:20:18,730
Co robisz?

222
00:20:19,510 --> 00:20:21,370
To nie jest śmieszne.

223
00:20:22,740 --> 00:20:25,610
- Nie ruszaj się.
- Co ty wyprawiasz?

224
00:20:27,770 --> 00:20:32,240
Pomyśl o grzesznikach ciekawych,
dokąd idą ich dusze.

225
00:20:32,910 --> 00:20:34,490
Po co mi to?

226
00:20:34,920 --> 00:20:37,160
By odpowiedzieć na to proste pytanie.

227
00:20:38,540 --> 00:20:41,670
Gdzie ocknie się moja dusza?

228
00:20:42,400 --> 00:20:45,860
Jakie życie mnie czeka,
gdy skończy się to?

229
00:20:49,080 --> 00:20:54,000
To pytanie od wieków
dręczy nasze żałosne sumienia.

230
00:20:54,080 --> 00:20:57,220
A ty, szczęściarzu...

231
00:20:58,860 --> 00:21:01,690
zaraz poznasz odpowiedź.

232
00:21:13,240 --> 00:21:16,650
Zaczynamy posiedzenie
komisji sędziowskiej.

233
00:21:16,730 --> 00:21:20,580
Ten rok ciekawie się zapowiada.

234
00:21:22,020 --> 00:21:26,720
Bardzo ciekawie. Ten rok
zapowiada się bardzo ciekawie.

235
00:21:26,810 --> 00:21:28,350
Dziękuję, Dwayne.

236
00:21:28,430 --> 00:21:30,290
Pierwsza sprawa:

237
00:21:30,370 --> 00:21:34,450
Ben Horne chce złożyć
propozycję zmian w konkursie.

238
00:21:34,540 --> 00:21:37,210
Nie zgłaszam sprzeciwu.

239
00:21:37,770 --> 00:21:41,670
- Ben, prosimy.
- Dziękuję.

240
00:21:42,920 --> 00:21:47,610
Dwayne, przy okazji gratuluję zaręczyn.

241
00:21:48,980 --> 00:21:50,810
Dziękuję bardzo.

242
00:21:50,890 --> 00:21:55,000
To piękna kobieta
i życzę wam szczęścia.

243
00:21:55,090 --> 00:21:57,200
Dziękuję bardzo.

244
00:21:59,400 --> 00:22:00,490
Panowie...

245
00:22:01,770 --> 00:22:03,690
sprawa jest prosta.

246
00:22:03,770 --> 00:22:09,460
Czasy paradowania w kostiumach
kąpielowych przeminęły.

247
00:22:10,250 --> 00:22:16,170
Wybory miss Twin Peaks
powinny być świętem

248
00:22:16,260 --> 00:22:19,750
ku czci wszelkich kobiecych zalet.

249
00:22:20,310 --> 00:22:23,420
Urody pojętej szerzej

250
00:22:23,500 --> 00:22:28,340
jako piękno umysłu, ducha, cnót i idei.

251
00:22:28,920 --> 00:22:33,110
Pozostaje mi tylko
gorąco temu przyklasnąć.

252
00:22:34,340 --> 00:22:37,980
Co on sprzedaje? Co sprzedajesz?

253
00:22:38,060 --> 00:22:43,170
Przejdźmy do sedna. Wybory miss
powinny mieć temat przewodni.

254
00:22:43,250 --> 00:22:47,360
Oto moja propozycja.
Temat tegorocznych wystąpień

255
00:22:47,440 --> 00:22:52,000
powinien brzmieć:
"Jak ratować nasze lasy?".

256
00:22:54,150 --> 00:22:59,920
Jest aktualny, słuszny, globalny,
a na dodatek...

257
00:23:01,080 --> 00:23:03,270
drodzy panowie...

258
00:23:03,360 --> 00:23:05,210
nasz.

259
00:23:06,020 --> 00:23:11,850
Oczywiście twój sprzeciw
wobec inwestycji w Ghostwood

260
00:23:11,930 --> 00:23:15,030
- nie ma tu nic do rzeczy?
- Pete...

261
00:23:16,980 --> 00:23:19,320
ta insynuacja mnie rani.

262
00:23:20,060 --> 00:23:22,850
Ochrona środowiska powinna być

263
00:23:22,930 --> 00:23:25,990
ponad wszelkimi
osobistymi interesami, moimi,

264
00:23:26,070 --> 00:23:32,290
twoimi czy jakimikolwiek innymi.

265
00:23:32,370 --> 00:23:34,940
Rozważymy to.

266
00:23:36,070 --> 00:23:38,300
Tak, rozważymy to.

267
00:23:38,750 --> 00:23:44,850
Mam taką nadzieję.
Dziękuję za uwagę. Burmistrzu.

268
00:23:47,590 --> 00:23:49,600
Myśli, że coś ugra.

269
00:23:50,370 --> 00:23:54,020
Pomysł jest sensowny. Dość sensowny.

270
00:23:54,100 --> 00:23:55,820
Też tak uważam.

271
00:23:58,000 --> 00:23:59,950
- Bobby.
- Szefie.

272
00:24:00,500 --> 00:24:03,950
- Nie zapomnij o pralni.
- Jasne.

273
00:24:11,860 --> 00:24:16,900
Zapraszamy pierwszą z uczestniczek.

274
00:24:16,980 --> 00:24:20,510
Lana? Podejdź, skarbie.

275
00:24:31,670 --> 00:24:34,550
Nie wyczuwacie przekrętu?

276
00:24:35,110 --> 00:24:37,530
Nie dam rady.

277
00:24:38,040 --> 00:24:40,980
Nigdy nie przemawiałam publicznie.

278
00:24:41,060 --> 00:24:46,230
Kiedy w czwartej klasie miałam
coś przeczytać na głos, to zemdlałam.

279
00:24:46,850 --> 00:24:50,160
Będzie dobrze. Ja ci to napiszę.

280
00:24:51,740 --> 00:24:55,000
Poradzisz sobie, nie martw się.

281
00:25:00,240 --> 00:25:04,860
- Donna? Ty też startujesz?
- Tak, a ty?

282
00:25:04,950 --> 00:25:09,130
Miałabym odpuścić? To podniecające!

283
00:25:14,670 --> 00:25:19,620
Kopę lat. Nie pytam,
co porabiałeś, bo chyba wiem.

284
00:25:20,170 --> 00:25:23,620
- Przerażasz mnie.
- To nie tak, jak myślisz.

285
00:25:23,710 --> 00:25:28,540
- Nie tak? A co mam myśleć?
- Że chodzę ze starszą kobietą.

286
00:25:30,260 --> 00:25:35,040
Skąd to zainteresowanie skamielinami?

287
00:25:35,130 --> 00:25:38,630
To nie tak, jak myślisz.

288
00:25:39,250 --> 00:25:43,680
Dobra. Wytłumacz mi to.

289
00:25:45,600 --> 00:25:47,720
Nie zrozumiesz.

290
00:25:48,120 --> 00:25:50,430
Bardzo bym chciał.

291
00:25:53,550 --> 00:25:55,880
Masz pojęcie,

292
00:25:55,960 --> 00:26:01,030
co potrafi dojrzała kobieta
o nadludzkiej sile?

293
00:26:11,250 --> 00:26:12,300
Przepraszam.

294
00:26:35,420 --> 00:26:43,440
Gdybyś mogła mi o niej powiedzieć
coś, co pomogłoby mi to pojąć.

295
00:26:43,520 --> 00:26:45,010
Na przykład?

296
00:26:46,880 --> 00:26:50,970
Dlaczego to wszystko robiła?
Czego chciała?

297
00:26:51,850 --> 00:26:54,940
Sama zadaję sobie to pytanie.

298
00:26:57,300 --> 00:26:59,250
Muszę to zrozumieć.

299
00:27:02,220 --> 00:27:05,620
Chyba bardzo wcześnie przekonała się,

300
00:27:05,700 --> 00:27:12,770
że łatwiej przetrwać, będąc taką,
jaką chcieliby ją widzieć inni.

301
00:27:14,330 --> 00:27:19,730
A tego, co zostało z niej samej,
nie pokazywała.

302
00:27:19,820 --> 00:27:23,980
- Więc to wszystko były kłamstwa.
- Kto to wie...

303
00:27:24,370 --> 00:27:27,040
Może nie dla niej.

304
00:27:29,530 --> 00:27:33,540
Wierzyła w to, co akurat było wygodne.

305
00:27:35,820 --> 00:27:41,710
Mimo tego, co nam zrobiła,
nie potrafię jej znienawidzić.

306
00:27:43,200 --> 00:27:44,590
Była...

307
00:27:45,630 --> 00:27:47,220
Była taka piękna.

308
00:27:56,600 --> 00:28:00,950
Mam coś, co może nam pomóc.

309
00:28:01,030 --> 00:28:02,250
Co?

310
00:28:06,240 --> 00:28:12,530
Zostawił mi je Thomas Eckhardt.
Może ma coś wspólnego z Josie.

311
00:28:12,610 --> 00:28:13,780
Co tam jest?

312
00:28:14,180 --> 00:28:17,520
Nie wiem, nie mogę otworzyć.

313
00:28:17,600 --> 00:28:18,880
Pokaż.

314
00:28:22,130 --> 00:28:26,440
Nie ma wieczka... ani zamka.

315
00:28:27,520 --> 00:28:29,130
Rany!

316
00:28:29,540 --> 00:28:35,350
Jakie piękne dziewczyny
startują w tym roku! Cześć, Harry.

317
00:28:35,430 --> 00:28:39,610
Zgłosiły się chyba
wszystkie panny z miasta.

318
00:28:39,690 --> 00:28:43,660
Udało się wam to otworzyć?

319
00:28:45,430 --> 00:28:46,570
Ty niezdaro!

320
00:28:52,860 --> 00:28:55,550
O rety...

321
00:28:56,410 --> 00:28:57,660
Co to?

322
00:29:06,690 --> 00:29:09,720
- Dawaj.
- Chwileczkę.

323
00:29:19,070 --> 00:29:22,560
Ja potrzymam.

324
00:29:45,770 --> 00:29:48,830
Latem przychodziłam tu
parę razy w tygodniu popływać.

325
00:29:48,920 --> 00:29:51,870
Dobrze trafiłem. Jest pięknie.

326
00:29:53,530 --> 00:29:57,300
- Przyroda była mi bliższa niż ludzie.
- Dlaczego?

327
00:29:57,880 --> 00:30:00,240
Nie miałam przyjaciół.

328
00:30:00,330 --> 00:30:05,150
Norma była towarzyska i szła do ludzi,

329
00:30:05,230 --> 00:30:08,990
a ja od nich uciekałam.
Żyłam wśród własnych myśli.

330
00:30:09,070 --> 00:30:10,400
To miłe miejsce.

331
00:30:11,500 --> 00:30:13,950
Ale sąsiedzi dziwni.

332
00:30:14,830 --> 00:30:16,710
Byli jacyś chłopcy?

333
00:30:16,800 --> 00:30:18,870
- Nie.
- Naprawdę?

334
00:30:20,230 --> 00:30:21,720
Jeden.

335
00:30:22,280 --> 00:30:24,620
- W liceum?
- Przed maturą.

336
00:30:27,070 --> 00:30:29,630
Dlatego poszłaś do klasztoru?

337
00:30:32,880 --> 00:30:34,800
Możemy o tym nie mówić?

338
00:30:36,370 --> 00:30:39,720
Jasne. Pomówimy, o czym zechcesz.

339
00:30:42,170 --> 00:30:45,890
Chcę wrócić do świata,
którego tak się bałam.

340
00:30:47,440 --> 00:30:48,560
Rozumiem.

341
00:30:49,570 --> 00:30:51,980
Lęki nie znikną, gdy od nich uciekamy.

342
00:30:53,740 --> 00:30:55,320
Są silniejsze.

343
00:30:58,510 --> 00:31:04,660
Muszę się zmierzyć z samą sobą.
Tutaj, gdzie to się stało.

344
00:31:17,070 --> 00:31:19,730
Życie bywa beznadziejne.

345
00:31:22,380 --> 00:31:25,410
Wiem, w jaki mrok można wpaść.

346
00:31:26,110 --> 00:31:30,310
To dlatego odeszłam.
Przez tego chłopaka.

347
00:31:33,920 --> 00:31:37,950
Przeżyłem coś podobnego
i chciałem uciec od świata.

348
00:31:39,990 --> 00:31:42,410
Może zdołam ci pomóc.

349
00:31:55,240 --> 00:31:57,970
- Nie znam cię.
- Nie.

350
00:31:59,740 --> 00:32:01,890
Uczę się ufać instynktowi.

351
00:32:04,000 --> 00:32:05,350
Co ci nakazuje?

352
00:32:08,690 --> 00:32:09,980
Zaufać.

353
00:33:16,640 --> 00:33:22,620
Dobry wieczór państwu.
Witam na wieczorku somelierskim,

354
00:33:22,710 --> 00:33:27,920
kolejnej z wielu imprez
sponsorowanych przez firmę Horne'ów

355
00:33:28,000 --> 00:33:32,940
w ramach akcji "Ratujmy Ghostwood".

356
00:33:33,430 --> 00:33:37,910
- Some-co?
- To coś z winami.

357
00:33:39,330 --> 00:33:40,820
Zaczynajmy.

358
00:33:42,540 --> 00:33:45,220
Lano, Lucy...

359
00:33:45,300 --> 00:33:47,700
pomóżcie mi.

360
00:33:58,370 --> 00:34:02,690
Najpierw omówimy wina czerwone.

361
00:34:02,770 --> 00:34:06,360
Niektórzy znawcy twierdzą,

362
00:34:06,440 --> 00:34:10,470
że nie ma innych win
prócz czerwonych. Tak, Andy?

363
00:34:11,100 --> 00:34:16,000
Są też wina białe i musujące.

364
00:34:18,060 --> 00:34:18,770
Dziękuję.

365
00:34:20,090 --> 00:34:23,640
Skoro wszyscy już mają...

366
00:34:23,730 --> 00:34:25,570
Jeszcze nie, do diabła!

367
00:34:25,860 --> 00:34:27,200
Wypluj to!

368
00:34:30,830 --> 00:34:31,950
Przepraszam.

369
00:34:33,270 --> 00:34:34,450
Dobrze.

370
00:34:35,660 --> 00:34:37,850
Przyszliśmy się uczyć.

371
00:34:38,980 --> 00:34:40,360
Wróćmy do wina.

372
00:34:42,300 --> 00:34:47,980
Najpierw zajmiemy się tak zwanym
nosem wina i ocenimy jego bukiet.

373
00:34:48,070 --> 00:34:50,600
Robimy to...

374
00:34:51,270 --> 00:34:53,040
Wąchając.

375
00:34:56,050 --> 00:34:57,650
Doskonale.

376
00:35:03,380 --> 00:35:06,540
Unosimy kieliszek...

377
00:35:07,610 --> 00:35:15,010
i energicznie nim obracając,
robimy głęboki wdech.

378
00:35:22,870 --> 00:35:30,890
Bardzo dobrze. Następnie
próbujemy, napełniając usta.

379
00:35:31,200 --> 00:35:38,380
Opłukujemy całą jamę ustną,
by wino dotarło do kubków smakowych

380
00:35:38,460 --> 00:35:42,070
w tyle języka, ale nie połykając.

381
00:35:42,410 --> 00:35:43,760
Wspaniale.

382
00:35:45,350 --> 00:35:48,280
I wypluwamy!

383
00:35:53,200 --> 00:35:57,220
Powoli wyszedł z mroku,
gdzie zostawił martwą dziewczynę,

384
00:35:57,300 --> 00:36:01,550
a ja zawołałem:
"Ani kroku, bo cię rozwalę!".

385
00:36:01,630 --> 00:36:05,820
- Co było potem?
- Krótki i wzruszający pogrzeb.

386
00:36:07,950 --> 00:36:10,220
Dale! Dobrze, że cię widzę.

387
00:36:10,300 --> 00:36:13,440
Wpadłem się pożegnać,
zaraz ruszam w drogę.

388
00:36:13,520 --> 00:36:16,270
Cieszę się, że zdążyłem.

389
00:36:16,720 --> 00:36:18,560
- Wracaj szybko.
- Jasne.

390
00:36:19,400 --> 00:36:21,440
- Pamiętasz Annie?
- Oczywiście.

391
00:36:21,530 --> 00:36:25,340
W Twin Peaks jest dużo pięknych pań,

392
00:36:25,430 --> 00:36:27,820
choć słyszę tylko jedną.

393
00:36:28,750 --> 00:36:30,240
Zapraszam na placek!

394
00:36:31,180 --> 00:36:32,620
Świetnie.

395
00:36:43,060 --> 00:36:44,790
Po trzy porcje.

396
00:36:46,140 --> 00:36:48,090
Podzielimy się z nimi?

397
00:36:49,170 --> 00:36:52,280
Powiadają, że miłość
wprawia w ruch ten świat

398
00:36:52,360 --> 00:36:56,390
i czyni zakochanych
najszczęśliwszymi na ziemi.

399
00:36:56,480 --> 00:37:01,220
Ale nikt nie wspomniał
o leczeniu słuchu.

400
00:37:01,310 --> 00:37:03,980
Shelly, jesteś czarodziejką

401
00:37:04,480 --> 00:37:07,400
i boginią zesłaną z niebios.

402
00:37:07,800 --> 00:37:11,490
Jestem tylko kelnerką,
a nie żadną boginią.

403
00:37:11,570 --> 00:37:18,770
Nie doceniasz się. Nie uważasz,
że jest cudowną istotą?

404
00:37:18,860 --> 00:37:19,950
Doskonale cię rozumiem.

405
00:37:20,840 --> 00:37:25,490
Shelly, spójrz mi w oczy,
zanim stracę odwagę.

406
00:37:26,510 --> 00:37:28,420
Zaraz wyjadę

407
00:37:28,780 --> 00:37:31,360
i nie wiem, kiedy tu wrócę,

408
00:37:31,440 --> 00:37:34,510
ale wiedz, że znajomość z tobą
to dla mnie więcej niż zaszczyt.

409
00:37:35,320 --> 00:37:37,080
Jestem wzruszony.

410
00:37:37,170 --> 00:37:39,930
I jeśli cię teraz nie pocałuję,

411
00:37:40,150 --> 00:37:42,990
będę tego żałował do końca życia.

412
00:37:49,520 --> 00:37:50,780
Co to ma znaczyć?

413
00:37:50,860 --> 00:37:56,660
Masz przed oczami obraz z profilu
dwóch osób w intymnej sytuacji.

414
00:37:57,820 --> 00:37:59,540
Chyba nigdy tego nie widział.

415
00:38:03,570 --> 00:38:07,170
Odwróć wzrok, chłopcze,
bo zaraz to powtórzymy.

416
00:38:14,520 --> 00:38:19,470
Co odkrył przed nami
ten pierwszy łyk? Jakie smaki?

417
00:38:20,930 --> 00:38:23,020
Ja czuję drewno.

418
00:38:24,910 --> 00:38:26,180
Niezupełnie.

419
00:38:26,940 --> 00:38:29,520
Kto jeszcze? Lana?

420
00:38:33,040 --> 00:38:34,030
Banany?

421
00:38:34,300 --> 00:38:40,580
Tak, jest tu nutka banana.
To chlorometanol. Dobrze.

422
00:38:42,070 --> 00:38:45,120
- Co jeszcze?
- Banany...

423
00:38:45,390 --> 00:38:48,630
- Czekolada.
- Słusznie.

424
00:38:49,080 --> 00:38:52,380
Odpuśćmy sobie wino
i zjedzmy deser bananowy.

425
00:38:55,720 --> 00:38:59,310
Spróbujmy, tym razem przełykając,

426
00:38:59,400 --> 00:39:02,230
i sprawdźmy, co jeszcze odkryjemy.

427
00:39:06,640 --> 00:39:11,250
Co teraz czujemy?
Co pieści nasze kubki smakowe?

428
00:39:11,330 --> 00:39:18,100
Coś takiego. Nie sądziłam,
że wino ma tyle zalet.

429
00:39:18,180 --> 00:39:21,620
Zdumiewające, prawda?
Dobre wino to symfonia.

430
00:39:27,500 --> 00:39:29,910
Jak sądzisz, Lucy?

431
00:39:34,950 --> 00:39:39,060
Jestem w ciąży. Nie mogę pić.

432
00:40:04,460 --> 00:40:05,770
Miłość to piekło.

433
00:40:07,750 --> 00:40:08,820
Słucham?

434
00:40:09,470 --> 00:40:12,090
Miłość to piekło.

435
00:40:14,770 --> 00:40:17,090
Według Hindusów – drabina do nieba.

436
00:40:20,000 --> 00:40:24,550
Wymyślili spacery
po rozżarzonych węglach.

437
00:40:24,640 --> 00:40:26,180
I samodyscyplinę.

438
00:40:26,840 --> 00:40:29,480
Nie pasuje do miłości.

439
00:40:29,560 --> 00:40:33,090
- Ziemskiej.
- A jest inna?

440
00:40:33,170 --> 00:40:36,070
Rzadko, ale się zdarza.

441
00:40:37,140 --> 00:40:41,490
- Wali jak ciężarówka. Bez litości.
- Czujesz, że żyjesz.

442
00:40:41,570 --> 00:40:44,910
Wszystko mocniej czujesz. Ból też.

443
00:40:46,060 --> 00:40:47,320
Zwłaszcza ból.

444
00:40:51,100 --> 00:40:52,800
Wciąż o tym myślę.

445
00:40:54,120 --> 00:40:58,500
- Nieźle pana sponiewierało.
- Jestem załatwiony.

446
00:40:58,580 --> 00:41:00,050
Ona czuje to samo?

447
00:41:01,380 --> 00:41:02,620
Mam nadzieję.

448
00:41:04,920 --> 00:41:07,820
Pana też trafiło?

449
00:41:09,540 --> 00:41:12,950
Jakby ktoś otworzył mi serce łomem.

450
00:41:13,040 --> 00:41:14,530
Też nieźle.

451
00:41:16,610 --> 00:41:18,860
Zbyt długo było zamknięte.

452
00:41:19,670 --> 00:41:21,380
To chyba lepiej.

453
00:41:21,460 --> 00:41:23,240
- Oby.
- Na zdrowie.

454
00:41:26,330 --> 00:41:29,740
- Panie Wheeler, telegram.
- Dziękuję.

455
00:41:41,300 --> 00:41:44,510
Cholera. Proszę pana?

456
00:41:45,900 --> 00:41:48,970
Proszę zgłosić w recepcji,
że wyjeżdżam. Dziękuję.

457
00:41:53,470 --> 00:41:55,430
- Powodzenia.
- Nawzajem.

458
00:42:13,440 --> 00:42:15,670
Podasz mi groszek?

459
00:42:19,010 --> 00:42:21,240
Mogę cię o coś spytać

460
00:42:21,330 --> 00:42:23,390
Dobrze znasz Benjamina Horne'a?

461
00:42:24,230 --> 00:42:29,160
Mówiłem Donnie o akcji "Ratujmy
Planetę", w której bierzesz udział.

462
00:42:30,070 --> 00:42:31,520
"Ratujmy Planetę"?

463
00:42:31,960 --> 00:42:36,310
Był tu wczoraj. Pojechałaś do niego?

464
00:42:38,110 --> 00:42:41,690
Tak, chce nam poświęcić nieco czasu.

465
00:42:43,060 --> 00:42:45,820
Popiera lokalne inicjatywy.

466
00:42:45,910 --> 00:42:50,240
To pewnie to.
Bo ktoś przysłał mamie róże.

467
00:42:52,510 --> 00:42:54,410
Podaj mi groszek.

468
00:42:54,690 --> 00:42:57,850
To bardzo romantyczne, prawda?

469
00:42:58,490 --> 00:43:01,250
Podaj mamie groszek.

470
00:43:09,330 --> 00:43:10,430
Co w szkole?

471
00:43:11,410 --> 00:43:12,480
Świetnie.

472
00:43:14,110 --> 00:43:16,390
Dziewczyny są przejęte wyborami miss.

473
00:43:17,980 --> 00:43:20,730
To tradycja.
Udzielam się przy tym od lat.

474
00:43:21,450 --> 00:43:22,440
Zgłosiłam się.

475
00:43:24,060 --> 00:43:25,210
Tak?

476
00:43:26,730 --> 00:43:28,660
Dotąd cię to nie interesowało.

477
00:43:29,450 --> 00:43:33,580
Mogłabym wygrać stypendium
i studiować zagranicą.

478
00:44:07,920 --> 00:44:09,760
Myśleliśmy, że to bomba.

479
00:44:09,840 --> 00:44:14,040
Nie jesteśmy saperami,
więc Andy przyniósł wykrywacz.

480
00:44:14,120 --> 00:44:17,310
Jeśli to bomba, to nie ma
w niej metalu i nic nie tyka.

481
00:44:17,390 --> 00:44:18,330
Pociągnąć

482
00:44:22,500 --> 00:44:24,540
Dawniej mogłem być pewien

483
00:44:24,630 --> 00:44:28,100
konsekwencji działania
chorego umysłu Windoma Earle'a,

484
00:44:28,190 --> 00:44:32,050
ale jego ostatnie posunięcia
mnie zaskakują.

485
00:44:32,130 --> 00:44:35,230
Zmienia zasady gry.

486
00:44:35,310 --> 00:44:38,580
Nie zdołam przewidzieć
jego kolejnego ruchu.

487
00:44:39,380 --> 00:44:40,190
Andy...

488
00:44:40,690 --> 00:44:44,060
każ wszystkim schować się
za samochodem.

489
00:44:48,300 --> 00:44:50,870
To może być ładunek plastiku...

490
00:44:52,390 --> 00:44:53,790
albo jakieś chemikalia.

491
00:44:54,450 --> 00:44:56,360
Wszystko jest możliwe.

492
00:44:57,860 --> 00:44:59,120
Boję się.

493
00:45:01,620 --> 00:45:04,710
Windom Earle gardzi logiką.

494
00:45:05,980 --> 00:45:10,630
A my musimy mierzyć się
z kaprysami tego szaleńca.

495
00:45:58,760 --> 00:46:03,640
Następny będzie ktoś, kogo znasz.

