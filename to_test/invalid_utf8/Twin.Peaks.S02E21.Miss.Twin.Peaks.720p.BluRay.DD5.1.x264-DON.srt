1
00:00:28,890 --> 00:00:33,660
MIASTECZKO TWIN PEAKS

2
00:03:08,590 --> 00:03:09,870
Ratuj Shelly.

3
00:03:10,570 --> 00:03:12,010
Ratuj Shelly!

4
00:03:59,630 --> 00:04:04,630
Kto, u diabła, wypuścił majora?
Bo przecież nie ty, prawda?

5
00:04:07,450 --> 00:04:10,380
Nie, nie ukarzę cię.

6
00:04:10,630 --> 00:04:13,210
Już nie zdoła nam zaszkodzić.

7
00:04:14,540 --> 00:04:16,930
A dla ciebie mam nową zabawkę.

8
00:04:32,910 --> 00:04:37,040
To nasz wkład do bufetu konkursu.

9
00:04:37,350 --> 00:04:40,800
I spodziewam się którejś z was
wśród finalistek.

10
00:04:40,890 --> 00:04:42,580
Daj spokój.

11
00:04:42,660 --> 00:04:46,890
Kto wie. To najważniejszy dzień w roku.

12
00:04:46,970 --> 00:04:51,590
I powinien wygrać ktoś, kto na to
zasługuje. Zwłaszcza teraz.

13
00:04:52,000 --> 00:04:53,640
Z powodu Laury Palmer?

14
00:04:54,470 --> 00:04:56,580
Jest nam to potrzebne.

15
00:04:58,680 --> 00:05:00,590
Jeden dzień nie wystarczy.

16
00:05:02,420 --> 00:05:06,900
Myślisz, że cię uhonorują
jako miss sprzed dwudziestu lat?

17
00:05:06,980 --> 00:05:10,870
- Nie przypominaj mi.
- Dziś też byś wygrała.

18
00:05:11,830 --> 00:05:14,030
Chcesz wpłynąć na mój werdykt?

19
00:05:16,480 --> 00:05:17,950
No właśnie.

20
00:05:18,030 --> 00:05:22,240
Na kogo zagłosujesz?
Możesz na nas dwie?

21
00:05:33,130 --> 00:05:35,220
- Cześć.
- Audrey...

22
00:05:35,960 --> 00:05:38,890
Pierwsza inteligentna twarz,
jaką dziś widzę.

23
00:05:40,050 --> 00:05:43,110
Przy tobie wszyscy
wyglądamy jak małpy.

24
00:05:43,990 --> 00:05:45,230
Co to za książki?

25
00:05:47,650 --> 00:05:54,740
Mam tu Koran, Bhagawadgitę,
Talmud, Stary i Nowy Testament

26
00:05:55,120 --> 00:06:02,670
i Daodejing.
Trzymam w rękach święte księgi,

27
00:06:02,750 --> 00:06:07,880
zawierające podstawowe zasady
humanizmu i filozofii dobra.

28
00:06:07,960 --> 00:06:12,320
Gdzieś tu są odpowiedzi,
których szukam.

29
00:06:12,410 --> 00:06:17,570
Zamierzam je przeczytać
od deski do deski, aż znajdę...

30
00:06:20,670 --> 00:06:22,890
A ja tylko o sobie.

31
00:06:24,080 --> 00:06:25,610
Chodzi o Jacka?

32
00:06:27,330 --> 00:06:29,490
Minął dopiero dzień.

33
00:06:30,460 --> 00:06:33,020
Jak przeżyję tydzień?

34
00:06:36,610 --> 00:06:42,700
Podzielę się z tobą mądrością
zawartą w tych tomach.

35
00:06:43,580 --> 00:06:46,160
Czas leczy rany.

36
00:06:47,800 --> 00:06:51,720
Wiem, co mówię, wierz mi.

37
00:06:53,130 --> 00:06:56,370
Mieliśmy tak mało czasu.

38
00:06:57,470 --> 00:07:01,220
Ale dobrze go wykorzystaliście.

39
00:07:03,210 --> 00:07:06,500
Najlepiej jak się da.

40
00:07:10,110 --> 00:07:12,470
Boję się, że już go nie zobaczę.

41
00:07:13,640 --> 00:07:15,780
Jack nie rzuca słów na wiatr.

42
00:07:15,860 --> 00:07:22,330
Jeśli powiedział, że wróci,
to wróci. Obiecuję ci to.

43
00:07:23,120 --> 00:07:27,410
Tego akurat nie możesz,
ale dziękuję za chęci.

44
00:07:29,040 --> 00:07:31,880
Przyszłam ci przekazać
wieści z Seattle.

45
00:07:32,430 --> 00:07:35,590
Racja. Słucham.

46
00:07:37,930 --> 00:07:40,830
Packardowie chcą sfinansować
projekt Ghostwood

47
00:07:40,910 --> 00:07:43,280
za pośrednictwem banku w Twin Peaks,

48
00:07:43,360 --> 00:07:46,770
ale bank się z tym nie afiszuje.

49
00:07:46,850 --> 00:07:50,150
Chyba w obawie przed złą prasą.

50
00:07:50,740 --> 00:07:52,420
Doprawdy?

51
00:07:54,440 --> 00:07:57,650
I właśnie to im zapewnimy.

52
00:07:59,110 --> 00:08:03,530
Zastanawiałaś się nad wzięciem
udziału w konkursie?

53
00:08:04,910 --> 00:08:06,360
Tato...

54
00:08:09,630 --> 00:08:15,100
za nic w świecie nie chciałabym
zostać symbolem seksu.

55
00:08:15,180 --> 00:08:20,970
Rozumiem cię, ale Miss Twin Peaks
to nie byle kto.

56
00:08:24,520 --> 00:08:31,970
Byłoby wspaniale, gdyby wygrał
ktoś o zainteresowaniach

57
00:08:32,050 --> 00:08:38,170
wykraczających poza tajniki makijażu
i przepis na placek z wiśniami.

58
00:08:40,900 --> 00:08:45,830
Jak wiesz, kandydatki
wygłaszają przemówienia.

59
00:08:45,920 --> 00:08:49,850
W tym roku tematem
jest ochrona środowiska

60
00:08:49,940 --> 00:08:53,770
i co musimy zrobić, by temu podołać.

61
00:08:53,860 --> 00:08:56,630
Jest na to inny sposób.

62
00:08:59,200 --> 00:09:03,700
Przemów do ludzi, powstrzymaj budowę

63
00:09:04,410 --> 00:09:07,750
i prowadź nas ku lepszej przyszłości.

64
00:09:14,140 --> 00:09:17,890
- Wciąż nad tym główkuje?
- Od rana.

65
00:09:18,880 --> 00:09:23,200
I uprzedzając twoje pytanie,
nadal szukamy majora Briggsa.

66
00:09:23,940 --> 00:09:29,450
Widzę tu związek z Błękitną Księgą
i sądzę, że majora porwał Earle.

67
00:09:29,530 --> 00:09:32,500
- Po co?
- Ciekawe...

68
00:09:35,510 --> 00:09:36,840
Nie wiem.

69
00:09:37,990 --> 00:09:41,340
Ale pozwól, że coś ci powiem.

70
00:09:42,310 --> 00:09:46,760
Earle szuka Czarnej Chaty od roku 1965.

71
00:09:47,390 --> 00:09:51,130
Ta partia szachów może mieć
większe znaczenie, niż myślimy.

72
00:09:51,210 --> 00:09:54,890
Do tego dochodzą
dziwne okoliczności śmierci Josie.

73
00:09:55,250 --> 00:09:57,520
Jakie? O co ci chodzi?

74
00:09:58,260 --> 00:09:59,780
Kiedy zginęła...

75
00:10:00,340 --> 00:10:04,070
Przepraszam, ale nie mówiłem o tym
z uwagi na twój stan.

76
00:10:04,860 --> 00:10:05,610
O czym?

77
00:10:07,690 --> 00:10:12,910
Trzęsła się ze strachu.
Drżała jak przerażone zwierzę.

78
00:10:13,530 --> 00:10:16,680
Powiedziałbym nawet,
że to strach ją zabił.

79
00:10:16,760 --> 00:10:20,630
W chwili jej śmierci widziałem Boba.

80
00:10:21,960 --> 00:10:25,230
Jakby przeniknął
przez szczelinę w czasie.

81
00:10:25,320 --> 00:10:29,200
Teraz dostrzegam związek między
jego obecnością i strachem Josie.

82
00:10:29,280 --> 00:10:30,640
Jakby to go zwabiło.

83
00:10:31,170 --> 00:10:33,520
Żerował na nim.

84
00:10:33,960 --> 00:10:37,310
Czy Boba coś łączy z Czarną Chatą?

85
00:10:38,350 --> 00:10:42,500
Chyba stamtąd przychodzi.
Myślę, że Czarna Chata

86
00:10:42,580 --> 00:10:46,910
to czające się w tych lasach zło,
o którym mówiłeś.

87
00:10:47,710 --> 00:10:52,660
Jeśli Earle jej szuka,
musimy go uprzedzić.

88
00:10:53,040 --> 00:10:55,210
To źródło wielkiej mocy.

89
00:10:56,050 --> 00:10:59,130
Większej, niż możemy to pojąć.

90
00:11:00,210 --> 00:11:02,020
Eureka!

91
00:11:03,800 --> 00:11:08,390
Dale, mam ochotę cię ucałować!

92
00:11:11,620 --> 00:11:15,420
To strach. To strach jest kluczem!

93
00:11:15,510 --> 00:11:18,280
Moje ulubione uczucie.

94
00:11:18,360 --> 00:11:26,300
I cały czas miałem to przed nosem!
Wszystko się zgadza.

95
00:11:26,390 --> 00:11:30,080
Te nocne stwory, krążące
na krawędzi naszych koszmarów,

96
00:11:30,160 --> 00:11:34,310
przychodzą, gdy poczują
nasz strach. Karmią się nim.

97
00:11:34,390 --> 00:11:40,760
To jak symbioza.
Naturo, jesteś doskonała!

98
00:11:40,840 --> 00:11:42,750
Nie zawiodłaś mnie.

99
00:11:43,340 --> 00:11:48,330
Ciesz się, Leo,
zwycięstwo jest bliskie.

100
00:11:48,420 --> 00:11:52,390
Wiemy, gdzie jest wejście
i kiedy ukaże się zamek,

101
00:11:52,470 --> 00:11:57,050
a teraz mamy klucz.

102
00:11:59,930 --> 00:12:02,630
Wybacz mi moje nagłe odejście.

103
00:12:03,420 --> 00:12:11,040
Wyruszam po moją królową
i na mroczny miesiąc miodowy.

104
00:12:12,530 --> 00:12:17,170
Nie byłem tak przejęty,
odkąd przebiłem Caroline aortę.

105
00:12:18,680 --> 00:12:23,170
Leo, czas się pożegnać.

106
00:12:23,770 --> 00:12:29,470
Podczas naszego pobytu
w tej przemiłej chatce

107
00:12:29,550 --> 00:12:32,320
zdążyłem cię nawet polubić.

108
00:12:32,400 --> 00:12:39,030
Byłeś dobrym towarzyszem,
posłusznym i wiernym.

109
00:12:39,780 --> 00:12:47,160
Masz piątkę za chęci.
Ale nawaliłeś, wypuszczając majora.

110
00:12:48,560 --> 00:12:52,950
Będziesz miał dużo czasu,
żeby to przemyśleć.

111
00:12:56,420 --> 00:13:00,050
Żegnaj, Leo. Powodzenia.

112
00:13:45,830 --> 00:13:51,680
Raz, dwa, trzy i cztery.
Raz i dwa.

113
00:13:51,760 --> 00:13:56,460
Z uczuciem!
Nie bójcie się i pokażcie nogi!

114
00:13:56,540 --> 00:13:58,810
Co to za taniec, panie Pinkle?

115
00:13:59,850 --> 00:14:04,100
Taniec natury, moja droga.
Ku jej czci!

116
00:14:04,330 --> 00:14:12,400
Teraz odwróćcie się i skłońcie,
jak młode drzewka na wietrze.

117
00:14:12,970 --> 00:14:17,240
Jeszcze trochę. I wytrzymajcie tak.

118
00:14:17,720 --> 00:14:21,620
Co właściwie czcimy tymi skłonami?

119
00:14:22,300 --> 00:14:26,120
Nie kwestionujcie wizji choreografa!

120
00:14:26,570 --> 00:14:30,920
Jesteście tylko płatkami
mojej róży. Proszę.

121
00:14:31,170 --> 00:14:36,410
Na raz. Raz, dwa, trzy...

122
00:14:39,190 --> 00:14:41,850
Śliczny balecik.

123
00:14:42,810 --> 00:14:46,740
Skupmy się, muszę wracać do pracy.

124
00:14:46,830 --> 00:14:52,060
Jakie cechy charakteryzują
Miss Twin Peaks?

125
00:14:52,590 --> 00:14:55,380
Uroda i władczość.

126
00:14:55,890 --> 00:14:57,760
To lubię.

127
00:14:57,840 --> 00:15:03,870
Elegancja, wyrafinowanie i klasa.

128
00:15:04,400 --> 00:15:07,730
Ja bym stawiała na oryginalność.

129
00:15:07,810 --> 00:15:12,340
- To nie podpada pod talent?
- Byłoby miło.

130
00:15:12,420 --> 00:15:16,340
Osobiście jestem wielbicielem stylu.

131
00:15:16,580 --> 00:15:21,650
To mi wystarczy do opracowania
kart do głosowania.

132
00:15:21,730 --> 00:15:23,680
Do zobaczenia wieczorem.

133
00:15:23,940 --> 00:15:25,640
Wspaniale.

134
00:15:30,060 --> 00:15:32,610
Cześć, Dick. Mam przerwę.

135
00:15:36,380 --> 00:15:39,200
Pomożesz mi znaleźć coś
w magazynku?

136
00:15:39,950 --> 00:15:44,580
- W magazynku?
- Zginął nam ważny rekwizyt.

137
00:15:59,810 --> 00:16:03,080
- Głupio mi.
- Nie ma powodu.

138
00:16:06,130 --> 00:16:09,490
Czego dokładnie szukamy?

139
00:16:14,120 --> 00:16:15,780
Co zrobiłam?

140
00:16:18,020 --> 00:16:19,790
Czego szukasz?

141
00:16:19,870 --> 00:16:24,420
Nie wiem, jak to się
dokładnie nazywa, ale...

142
00:16:26,080 --> 00:16:31,240
Czy coś tutaj ci to przypomina?

143
00:16:34,680 --> 00:16:37,070
To może być to.

144
00:16:37,150 --> 00:16:39,830
O rany...

145
00:16:39,910 --> 00:16:42,340
Chyba znalazłaś.

146
00:16:52,060 --> 00:16:57,620
13:17. Zakończyłem drugą dziś
medytację zastępującą sen.

147
00:16:57,710 --> 00:17:01,180
Czuję się świeżo
i znów sobie uświadomiłem,

148
00:17:01,260 --> 00:17:05,530
w jak niewielkim stopniu mieszkańcy Ziemi
wykorzystują swoje możliwości.

149
00:17:07,000 --> 00:17:09,670
Wciąż pracujemy
nad rysunkiem z jaskini.

150
00:17:09,760 --> 00:17:13,150
Jestem przekonany, że kryje odpowiedź.

151
00:17:13,230 --> 00:17:17,210
I mam pewność, że Windom Earle
szuka tego samego co my,

152
00:17:17,290 --> 00:17:20,990
ale z odmiennych pobudek.
Jeśli mam rację

153
00:17:21,080 --> 00:17:25,700
co do potęgi tego miejsca,
to nie może dotrzeć tam pierwszy.

154
00:17:26,640 --> 00:17:31,590
Pozwól, że w tej chwili
wspomnę o Annie Blackburn.

155
00:17:32,060 --> 00:17:38,060
Jest wyjątkową osobą,
o reakcjach czystych jak u dziecka.

156
00:17:38,610 --> 00:17:41,920
Nie myślałem tak o żadnej kobiecie,
prócz Caroline.

157
00:17:42,000 --> 00:17:46,110
Musiałem spotkać Annie, by sobie
uświadomić, jakie szare było moje życie

158
00:17:46,190 --> 00:17:50,030
od jej śmierci. Zimne i samotne...

159
00:17:52,120 --> 00:17:55,140
Choć czasami samotność mi służy.

160
00:18:03,300 --> 00:18:06,300
Annie, jesteś doskonale punktualna.

161
00:18:06,800 --> 00:18:08,880
Choreograf tak nie uważa.

162
00:18:11,300 --> 00:18:13,030
Co się dzieje?

163
00:18:13,120 --> 00:18:16,210
Za sześć godzin mam wygłosić mowę,

164
00:18:16,290 --> 00:18:20,390
a nic sobie nie napisałam
i nie wiem, co mam powiedzieć.

165
00:18:20,470 --> 00:18:24,740
Jestem przerażona
i muszę z tobą porozmawiać.

166
00:18:25,280 --> 00:18:26,620
Jak brzmi temat?

167
00:18:26,710 --> 00:18:32,300
"Jak ratować nasze lasy".
Nie chcę tam stać jak tępa Barbie

168
00:18:32,390 --> 00:18:37,090
i bredzić o wygaszaniu ognisk.
Nie mam pomysłu.

169
00:18:37,170 --> 00:18:40,540
- Boisz się publicznych wystąpień.
- Tak.

170
00:18:40,620 --> 00:18:41,680
No dobrze...

171
00:18:44,780 --> 00:18:47,800
Lasy trzeba chronić,
bo ich nie szanujemy.

172
00:18:48,400 --> 00:18:49,720
Wcale.

173
00:18:50,130 --> 00:18:54,390
Czy gdyby budowa w Ghostwood
pociągnęła za sobą ofiary, miałaby szanse?

174
00:18:56,000 --> 00:19:00,010
Drzewa to nie ludzie, choć też żyją.

175
00:19:01,700 --> 00:19:04,670
Lasy są pełne piękna i spokoju.

176
00:19:06,910 --> 00:19:08,860
Część już zniszczono.

177
00:19:09,270 --> 00:19:12,490
Sadziłam drzewa, ale nie urosły.

178
00:19:13,250 --> 00:19:15,770
Las ma i ciemne strony.

179
00:19:17,740 --> 00:19:20,870
Widzę w twojej twarzy własne życie.

180
00:19:20,950 --> 00:19:23,170
Nie wiem, czy tego chcę.

181
00:19:31,630 --> 00:19:33,840
A ja nie chcę gadać o drzewach.

182
00:19:44,590 --> 00:19:46,900
Teraz chcę tylko kochać się z tobą.

183
00:20:09,590 --> 00:20:14,430
To ja z poprzednim mistrzem Yakimy.

184
00:20:15,220 --> 00:20:18,480
Był milutki, ale powolny. Następne.

185
00:20:21,570 --> 00:20:26,320
A to ja z Brockiem Farmingtonem,
trzykrotnym mistrzem Spokane.

186
00:20:26,620 --> 00:20:32,890
Ma potężne barki
i ramiona jak dźwigary mostu.

187
00:20:33,900 --> 00:20:36,790
Położyłam go w piątej minucie.

188
00:20:37,610 --> 00:20:38,540
Następne.

189
00:20:41,730 --> 00:20:45,000
A to ja z moimi pucharami.

190
00:20:47,900 --> 00:20:49,380
Brawo.

191
00:20:50,670 --> 00:20:52,680
Dziękujemy.

192
00:20:55,400 --> 00:21:00,010
Chyba wszyscy zastanawiacie się
nad celem naszego spotkania.

193
00:21:00,090 --> 00:21:05,310
Wiem z doświadczenia,
że gdy ludzie myślą o rozwo...

194
00:21:05,390 --> 00:21:06,960
o rozstaniu...

195
00:21:09,090 --> 00:21:15,920
to czasami łatwiej mówić
o uczuciach w otoczeniu innych.

196
00:21:17,280 --> 00:21:19,940
Nadine, zaczniemy od ciebie.

197
00:21:24,050 --> 00:21:29,580
To się chyba stało, kiedy zobaczyłam
w szkole tyłeczek Mike'a.

198
00:21:32,960 --> 00:21:34,840
A jak się teraz czujesz?

199
00:21:37,460 --> 00:21:39,950
Chyba trochę winna.

200
00:21:42,080 --> 00:21:45,890
Jest nam z Mikiem cudownie,

201
00:21:46,450 --> 00:21:52,860
dziś są wybory miss,
a pan Pinkle uważa, że mam talent.

202
00:21:54,990 --> 00:22:00,750
Jestem taka szczęśliwa
i martwi mnie smutek Eda.

203
00:22:02,420 --> 00:22:07,230
Ed, chcesz jej o czymś powiedzieć?

204
00:22:12,940 --> 00:22:16,340
Norma i ja chcemy się pobrać.

205
00:22:21,350 --> 00:22:22,610
Tak?

206
00:22:24,340 --> 00:22:27,210
To cudownie.

207
00:22:27,300 --> 00:22:30,570
Bo Mike i ja też bierzemy ślub.

208
00:23:26,100 --> 00:23:27,890
Którędy do zamku?

209
00:23:43,990 --> 00:23:45,940
- Czekałem na ciebie.
- Dziękuję.

210
00:23:46,020 --> 00:23:48,060
Niewiele mówi.

211
00:23:48,140 --> 00:23:51,610
Jest w dobrej kondycji,
a doktor zbadał krew.

212
00:23:53,280 --> 00:23:54,570
Haloperydol?

213
00:23:58,480 --> 00:23:59,240
Garlandzie?

214
00:24:00,350 --> 00:24:02,930
Czy to był Windom Earle?

215
00:24:03,020 --> 00:24:04,280
Garland?

216
00:24:05,810 --> 00:24:07,390
Dziwne imię.

217
00:24:08,970 --> 00:24:10,620
Judy Garland?

218
00:24:11,150 --> 00:24:12,790
To był Windom Earle?

219
00:24:15,690 --> 00:24:18,720
Chyba Bóg.

220
00:24:19,680 --> 00:24:21,780
Dokąd pana zabrał?

221
00:24:23,000 --> 00:24:24,100
Do lasu.

222
00:24:25,190 --> 00:24:26,430
A dokładniej?

223
00:24:27,890 --> 00:24:31,560
Było pięknie i ciemno.

224
00:24:32,050 --> 00:24:36,610
Król Rumunii nie przybył.

225
00:24:42,540 --> 00:24:44,450
To potrwa.

226
00:24:45,770 --> 00:24:48,480
- Mamy czas?
- Niewiele.

227
00:24:48,560 --> 00:24:50,660
Niech tu zostanie, może się otrząśnie.

228
00:24:56,030 --> 00:24:58,050
Jest źle.

229
00:24:58,140 --> 00:25:02,470
Jeśli drzwi do Czarnej Chaty istnieją,
to w jakimś punkcie czasu.

230
00:25:02,860 --> 00:25:06,760
Zazwyczaj takie obiekty
istnieją w czasie i miejscu.

231
00:25:07,290 --> 00:25:12,960
Dla kontrastu spadająca gwiazda istnieje
w czasie i kontinuum przestrzeni,

232
00:25:13,040 --> 00:25:15,720
ale z jej punktu widzenia jest inaczej.

233
00:25:15,800 --> 00:25:17,520
Nie rozumiem.

234
00:25:18,380 --> 00:25:21,120
Jeśli nie trafimy
w odpowiednie miejsce i chwilę,

235
00:25:21,210 --> 00:25:23,410
nie znajdziemy wejścia.

236
00:25:23,490 --> 00:25:24,630
Szeryfie...

237
00:25:25,520 --> 00:25:28,370
czy coś łączy z tym klub 4H?

238
00:25:29,230 --> 00:25:30,700
Wątpię.

239
00:25:35,610 --> 00:25:38,240
To musi być ostatnie. Patrzcie!

240
00:25:38,330 --> 00:25:40,210
Mam tego dość.

241
00:25:40,590 --> 00:25:45,180
Pudełka w pudełkach.
Oby to było coś warte.

242
00:25:45,560 --> 00:25:47,810
Może to nie pudełko,

243
00:25:48,220 --> 00:25:51,350
tylko lity blok stali.

244
00:25:51,430 --> 00:25:54,810
- Ostatni żart Eckhardta.
- Niezły dowcip.

245
00:25:55,320 --> 00:25:59,230
Może byście coś wymyślili.

246
00:26:00,110 --> 00:26:01,240
Dawaj!

247
00:26:01,320 --> 00:26:08,240
Nie do wiary, że takie coś
opiera się mojemu imadłu...

248
00:26:09,820 --> 00:26:13,910
Raz, dwa i trzy...

249
00:26:16,420 --> 00:26:18,710
Przepraszam, to moja wina.

250
00:26:25,500 --> 00:26:27,300
Głupie pudełka!

251
00:26:46,040 --> 00:26:48,030
Przeklęte pudełka!

252
00:26:53,360 --> 00:26:55,840
Świetny strzał.

253
00:26:55,930 --> 00:26:57,540
Spójrzcie.

254
00:26:59,110 --> 00:27:00,530
Klucz!

255
00:27:04,110 --> 00:27:08,470
A gdzie jest klucz, musi być zamek.

256
00:27:09,620 --> 00:27:11,470
Trzeba go pilnować.

257
00:27:12,600 --> 00:27:16,600
Tak, na widoku.

258
00:27:17,030 --> 00:27:20,370
Nie ufacie sobie?

259
00:27:21,970 --> 00:27:26,550
- Jasne, że tak.
- Jak brat i siostra.

260
00:27:29,510 --> 00:27:30,950
Może tutaj?

261
00:27:33,050 --> 00:27:37,920
Pod kloszem. Na oczach wszystkich.

262
00:27:38,000 --> 00:27:42,700
- Więc mamy klucz?
- Mamy.

263
00:27:54,650 --> 00:27:56,620
Wyglądasz prześlicznie.

264
00:27:57,470 --> 00:27:59,060
Przeczytaj nam przemowę.

265
00:28:00,940 --> 00:28:03,070
Wolałabym pomówić o prawdzie.

266
00:28:05,000 --> 00:28:06,320
O jakiej?

267
00:28:06,840 --> 00:28:10,610
Coś cię łączy z Benjaminem
Horne'em i chcę wiedzieć co.

268
00:28:10,950 --> 00:28:13,110
Nie mów tak do matki.

269
00:28:13,190 --> 00:28:16,910
Przestańcie mnie traktować jak dziecko.

270
00:28:17,550 --> 00:28:19,710
O co tu chodzi?

271
00:28:21,140 --> 00:28:26,420
Rozumiem cię, ale czasami
powinnaś nam zaufać.

272
00:28:26,630 --> 00:28:31,330
- Jesteś młoda i niewiele wiesz.
- Znam granice kłamstwa.

273
00:28:31,410 --> 00:28:35,760
Każecie mi mówić prawdę,
więc róbcie to samo.

274
00:28:38,790 --> 00:28:42,270
Jeśli ty mi tego nie powiesz,
spytam pana Horne'a.

275
00:28:42,520 --> 00:28:44,610
- Nie.
- Więc mów!

276
00:28:48,620 --> 00:28:49,710
Dobrze...

277
00:28:50,670 --> 00:28:53,500
sama tego chciałaś.

278
00:29:15,760 --> 00:29:17,290
Na Boga!

279
00:29:17,800 --> 00:29:18,720
Andy!

280
00:29:19,200 --> 00:29:20,760
Spójrz tutaj!

281
00:29:20,840 --> 00:29:25,510
To, co uznałeś za klub 4H,
to symbole astrologiczne.

282
00:29:25,590 --> 00:29:26,960
Czyli planety?

283
00:29:27,040 --> 00:29:31,920
Tak. Jowisz i Saturn. Są i inne,

284
00:29:32,000 --> 00:29:36,310
ale to dotyczy tych planet
w określonej pozycji –

285
00:29:36,390 --> 00:29:39,700
- koniunkcji Jowisza i Saturna.
- Co to znaczy?

286
00:29:40,120 --> 00:29:46,130
Podczas ich koniunkcji dochodzi
do wielkich zmian władzy i losu.

287
00:29:46,210 --> 00:29:49,570
Jowisz oznacza ekspansję,
a Saturn opór.

288
00:29:49,650 --> 00:29:53,780
Ich koniunkcja zwiastuje
stan napięcia i koncentracji mocy.

289
00:29:53,860 --> 00:29:58,260
To czas gwałtownych zmian,
dobrych i złych.

290
00:29:58,350 --> 00:29:59,850
Kiedy to będzie?

291
00:30:01,430 --> 00:30:04,560
Zobaczmy. Według annałów...

292
00:30:05,580 --> 00:30:09,110
trwa od stycznia do czerwca.

293
00:30:11,380 --> 00:30:15,440
Boże! Drzwi do chaty.
Wtedy się otwierają.

294
00:30:15,520 --> 00:30:19,700
To rozwiązanie zagadki.
Mówi nam, kiedy to się stanie.

295
00:30:19,930 --> 00:30:22,840
Chronić królową...

296
00:30:22,920 --> 00:30:26,910
Skoro mówi kiedy, to pewnie i gdzie.

297
00:30:26,990 --> 00:30:31,960
Strach i miłość otworzą drzwi.

298
00:30:33,360 --> 00:30:34,760
Co powiedział?

299
00:30:34,840 --> 00:30:39,430
Że strach i miłość otworzą drzwi.

300
00:30:40,360 --> 00:30:46,580
Dwoje drzwi, dwie chaty. Strach
otwiera Czarną, a miłość tę drugą.

301
00:30:46,660 --> 00:30:47,880
Czyli?

302
00:30:47,960 --> 00:30:49,820
Nie wiem. Właśnie na to wpadłem.

303
00:30:49,900 --> 00:30:53,190
Co z królową?

304
00:30:54,030 --> 00:30:56,450
- Jasne, królowa!
- Rumuńska?

305
00:30:56,530 --> 00:31:00,020
Nie. Figura szachowa. Pomyśl.

306
00:31:00,100 --> 00:31:03,920
- Jeśli Earle zbije królową...
- Partia trwa, aż zbije króla.

307
00:31:04,010 --> 00:31:07,270
Może wystarczy mu otwarcie drzwi.

308
00:31:09,770 --> 00:31:11,550
Co to za królowa?

309
00:31:11,900 --> 00:31:14,800
Zaczekaj. Królowa... Korona...
Królowa, Harry.

310
00:31:14,880 --> 00:31:19,860
Andy, proszę! Harry. Miss Twin Peaks.

311
00:31:19,940 --> 00:31:21,570
No jasne.

312
00:31:22,630 --> 00:31:24,520
Agencie Cooper, proszę!

313
00:31:34,780 --> 00:31:36,470
Mieliśmy pluskwę.

314
00:31:37,340 --> 00:31:41,870
Tego nie przysłała Josie,
tylko Windom Earle.

315
00:31:43,090 --> 00:31:47,390
Wyprzedza nas,
bo cały czas mu pomagaliśmy.

316
00:31:47,470 --> 00:31:49,650
- Kiedy zaczyna się konkurs?
- Lada chwila.

317
00:31:49,730 --> 00:31:50,750
Jedźmy!

318
00:31:52,100 --> 00:31:54,640
Agencie Cooper!

319
00:33:46,660 --> 00:33:50,830
Witam państwa.
To był całkiem niezły weekend.

320
00:33:50,910 --> 00:33:53,710
Zapraszam na wybory Miss Twin Peaks,

321
00:33:53,940 --> 00:33:58,040
podczas których spośród
tych utalentowanych dam

322
00:33:58,130 --> 00:34:01,700
wyłonimy najpiękniejszą.

323
00:34:01,780 --> 00:34:05,810
Zaczynamy od popisów
naszych kandydatek.

324
00:34:05,900 --> 00:34:07,550
Lucy Moran!

325
00:34:38,880 --> 00:34:40,640
To miasto to dziura.

326
00:34:59,600 --> 00:35:01,640
Ściągnęła pani rodzinę?

327
00:35:46,810 --> 00:35:51,340
Ściągam posiłki, obstawimy
cały lokal. Będą lada moment.

328
00:35:51,430 --> 00:35:55,340
Ktokolwiek wygra, ma otrzymać
całodobową ochronę.

329
00:35:55,430 --> 00:35:58,040
- Dopilnuj tego.
- Jasne.

330
00:35:58,120 --> 00:36:04,560
Kolej na Lanę Budding-Milford,
która wykona taniec...

331
00:36:05,350 --> 00:36:09,500
akrobatyczno-jazzowo-egzotyczny.
Lana?

332
00:36:09,590 --> 00:36:10,810
Agencie Cooper?

333
00:37:13,270 --> 00:37:16,870
To jest dopiero sztuka!

334
00:37:30,740 --> 00:37:34,300
Las można uratować
tylko w jeden sposób.

335
00:37:34,950 --> 00:37:37,810
Nie wolno patrzeć, jak umiera.

336
00:37:38,450 --> 00:37:41,640
Tak nakazuje prawo natury,
bardziej życiowe

337
00:37:41,720 --> 00:37:43,880
niż wszystkie nasze przepisy.

338
00:37:45,070 --> 00:37:49,110
Gdy zagrożone jest coś nam drogie,
trzeba to ratować,

339
00:37:49,610 --> 00:37:51,860
bo przepadnie na zawsze.

340
00:37:52,710 --> 00:37:53,840
Dziękuję.

341
00:37:58,280 --> 00:38:02,790
Dziękujemy Audrey Horne.
A teraz przerywnik muzyczny.

342
00:38:05,390 --> 00:38:06,970
Muszę iść.

343
00:38:16,890 --> 00:38:19,620
Co pana łączy z moją matką?

344
00:38:20,870 --> 00:38:21,920
Donna...

345
00:38:25,600 --> 00:38:31,230
Najlepiej byłoby się spotkać
i wspólnie to omówić.

346
00:38:31,450 --> 00:38:32,690
Raczej nie.

347
00:38:34,890 --> 00:38:37,450
To nie jest dobra pora ani miejsce.

348
00:38:37,660 --> 00:38:42,800
Stare zdjęcia i akcje na strychu.
Miłosne listy do mamy...

349
00:38:42,880 --> 00:38:46,670
Chciałbym móc wyznać prawdę.

350
00:38:46,750 --> 00:38:49,280
Akt urodzenia bez nazwiska ojca.

351
00:38:49,360 --> 00:38:51,170
O co tu chodzi?

352
00:38:51,590 --> 00:38:53,910
Dlaczego wszyscy kłamią?

353
00:39:02,000 --> 00:39:03,120
Donna...

354
00:39:07,360 --> 00:39:09,730
Twoja matka i ja...

355
00:39:14,370 --> 00:39:15,970
Jest pan moim ojcem.

356
00:39:22,690 --> 00:39:25,070
Chciałabym tu zacytować

357
00:39:25,150 --> 00:39:28,800
słowa wodza Seattle
z plemienia Sukuamiszów.

358
00:39:28,890 --> 00:39:32,080
"Wy zapominacie o zmarłych
i nie wracają.

359
00:39:32,170 --> 00:39:35,340
Nasi nigdy nie zapomną ziemi,
która ich karmiła.

360
00:39:35,420 --> 00:39:40,660
Nadal kochają żyzne doliny,
szemrzące potoki i dostojne góry.

361
00:39:40,750 --> 00:39:43,470
Gdy ostatni z czerwonoskórych
zejdzie z tego świata,

362
00:39:43,560 --> 00:39:46,600
ich duchy wciąż będą żyć
wśród drzew i na wybrzeżach."

363
00:39:48,200 --> 00:39:52,520
Indianie kochają ziemię,
jak dziecko bicie serca matki.

364
00:39:53,140 --> 00:39:55,650
Dlaczego mamy utracić to piękno?

365
00:39:55,740 --> 00:39:58,880
Zacznijmy ratowanie lasów
od ocalenia uczuć,

366
00:39:58,970 --> 00:40:01,210
które co dnia w nas umierają.

367
00:40:01,290 --> 00:40:03,370
Tego, czego się wyparliśmy.

368
00:40:04,120 --> 00:40:06,910
Bo jeśli nie uszanujemy siebie,

369
00:40:07,170 --> 00:40:09,990
nie będziemy szanować
ziemi pod nogami.

370
00:40:10,460 --> 00:40:15,550
Postarajmy się stąpać po niej lekko
i zostawić coś po sobie.

371
00:40:16,330 --> 00:40:20,670
Bądźmy nowymi wojownikami,
wojownikami ducha,

372
00:40:20,750 --> 00:40:23,390
kochającymi ziemię
i chcącymi ją ratować.

373
00:40:24,340 --> 00:40:25,870
Dziękuję państwu.

374
00:40:35,070 --> 00:40:37,240
Dziękujemy kandydatkom.

375
00:40:37,580 --> 00:40:42,030
Teraz nastąpi głosowanie,
a później ogłoszenie werdyktu.

376
00:40:44,740 --> 00:40:47,580
Zapomnieliście, jaki dziś dzień?

377
00:40:47,660 --> 00:40:49,650
Dzień wyborów miss?

378
00:40:50,420 --> 00:40:54,350
Dziś moje dziecko dostanie tatę.

379
00:40:54,910 --> 00:40:56,960
Miałem to na końcu języka.

380
00:40:58,170 --> 00:41:04,390
Postanowiłam, że niezależnie od tego,
kto jest ojcem biologicznym,

381
00:41:04,950 --> 00:41:08,300
to Andy wychowa moje dziecko.

382
00:41:08,390 --> 00:41:09,520
Naprawdę?

383
00:41:10,470 --> 00:41:15,190
Przykro mi, Dick, ale muszę mieć
na uwadze dobro maleństwa.

384
00:41:15,270 --> 00:41:19,820
Nie szkodzi, bardzo słusznie.
Gratuluję, Andrew.

385
00:41:19,900 --> 00:41:24,110
A teraz muszę iść i oddać głos.

386
00:41:27,710 --> 00:41:29,310
Niech żyją dzieci.

387
00:41:31,880 --> 00:41:34,270
Nie jesteś zawiedziony.

388
00:41:35,280 --> 00:41:41,050
To dla mnie zaszczyt, że mnie wybrałaś.

389
00:41:41,530 --> 00:41:46,220
Przyrzekam, że będę wspaniałym ojcem,

390
00:41:47,030 --> 00:41:50,360
ale teraz...

391
00:41:50,970 --> 00:41:53,860
muszę iść poszukać agenta Coopera.

392
00:41:58,420 --> 00:41:59,750
Faceci...

393
00:42:05,820 --> 00:42:07,950
Pora ogłosić wyniki.

394
00:42:10,080 --> 00:42:12,140
W konkursie zwyciężyła

395
00:42:12,780 --> 00:42:15,460
i tytuł Miss Twin Peaks otrzymuje...

396
00:42:18,180 --> 00:42:19,170
Annie Blackburn!

397
00:42:36,600 --> 00:42:39,020
Proszę państwa, oto nowa Miss!

398
00:42:50,090 --> 00:42:55,750
To skandal! Od kiedy ona tu mieszka?

399
00:42:56,680 --> 00:42:58,040
Co w ciebie wstąpiło?

400
00:42:59,150 --> 00:43:01,760
Jej przemowa była piękna, spójna

401
00:43:01,840 --> 00:43:05,380
i zdolna poruszyć nawet
najgorszego zatwardzialca.

402
00:43:35,450 --> 00:43:37,300
Proszę o spokój!

403
00:44:36,720 --> 00:44:38,000
Pomogę ci.

404
00:44:58,870 --> 00:45:00,620
- Porwał Annie.
- Co?

405
00:45:00,710 --> 00:45:02,360
Ma Annie!

406
00:45:02,440 --> 00:45:05,170
Drań nie uciekł daleko. Dorwę go!

407
00:45:07,100 --> 00:45:09,470
Agencie Cooper, wszędzie pana szukam.

408
00:45:10,410 --> 00:45:13,160
Będziesz tam potrzebny.

409
00:45:13,240 --> 00:45:17,560
To ważne. Wreszcie rozgryzłem
ten rysunek z jaskini.

410
00:45:18,130 --> 00:45:21,010
- Co?
- Czułem, że skądś to znam.

411
00:45:21,090 --> 00:45:26,620
Wiem, dokąd iść.
To nie jest zagadka, tylko mapa.

