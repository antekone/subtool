1
00:00:28,920 --> 00:00:33,660
MIASTECZKO TWIN PEAKS

2
00:01:48,210 --> 00:01:54,270
Pamiętam, jak wyszedłem z płomieni,
jakiś kształt w mroku.

3
00:01:54,350 --> 00:01:55,680
I nic więcej.

4
00:01:56,530 --> 00:02:02,820
Aż po dwóch dniach stanąłem
nad naszym wygasłym ogniskiem.

5
00:02:02,910 --> 00:02:08,450
Istnieją nowe techniki,
które pomogą przełamać amnezję.

6
00:02:08,530 --> 00:02:11,860
Moje wspomnienia
są odporne na hipnozę.

7
00:02:12,600 --> 00:02:17,850
Czuję je, są bardzo namacalne,
jak zapachy i uczucia.

8
00:02:19,120 --> 00:02:23,850
Wszystko jest znajome,
ale poza moim zasięgiem.

9
00:02:23,940 --> 00:02:26,200
Co jeszcze pan pamięta?

10
00:02:26,280 --> 00:02:32,760
Bardzo mało. Tylko niepokojący
obraz wielkiej sowy.

11
00:02:33,290 --> 00:02:36,040
- Przytłaczający.
- Wielka sowa?

12
00:02:40,820 --> 00:02:43,360
Wielka sowa. Jak wielka?

13
00:02:44,620 --> 00:02:47,300
Dość, by przesłonić moją pamięć.

14
00:02:48,430 --> 00:02:53,470
Za prawym uchem
ma trzy regularne, trójkątne ślady.

15
00:02:53,560 --> 00:02:59,860
Majorze, czego dokładnie
dotyczy pańska praca?

16
00:02:59,940 --> 00:03:01,750
Ta informacja jest...

17
00:03:03,040 --> 00:03:05,780
jak wciąż to powtarzam...

18
00:03:06,730 --> 00:03:08,240
tajna.

19
00:03:11,650 --> 00:03:16,500
Choć teraz te tajemnice
tracą na znaczeniu.

20
00:03:21,500 --> 00:03:26,980
Być może są informacje tak ważne,

21
00:03:27,950 --> 00:03:31,720
że przemogą naszą chęć ich ukrycia.

22
00:03:31,800 --> 00:03:35,290
Informacje o takim znaczeniu,
iż nie należą...

23
00:03:39,040 --> 00:03:40,470
Boże...

24
00:03:42,010 --> 00:03:44,970
Która dusza to zniesie?

25
00:03:46,740 --> 00:03:48,110
Moja?

26
00:03:51,960 --> 00:03:55,770
Może zacznijmy od początku.

27
00:03:59,220 --> 00:04:02,000
Słyszał pan
o projekcie Błękitna Księga?

28
00:04:02,540 --> 00:04:04,560
Słyszałem.

29
00:04:04,640 --> 00:04:09,260
To badania fenomenu znanego jako UFO.

30
00:04:09,350 --> 00:04:12,140
Zarzucone w 1969.

31
00:04:13,290 --> 00:04:20,160
Ale kontynuujemy je nieoficjalnie,

32
00:04:20,940 --> 00:04:27,940
nadal obserwując niebo i, jak tu,
w Twin Peaks, ziemię pod nim.

33
00:04:31,340 --> 00:04:35,010
Szukamy miejsca zwanego Białą Chatą.

34
00:04:37,310 --> 00:04:40,610
- Majorze Briggs?
- Spodziewałem się was.

35
00:04:40,690 --> 00:04:44,000
- Zaczekajcie chwilę.
- To rozkaz pułkownika.

36
00:04:44,080 --> 00:04:46,930
Nie obchodzą mnie jego rozkazy.
To mój posterunek,

37
00:04:47,010 --> 00:04:49,550
a major jest moim przyjacielem.

38
00:04:52,450 --> 00:04:56,860
Obawiam się, że musimy
przełożyć naszą rozmowę.

39
00:04:56,940 --> 00:05:02,060
- Na pewno chce pan z nimi iść?
- Tak. Do widzenia.

40
00:05:38,600 --> 00:05:41,940
Śmiało, telefon nie gryzie.

41
00:05:42,020 --> 00:05:45,990
Co wy tam wiecie? To zbiry.

42
00:05:46,490 --> 00:05:49,160
A znałem już niejednego.

43
00:05:49,600 --> 00:05:53,040
Przy niektórych Jean Renault to pętak.

44
00:05:53,120 --> 00:05:56,470
Ernie! Dzwoń.

45
00:05:57,830 --> 00:06:03,330
- Transakcja już umówiona?
- Próbujemy, ale Ernie ma pietra.

46
00:06:03,410 --> 00:06:04,770
Pietra?

47
00:06:04,850 --> 00:06:07,790
Zebrałem całą odwagę. Dawajcie go...

48
00:06:13,300 --> 00:06:17,440
Proszę, nie zmuszajcie mnie!
To samobójstwo.

49
00:06:17,530 --> 00:06:20,730
Błagam, dla naszego wspólnego dobra.

50
00:06:27,140 --> 00:06:28,250
Halo?

51
00:06:29,510 --> 00:06:32,290
Mogę mówić z Jeanem Renault?

52
00:06:36,660 --> 00:06:39,950
Cześć, Jean. Mam poważnego
klienta spoza miasta.

53
00:06:43,680 --> 00:06:45,040
Cześć, Lucy.

54
00:06:57,110 --> 00:07:00,630
- Gdzieś ty był?
- Kot wlazł na drzewo.

55
00:07:01,170 --> 00:07:03,470
Policja wciąż ma coś do roboty.

56
00:07:03,560 --> 00:07:07,320
Na nasze nieszczęście
ja niewiele zdziałałem.

57
00:07:07,400 --> 00:07:10,690
Byłem w biurze fundacji Pomocna Dłoń,

58
00:07:10,770 --> 00:07:15,700
gdzie kilka siwowłosych dam
niemal jadło mi z ręki.

59
00:07:15,780 --> 00:07:20,730
Niestety akta Nicky'ego
wróciły do sierocińca.

60
00:07:20,820 --> 00:07:23,510
Nie poznamy prawdy o jego rodzicach.

61
00:07:23,590 --> 00:07:26,360
Odwagi, nie rozpaczaj.

62
00:07:26,450 --> 00:07:31,290
Naszym celem jest sierociniec,
a w nim akta Nicky'ego.

63
00:07:32,190 --> 00:07:34,960
- Powiemy Lucy?
- Wykluczone.

64
00:07:35,930 --> 00:07:39,070
To męska sprawa.

65
00:07:40,050 --> 00:07:41,180
Chodź.

66
00:07:51,640 --> 00:07:52,980
I co?

67
00:07:53,070 --> 00:07:57,260
Przejrzałam ogłoszenia
we wszystkich gazetach z listy

68
00:07:57,350 --> 00:08:00,340
i nie znalazłam
żadnych szachowych ruchów

69
00:08:00,420 --> 00:08:05,430
ani wzmianki o Windomie Earle'u.
Przykro mi.

70
00:08:05,510 --> 00:08:06,830
Nie szkodzi.

71
00:08:18,580 --> 00:08:20,130
Dolać ci kawy?

72
00:08:22,310 --> 00:08:23,590
Proszę.

73
00:08:31,490 --> 00:08:33,060
Wszystko w porządku?

74
00:08:34,890 --> 00:08:36,440
W najlepszym.

75
00:08:40,810 --> 00:08:41,950
Reszty nie trzeba.

76
00:08:45,860 --> 00:08:47,910
MUSIMY POROZMAWIAĆ

77
00:08:56,530 --> 00:08:58,300
Do zobaczenia.

78
00:09:07,270 --> 00:09:10,460
Jedz, Leo. To bardzo smaczne.

79
00:09:19,480 --> 00:09:23,430
- To twoja kolej, żeby go karmić!
- Mam trening.

80
00:09:24,060 --> 00:09:25,900
Sezon futbolowy się skończył.

81
00:09:26,600 --> 00:09:30,580
Baseballowy trwa. To sport narodowy.

82
00:09:30,660 --> 00:09:33,870
Dobra, posprzątasz, jak wrócisz.

83
00:09:36,890 --> 00:09:38,750
Nie wrócę.

84
00:09:38,830 --> 00:09:39,920
Co?

85
00:09:41,430 --> 00:09:44,190
Dostałem robotę u Bena Horne'a.

86
00:09:44,740 --> 00:09:47,990
Mówią ci coś słowa doskonała okazja?

87
00:09:48,080 --> 00:09:50,380
To dla mnie przełom.

88
00:09:50,460 --> 00:09:53,890
Mam coś lepszego do roboty
niż kąpanie Leo.

89
00:09:53,970 --> 00:09:57,830
A ja? Nie mam nic lepszego?

90
00:09:59,140 --> 00:10:01,150
Nic mi nie przychodzi do głowy.

91
00:10:05,610 --> 00:10:06,720
Bobby!

92
00:10:20,950 --> 00:10:22,230
Boże!

93
00:10:39,430 --> 00:10:41,140
Stacja Eda.

94
00:10:41,230 --> 00:10:43,820
- Wujku? To ja.
- Gdzie jesteś?

95
00:10:43,900 --> 00:10:46,930
- Wszystko gra?
- W porządku.

96
00:10:48,140 --> 00:10:50,030
Mam prośbę.

97
00:10:51,560 --> 00:10:56,260
- Wypłać wszystko z mojego konta.
- Masz 12 dolarów.

98
00:10:56,350 --> 00:11:02,250
Wszystko. I prześlij dla mnie
do Wallies. To bar przy 96-ej.

99
00:11:02,870 --> 00:11:07,460
- Masz kłopoty?
- Nie, ale nie mogę nic powiedzieć.

100
00:11:07,550 --> 00:11:11,930
Naprawdę teraz nie mogę.
Później pogadamy.

101
00:11:12,010 --> 00:11:15,380
- Uważaj na siebie. I zadzwoń.
- Dobra. Cześć.

102
00:11:16,740 --> 00:11:19,190
- Zamiejscowa?
- Do Twin Peaks.

103
00:11:19,790 --> 00:11:23,730
- Tęsknisz do domu?
- Nie.

104
00:11:24,460 --> 00:11:27,070
Opowiedz mi o Twin Peaks.

105
00:11:29,090 --> 00:11:30,450
Nie ma o czym.

106
00:11:32,110 --> 00:11:34,370
Dlaczego wyjechałeś?

107
00:11:34,900 --> 00:11:36,750
Przez kobietę.

108
00:11:37,150 --> 00:11:39,670
Mówiłeś nawet o kilku.

109
00:11:41,480 --> 00:11:45,320
Moja dziewczyna zginęła.
Miała na imię Laura.

110
00:11:47,580 --> 00:11:50,550
Sądziłem, że ją znam, ale myliłem się.

111
00:11:51,540 --> 00:11:54,080
Chyba nikt jej tak naprawdę nie znał.

112
00:11:54,160 --> 00:11:55,850
Przykro mi.

113
00:11:57,700 --> 00:12:00,100
Moje życie miało kiedyś sens.

114
00:12:01,710 --> 00:12:04,570
Nie zawsze było najlepsze, ale moje.

115
00:12:06,300 --> 00:12:07,990
Czułem, że jest moje.

116
00:12:10,780 --> 00:12:14,210
A potem zginęła Laura...

117
00:12:17,770 --> 00:12:19,830
i teraz jest czyjeś.

118
00:12:21,320 --> 00:12:22,700
Laury.

119
00:12:29,130 --> 00:12:31,560
Chociaż robiłem, co mogłem...

120
00:12:32,260 --> 00:12:34,630
i starałem się pomóc...

121
00:12:38,630 --> 00:12:40,900
zamordowano następną dziewczynę.

122
00:12:43,730 --> 00:12:46,560
Chciałem wsiąść na motor i uciec.

123
00:12:47,830 --> 00:12:51,860
Odjechać tak daleko, jak zdołam.

124
00:12:53,550 --> 00:12:55,510
Znam to uczucie.

125
00:13:01,450 --> 00:13:02,770
Dobrze je znam.

126
00:13:18,420 --> 00:13:20,400
Dlaczego pozwalasz się krzywdzić?

127
00:13:28,390 --> 00:13:31,970
Jeffrey wyjeżdża, potrzebuję cię.

128
00:13:32,050 --> 00:13:34,470
- Pomożesz mi?
- W czym?

129
00:13:35,690 --> 00:13:37,130
Musisz mi pomóc.

130
00:13:51,390 --> 00:13:54,910
- Cześć, Mike.
- Rany!

131
00:13:54,990 --> 00:13:57,220
Napijesz się czegoś?

132
00:13:58,040 --> 00:14:01,700
A może weźmy
porcję placka z wiśniami?

133
00:14:01,790 --> 00:14:02,870
I dwa widelce.

134
00:14:02,950 --> 00:14:05,930
Pani Hurley... Nadine,
sam zjem mój placek.

135
00:14:06,010 --> 00:14:09,990
Nawet dwie porcje, ale sam.

136
00:14:12,590 --> 00:14:15,930
- To pieczeń rzymską.
- Nie. Placek i kawę.

137
00:14:16,010 --> 00:14:18,880
Wypiję 16 filiżanek
i nie chcę z tobą gadać,

138
00:14:18,960 --> 00:14:21,420
nie chcę z tobą chodzić,
widzieć cię ani znać.

139
00:14:21,500 --> 00:14:27,110
Dociera to do ciebie?
Czy potrzebujesz wyroku sądu?

140
00:14:31,280 --> 00:14:37,010
Mike, jesteś najprzystojniejszym
chłopakiem, jakiego znam.

141
00:14:38,140 --> 00:14:45,460
I bardzo bym chciała
się z tobą umówić.

142
00:14:59,710 --> 00:15:05,070
Przepraszam, czasem
nie mogę się powstrzymać.

143
00:15:24,420 --> 00:15:25,390
Dokąd idziesz?

144
00:15:26,610 --> 00:15:28,590
Mam coś do załatwienia.

145
00:15:29,090 --> 00:15:32,730
Nie za wcześnie?
Mamy kupę ludzi na śniadaniu.

146
00:15:34,030 --> 00:15:38,030
Poradzisz sobie z jajecznicą.
Niedługo wrócę.

147
00:15:39,580 --> 00:15:41,620
Potraktuj to jak egzamin.

148
00:15:49,120 --> 00:15:50,550
Dobra.

149
00:16:17,110 --> 00:16:19,450
- Myślałem, że zamieszkasz u mnie.
- Przykro mi.

150
00:16:19,540 --> 00:16:23,670
- Co się stało? Co ty tu robisz?
- To mój dom.

151
00:16:23,760 --> 00:16:27,530
Dom? Po tym, co ci zrobiła Catherine?

152
00:16:28,910 --> 00:16:30,440
Nie mam wyjścia.

153
00:16:42,150 --> 00:16:46,090
- Idź do domu.
- Zaopiekuję się tobą.

154
00:16:46,180 --> 00:16:48,520
- Nie.
- Proszę.

155
00:16:49,200 --> 00:16:53,790
Tu jestem bezpieczna. Zostanę tutaj.
Tobie też nic nie grozi.

156
00:17:02,470 --> 00:17:03,970
Przestań.

157
00:17:04,050 --> 00:17:06,400
Spójrz na mnie.

158
00:17:06,490 --> 00:17:10,510
Jestem tylko służącą,
to nie dla ciebie.

159
00:17:12,380 --> 00:17:14,590
Mnie wystarczy.

160
00:17:17,870 --> 00:17:19,240
Kocham cię.

161
00:17:22,300 --> 00:17:23,750
I pragnę.

162
00:18:00,070 --> 00:18:01,330
Co się dzieje?

163
00:18:04,730 --> 00:18:08,760
Co? Co się stało?

164
00:18:09,300 --> 00:18:15,530
Sierżancie! Do mnie i meldujcie!
Są jakieś wieści?

165
00:18:19,870 --> 00:18:24,860
Gdzie generał Stuart
i jego kawaleria? Już nadciąga.

166
00:18:24,940 --> 00:18:27,850
Niech go szlag! Niedługo tu będzie.

167
00:18:27,930 --> 00:18:28,780
Wiem o tym!

168
00:18:29,250 --> 00:18:33,030
Oby naprawdę się zjawił.
Dla naszego dobra.

169
00:18:33,590 --> 00:18:37,840
Bo tu spotkamy się z wrogiem.

170
00:18:37,920 --> 00:18:40,820
Spotkamy się przed świtem.

171
00:18:41,550 --> 00:18:42,960
Tato?

172
00:18:46,790 --> 00:18:47,950
Tak, córeczko?

173
00:18:49,980 --> 00:18:51,750
Co robisz?

174
00:18:53,300 --> 00:18:55,390
Toczy się wojna.

175
00:18:56,330 --> 00:18:59,900
- Wojna?
- Między stanami.

176
00:19:02,270 --> 00:19:05,620
Potrzebujesz pomocy.

177
00:19:06,190 --> 00:19:08,520
Nic mi nie dolega.

178
00:19:09,720 --> 00:19:14,990
Potrzebuję tylko czasu,
to ważny dzień.

179
00:19:15,070 --> 00:19:19,560
Tak, bardzo ważny.
Musimy ratować hotel. Ty i ja.

180
00:19:19,640 --> 00:19:22,940
- Skarbie...
- Co?

181
00:19:23,020 --> 00:19:25,590
Klęczysz na generale Stuarcie.

182
00:19:30,530 --> 00:19:36,310
Generale Lee! Generale,
stary Jeb się zbliża.

183
00:19:38,280 --> 00:19:43,570
Nie chcę więcej wozów!
Potrzebuję wiernych żołnierzy!

184
00:19:44,410 --> 00:19:49,690
Mówi Audrey. Chcę mówić
z Jerrym Horne'em, to ważne.

185
00:19:54,330 --> 00:19:56,680
Musimy pomówić.

186
00:20:00,540 --> 00:20:01,690
Wejdź.

187
00:20:07,740 --> 00:20:09,020
Ty pierwsza.

188
00:20:13,330 --> 00:20:16,240
Moja ostatnia myśl
przed snem jest o tobie.

189
00:20:17,220 --> 00:20:20,200
I o tobie myślę, gdy budzę się rano.

190
00:20:21,870 --> 00:20:24,290
Świat rozpada się na kawałki

191
00:20:24,370 --> 00:20:27,400
i za wszelką cenę chce nas rozdzielić.

192
00:20:28,910 --> 00:20:31,350
Ale my się kochamy.

193
00:20:33,220 --> 00:20:35,180
Chcę być z tobą.

194
00:20:36,230 --> 00:20:38,600
Choćby nie wiem, co się działo.

195
00:20:50,640 --> 00:20:53,840
- Twoja kolej.
- Później.

196
00:21:06,310 --> 00:21:11,080
- Uważaj na włosy.
- Okropnie się pan poci.

197
00:21:13,550 --> 00:21:17,110
To nerwowe. Nadmierna potliwość.

198
00:21:17,690 --> 00:21:22,370
Mam to od dziecka, ale ujawniło się
na wojnie w Korei. Pamiętacie?

199
00:21:23,300 --> 00:21:26,210
Dowodziłem batalionem
przy 49. równoleżniku.

200
00:21:26,300 --> 00:21:28,710
Byliśmy bandą żółtodziobów

201
00:21:28,790 --> 00:21:31,150
i nie wiedziałem,
w jakie piekło idziemy.

202
00:21:31,230 --> 00:21:31,920
Ernie!

203
00:21:32,440 --> 00:21:35,300
Wróć do teraźniejszości.

204
00:21:35,640 --> 00:21:38,320
Powtórz kolejno plan operacji.

205
00:21:41,490 --> 00:21:46,860
Zabieram Denise
na Farmę Zdechłego Psa,

206
00:21:47,110 --> 00:21:50,030
przedstawiam ją Renault...

207
00:21:52,910 --> 00:21:55,230
asystuję przy transakcji...

208
00:21:55,810 --> 00:21:57,660
Kończę sprawę i wynoszę się do diabła.

209
00:21:58,230 --> 00:22:01,790
Dobrze. Harry? Teraz wkraczacie wy.

210
00:22:05,330 --> 00:22:10,020
Chciałbym móc z wami pójść,
ale chwilowo nie mam uprawnień.

211
00:22:10,110 --> 00:22:12,020
Fakt.

212
00:22:12,110 --> 00:22:14,010
Przemyślałem to...

213
00:22:15,440 --> 00:22:17,470
i mianuję cię moim zastępcą.

214
00:22:18,570 --> 00:22:20,940
Strata FBI to mój zysk.

215
00:22:22,730 --> 00:22:24,640
Mam nadzieję temu sprostać.

216
00:22:25,970 --> 00:22:29,810
Gotowe. Ale trzeba go wytrzeć.

217
00:22:34,100 --> 00:22:40,290
Ile czasu minie od zakończenia
transakcji do ataku?

218
00:22:40,370 --> 00:22:42,240
Ty wychodzisz, my wchodzimy.

219
00:22:42,630 --> 00:22:45,500
- Mogę o coś prosić?
- O co?

220
00:22:45,860 --> 00:22:48,960
Nie zmuszajcie mnie.
Nie nadaję się, jestem tchórzem.

221
00:22:49,040 --> 00:22:51,380
To nie jest robota dla cywila.

222
00:22:53,900 --> 00:22:58,220
To atak paniki, już mi lepiej.
Do roboty.

223
00:22:59,400 --> 00:23:02,150
- Gdzie jest Denise?
- Nie wiem.

224
00:23:03,030 --> 00:23:05,550
Mówcie mi Dennis.

225
00:23:06,930 --> 00:23:10,730
Pomyślałem, że tak będzie stosowniej.

226
00:23:14,780 --> 00:23:15,990
I jak?

227
00:23:23,340 --> 00:23:26,620
Nasze dochodzenie jest tajne.

228
00:23:29,360 --> 00:23:32,740
Trzeba się włamać.

229
00:23:37,050 --> 00:23:38,300
Wchodzimy.

230
00:23:40,180 --> 00:23:44,690
- Gdzie są wszyscy?
- Na obiedzie. Jest kartoteka.

231
00:23:44,780 --> 00:23:45,850
N...

232
00:23:53,190 --> 00:23:59,230
Nails, Neaster, Needleman.

233
00:23:59,310 --> 00:24:02,350
Nicholas Needleman, nasz Nicky.

234
00:24:02,430 --> 00:24:05,280
- Przeczytamy w samochodzie.
- Cierpliwości.

235
00:24:06,270 --> 00:24:08,460
Mamy ogólne dane...

236
00:24:09,570 --> 00:24:13,970
Miejsce urodzenia, adopcja...

237
00:24:15,960 --> 00:24:18,320
Jak widzę, pierwsza z wielu.

238
00:24:21,900 --> 00:24:23,140
Robi się ciekawie.

239
00:24:25,350 --> 00:24:28,160
Co ty ukrywasz, Nicky?

240
00:24:33,550 --> 00:24:34,980
Dzień dobry.

241
00:24:40,660 --> 00:24:42,340
Państwo Brunston.

242
00:24:44,190 --> 00:24:47,450
Jesteśmy trochę wcześniej,
ale tak bardzo chcemy go zobaczyć.

243
00:24:47,760 --> 00:24:52,350
- Możemy? Gdzie jest Donnie?
- Mały Donnie...

244
00:24:53,700 --> 00:24:54,630
umiera...

245
00:24:57,730 --> 00:24:59,470
Umiera ze zmęczenia.

246
00:25:00,640 --> 00:25:04,890
Obawiam się, że nie jest w formie.

247
00:25:05,150 --> 00:25:07,890
Wczoraj tryskał zdrowiem.

248
00:25:10,560 --> 00:25:13,510
Chwileczkę... Woody.

249
00:25:13,590 --> 00:25:16,020
Pozwól mi państwu pomóc.

250
00:25:17,790 --> 00:25:19,940
Na czym stanęliśmy?

251
00:25:20,950 --> 00:25:23,420
Ed? To ja, Donna.

252
00:25:26,110 --> 00:25:29,120
- Musisz mi pomóc.
- Co się stało?

253
00:25:29,200 --> 00:25:33,200
Szukam Jamesa. Nie przyszedł
do szkoły ani do baru.

254
00:25:33,280 --> 00:25:35,500
Byłam u niego w domu, ale go nie ma.

255
00:25:35,590 --> 00:25:37,980
Nic mu nie jest, dzwonił.

256
00:25:38,060 --> 00:25:42,310
Prosił, żebym mu wysłał pieniądze
do baru przy 96-ej.

257
00:25:42,400 --> 00:25:45,780
Nie pogadaliśmy za dużo.
Tylko tyle wiem.

258
00:25:45,860 --> 00:25:49,700
- Zawiozę to.
- Co powiesz rodzicom?

259
00:25:49,780 --> 00:25:52,150
Jakoś im to wytłumaczę.

260
00:25:53,850 --> 00:25:55,120
Dzięki.

261
00:26:02,840 --> 00:26:04,510
Pójdę już.

262
00:26:04,600 --> 00:26:05,970
Zadzwonię.

263
00:26:06,780 --> 00:26:08,960
Mamy sporo do omówienia.

264
00:26:19,350 --> 00:26:23,010
Och, Ed...
Czego się nie robi z miłości.

265
00:26:32,590 --> 00:26:34,250
Eddie, wróciłam!

266
00:26:36,580 --> 00:26:39,190
Eddie, uważaj!

267
00:26:57,680 --> 00:27:01,190
Ty łobuzie!

268
00:27:09,690 --> 00:27:10,920
Eddie?

269
00:27:12,960 --> 00:27:14,610
Słyszysz mnie?

270
00:27:18,120 --> 00:27:20,210
Jestem przy tobie.

271
00:27:21,070 --> 00:27:25,920
Nic się nie martw.
Już ja się tobą zajmę.

272
00:27:35,260 --> 00:27:38,880
Pułkownik Chamberlain
z 20. pułkiem z Maine

273
00:27:38,960 --> 00:27:42,290
utknął na szczycie Little Round Top.

274
00:27:43,160 --> 00:27:50,210
Jego jedyną szansą
było przesunięcie lewej flanki

275
00:27:50,290 --> 00:27:55,100
w dół zbocza, jak skrzydło bramy.
Ale my tam czekaliśmy.

276
00:27:56,630 --> 00:28:02,000
I rozpaczliwy manewr Chamberlaina
zakończył się klęską!

277
00:28:03,050 --> 00:28:08,370
Zdobyliśmy Little Round Top.
To był punkt zwrotny

278
00:28:08,460 --> 00:28:12,840
i teraz mogliśmy odepchnąć
oddziały Jankesów

279
00:28:12,930 --> 00:28:15,660
i oczyścić przedpole
dla wielkiej szarży Picketta.

280
00:28:18,530 --> 00:28:21,040
I zwyciężyliśmy!

281
00:28:24,480 --> 00:28:26,880
Zwycięstwo!

282
00:28:37,690 --> 00:28:38,970
Generale Meade...

283
00:28:40,330 --> 00:28:44,790
poniósł pan haniebną klęskę.

284
00:28:45,800 --> 00:28:50,290
Przyjmuję pańską
bezwarunkową kapitulację.

285
00:28:52,860 --> 00:28:56,650
Dobra, wie pan co?

286
00:28:56,730 --> 00:29:00,580
Najpierw pójdę pogadać
z prezydentem Lincolnem.

287
00:29:00,660 --> 00:29:03,660
Niech pan tu zaczeka...

288
00:29:07,740 --> 00:29:12,970
Aż wrócę w sprawie
tej kapitulacji. Muszę lecieć.

289
00:29:15,070 --> 00:29:18,200
Nie każmy prezydentowi czekać.

290
00:29:20,450 --> 00:29:21,560
Generale!

291
00:29:24,640 --> 00:29:28,430
Mam dla ciebie dobrą i złą wiadomość.

292
00:29:29,600 --> 00:29:33,760
Zła jest taka, że twojemu staremu
całkiem odbiło,

293
00:29:33,840 --> 00:29:37,140
a dobra, że chyba
wygra wojnę secesyjną.

294
00:29:37,230 --> 00:29:41,840
Wiem. Stryj Jerry
wraca najbliższym samolotem,

295
00:29:41,930 --> 00:29:44,940
a jutro przyjdzie doktor Jacoby.

296
00:29:45,760 --> 00:29:50,560
Może tacie trzeba zrobić zastrzyk?
To by mu pomogło.

297
00:29:50,650 --> 00:29:53,280
Nie martw się, mała, ja tu jestem.

298
00:29:53,370 --> 00:29:55,310
Nie mów do mnie "mała".

299
00:30:10,690 --> 00:30:12,480
Wejść!

300
00:30:15,420 --> 00:30:16,620
Catherine!

301
00:30:23,870 --> 00:30:25,250
Witaj, Ben.

302
00:30:27,950 --> 00:30:30,410
Przyszłaś się nade mną pastwić?

303
00:30:31,770 --> 00:30:34,600
Nacieszyć się moim upadkiem?

304
00:30:36,930 --> 00:30:40,220
Proszę, śmiej się.

305
00:30:42,240 --> 00:30:44,020
Pokonałaś mnie,

306
00:30:45,280 --> 00:30:48,060
jak ja generała Meada.

307
00:30:49,440 --> 00:30:50,800
Tak...

308
00:30:52,190 --> 00:30:54,780
chciałam się tym nacieszyć.

309
00:30:58,720 --> 00:31:03,160
Wykantowałeś mnie. Próbowałeś zabić.

310
00:31:04,280 --> 00:31:07,990
Chciałam cię pogrzebać tak głęboko,

311
00:31:08,540 --> 00:31:13,410
żeby twoje szczątki odkopali
dopiero za kilka pokoleń.

312
00:31:13,500 --> 00:31:19,200
Szczurus wredus americanus.
Nie karmić.

313
00:31:23,770 --> 00:31:25,660
Nie ufać.

314
00:31:28,520 --> 00:31:30,770
Chyba...

315
00:31:33,960 --> 00:31:36,100
nie jestem godzien zaufania.

316
00:31:43,060 --> 00:31:47,360
A mimo tego wszystkiego,
co o tobie wiem...

317
00:31:49,190 --> 00:31:52,420
jestem tutaj.

318
00:31:54,240 --> 00:31:56,210
I pragnę cię.

319
00:31:56,290 --> 00:31:59,020
Raczysz żartować.

320
00:32:03,470 --> 00:32:05,720
Pragnę cię.

321
00:32:07,350 --> 00:32:11,340
To okropne, ale silniejsze ode mnie.

322
00:32:17,480 --> 00:32:20,460
Rozpalasz moje ciało.

323
00:32:25,060 --> 00:32:28,050
Pocałuj mnie, generale Lee.

324
00:32:45,670 --> 00:32:48,460
Do zwycięstwa!

325
00:32:49,540 --> 00:32:53,570
Prędzej, Jim. Szampan się grzeje.

326
00:32:53,650 --> 00:32:55,770
Nie mów do mnie Jim.

327
00:32:56,910 --> 00:32:59,850
Jeszcze kawałek.

328
00:33:02,950 --> 00:33:06,080
- Co robisz?
- Możesz spojrzeć!

329
00:33:08,820 --> 00:33:10,850
Dokąd chcesz jechać?

330
00:33:11,860 --> 00:33:16,460
Jeffrey wróci przed północą.
Nie mamy wiele czasu.

331
00:33:19,610 --> 00:33:22,770
- Wykorzystajmy to, co mamy.
- Racja.

332
00:33:33,180 --> 00:33:36,940
Za przystojnych nieznajomych.
I wyremontowane silniki.

333
00:33:41,800 --> 00:33:46,230
Nie sądziłam, że ci się uda.
Jesteś cudotwórcą.

334
00:33:46,310 --> 00:33:51,080
- Wcale nie.
- Nie bądź taki skromny.

335
00:33:52,070 --> 00:33:56,590
Zasłużyłeś. Jesteś silny i miły.

336
00:33:57,950 --> 00:33:59,710
Masz cudownie szczerą twarz.

337
00:34:01,660 --> 00:34:03,380
Nie śmiej się, to rzadkość.

338
00:34:04,370 --> 00:34:07,510
Wystarczy spojrzeć
i już wiem, o czym myślisz.

339
00:34:07,840 --> 00:34:11,570
Muszę to lepiej ukrywać.

340
00:34:11,660 --> 00:34:12,930
Nie musisz.

341
00:34:14,440 --> 00:34:15,520
Nie przede mną.

342
00:34:17,950 --> 00:34:20,720
Co zamierzasz?

343
00:34:20,810 --> 00:34:22,890
Nie mam żadnych planów.

344
00:34:22,970 --> 00:34:24,470
Dokąd pojedziesz?

345
00:34:25,310 --> 00:34:26,630
Nie wiem.

346
00:34:28,110 --> 00:34:29,610
Nie jedź.

347
00:34:31,880 --> 00:34:33,670
Nie wyjeżdżaj.

348
00:34:34,660 --> 00:34:36,690
Daj mi trochę czasu.

349
00:34:37,540 --> 00:34:40,330
Jest sto powodów,
dla których musisz zostać.

350
00:35:38,160 --> 00:35:43,600
Nie zaczekamy na Hanka?
Rano mówił, że chce przy tym być.

351
00:35:43,680 --> 00:35:46,280
- Zacznijmy bez niego.
- Jak chcesz.

352
00:35:50,260 --> 00:35:53,340
Ernie... co ci jest?

353
00:35:53,420 --> 00:35:57,710
Nadmiernie się pocę.
Mam to od dziecka.

354
00:35:58,020 --> 00:36:05,110
Na Guam złapałem żółtą febrę,
a skutki uboczne są koszmarne.

355
00:36:05,910 --> 00:36:10,260
- Najgorsze są te poty...
- Usiądź...

356
00:36:10,750 --> 00:36:12,390
i odetchnij.

357
00:36:14,330 --> 00:36:16,460
Wszystko się zgadza.

358
00:36:16,550 --> 00:36:18,810
Pospieszmy się, muszę złapać samolot.

359
00:36:20,210 --> 00:36:22,940
Jean, patrz.

360
00:36:23,020 --> 00:36:24,410
Jego koszula dymi.

361
00:36:27,540 --> 00:36:28,400
Podsłuch padł.

362
00:36:29,340 --> 00:36:30,510
Cooper!

363
00:36:37,450 --> 00:36:39,080
Przyszedł tu po mnie.

364
00:36:40,050 --> 00:36:44,360
- Przepuścicie mnie do granicy.
- Uwolnij zakładników.

365
00:36:44,440 --> 00:36:47,070
Mamy tu sprzeczność interesów.

366
00:36:47,790 --> 00:36:51,510
- Będziemy strzelać.
- I wszyscy zginą.

367
00:36:54,060 --> 00:36:55,100
Proponuję wymianę.

368
00:36:57,290 --> 00:36:58,430
Jaką?

369
00:36:59,360 --> 00:37:01,900
Ernie Niles i agent Bryson za mnie.

370
00:37:13,480 --> 00:37:15,360
- Wezwij wsparcie.
- Policja stanowa?

371
00:37:15,440 --> 00:37:16,680
Ściągnij ich.

372
00:37:21,490 --> 00:37:24,950
Lucy, tu Hawk.
��cz z policj� stanow�.

373
00:38:01,960 --> 00:38:04,710
Jak się miewa nasz chłopaczek?

374
00:38:04,790 --> 00:38:08,340
Śpi i marzy o miłości.

375
00:38:15,900 --> 00:38:19,360
Szczęściarz z niego.
Wielki szczęściarz.

376
00:38:38,140 --> 00:38:40,930
Przeszli do środkowego pokoju.

377
00:38:41,020 --> 00:38:43,040
Andy, skup się.

378
00:38:44,930 --> 00:38:45,750
Przyjechali nowi.

379
00:38:47,910 --> 00:38:49,220
Są snajperzy.

380
00:38:51,930 --> 00:38:53,550
Paktujmy albo wiejmy.

381
00:38:57,970 --> 00:39:01,010
Wypuszczą nas, agencie Cooper?

382
00:39:03,180 --> 00:39:04,100
Nie.

383
00:39:04,970 --> 00:39:08,080
Pójdą na układ?

384
00:39:10,490 --> 00:39:11,230
Nie.

385
00:39:11,900 --> 00:39:13,800
To co proponujesz?

386
00:39:14,860 --> 00:39:16,270
Poddajcie się.

387
00:39:23,450 --> 00:39:24,910
Zwariowałeś?

388
00:39:25,720 --> 00:39:32,840
Nie. Najpierw zdecydujmy,
czy go zabić, czy nie.

389
00:39:35,240 --> 00:39:38,380
- Zginiemy obaj.
- Wiem.

390
00:39:39,540 --> 00:39:41,930
Tak ci zależy na mojej śmierci?

391
00:39:43,480 --> 00:39:47,850
Straciłem dwóch braci. To twoja wina.

392
00:39:49,600 --> 00:39:50,860
Dlaczego?

393
00:39:59,970 --> 00:40:02,130
Zanim się zjawiłeś,

394
00:40:02,870 --> 00:40:05,950
w Twin Peaks było spokojnie.

395
00:40:06,590 --> 00:40:10,900
Moi bracia sprzedawali prochy
uczniom i szoferakom.

396
00:40:11,510 --> 00:40:14,610
Jednooki Jack gościł
biznesmenów i turystów.

397
00:40:14,700 --> 00:40:17,880
Zwykli ludzie wiedli zwykłe życie.

398
00:40:19,020 --> 00:40:21,890
Po śmierci jednej panienki...

399
00:40:23,140 --> 00:40:27,310
przyjechałeś ty
i wszystko się zmieniło.

400
00:40:27,390 --> 00:40:34,150
Mój brat Bernard
został zastrzelony w lesie,

401
00:40:35,090 --> 00:40:39,250
a drugiego udusił poduszką
oszalały z żalu ojciec.

402
00:40:39,330 --> 00:40:42,010
Porwania, śmierć...

403
00:40:42,880 --> 00:40:47,820
Nagle spokój zniknął.

404
00:40:48,440 --> 00:40:52,570
Zwyczajne sny...

405
00:40:52,660 --> 00:40:54,750
zmieniły się w koszmary.

406
00:40:56,380 --> 00:41:00,460
Jeśli umrzesz,
może będziesz ostatni.

407
00:41:01,180 --> 00:41:05,840
Może to ty przywiozłeś koszmary.

408
00:41:06,420 --> 00:41:12,740
I może zginą wraz z tobą.

409
00:41:14,260 --> 00:41:17,150
Nie mam pojęcia, o czym gadasz...

410
00:41:18,780 --> 00:41:22,740
ale wpadliśmy jak cholera.

411
00:41:23,560 --> 00:41:27,120
I nie zamierzam się poddawać.

412
00:41:29,360 --> 00:41:30,820
Jean?

413
00:41:30,900 --> 00:41:33,650
Patrz. Zamawiałeś jakieś żarcie?

414
00:41:33,730 --> 00:41:39,590
Nie. Ale niech wejdzie.

415
00:41:39,880 --> 00:41:41,340
To tylko dziewczyna.

416
00:41:56,790 --> 00:41:58,690
Zjecie kolację?

417
00:42:07,430 --> 00:42:09,450
Ja cię skądś znam.

418
00:42:40,020 --> 00:42:43,860
- Nic ci nie jest?
- Nic.

419
00:42:43,940 --> 00:42:45,150
Świetnie, Denise.

420
00:42:45,930 --> 00:42:50,830
Pokazałam tylko nogi.
Dziękuj szeryfowi, to jego pomysł.

421
00:42:55,850 --> 00:42:58,020
Czasem improwizujemy.

422
00:42:59,140 --> 00:43:00,960
Nie żyje.

423
00:43:19,950 --> 00:43:21,140
Bobby?

424
00:43:53,070 --> 00:43:54,640
To nie jest śmieszne.

425
00:44:10,500 --> 00:44:11,890
Shelly...

426
00:44:18,530 --> 00:44:21,260
Jaka znów bomba?

427
00:44:22,040 --> 00:44:25,710
Ktoś zadzwonił z wiadomością,
że w lesie podłożono bombę,

428
00:44:25,790 --> 00:44:30,160
ale nie powiedział gdzie.
A potem był straszny wybuch

429
00:44:30,240 --> 00:44:34,070
i zgasło światło, więc wezwałam straż.

430
00:44:34,150 --> 00:44:36,380
- Bardzo słusznie.
- Był pożar?

431
00:44:36,460 --> 00:44:40,440
Dwa. Pierwszy w stacji rozdzielczej.
Ale w remizie...

432
00:44:40,530 --> 00:44:44,340
Sprawdzę generator, coś nawala.

433
00:44:45,790 --> 00:44:48,730
Graliśmy w bingo
i zbierali się powoli.

434
00:44:48,820 --> 00:44:51,630
A potem był ten wybuch,
o którym mówiłam.

435
00:44:51,720 --> 00:44:54,300
Walnęło mi prosto nad głową.

436
00:44:54,390 --> 00:44:58,350
Okazało się, że to transformator
na słupie nad naszym dachem.

437
00:44:58,430 --> 00:45:00,880
Nie mogliśmy od razu wejść,

438
00:45:00,960 --> 00:45:05,190
bo sypało iskrami i baliśmy się,
że posterunek się zajmie.

439
00:45:05,280 --> 00:45:06,480
Wiadomo, co się stało?

440
00:45:06,570 --> 00:45:10,200
Komendant Kipple powiedział,
że to podejrzane.

441
00:45:10,290 --> 00:45:14,530
Podczas pożaru nawdychał się
dymu i miał chrypkę.

442
00:45:14,610 --> 00:45:16,480
Ugotuję mu rosół.

443
00:45:18,260 --> 00:45:19,500
Harry?

444
00:45:21,600 --> 00:45:24,720
Lepiej tu przyjdź, ale sam.

445
00:45:24,800 --> 00:45:26,240
Zostań tu.

446
00:45:36,030 --> 00:45:37,240
Dobry Boże...

447
00:45:39,690 --> 00:45:41,620
Kto to jest?

448
00:45:41,700 --> 00:45:43,210
Nie wiem.

449
00:45:43,760 --> 00:45:45,510
To partia szachów...

450
00:45:46,580 --> 00:45:49,210
i następny ruch Windoma Earle'a.

