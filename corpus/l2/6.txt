1
00:00:00,000 --> 00:00:00,040
"Miasteczko Twin Peaks" odc. 11 (2x04)
kraps v4.01 23.02.07

2
00:01:47,060 --> 00:01:49,650
Tatusiu.

3
00:01:55,490 --> 00:01:58,070
Tatusiu.

4
00:01:58,400 --> 00:02:00,950
Leland.

5
00:02:02,030 --> 00:02:04,620
Tatusiu.

6
00:02:04,870 --> 00:02:07,370
Leland.

7
00:02:08,330 --> 00:02:12,000
Tatusiu. Tatusiu. Leland.

8
00:02:16,800 --> 00:02:19,510
Leland!

9
00:02:27,270 --> 00:02:30,690
Masz prawo do adwokata.

10
00:02:30,730 --> 00:02:34,070
Zrzekam si� tego prawa.

11
00:02:34,940 --> 00:02:37,900
Zadam ci kilka pyta�.

12
00:02:38,070 --> 00:02:42,070
Wszystko, co powiesz, mo�e by�
wykorzystane przeciwko tobie w s�dzie.

13
00:02:42,240 --> 00:02:45,990
- Rozumiesz?
- Tak, oczywi�cie.

14
00:02:49,540 --> 00:02:53,130
Czy w pi�tek, trzeciego marca

15
00:02:53,170 --> 00:02:57,710
uda�e� si� do szpitala
Calhoun Memorial?

16
00:02:57,880 --> 00:03:00,180
Tak.

17
00:03:00,800 --> 00:03:03,930
Dlaczego tam poszed�e�?

18
00:03:03,970 --> 00:03:07,020
Szuka�em kogo�.

19
00:03:07,270 --> 00:03:08,350
Kogo?

20
00:03:09,600 --> 00:03:11,520
Zab�jcy mojej c�rki.

21
00:03:15,610 --> 00:03:17,400
Zna�e� jego nazwisko?

22
00:03:17,440 --> 00:03:18,690
Nie.

23
00:03:18,740 --> 00:03:21,820
Tak. Teraz je znam.

24
00:03:23,950 --> 00:03:26,870
Jacques Renault.

25
00:03:27,080 --> 00:03:30,870
Dlaczego uzna�e�, �e zabi� Laur�.

26
00:03:33,330 --> 00:03:36,420
Aresztowali�cie go.

27
00:03:41,670 --> 00:03:44,390
Lelandzie...

28
00:03:44,800 --> 00:03:48,510
czy zabi�e� Jacques'a Renault?

29
00:03:52,140 --> 00:03:55,100
Zabi� moj� Laur�.

30
00:03:57,360 --> 00:04:01,570
Do�wiadczyli�cie kiedy� straty zupe�nej?

31
00:04:03,610 --> 00:04:07,330
Nikomu z nas nie jest obcy �al.

32
00:04:07,780 --> 00:04:10,830
To wi�cej ni� �al.

33
00:04:12,580 --> 00:04:17,750
W �rodku... wewn�trz...
ka�da kom�rka wyje.

34
00:04:19,630 --> 00:04:23,590
Nic innego nie s�ycha�.

35
00:04:29,680 --> 00:04:32,100
Tak...

36
00:04:34,890 --> 00:04:38,230
Zabi�em go... zabi�em...

37
00:04:40,110 --> 00:04:43,190
Tak... tak!

38
00:04:56,540 --> 00:05:00,250
- Og�osi czasow� niepoczytalno��.
- Przeprowadzimy badania psychiatryczne.

39
00:05:00,300 --> 00:05:03,010
Rodzice nie powinni
grzeba� swoich dzieci.

40
00:05:03,050 --> 00:05:04,800
Kto przeszed� to, co on...

41
00:05:04,920 --> 00:05:07,680
Pochwala pan zab�jstwo?

42
00:05:07,720 --> 00:05:10,600
Nie.

43
00:05:13,520 --> 00:05:16,390
Doktorze!

44
00:05:17,190 --> 00:05:22,030
- Jak si� masz, Andy?
- �wietnie.

45
00:05:22,070 --> 00:05:26,030
Masz jaki� problem?

46
00:05:27,070 --> 00:05:31,080
- Osobisty.
- Jestem twoim lekarzem.

47
00:05:31,240 --> 00:05:33,580
Chodzi o badanie spermy.

48
00:05:37,500 --> 00:05:39,380
Wie pan - obla�em.

49
00:05:39,420 --> 00:05:42,550
To nie jest odpowiednie
okre�lenie.

50
00:05:42,710 --> 00:05:47,760
Zastanawia�em si�, czy takie
badanie mo�na powt�rzy�.

51
00:05:47,930 --> 00:05:51,430
Jak egzamin na prawo jazdy.

52
00:05:52,100 --> 00:05:56,100
Nosi�em codziennie bokserki,
jak mi pan kaza�.

53
00:05:56,270 --> 00:06:00,270
- Chcesz znowu spr�bowa�?
- Tak.

54
00:06:00,440 --> 00:06:03,940
Potrzebna mi b�dzie pr�bka.

55
00:06:04,230 --> 00:06:05,190
Teraz?

56
00:06:05,230 --> 00:06:08,400
W�� do torebki.
Zaczekam w samochodzie.

57
00:06:43,440 --> 00:06:45,070
[ TOALETY ]

58
00:06:58,870 --> 00:07:02,290
Lucy, jest tu �mieciarka...

59
00:07:02,330 --> 00:07:05,290
Przymknij si�.

60
00:07:06,170 --> 00:07:10,170
Po po�udniu przyje�d�a s�dzia.
Clinton Sternwood.

61
00:07:10,340 --> 00:07:12,930
Obje�d�a okr�g teren�wk�.

62
00:07:12,970 --> 00:07:18,850
- Amator zielonej trawki?
- Ostatni z le�nych ludzi. Wygin�li.

63
00:07:20,770 --> 00:07:24,060
Rozstrzygnie o kaucji Lelanda
i o procesie Leo.

64
00:07:24,110 --> 00:07:26,770
Leland wyjdzie za kaucj�?

65
00:07:26,820 --> 00:07:28,820
To zale�y od prokuratora stanowego.

66
00:07:28,860 --> 00:07:32,070
Czyli Daryla Lodwicka.
Te� tu jedzie.

67
00:07:32,110 --> 00:07:36,120
- Mamy co� od Hawka?
- Dzwoni� rano.

68
00:07:37,490 --> 00:07:45,170
Podobno �aden Robertson
nigdy nie mieszka� obok Palmer�w.

69
00:07:45,840 --> 00:07:48,800
Mamy obecny adres ostatniego mieszka�ca.

70
00:07:48,960 --> 00:07:50,880
Kalispell.

71
00:07:51,050 --> 00:07:54,010
Sprawdzimy go po po�udniu.

72
00:07:54,180 --> 00:07:59,020
- Hej! Uwa�aj, Andy!
- Przepraszam, Bob... o Bo�e!

73
00:08:05,940 --> 00:08:08,900
Andy, nie ruszaj si�!

74
00:08:09,400 --> 00:08:13,400
- Sk�d to masz?
- Nie mog� powiedzie�.

75
00:08:13,570 --> 00:08:16,410
Andy!

76
00:08:17,120 --> 00:08:21,330
Prosz�, agencie Cooper,
to sprawa osobista.

77
00:08:21,370 --> 00:08:23,710
Pytam o buty.

78
00:08:25,460 --> 00:08:30,880
Przepraszam. Kupi�em je wczoraj
od Phillipa Gerarda.

79
00:08:34,840 --> 00:08:38,850
Takie same znale�li�my
u Leo Johnsona.

80
00:08:39,010 --> 00:08:43,020
- Olbrzym m�wi�, �e jest tam wskaz�wka.
- My�la�em, �e chodzi�o o kokain�.

81
00:08:43,180 --> 00:08:46,190
Musimy odnale��
Phillipa Gerarda - jednor�kiego.

82
00:08:46,350 --> 00:08:50,360
Mog� ju� i��, agencie Cooper?
Nie wiem, ile czasu mi zosta�o.

83
00:08:50,530 --> 00:08:53,280
Skoro musisz.

84
00:08:57,820 --> 00:09:01,410
Co si� dzieje z Lucy?

85
00:09:18,680 --> 00:09:20,600
- Panie Horne.
- M�w.

86
00:09:20,760 --> 00:09:24,770
Mam znajom� w gazecie w Seattle.
Chodzi�a do szko�y z moj� siostr�.

87
00:09:24,930 --> 00:09:26,850
Przyja�nimy si�.

88
00:09:27,020 --> 00:09:31,020
Dzwoni�a dzi� do mnie
ze �ci�le tajn� wiadomo�ci�.

89
00:09:31,190 --> 00:09:34,150
Do Twin Peaks przyje�d�a M.T. Wentz.

90
00:09:34,320 --> 00:09:39,370
- M.T. Wentz?
- To ten dziennikarz.

91
00:09:39,530 --> 00:09:41,620
Oczywi�cie, dziennikarz.

92
00:09:41,660 --> 00:09:44,700
Podr�uje incognito,
p�aci got�wk� - nigdy kart�.

93
00:09:44,750 --> 00:09:47,710
Nikt nie wie, co to za jeden.

94
00:09:47,870 --> 00:09:50,840
Nawet w gazecie nie wiedz�, kto to jest.

95
00:09:51,000 --> 00:09:55,010
- Przychylna recenzja nie zaszkodzi.
- On przyje�d�a...

96
00:09:55,170 --> 00:09:58,130
Dogl�daj ksi�gi go�ci.
Informuj mnie na bie��co.

97
00:09:58,300 --> 00:10:02,470
- Przyjrzyj si� nowym go�ciom.
- Zw�aszcza je�li p�ac� got�wk�.

98
00:10:02,510 --> 00:10:07,560
Jeszcze nie ma 9:30,
a ju� odwali�a� kawa� dobrej roboty.

99
00:10:19,200 --> 00:10:23,080
Panie Horne, �wietnie pan wygl�da.

100
00:10:24,410 --> 00:10:27,370
- Czym mog� s�u�y�?
- Ju� si� poznali�my.

101
00:10:27,540 --> 00:10:30,500
Sprzedaj� ubezpieczenia
drobnym przedsi�biorstwom.

102
00:10:30,670 --> 00:10:33,630
Takim jak "Jednooki Jack".

103
00:10:33,790 --> 00:10:36,550
Jean Renault.

104
00:10:36,920 --> 00:10:41,340
Co to za okazja?
Mam p�aci� wy�sze sk�adki?

105
00:10:42,140 --> 00:10:45,100
Nic z tych rzeczy.

106
00:10:45,260 --> 00:10:47,730
Prosz�.

107
00:11:09,250 --> 00:11:11,960
Ty draniu...

108
00:11:12,380 --> 00:11:16,420
- Ty draniu!
- Myli si� pan, panie Horne.

109
00:11:16,590 --> 00:11:18,550
Jestem tylko pos�a�cem.

110
00:11:18,590 --> 00:11:21,050
Czyim?

111
00:11:22,840 --> 00:11:26,430
��daj� sporej sumy pieni�dzy.

112
00:11:27,430 --> 00:11:29,270
Ile?

113
00:11:29,390 --> 00:11:31,770
Ja te� mam swoje ��dania.

114
00:11:33,270 --> 00:11:37,230
Nie s� w stanie sami panu zap�aci�?

115
00:11:38,480 --> 00:11:40,400
Po pierwsze:

116
00:11:40,570 --> 00:11:44,570
Pa�skim interesem kieruje
banda z�odziejaszk�w i g�upk�w.

117
00:11:44,740 --> 00:11:49,370
Wie pan, �e kradn� panu
ka�dego zarobionego dolara?

118
00:11:49,950 --> 00:11:53,960
- Podejrzewa�em.
- Potrzebny panu wsp�lnik.

119
00:11:54,130 --> 00:11:57,840
Kto�, kto zadba o te drobiazgi.

120
00:11:58,300 --> 00:12:01,760
- Mam ju� wsp�lnika.
- Dobrze wi�c.

121
00:12:02,090 --> 00:12:06,260
Mam pom�c, czy nie?

122
00:12:08,720 --> 00:12:12,060
- Dobrze.
- Jeszcze jedno.

123
00:12:16,020 --> 00:12:19,980
Okup ma przynie�� ten cz�owiek.

124
00:12:20,190 --> 00:12:22,950
To agent FBI.

125
00:12:24,360 --> 00:12:29,910
- Chce pan odzyska� c�rk�?
- Oczywi�cie, �e chc�, ale nie mog�...

126
00:12:29,950 --> 00:12:33,080
Got�wka, partnerstwo

127
00:12:35,750 --> 00:12:38,590
i agent FBI.

128
00:12:39,000 --> 00:12:46,010
- To ma by� sprawiedliwa wymiana?
- Mo�e si� op�aci�.

129
00:12:49,430 --> 00:12:53,180
Zadzwoni� jutro w samo po�udnie.

130
00:13:15,500 --> 00:13:17,920
Janet.

131
00:13:18,130 --> 00:13:21,090
Znajd� agenta Coopera.
To pilne.

132
00:13:24,880 --> 00:13:28,260
�artujesz? Jak wygl�da?

133
00:13:29,050 --> 00:13:33,470
Wygl�dasz dzi� wyj�tkowo �adnie.
Jaka� specjalna okazja?

134
00:13:33,640 --> 00:13:37,600
Um�wi�am si� na obiad z kim�,
kogo pozna�am wo��c jedzenie.

135
00:13:37,650 --> 00:13:40,400
Sami ob�o�nie chorzy.
Kogo mo�na pozna�?

136
00:13:40,440 --> 00:13:43,440
Nie zrozumia�by�.

137
00:13:44,740 --> 00:13:47,700
Nie rozlej zupy.

138
00:13:47,860 --> 00:13:49,780
Hank, pos�uchaj.

139
00:13:49,950 --> 00:13:53,870
Znajoma z hotelu m�wi,
�e do miasta przyje�d�a M.T. Wentz.

140
00:13:53,910 --> 00:13:55,790
Jaki Empik?

141
00:13:56,080 --> 00:13:59,500
To najs�ynniejszy w stanie
recenzent kulinarny.

142
00:13:59,630 --> 00:14:03,590
Dobra recenzja przysporzy nam klient�w.

143
00:14:03,630 --> 00:14:07,420
Louie postara si� go tu przys�a�.

144
00:14:08,840 --> 00:14:13,390
- Jak on wygl�da?
- Nie wiadomo. To tajemnica.

145
00:14:13,930 --> 00:14:18,770
- Ma podobno przyjecha� dzisiaj.
- Mamy jeszcze czas.

146
00:14:19,940 --> 00:14:22,980
Czas na co?

147
00:14:25,860 --> 00:14:28,740
Na co mamy czas?

148
00:14:29,570 --> 00:14:33,330
Przynios� kwiaty, mo�e �wiece...

149
00:14:34,790 --> 00:14:36,910
Mo�e jakie� obrusy.

150
00:14:36,950 --> 00:14:39,830
Co by tu doda� do menu?
Nowa specjalno�� kuchni?

151
00:14:39,870 --> 00:14:42,840
Zawsze my�la�am o daniach regionalnych.

152
00:14:43,380 --> 00:14:46,760
Kochanie, wystroimy knajp�
jak na gwiazdk�.

153
00:14:46,800 --> 00:14:48,970
Powinna� te� zadzwoni� do Du�ego Eda.

154
00:14:50,840 --> 00:14:52,800
Po co?

155
00:14:53,470 --> 00:14:55,430
Przyja�nicie si�, prawda?

156
00:14:55,470 --> 00:14:57,640
Mo�e ten Wentz zatrzyma si� na stacji.

157
00:14:57,680 --> 00:15:01,310
Ed m�g�by poleca� nas
wszystkim nieznajomym.

158
00:15:01,520 --> 00:15:03,730
Zadzwoni�.

159
00:15:04,400 --> 00:15:07,150
Koniecznie.

160
00:15:24,960 --> 00:15:27,550
Za co wypijemy?

161
00:15:27,590 --> 00:15:30,130
Za Laur�.

162
00:15:31,760 --> 00:15:36,100
Za Laur� w naszych sercach
i wspomnieniach.

163
00:15:49,490 --> 00:15:53,490
To jej pami�tnik.
M�wi�em ci, �e go mam?

164
00:15:53,660 --> 00:15:56,620
Pami�tnik Laury?

165
00:15:57,490 --> 00:16:02,120
Mog� ci co� z niego przeczyta�.
To chyba na miejscu.

166
00:16:02,160 --> 00:16:05,460
Prosz�.

167
00:16:13,510 --> 00:16:18,560
Nadal l�kam si� opowiedzie� jej
o moich fantazjach i nocnych koszmarach.

168
00:16:18,720 --> 00:16:22,730
Czasem od razu mnie rozumie,
kiedy indziej chichocze

169
00:16:22,770 --> 00:16:26,900
i nie potrafi� spyta�,
dlaczego j� to �mieszy.

170
00:16:27,060 --> 00:16:32,070
Wi�c zn�w czuj� si� podle
i znowu d�ugo nic nie m�wi�.

171
00:16:33,320 --> 00:16:35,240
Bardzo kocham Donn�,

172
00:16:35,410 --> 00:16:39,280
ale czasem boj� si�,
�e odesz�aby,

173
00:16:40,120 --> 00:16:44,120
gdyby si� dowiedzia�a,
jakie jest moje wn�trze.

174
00:16:44,790 --> 00:16:48,790
Czarne i ponure,
zatopione w snach o...

175
00:16:48,960 --> 00:16:53,670
wielkich m�czyznach i o tym,
jak by mnie brali...

176
00:16:54,170 --> 00:16:57,720
i podporz�dkowywali sobie...

177
00:16:59,390 --> 00:17:02,100
Przepraszam.

178
00:17:02,520 --> 00:17:05,440
Nie przepraszaj.

179
00:17:05,640 --> 00:17:10,900
My�l�, �e mo�e... to dow�d.
Mo�e powinni�my da� to szeryfowi.

180
00:17:11,900 --> 00:17:14,190
Nie.

181
00:17:15,030 --> 00:17:19,030
To znaczy... przeczyta�em go
od deski do deski.

182
00:17:19,200 --> 00:17:23,000
Nie znajdziemy w nim rozwi�zania.

183
00:17:24,410 --> 00:17:27,750
Poza tym, da�a go mnie.

184
00:17:28,630 --> 00:17:31,250
Dlaczego?

185
00:17:31,750 --> 00:17:35,760
Ludzie do mnie przychodz�
i opowiadaj� mi o swoim �yciu,

186
00:17:35,920 --> 00:17:38,890
i o �wiecie na zewn�trz.

187
00:17:39,050 --> 00:17:45,560
Umieszczam te historie
w szerszym kontek�cie.

188
00:17:46,560 --> 00:17:49,310
Taka �yj�ca powie��.

189
00:17:49,480 --> 00:17:53,230
- Jacy ludzie?
- Przyjaciele...

190
00:17:53,650 --> 00:17:56,320
kochanki...

191
00:17:58,860 --> 00:18:02,990
Mo�e... ty te� kiedy� co� mi opowiesz?

192
00:18:10,830 --> 00:18:13,170
Mia� pan racj�.

193
00:18:13,210 --> 00:18:16,170
Porywacze odezwali si� dzi� rano.

194
00:18:16,420 --> 00:18:20,010
Mia� pan ca�kowit� racj�.

195
00:18:20,220 --> 00:18:22,100
Dlaczego nie powiadomi� pan szeryfa?

196
00:18:22,220 --> 00:18:25,430
Zabij� j�, je�li pojawi si� policja.
Dlatego wezwa�em pana.

197
00:18:25,520 --> 00:18:27,890
Nie ma powodu rezygnowa�
ze zwyk�ego post�powania.

198
00:18:27,940 --> 00:18:31,440
Zwyk�e post�powanie
mo�e kosztowa� moj� c�rk� �ycie.

199
00:18:35,150 --> 00:18:38,360
��czy pana z Audrey...

200
00:18:40,450 --> 00:18:43,450
...szczeg�lna wi�.

201
00:18:43,740 --> 00:18:48,080
Pan jeden rozumie,
ile ona dla mnie znaczy.

202
00:18:50,500 --> 00:18:54,000
Wymiany chc� dokona� jutro.

203
00:18:54,880 --> 00:18:59,380
Audrey za 125 000 dolar�w...

204
00:18:59,930 --> 00:19:02,590
...got�wk�.

205
00:19:03,680 --> 00:19:05,560
Pytam wi�c...

206
00:19:09,940 --> 00:19:12,270
...dostarczy je pan?

207
00:19:21,490 --> 00:19:24,830
- Josie!
- Och, Pete...

208
00:19:25,530 --> 00:19:28,000
Tak mi przykro.

209
00:19:28,750 --> 00:19:31,000
Tak, mnie te�.

210
00:19:34,880 --> 00:19:39,630
Wci�� my�l� o Andrew.
Jak post�pi�by w tym wypadku.

211
00:19:41,340 --> 00:19:45,720
Na szcz�cie Catherine
wszystkim si� zaj�a.

212
00:19:47,600 --> 00:19:50,350
O co chodzi?

213
00:19:52,600 --> 00:19:55,690
Catherine zgin�a w po�arze.

214
00:19:56,360 --> 00:19:58,480
Bo�e, nie...

215
00:20:12,160 --> 00:20:15,710
Planujemy pogrzeb...

216
00:20:15,960 --> 00:20:18,630
...ale nie znale�li jeszcze cia�a.

217
00:20:20,380 --> 00:20:23,680
Zrobimy to za par� dni.

218
00:20:24,470 --> 00:20:28,260
Tylko nie wiem, co pochowamy.

219
00:20:37,900 --> 00:20:42,610
T�dy, �pi�ca kr�lewno, tatu� czeka.

220
00:20:45,530 --> 00:20:50,330
Panna Horne by�a bardzo niegrzeczna.
Nie chce przyj�� lekarstwa.

221
00:20:50,370 --> 00:20:51,370
Prosz�...

222
00:20:51,410 --> 00:20:55,420
Prawda, panno Horne?

223
00:20:59,040 --> 00:21:02,260
Czy traktowano ci� �le?

224
00:21:04,590 --> 00:21:07,340
Uderzy� mnie.

225
00:21:08,180 --> 00:21:12,310
Nieprawda... to nie tak, jak my�lisz...

226
00:21:13,600 --> 00:21:15,520
Niedobrze.

227
00:21:16,190 --> 00:21:20,190
To si� wi�cej nie powt�rzy,
dop�ki jestem przy tobie.

228
00:21:20,570 --> 00:21:23,240
Rozumiesz?

229
00:21:25,570 --> 00:21:30,620
Rozmawia�em dzi� z twoim ojcem.
Nic ci nie grozi.

230
00:21:31,870 --> 00:21:36,920
Wszystko b�dzie dobrze.
Ufasz mi, prawda Audrey?

231
00:21:38,960 --> 00:21:42,960
Widzisz, Audrey...
w podobnych sytuacjach...

232
00:21:43,880 --> 00:21:47,680
nale�y post�powa� profesjonalnie.

233
00:21:48,140 --> 00:21:50,350
Jeste�my rozs�dni.

234
00:21:50,550 --> 00:21:55,810
I przekonasz si�, �e zawsze uda si�
wymy�li� co� rozs�dnego,

235
00:21:55,850 --> 00:22:00,310
kiedy rozs�dni ludzie
zechc� si� przy�o�y� do...

236
00:22:38,940 --> 00:22:43,440
Lucy, musimy porozmawia�.

237
00:22:43,520 --> 00:22:51,570
Funkcjonariuszu Brennan, my�la�am,
�e woli pan towarzystwo �wierszczyk�w!

238
00:22:52,700 --> 00:22:57,040
Andy, zaczerpnij �wie�ego powietrza.

239
00:23:02,290 --> 00:23:07,090
Lucy, nadesz�a pora, �eby�
dla dobra nas wszystkich,

240
00:23:07,130 --> 00:23:10,880
powiedzia�a wreszcie, co ci� gn�bi.

241
00:23:14,010 --> 00:23:17,680
Andy i ja chodzili�my ze sob�.

242
00:23:17,720 --> 00:23:19,980
Zauwa�y�em.

243
00:23:20,020 --> 00:23:25,360
Po p�tora roku zacz�y
do mnie dociera� pewne rzeczy.

244
00:23:25,400 --> 00:23:29,320
Niezbyt wa�ne, w�a�ciwie drobiazgi.

245
00:23:33,570 --> 00:23:40,540
Nigdy nie �wiczy, nie myje samochodu
i nawet nie ma sportowej marynarki!

246
00:23:40,580 --> 00:23:44,000
Pr�bowa�a� temu zaradzi�?

247
00:23:44,040 --> 00:23:46,710
Obejrza�am jeden program w telewizji

248
00:23:46,750 --> 00:23:50,590
i uzna�am, �e potrzebuj�
czasu dla siebie,

249
00:23:50,630 --> 00:23:55,470
Wtedy pozna�am Dicka Tremayne'a.

250
00:23:55,510 --> 00:24:00,270
Z dzia�u odzie�y m�skiej
w sklepie Horne'a.

251
00:24:00,730 --> 00:24:06,610
Ma wiele marynarek, utrzymuje siebie
i sw�j samoch�d w doskona�ej formie.

252
00:24:08,020 --> 00:24:12,030
Mo�e zachowywa� si� jak osio�, ale...

253
00:24:13,240 --> 00:24:16,200
przynajmniej by� inny.

254
00:24:16,370 --> 00:24:20,370
Nadal widujesz si� z tym... Dickiem?

255
00:24:23,670 --> 00:24:25,960
Nie.

256
00:24:27,460 --> 00:24:31,670
Chcesz wr�ci� do Andy'ego?

257
00:24:34,090 --> 00:24:36,010
Nie wiem.

258
00:24:36,180 --> 00:24:40,680
Czy mo�esz mi powiedzie�, czego chcesz?

259
00:24:41,390 --> 00:24:44,020
Nie wiem!

260
00:24:48,690 --> 00:24:51,360
Przykro mi, Harry. Chcia�em pom�c.

261
00:24:51,400 --> 00:24:53,570
Mi�o, �e pr�bowa�e�.

262
00:24:53,610 --> 00:24:58,070
Ale to jak wyr�wnywa� wyboje
na autostradzie nr 9.

263
00:24:58,120 --> 00:25:01,080
Pierwszy wi�kszy deszcz i...

264
00:25:01,240 --> 00:25:03,960
Ju� to przerabia�e�?

265
00:25:04,000 --> 00:25:05,420
I owszem.

266
00:25:11,380 --> 00:25:13,720
Znalaz�em si� w trudnej sytuacji.

267
00:25:13,760 --> 00:25:18,930
Przemy�la�em wszystkie aspekty
i mam tylko jedno wyj�cie.

268
00:25:20,010 --> 00:25:22,970
Prosz� o przys�ug�. Mi�dzy nami.

269
00:25:23,140 --> 00:25:25,060
S�ucham.

270
00:25:25,230 --> 00:25:29,230
Daj mi jednego ch�opca z Czytelni.
Najlepszego.

271
00:25:29,400 --> 00:25:33,190
Lepiej, �eby� nie wiedzia� po co.

272
00:25:34,610 --> 00:25:37,700
Ty decydujesz.

273
00:25:37,740 --> 00:25:40,410
Czy powinienem o czym� wiedzie�?

274
00:25:40,450 --> 00:25:42,790
Zdecydowanie nie.

275
00:25:42,950 --> 00:25:46,750
Za�atwi� to.
O 21:30 w Roadhouse.

276
00:25:47,120 --> 00:25:50,040
Dzi�kuj�, Harry.

277
00:26:17,780 --> 00:26:21,450
- To musi by� on.
- Do dzie�a.

278
00:26:22,490 --> 00:26:25,290
Dobry wiecz�r panu.
Witamy w "RR".

279
00:26:25,330 --> 00:26:29,500
- Dzi�kuj�, mog� usi���?
- Oczywi�cie, prosz� t�dy.

280
00:26:37,220 --> 00:26:39,130
Mamy dzi� dwa pyszne dania dnia...

281
00:26:39,300 --> 00:26:45,560
Nie w�tpi�, ale poprosz�
cheeseburgera, col� i frytki.

282
00:26:46,600 --> 00:26:49,270
Oczywi�cie.

283
00:26:51,810 --> 00:26:55,780
Przepraszam, Toad, doko�cz w kuchni.

284
00:26:58,700 --> 00:27:00,740
Tylko niczego nie dotykaj.

285
00:27:01,530 --> 00:27:05,160
- Chcia� tylko cheeseburgera.
- To musi by� jaki� wybieg.

286
00:27:05,200 --> 00:27:07,950
- Tak uwa�asz?
- Zdecydowanie. Trzeba...

287
00:27:15,090 --> 00:27:18,050
- �azienka?
- T�dy, prosz� pana.

288
00:27:18,470 --> 00:27:23,550
Niedawno j� odnowili�my. �adnie wysz�o.
Bardzo dyskretna.

289
00:27:23,640 --> 00:27:25,560
Prosz� za mn�. Przepraszam.

290
00:27:26,140 --> 00:27:29,180
Pyszno�ci...

291
00:27:29,230 --> 00:27:33,610
Toad! Zostaw to!

292
00:27:54,540 --> 00:27:57,500
Dzi�kuj�, �e przysz�a�.

293
00:27:57,800 --> 00:28:01,380
- Nie wiedzia�am...
- Czego?

294
00:28:02,050 --> 00:28:05,010
Nie wiedzia�am, czy jeste� z�a.

295
00:28:05,180 --> 00:28:07,810
I jak bardzo.

296
00:28:07,850 --> 00:28:09,140
Prze�yj�.

297
00:28:11,560 --> 00:28:14,230
Mi�dzy mn�, a Jamesem nic nie ma.

298
00:28:14,270 --> 00:28:18,690
A nawet je�li, to kto powiedzia�,
�e nie mo�emy si� z nikim spotyka�?

299
00:28:18,730 --> 00:28:21,650
Na pewno nie ja.

300
00:28:22,900 --> 00:28:27,530
Chcesz powiedzie�,
�e widujesz si� z kim� innym?

301
00:28:29,660 --> 00:28:31,330
Potrzebuj� twojej pomocy.

302
00:28:34,870 --> 00:28:38,550
- Jakiej?
- Harold Smith ma pami�tnik Laury.

303
00:28:38,590 --> 00:28:42,050
My�la�am, �e ma go szeryf.

304
00:28:42,760 --> 00:28:47,930
Harold twierdzi, �e ma drugi,
o kt�rym nie wiedzia�am.

305
00:28:47,970 --> 00:28:50,430
Bo�e...

306
00:28:51,100 --> 00:28:54,600
Trzeba powiedzie� Jamesowi.

307
00:28:55,270 --> 00:29:00,230
Je�li Laura pisa�a sekretny pami�tnik
i Harold go ma...

308
00:29:01,530 --> 00:29:05,740
...zdob�d� go, z tob�, czy bez ciebie.

309
00:29:07,780 --> 00:29:10,910
Przeka� to Jamesowi.

310
00:29:19,880 --> 00:29:23,670
PROKURATOR STANOWY
DARYL LODWICK

311
00:29:45,240 --> 00:29:49,870
Wiem, �e przep�aci�am,
ale nie mog�am si� oprze�.

312
00:29:52,120 --> 00:29:56,120
W sklepie m�wili,
�e jeszcze dzia�aj� tylko dlatego,

313
00:29:56,410 --> 00:30:00,130
�e Josie Packard u nich kupuje.

314
00:30:02,670 --> 00:30:06,630
- Podoba mi si�.
- Naprawd�?

315
00:30:09,970 --> 00:30:13,970
- Jeste� taki zimny...
- Josie... musz� wiedzie�.

316
00:30:14,140 --> 00:30:17,100
- Naprawd� by�a� w Seattle?
- Tak.

317
00:30:17,270 --> 00:30:19,940
Sk�d ta nieufno��?

318
00:30:19,980 --> 00:30:21,980
Sp�jrz na te wszystkie pud�a.

319
00:30:22,020 --> 00:30:24,650
My�lisz, �e kupi�am to w hurtowni?

320
00:30:24,690 --> 00:30:26,950
Dlaczego nie uprzedzi�a�,
�e wyje�d�asz?

321
00:30:26,990 --> 00:30:29,740
Wiesz, �e musia�am uciec.
Ba�am si�.

322
00:30:29,780 --> 00:30:32,280
Wiem... Bena i Catherine.

323
00:30:32,330 --> 00:30:36,330
- Przecie� wiesz.
- A teraz Catherine nie �yje.

324
00:30:36,700 --> 00:30:40,960
Harry... chyba nie my�lisz...

325
00:30:41,000 --> 00:30:43,290
Sam ju� nie wiem, co my�le�.

326
00:30:43,340 --> 00:30:49,180
My�lisz, �e mog�abym podpali� tartak?
�e zniszczy�abym wszystko, co mam?

327
00:30:49,220 --> 00:30:52,180
A odszkodowanie?

328
00:30:52,470 --> 00:30:56,930
Bo�e... jak mo�esz?

329
00:30:58,690 --> 00:31:03,020
Jak mo�esz... tak �le o mnie my�le�?

330
00:31:03,440 --> 00:31:06,990
Jak mo�esz tak mnie rani�?

331
00:31:20,920 --> 00:31:22,830
Po prostu martwi�em si�.

332
00:31:25,250 --> 00:31:27,710
Wybacz.

333
00:31:27,960 --> 00:31:31,680
Przytul mnie...

334
00:31:33,140 --> 00:31:37,770
We� mnie... chc�, �eby� mnie wzi��.

335
00:31:38,640 --> 00:31:43,230
Josie, dlaczego?
Dlaczego mi nie powiedzia�a�?

336
00:31:50,400 --> 00:31:53,870
- Rozedrzyj to.
- Co?

337
00:31:54,240 --> 00:31:58,080
Rozedrzyj to.

338
00:32:09,050 --> 00:32:12,130
Kocham ci�.

339
00:32:13,590 --> 00:32:16,930
Potrzebuj� ci�.

340
00:32:23,310 --> 00:32:27,860
Pragn� ci� ponad �ycie.

341
00:33:03,140 --> 00:33:04,310
Witaj, �licznotko!

342
00:33:04,350 --> 00:33:07,900
Komu trzeba si� podliza�,
�eby dosta� troch� kawy?

343
00:33:07,940 --> 00:33:11,440
S�dzia Sternwood!
Jak si� pan miewa?

344
00:33:11,480 --> 00:33:17,120
Lucy, jeste� �ykiem �wie�ej wody
dla znu�onych oczu.

345
00:33:17,160 --> 00:33:19,780
Co ci� martwi?

346
00:33:19,830 --> 00:33:23,290
Wola�abym nie m�wi�.

347
00:33:25,040 --> 00:33:27,960
U�ciskasz mnie?

348
00:33:32,340 --> 00:33:35,340
�ycie jest ci�kie, skarbie.

349
00:33:35,510 --> 00:33:40,140
Ale na �wiecie jest gorzej
ni� tu, w Twin Peaks.

350
00:33:40,720 --> 00:33:44,680
Clinton! Przepraszam za sp�nienie.

351
00:33:45,940 --> 00:33:50,440
- Dopiero przyjecha�em.
- Dobrze pana widzie�.

352
00:33:52,190 --> 00:33:55,150
Jak jej na imi�, Harry?

353
00:33:56,110 --> 00:34:00,370
Wygl�dasz, jakby� mia�
k�opoty z klaczk�.

354
00:34:00,530 --> 00:34:03,490
Je�li nie da si� ujarzmi�,
masz dwa wyj�cia.

355
00:34:03,660 --> 00:34:07,670
- Ale nie b�d� ci� tym zanudza�.
- Harry?

356
00:34:07,830 --> 00:34:11,840
S�dzia Sternwood.
Agent Dale Cooper, FBI.

357
00:34:12,000 --> 00:34:14,960
- Jak si� masz, m�odzie�cze?
- Panie s�dzio.

358
00:34:15,130 --> 00:34:18,090
Macie wiele wsp�lnego.

359
00:34:18,260 --> 00:34:22,260
Sid przyniesie kalendarz,
kiedy tylko zatankuje w�z.

360
00:34:22,430 --> 00:34:26,430
Odkorkujesz t� butelczyn� irlandzkiej,
kt�r� dla mnie chowasz?

361
00:34:26,600 --> 00:34:28,520
Prosz� t�dy.

362
00:34:28,690 --> 00:34:32,690
Panie Cooper, jak pan
znajduje ten zak�tek?

363
00:34:33,230 --> 00:34:35,150
To niebo, prosz� pana.

364
00:34:35,740 --> 00:34:40,110
W ostatnim tygodniu w tym niebie
mieli�my: podpalenie,

365
00:34:40,160 --> 00:34:44,950
kilka zab�jstw
i zamach na �ycie agenta federalnego.

366
00:34:45,370 --> 00:34:50,040
Niebo to obszerne
i ciekawe miejsce, prosz� pana.

367
00:34:59,010 --> 00:35:03,050
Lucy, nie mog� spa�.
Nie mog� je��.

368
00:35:03,220 --> 00:35:06,600
Jestem w strasznym stanie.

369
00:35:07,890 --> 00:35:09,980
Tak?

370
00:35:10,020 --> 00:35:13,650
By�em g�upcem, chamem,
�e si� tak zachowa�em.

371
00:35:13,730 --> 00:35:17,110
Jest mi bardzo, bardzo wstyd.

372
00:35:18,860 --> 00:35:19,780
Tak?

373
00:35:19,990 --> 00:35:24,830
D�ugo si� zastanawia�em
i oto, co wymy�li�em.

374
00:35:25,030 --> 00:35:29,660
Powinienem... musz� zrobi� to,
co do mnie nale�y.

375
00:35:32,330 --> 00:35:35,000
To znaczy?

376
00:35:36,500 --> 00:35:41,930
To wszystko, co uda�o mi si� wyskroba�.
Jestem sp�ukany.

377
00:35:49,350 --> 00:35:52,060
Co to jest?

378
00:35:52,150 --> 00:35:54,860
650 dolar�w.

379
00:35:55,270 --> 00:35:59,400
Dowiadywa�em si�.
To wi�cej ni� trzeba.

380
00:36:01,530 --> 00:36:03,490
Na co?

381
00:36:03,910 --> 00:36:06,990
�eby... za�atwi�...

382
00:36:08,870 --> 00:36:12,870
No wiesz, tw�j drobny problem.

383
00:36:14,080 --> 00:36:17,920
Richardzie, pos�uchaj, co zrobisz.

384
00:36:19,300 --> 00:36:25,390
We�miesz swoje pieni�dze,
w�o�ysz je z powrotem do kieszeni.

385
00:36:25,550 --> 00:36:29,020
Odwr�cisz si� i przejdziesz
przez podw�jne drzwi

386
00:36:29,060 --> 00:36:31,060
te drugie czasem si� zacinaj�.

387
00:36:31,100 --> 00:36:34,440
P�jdziesz na parking,
wsi�dziesz do samochodu,

388
00:36:34,480 --> 00:36:36,980
przekr�cisz kluczyki i...

389
00:36:37,020 --> 00:36:42,070
NIGDY WI�CEJ, DO KO�CA �YCIA,
SI� DO MNIE NIE ODEZWIESZ!!

390
00:36:42,240 --> 00:36:46,160
Jeszcze s�owo, a zaczn� krzycze�!!

391
00:36:46,410 --> 00:36:49,200
Wyjd�, prosz�.

392
00:37:20,860 --> 00:37:24,650
Lelandzie, to trudne...

393
00:37:26,570 --> 00:37:33,330
Przyjmij najszczersze wyrazy wsp�czucia
z powodu twojej straszliwej straty.

394
00:37:33,370 --> 00:37:35,290
Dzi�kuj�, panie s�dzio.

395
00:37:35,460 --> 00:37:39,170
Stawa�e� przede mn� wiele razy.

396
00:37:40,040 --> 00:37:46,880
Uwa�am ci� za przyzwoitego cz�owieka
i zdolnego prawnika.

397
00:37:46,930 --> 00:37:53,020
Spotkanie w tych okoliczno�ciach
jest dla nas wszystkich ciosem.

398
00:37:53,390 --> 00:38:00,110
Prawo jest ostoj�, kt�ra pozwala
przetrwa� niebezpieczny czas pr�by.

399
00:38:00,520 --> 00:38:06,610
Jednak�e wymaga od nas poddania si�
jego wyrokom i wy�szym celom.

400
00:38:06,740 --> 00:38:13,870
Zanim wejdziemy w swoje role
w tym bolesnym dramacie,

401
00:38:14,040 --> 00:38:16,790
powiem tylko,

402
00:38:17,160 --> 00:38:22,210
�e gdy te w�t�e cienie,
kt�re teraz zamieszkujemy

403
00:38:22,380 --> 00:38:25,260
zejd� ze sceny,

404
00:38:25,510 --> 00:38:34,720
spotkamy si� znowu i razem
wzniesiemy toast w Walhalli.

405
00:38:39,400 --> 00:38:42,190
Niech tak si� stanie.

406
00:38:42,230 --> 00:38:47,400
Rozumiem, �e zamierzasz wyst�powa�
w swoim w�asnym imieniu.

407
00:38:48,490 --> 00:38:51,030
Tak jest.

408
00:38:51,620 --> 00:38:54,450
Gdzie prokurator Lodwick?

409
00:38:54,490 --> 00:38:55,620
Sp�nia si�.

410
00:38:55,660 --> 00:38:59,460
Od��my kwesti� kaucji do rana.

411
00:38:59,920 --> 00:39:02,880
Czy warunki s� zno�ne, Lelandzie?

412
00:39:03,040 --> 00:39:06,590
Tak. Wszyscy s� bardzo mili.

413
00:39:07,420 --> 00:39:10,380
Zatem mo�emy si� po�egna�.
Zast�pco.

414
00:39:10,760 --> 00:39:13,300
Dobranoc.

415
00:39:14,970 --> 00:39:17,520
Dzi�kuj�.

416
00:39:25,070 --> 00:39:30,200
Wyra�� odczucia nas wszystkich:
mamy trudne zawody.

417
00:39:37,750 --> 00:39:40,540
Mo�emy jecha�.

418
00:39:41,620 --> 00:39:43,540
Cze��, Sid.

419
00:39:45,420 --> 00:39:48,420
Sid, poznaj agenta specjalnego Coopera.

420
00:39:48,460 --> 00:39:51,430
- Agencie Cooper.
- Mi�o mi pani� pozna�.

421
00:39:54,260 --> 00:39:58,100
Sp�dzili�my osiem godzin w podr�y.

422
00:39:58,140 --> 00:40:02,100
Sid i ja udamy si� do Great Northern.

423
00:40:02,140 --> 00:40:06,770
Rozpakujemy si� i zjemy kolacj�.

424
00:40:06,820 --> 00:40:10,110
Do zobaczenia w s�dzie.

425
00:40:14,370 --> 00:40:16,780
�ona?

426
00:40:17,490 --> 00:40:20,290
Kancelistka.

427
00:40:20,330 --> 00:40:22,920
O rany!

428
00:40:25,040 --> 00:40:29,050
Czy wszystko gotowe na dzi� wiecz�r?

429
00:40:29,210 --> 00:40:29,960
Przyjdzie.

430
00:40:41,730 --> 00:40:42,940
To jest Katrina?

431
00:40:46,940 --> 00:40:50,480
A ty zostaniesz miss czego?

432
00:40:52,150 --> 00:40:55,450
Chod�cie, dziewczyny...

433
00:41:35,360 --> 00:41:38,200
Za�atwione, panie Tojamura.

434
00:41:38,240 --> 00:41:42,080
- Jak� kart� kredytow� pan p�aci?
- Wol� got�wk�.

435
00:41:42,120 --> 00:41:45,210
Got�wk�? Oczywi�cie.

436
00:41:45,250 --> 00:41:48,830
Sk�d pan przyjecha�?

437
00:41:48,880 --> 00:41:50,540
Z Seattle.

438
00:41:50,590 --> 00:41:53,880
Seattle? To niedaleko.

439
00:41:56,340 --> 00:42:01,310
�wietnie, panie Tojamura.
Boy zaprowadzi pana do apartamentu.

440
00:42:01,350 --> 00:42:05,350
Je�li zechce pan skosztowa�
specjalno�ci miejscowej kuchni,

441
00:42:06,230 --> 00:42:09,610
polecam restauracj� "RR".

442
00:42:10,400 --> 00:42:14,320
- Pyta� o Norm�.
- Dzi�kuj� bardzo.

443
00:42:22,950 --> 00:42:27,870
Norma? M�wi Louie.
My�l�, �e orze� wyl�dowa�.

444
00:42:31,210 --> 00:42:34,170
Pete, poznaj mojego
kuzyna Jonathana.

445
00:42:35,970 --> 00:42:38,550
Ch�tnie napi�by si� kawy.

446
00:42:38,590 --> 00:42:43,640
Pomo�e mu wyregulowa� po d�ugim locie
jego zegar biograficzny.

447
00:42:43,810 --> 00:42:46,520
Biologiczny.

448
00:42:46,930 --> 00:42:50,900
W�a�nie zaparzy�em kaw�, ju� podaj�.

449
00:42:51,110 --> 00:42:54,690
Biologiczny.

450
00:42:57,360 --> 00:43:01,990
Jak tu wytrzyma�a� sze�� lat?
W�r�d tych kmiotk�w i roboli.

451
00:43:02,030 --> 00:43:05,950
- Wszyscy mamy prac� do wykonania.
- Twoja zbli�a si� ku ko�cowi.

452
00:43:06,000 --> 00:43:08,540
Sprzedajesz tartak i ziemi� Packard�w?

453
00:43:08,580 --> 00:43:12,840
Nied�ugo. Potrzebny mi podpis Pete'a
jako spadkobiercy Catherine.

454
00:43:13,130 --> 00:43:14,800
- Spodziewasz si� trudno�ci?
- Nie.

455
00:43:15,210 --> 00:43:16,130
Ile to potrwa?

456
00:43:16,710 --> 00:43:17,760
Dwa dni.

457
00:43:17,800 --> 00:43:23,140
Czekaj� na nas w Hongkongu.
Pan Eckhardt pragnie ci� zobaczy�.

458
00:43:24,050 --> 00:43:27,810
B�d� jeszcze jakie� przeszkody?

459
00:43:29,270 --> 00:43:33,690
- Mo�e by� problem z Hankiem.
- Za�atwi� to.

460
00:43:35,020 --> 00:43:37,990
- Czy kto� nas podejrzewa?
- Nie.

461
00:43:38,070 --> 00:43:41,030
- A szeryf?
- Nic dla mnie nie znaczy.

462
00:43:41,820 --> 00:43:43,450
Nie o to pyta�em.

463
00:43:54,340 --> 00:43:57,420
21:30. Punktualnie.

464
00:43:58,510 --> 00:44:01,630
- Przyszed�?
- Tak.

465
00:44:11,020 --> 00:44:13,900
�pieszymy si�?

466
00:44:14,150 --> 00:44:17,900
Postawi� ci piwo.

467
00:44:22,490 --> 00:44:26,030
Ju� id�! Spokojnie!

468
00:44:27,700 --> 00:44:30,500
Ju� zamkni�...

469
00:44:53,810 --> 00:44:56,560
O co chodzi?

470
00:45:03,200 --> 00:45:06,240
Czy my si� znamy?

471
00:45:13,040 --> 00:45:15,960
Dobra...

472
00:45:36,560 --> 00:45:40,770
S�uchaj... pole�� tu sobie.

473
00:45:41,780 --> 00:45:44,320
Co jest?

474
00:45:54,290 --> 00:45:58,540
Bracie krwi, nast�pnym razem
rozwal� ci �eb.

475
00:46:07,680 --> 00:46:10,680
Tekst polski i opracowanie
Jakub "Qbin" Jedynak

476
00:46:10,720 --> 00:46:13,600
Redakcja: Kraps
<kraps(at)poczta.onet.pl>

477
00:46:13,640 --> 00:46:16,520
Polska strona o Davidzie Lynchu
www.lynchland.pl

478
00:46:18,520 --> 00:46:21,520
.:: Napisy24 - Nowy Wymiar Napis�w ::.
Napisy24.pl

