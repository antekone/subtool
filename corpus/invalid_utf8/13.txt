1
00:00:36,600 --> 00:00:46,700
{y:b}Wersja polska: BDiP.pl
(Dakota, melyana)

2
00:00:51,700 --> 00:01:01,700
{y:b}Premiera napisĂłw: 28.08.2017
Aktualizacja: brak

3
00:01:06,800 --> 00:01:16,800
{y:b}ZnajdĹş nas:
facebook.com/BDiP.pl

4
00:03:52,700 --> 00:03:55,000
PrzyjechaliĹ›my.

5
00:03:55,500 --> 00:04:01,500
â€“ Co teraz?
â€“ UwaĹĽaj, przekonasz siÄ™.

6
00:04:03,100 --> 00:04:05,000
Szukam pewnego miejsca.

7
00:04:07,300 --> 00:04:12,400
â€“ Rozumiesz?
â€“ â€žPewnego miejscaâ€ť?

8
00:04:16,600 --> 00:04:23,000
Trzy osoby daĹ‚y mi koordynaty.
Dwie siÄ™ pokrywajÄ….

9
00:04:26,000 --> 00:04:32,000
â€“ Co powinienem zrobiÄ‡?
â€“ SprawdziÄ‡ te, ktĂłre siÄ™ pokrywajÄ….

10
00:04:32,800 --> 00:04:35,300
Bystry chĹ‚opak.

11
00:04:35,900 --> 00:04:39,400
JesteĹ›my bardzo blisko tych,
ktĂłre siÄ™ pokrywajÄ….

12
00:04:40,400 --> 00:04:41,700
To dokĹ‚adnie tam.

13
00:04:44,500 --> 00:04:47,400
â€“ Idziemy tam?
â€“ Tak.

14
00:04:48,400 --> 00:04:50,400
I to teraz.

15
00:05:14,300 --> 00:05:16,500
Ludzie?

16
00:06:00,500 --> 00:06:02,600
Dobry BoĹĽe.

17
00:06:16,700 --> 00:06:22,000
â€“ To tutaj?
â€“ Pewnie na tej skale.

18
00:06:30,000 --> 00:06:35,100
Jestem od ciebie 25 lat starszy.
Bierz to i ruszaj.

19
00:06:36,900 --> 00:06:41,000
Kiedy bÄ™dziesz blisko, zabipczy.
Potem dĹşwiÄ™k bÄ™dzie ciÄ…gĹ‚y.

20
00:06:43,500 --> 00:06:45,600
Dasz znaÄ‡, co znalazĹ‚eĹ›.

21
00:08:13,900 --> 00:08:15,300
Jestem!

22
00:08:51,300 --> 00:08:53,400
Ĺ»egnaj, mĂłj synu.

23
00:09:03,600 --> 00:09:05,800
Niedobra lornetka!

24
00:09:05,900 --> 00:09:07,900
Niedobra!

25
00:09:08,000 --> 00:09:10,800
Niedobra, niedobra!

26
00:09:28,000 --> 00:09:29,600
:-) WSZYSTKO.

27
00:10:45,100 --> 00:10:50,900
â€“ SĹ‚yszaĹ‚aĹ› rano tÄ™ ptaszynÄ™?
â€“ SiÄ™ wie, kurwa.

28
00:11:11,800 --> 00:11:14,100
Kto to?

29
00:11:19,800 --> 00:11:21,800
Czego tu chcÄ…?

30
00:11:49,400 --> 00:11:55,500
â€“ Chyba nikogo nie ma.
â€“ Jak to wymĂłĹĽdĹĽyĹ‚eĹ›, Sherlocku?

31
00:11:57,600 --> 00:12:02,100
Wilson, kutasie, zaparkuj
gdzieĹ› z boku i obserwuj dom.

32
00:12:02,700 --> 00:12:03,700
PodejdĹş.

33
00:12:04,200 --> 00:12:09,800
Sprawdzimy tÄ™ ubezpieczalniÄ™.
Lucky 7 Insurance.

34
00:12:09,900 --> 00:12:12,600
Wilson, ruchy!

35
00:12:25,900 --> 00:12:28,800
KrzyĹĽyk na drogÄ™.

36
00:13:07,400 --> 00:13:09,500
DostaĹ‚em wiadomoĹ›Ä‡.

37
00:13:09,600 --> 00:13:14,790
Jest w stanie Ĺ›piÄ…czki,
ale funkcje ĹĽyciowe sÄ… silne.

38
00:13:14,800 --> 00:13:18,990
Ludzie pogrÄ…ĹĽeni w Ĺ›piÄ…czce
mogÄ… w niej zostaÄ‡ lata.

39
00:13:19,000 --> 00:13:21,200
Z Dougiem bÄ™dzie inaczej.

40
00:13:21,300 --> 00:13:24,300
Mamo, czy Ĺ›piÄ…czka
ma coĹ› wspĂłlnego z prÄ…dem?

41
00:13:24,400 --> 00:13:29,400
â€“ Nie, kochanie.
â€“ W tym przypadku ma.

42
00:13:39,400 --> 00:13:42,290
{y:i}â€“ MĂłwiĹ‚em, ĹĽe wezmÄ™.
â€“ Nie wiem, czy...

43
00:13:42,300 --> 00:13:44,400
Tutaj jest.

44
00:13:57,400 --> 00:14:01,400
Bushnell, przyjechaliĹ›my,
jak tylko siÄ™ dowiedzieliĹ›my.

45
00:14:01,700 --> 00:14:06,100
To bracia Mitchum, Janey-E.
Przyjaciele Dougieâ€™ego.

46
00:14:07,000 --> 00:14:11,800
Sonny Jim, to panowie,
ktĂłrzy kupili plac zabaw i auto.

47
00:14:11,900 --> 00:14:14,100
â€“ DziÄ™kujemy.
â€“ To drobiazg.

48
00:14:14,200 --> 00:14:17,890
KaĹĽdy dzieciak
powinien mieÄ‡ plac zabaw.

49
00:14:17,900 --> 00:14:20,300
MaĹ‚y siĹ‚acz.

50
00:14:20,600 --> 00:14:26,500
WidywaliĹ›my takie sytuacje.
CzĹ‚owiek nie ma ochoty gotowaÄ‡.

51
00:14:26,600 --> 00:14:32,500
Szpitalne ĹĽarcie nie jest dobre,
a musisz coĹ› jeĹ›Ä‡, Jimmy.

52
00:14:32,600 --> 00:14:37,600
â€“ To tak zwane koreczki.
â€“ CzÄ™stuj siÄ™.

53
00:14:38,000 --> 00:14:41,700
ZĹ‚ap jak koreczek.
Zasadnie tak siÄ™ nazywa.

54
00:14:41,800 --> 00:14:45,490
â€“ Dziewczyny, skĹ‚adany stolik.
â€“ MoĹĽe zjemy na zewnÄ…trz?

55
00:14:45,500 --> 00:14:48,800
Nie zabawimy dĹ‚ugo.
WziÄ™liĹ›my to tylko dla was.

56
00:14:48,900 --> 00:14:53,600
WyĹ‚Ä…cznie chcieliĹ›my pomĂłc
i zobaczyÄ‡ Dougieâ€™ego.

57
00:14:56,500 --> 00:15:00,900
BiorÄ…c pod uwagÄ™ okolicznoĹ›ci,
nie wyglÄ…da najgorzej.

58
00:15:01,200 --> 00:15:04,590
Lekarze robiÄ…, co mogÄ….

59
00:15:04,600 --> 00:15:11,300
MogÄ™ prosiÄ‡ o klucze?
Do domu teĹĽ coĹ› przywieziemy.

60
00:15:19,300 --> 00:15:21,600
DziÄ™kujÄ™.

61
00:15:23,600 --> 00:15:28,000
Co byĹ‚o powodem?

62
00:15:28,800 --> 00:15:30,300
PrÄ…d?

63
00:16:25,900 --> 00:16:27,000
Mamo?

64
00:16:27,600 --> 00:16:33,800
â€“ MuszÄ™ siku.
â€“ Poszukajmy toalety.

65
00:17:00,600 --> 00:17:03,300
â€“ SĹ‚ucham?
{y:i}â€“ MĂłwi Phil Bisby.

66
00:17:03,400 --> 00:17:05,100
{y:i}FBI szuka Dougieâ€™ego.

67
00:17:05,200 --> 00:17:06,800
â€“ Co?
{y:i}â€“ FBI.

68
00:17:07,300 --> 00:17:09,800
â€“ FBI?
{y:i}â€“ SzukajÄ… Dougieâ€™ego.

69
00:17:09,900 --> 00:17:12,790
â€“ Dougieâ€™ego?
{y:i}â€“ No.

70
00:17:12,800 --> 00:17:15,900
A co takiego mĂłgĹ‚ zrobiÄ‡?
Jest w Ĺ›piÄ…czce.

71
00:17:16,000 --> 00:17:19,300
â€“ MĂłwiĹ‚eĹ›, ĹĽe tu jesteĹ›my?
{y:i}â€“ Tak, od razu wyszli.

72
00:17:19,400 --> 00:17:21,700
â€“ NaprawdÄ™?
{y:i}â€“ No.

73
00:17:21,800 --> 00:17:25,600
â€“ Kiedy wyszli?
{y:i}â€“ JakieĹ› 10 minut temu.

74
00:17:26,800 --> 00:17:28,100
W porzÄ…dku.

75
00:18:42,000 --> 00:18:44,900
To sobie poczekamy.

76
00:18:48,900 --> 00:18:52,600
â€“ PamiÄ™tasz Sammyâ€™ego?
â€“ No.

77
00:18:52,800 --> 00:18:55,000
UmarĹ‚.

78
00:18:57,300 --> 00:19:02,100
ByĹ‚ porzÄ…dnym czĹ‚owiekiem.
WisiaĹ‚em mu sianko.

79
00:19:05,100 --> 00:19:06,700
DrÄ™czy ciÄ™ to?

80
00:19:31,300 --> 00:19:34,000
A teraz co?

81
00:19:41,600 --> 00:19:43,200
Jest tam Dougie?

82
00:19:44,700 --> 00:19:46,090
Douglas?

83
00:19:46,100 --> 00:19:48,890
A ktĂłreĹ› z nich przypomina
naszego szefa?

84
00:19:48,900 --> 00:19:52,600
â€“ Nie ma tam Dougieâ€™ego, gĹ‚Ä…bie.
â€“ MoĹĽesz siÄ™ uspokoiÄ‡?

85
00:19:52,700 --> 00:19:58,000
To ostatnia paczka, Hutch.
Ostatnia, w dupÄ™ maÄ‡, paczka.

86
00:19:59,000 --> 00:20:02,600
â€“ Masz okres?
â€“ A jeĹ›li nawet, kurwa?

87
00:20:04,500 --> 00:20:05,500
Spoko.

88
00:20:13,400 --> 00:20:16,200
JakiĹ› zajebany cyrk.

89
00:20:23,400 --> 00:20:27,800
Limuzyna, dziewczyny w rĂłĹĽu.

90
00:20:29,500 --> 00:20:31,400
Brak Douglasa Jonesa.

91
00:20:57,000 --> 00:20:59,000
Co, do chuja?

92
00:21:12,100 --> 00:21:14,900
Stoicie na moim podjeĹşdzie.

93
00:21:18,200 --> 00:21:22,100
â€“ Nie stoimy na twoim podjeĹşdzie.
â€“ Nawet w pobliĹĽu, dupku.

94
00:21:24,000 --> 00:21:27,500
Spierdalaj liĹ›cie liczyÄ‡!

95
00:21:32,000 --> 00:21:33,000
PrzesunÄ™ wĂłz.

96
00:21:59,200 --> 00:22:01,700
Jebaka!

97
00:22:11,800 --> 00:22:15,300
â€“ Co to ma, kurwa, byÄ‡?
â€“ Ten dupek mnie wkurwia.

98
00:22:16,300 --> 00:22:18,500
â€“ Chantal!
â€“ TrafiĹ‚ mnie.

99
00:22:18,800 --> 00:22:20,500
To nam wszystko zjebie.

100
00:22:24,500 --> 00:22:25,600
Spieprzajmy stÄ…d.

101
00:23:20,600 --> 00:23:23,900
Co to, kurwa, za okolica?

102
00:23:25,400 --> 00:23:28,100
Ludzie majÄ… wiele napiÄ™Ä‡.

103
00:23:41,000 --> 00:23:44,500
FBI, odĹ‚ĂłĹĽ broĹ„!

104
00:23:47,600 --> 00:23:50,000
PowiedziaĹ‚em: odĹ‚ĂłĹĽ!

105
00:23:53,100 --> 00:23:55,390
Teraz powoli od niej odejdĹş.

106
00:23:55,400 --> 00:24:00,400
Schowaj pistolet.
Bierzmy dziewczyny i spadamy.

107
00:24:08,400 --> 00:24:11,700
PotrzebujÄ™ posiĹ‚kĂłw i karetki
przy Lancelot Court.

108
00:24:12,500 --> 00:24:13,900
Lancelot Court!

109
00:25:23,100 --> 00:25:26,800
ObudziĹ‚eĹ› siÄ™.

110
00:25:28,200 --> 00:25:29,700
W stu procentach.

111
00:25:30,500 --> 00:25:32,500
Nareszcie.

112
00:25:35,900 --> 00:25:38,600
Ten drugi...

113
00:25:39,900 --> 00:25:45,700
nie wrĂłciĹ‚.

114
00:25:48,000 --> 00:25:50,700
CaĹ‚y czas tam jest.

115
00:25:54,100 --> 00:25:56,100
WeĹş to.

116
00:26:11,200 --> 00:26:14,400
Czy masz nasiono?

117
00:26:30,000 --> 00:26:31,800
ZrĂłb mi jeszcze jedno.

118
00:26:33,800 --> 00:26:37,100
Rozumiem.

119
00:27:00,000 --> 00:27:02,700
â€“ Dougie?
â€“ Tato?

120
00:27:06,100 --> 00:27:07,400
CzeĹ›Ä‡, Sonny Jim.

121
00:27:08,300 --> 00:27:13,100
â€“ O mĂłj... Dougie.
â€“ CzeĹ›Ä‡, Janey-E.

122
00:27:15,500 --> 00:27:18,400
ObudziĹ‚ siÄ™, wiedziaĹ‚em!

123
00:27:19,200 --> 00:27:22,190
â€“ MogĹ‚abyĹ› pĂłjĹ›Ä‡ po lekarza?
â€“ Tak.

124
00:27:22,200 --> 00:27:25,300
â€“ MoĹĽe pĂłjdziesz z mamÄ…?
â€“ W porzÄ…dku.

125
00:27:27,200 --> 00:27:29,500
Kanapki, umieram z gĹ‚odu.

126
00:27:31,000 --> 00:27:34,200
Dzwonili z biura.
Podobno FBI ciÄ™ tam szukaĹ‚o.

127
00:27:36,000 --> 00:27:36,700
Ĺšwietnie.

128
00:27:37,700 --> 00:27:39,900
CaĹ‚kiem nieĹşle to przeĹĽyĹ‚eĹ›.

129
00:27:42,400 --> 00:27:45,600
â€“ Co pan wyprawia?
â€“ Nie potrzebujÄ™ juĹĽ tej kroplĂłwki.

130
00:27:45,700 --> 00:27:48,500
Czy moĹĽe pani potwierdziÄ‡,
ĹĽe mĂłj stan jest dobry?

131
00:27:48,800 --> 00:27:50,200
WychodzÄ™ stÄ…d.

132
00:27:50,600 --> 00:27:53,700
Bushnell, poproszÄ™ moje ubrania.
SÄ… w szafce za tobÄ….

133
00:27:53,800 --> 00:27:57,500
â€“ Czy to na pewno dobry pomysĹ‚?
â€“ To dobry pomysĹ‚.

134
00:27:58,800 --> 00:28:03,500
To jest caĹ‚kiem dobry pomysĹ‚.
PrzygotujÄ™ pana wypis.

135
00:28:03,800 --> 00:28:07,700
Janey-E, podjedĹş autem.
UbiorÄ™ siÄ™ i spotkamy siÄ™ na dole.

136
00:28:09,500 --> 00:28:13,700
ChodĹşmy, Sonny Jim.
Tata prosi, ĹĽebyĹ›my podjechali.

137
00:28:15,900 --> 00:28:17,000
DziÄ™ki, Bushnell.

138
00:28:27,400 --> 00:28:32,100
â€“ Tata siÄ™ rozgadaĹ‚.
â€“ Tak, bardzo.

139
00:28:35,000 --> 00:28:40,000
MuszÄ™ poĹĽyczyÄ‡ twĂłj rewolwer,
ktĂłry nosisz pod lewÄ… pachÄ….

140
00:28:40,800 --> 00:28:42,400
Jasne, Dougie.

141
00:28:47,600 --> 00:28:51,090
Wszystko w porzÄ…dku?
MogÄ™ ci w jakiĹ› sposĂłb pomĂłc?

142
00:28:51,100 --> 00:28:54,600
â€“ ZadzwoĹ„ do braci Mitchum.
â€“ OczywiĹ›cie.

143
00:28:55,800 --> 00:29:00,100
Dali mi swĂłj prywatny numer,
mam go na szybkim wybieraniu.

144
00:29:02,700 --> 00:29:04,000
Halo?

145
00:29:04,200 --> 00:29:05,500
Czy to Brad?

146
00:29:06,500 --> 00:29:11,100
A, Rod, czekaj.
Dougie chce z tobÄ… pogadaÄ‡.

147
00:29:12,400 --> 00:29:15,300
Za 20 minut bÄ™dÄ™ w kasynie
z mojÄ… rodzinÄ….

148
00:29:15,400 --> 00:29:17,900
{y:i}â€“ Spotkamy siÄ™ w holu.
â€“ Jak sobie ĹĽyczysz.

149
00:29:18,000 --> 00:29:21,500
â€“ PotrzebujÄ™ samolotu do Spokane.
â€“ {y:i}Tankujemy samolot.

150
00:29:21,600 --> 00:29:23,000
WĹ‚aĹ›nie go tankujemy.

151
00:29:23,200 --> 00:29:25,200
Lecimy do Spokane.

152
00:29:25,800 --> 00:29:30,190
{y:i}â€“ Za 20 minut w holu.
â€“ Masz to jak w banku.

153
00:29:30,200 --> 00:29:32,690
Tak, tankujcie, lecimy do Spokane.

154
00:29:32,700 --> 00:29:38,100
â€“ Spotykamy siÄ™ z nim w holu.
â€“ Dziewczyny, lecimy.

155
00:29:39,200 --> 00:29:40,800
Ciekawe, co kombinuje.

156
00:29:42,300 --> 00:29:43,300
ChodĹşmy!

157
00:29:45,200 --> 00:29:48,800
Mam przeczucie,
ĹĽe zadzwoni niejaki Gordon Cole.

158
00:29:49,000 --> 00:29:51,900
Przeczytaj mu tÄ™ wiadomoĹ›Ä‡.

159
00:29:59,500 --> 00:30:01,600
Dobry z ciebie czĹ‚owiek.

160
00:30:01,700 --> 00:30:05,100
NieprÄ™dko zapomnÄ™
o twojej dobroci i przyzwoitoĹ›ci.

161
00:30:06,800 --> 00:30:07,600
A co z FBI?

162
00:30:09,400 --> 00:30:11,200
Ja jestem z FBI.

163
00:30:16,900 --> 00:30:18,700
PrzesuĹ„ siÄ™, ja prowadzÄ™.

164
00:30:18,800 --> 00:30:21,800
â€“ Ale, Dougie...
â€“ Janey-E, w porzÄ…dku.

165
00:30:25,400 --> 00:30:28,600
â€“ Co siÄ™ dzieje, Dougie?
â€“ Zapnij pasy.

166
00:31:06,200 --> 00:31:09,400
Janey-E, jak dojechaÄ‡
do kasyna Silver Mustang?

167
00:31:09,500 --> 00:31:13,600
â€“ Znowu bÄ™dziesz graÄ‡?
â€“ Spotkamy siÄ™ z braÄ‡mi Mitchum.

168
00:31:23,200 --> 00:31:26,300
Tata bardzo dobrze jeĹşdzi.

169
00:31:48,900 --> 00:31:51,000
:-) WSZYSTKO.

170
00:32:37,300 --> 00:32:38,800
JuĹĽ pamiÄ™tam.

171
00:32:42,000 --> 00:32:43,600
Coop.

172
00:32:44,500 --> 00:32:46,300
JuĹĽ pamiÄ™tam.

173
00:32:58,200 --> 00:33:00,300
Oby to zadziaĹ‚aĹ‚o.

174
00:35:09,800 --> 00:35:11,700
WejdĹş, Diane.

175
00:35:35,300 --> 00:35:38,100
PytaĹ‚eĹ› o noc,
kiedy Cooper mnie odwiedziĹ‚.

176
00:35:40,900 --> 00:35:42,300
Opowiem ci o niej.

177
00:35:43,000 --> 00:35:46,300
â€“ MoĹĽe drinka?
â€“ PoproszÄ™.

178
00:36:30,000 --> 00:36:35,800
To byĹ‚o jakieĹ› trzy,
a moĹĽe cztery lata po tym,

179
00:36:35,900 --> 00:36:39,400
jak przestaĹ‚am
dostawaÄ‡ wieĹ›ci od Coopera.

180
00:36:41,400 --> 00:36:42,800
WciÄ…ĹĽ pracowaĹ‚am w FBI.

181
00:36:44,700 --> 00:36:45,800
Pewnej nocy...

182
00:36:47,800 --> 00:36:49,600
bez pukania,

183
00:36:51,000 --> 00:36:53,600
bez dzwonka...

184
00:36:56,800 --> 00:36:58,800
wszedĹ‚ do mojego mieszkania.

185
00:37:00,400 --> 00:37:02,900
StaĹ‚am w salonie.

186
00:37:09,300 --> 00:37:11,900
Tak siÄ™ cieszyĹ‚am, ĹĽe go widzÄ™.

187
00:37:18,800 --> 00:37:21,000
PrzytuliĹ‚am go mocno.

188
00:37:28,500 --> 00:37:35,300
Wtedy usiedliĹ›my na kanapie.
ZaczÄ™liĹ›my rozmawiaÄ‡.

189
00:37:37,300 --> 00:37:43,000
ChciaĹ‚am usĹ‚yszeÄ‡, gdzie byĹ‚

190
00:37:44,900 --> 00:37:46,900
i co robiĹ‚.

191
00:37:55,000 --> 00:38:00,600
A on chciaĹ‚ tylko wiedzieÄ‡,
co siÄ™ dziaĹ‚o w FBI.

192
00:38:03,900 --> 00:38:06,300
CzuĹ‚am siÄ™ jak na przesĹ‚uchaniu.

193
00:38:10,900 --> 00:38:12,700
Ale...

194
00:38:14,000 --> 00:38:19,900
prĂłbowaĹ‚am to sobie wytĹ‚umaczyÄ‡
zwyczajnÄ… ciekawoĹ›ciÄ….

195
00:38:26,200 --> 00:38:28,300
Potem siÄ™ pochyliĹ‚...

196
00:38:33,800 --> 00:38:38,700
PochyliĹ‚ siÄ™ i mnie pocaĹ‚owaĹ‚.

197
00:38:40,400 --> 00:38:42,800
WczeĹ›niej zrobiĹ‚ to tylko raz.

198
00:38:46,800 --> 00:38:49,600
Ale gdy...

199
00:38:52,400 --> 00:38:55,000
dotknÄ…Ĺ‚ moich ust...

200
00:38:58,400 --> 00:39:01,600
coĹ› siÄ™ zmieniĹ‚o.

201
00:39:04,500 --> 00:39:06,800
PoczuĹ‚am niepokĂłj.

202
00:39:10,000 --> 00:39:12,500
ZauwaĹĽyĹ‚, ĹĽe siÄ™ bojÄ™.

203
00:39:25,000 --> 00:39:31,800
UĹ›miechnÄ…Ĺ‚ siÄ™.

204
00:39:35,200 --> 00:39:37,500
A jego twarz...

205
00:39:39,600 --> 00:39:42,300
I wtedy siÄ™ zaczÄ™Ĺ‚o.

206
00:39:43,600 --> 00:39:48,000
ZgwaĹ‚ciĹ‚ mnie.

207
00:39:59,300 --> 00:40:05,800
Potem gdzieĹ› mnie zabraĹ‚.

208
00:40:08,000 --> 00:40:15,000
Na jakÄ…Ĺ› starÄ… stacjÄ™ benzynowÄ….
StarÄ… stacjÄ™ benzynowÄ….

209
00:40:40,700 --> 00:40:43,300
Jestem w biurze szeryfa.

210
00:40:49,500 --> 00:40:52,000
W biurze szeryfa.

211
00:40:54,900 --> 00:41:01,800
WysĹ‚aĹ‚am mu wspĂłĹ‚rzÄ™dne.
Jestem w biurze szeryfa, bo...

212
00:41:10,900 --> 00:41:12,800
Nie jestem sobÄ….

213
00:41:13,200 --> 00:41:19,700
Nie jestem sobÄ….

214
00:41:47,800 --> 00:41:51,600
SÄ… prawdziwi, to tulpa.

215
00:41:55,300 --> 00:41:56,500
Biuro szeryfa?

216
00:42:23,300 --> 00:42:28,300
KtoĹ› ciÄ™ stworzyĹ‚.

217
00:42:30,900 --> 00:42:34,700
Wiem, pierdol siÄ™.

218
00:43:30,100 --> 00:43:35,600
â€“ Dougie, Ĺ›wietnie wyglÄ…dasz.
â€“ Super.

219
00:43:35,700 --> 00:43:39,200
â€“ Wszystko gotowe, samolot czeka.
â€“ DokÄ…d lecimy?

220
00:43:42,200 --> 00:43:47,000
ChĹ‚opcy, dajcie mi chwilkÄ™,
a wy chodĹşcie ze mnÄ….

221
00:43:58,200 --> 00:44:04,800
â€“ Jest bardzo pewny siebie.
â€“ MoĹĽe to przez Ĺ›piÄ…czkÄ™?

222
00:44:09,400 --> 00:44:10,800
Efekt uboczny.

223
00:44:17,400 --> 00:44:19,000
MuszÄ™ na trochÄ™ wyjechaÄ‡.

224
00:44:22,400 --> 00:44:26,400
ByĹ‚o mi z wami bardzo dobrze.

225
00:44:29,000 --> 00:44:33,900
â€“ Co takiego?
â€“ WypeĹ‚niliĹ›cie moje serce.

226
00:44:36,200 --> 00:44:38,200
Co ty mĂłwisz?

227
00:44:41,700 --> 00:44:47,800
JesteĹ›my rodzinÄ….
Dougie... to znaczy, ja wrĂłcÄ™.

228
00:44:53,500 --> 00:44:58,500
â€“ Nie jesteĹ› Dougie?
â€“ Co?

229
00:45:01,100 --> 00:45:04,200
JesteĹ› moim tatÄ….

230
00:45:05,700 --> 00:45:07,900
Jestem twoim tatÄ….

231
00:45:09,300 --> 00:45:14,300
Jestem i ciÄ™ kocham.
Kocham was oboje.

232
00:45:29,500 --> 00:45:33,900
MuszÄ™ juĹĽ iĹ›Ä‡,
ale wkrĂłtce siÄ™ zobaczymy.

233
00:45:34,400 --> 00:45:37,900
WejdÄ™ przez te czerwone drzwi
i zostanÄ™ w domu na zawsze.

234
00:45:58,000 --> 00:45:59,800
Nie idĹş.

235
00:46:03,200 --> 00:46:05,200
MuszÄ™.

236
00:46:24,500 --> 00:46:29,900
Kimkolwiek jesteĹ›, dziÄ™kujÄ™.

237
00:47:09,500 --> 00:47:13,300
â€“ Dougie, wyjaĹ›nijmy jedno.
â€“ Czekaj, teĹĽ chcÄ™ to usĹ‚yszeÄ‡.

238
00:47:13,400 --> 00:47:15,400
Gdzie moja Krwawa Mary?

239
00:47:39,300 --> 00:47:40,500
Dobra.

240
00:47:40,700 --> 00:47:44,500
Nie sprzedajesz ubezpieczeĹ„,
tylko jesteĹ› agentem FBI,

241
00:47:45,600 --> 00:47:47,500
ktĂłry zaginÄ…Ĺ‚ 25 lat temu.

242
00:47:48,400 --> 00:47:55,200
I mamy zawieĹşÄ‡ ciÄ™ do Twin Peaks,
na posterunek szeryfa.

243
00:47:56,600 --> 00:48:00,300
Dougie, kochamy ciÄ™,

244
00:48:01,200 --> 00:48:05,200
ale nie jesteĹ›my mile widziani
w takich miejscach.

245
00:48:05,300 --> 00:48:09,100
I przez takich ludzi.

246
00:48:10,500 --> 00:48:15,900
Rozumiem was caĹ‚kowicie.
Przyjaciele, to siÄ™ zaraz zmieni.

247
00:48:16,800 --> 00:48:21,700
OsobiĹ›cie mogÄ™ zaĹ›wiadczyÄ‡,
ĹĽe obaj macie serca ze zĹ‚ota.

248
00:48:22,500 --> 00:48:26,300
To prawda.

249
00:48:40,500 --> 00:48:42,300
{y:i}Panie i panowie.

250
00:48:42,600 --> 00:48:49,800
Roadhouse ma przyjemnoĹ›Ä‡ powitaÄ‡
Edwarda Louisa Seversona.

251
00:51:09,200 --> 00:51:10,800
Dwa razy martini.

252
00:51:24,600 --> 00:51:26,000
Dwa razy martini.

253
00:52:25,000 --> 00:52:27,400
DziÄ™kujÄ™.

254
00:52:43,800 --> 00:52:50,000
â€“ Za nas, Audrey.
â€“ Za Billyâ€™ego.

255
00:52:53,100 --> 00:52:59,200
{y:i}Panie i panowie, taniec Audrey.

256
00:55:23,100 --> 00:55:27,800
Monique!
To moja ĹĽona, dupku!

257
00:55:38,600 --> 00:55:40,000
Zabierz mnie stÄ…d.

258
00:55:42,700 --> 00:55:45,100
Co?

259
00:55:46,800 --> 00:56:08,700
{y:b}Wersja polska:
facebook.com/BDiP.pl

