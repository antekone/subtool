conv_cp1250="iconv -f utf8 -t cp1250//TRANSLIT"
conv_l2="iconv -f utf8 -t latin2//TRANSLIT"

set -euo pipefail
set -x

mkdir cp1250 || true
mkdir l2 || true

cat utf8/1.txt | $conv_cp1250 > cp1250/1.txt
cat utf8/2.txt | $conv_cp1250 > cp1250/2.txt
cat utf8/3.txt | $conv_cp1250 > cp1250/3.txt
cat utf8/4.txt | $conv_cp1250 > cp1250/4.txt
cat utf8/5.txt | $conv_cp1250 > cp1250/5.txt
cat utf8/6.txt | $conv_cp1250 > cp1250/6.txt
cat utf8/7.txt | $conv_cp1250 > cp1250/7.txt

cat utf8/1.txt | $conv_l2 > l2/1.txt
cat utf8/2.txt | $conv_l2 > l2/2.txt
cat utf8/3.txt | $conv_l2 > l2/3.txt
cat utf8/4.txt | $conv_l2 > l2/4.txt
cat utf8/5.txt | $conv_l2 > l2/5.txt
cat utf8/6.txt | $conv_l2 > l2/6.txt
cat utf8/7.txt | $conv_l2 > l2/7.txt
